# -*- coding: utf-8 -*-

__author__ = 'hramik'

import datetime, sys, os
from django.utils.encoding import force_unicode
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager

from django.db import models
from django.utils.translation import gettext_lazy as _
from sorl.thumbnail import ImageField
from PIL import Image

from tri.models import UserProfile
from tri.models import Region
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField

TYPE_CHOICES = (
    ('M', 'on main'),
    ('T', 'on top'),
)


class SiteInfo(models.Model):
    site = models.ForeignKey(Site)

    is_shop = models.BooleanField(verbose_name="Is Shop", default=True)

    fake_seo = models.BooleanField(
        default=False,
        verbose_name="Вафлить СЕО"
    )

    logo = models.ImageField(upload_to='sites/logos', verbose_name=_('Logo'), blank=True, help_text="")
    favicon = models.ImageField(upload_to='sites/favicon', verbose_name=_('Favicon'), blank=True, help_text="")

    main_ornament = models.ImageField(upload_to='sites/ornaments', verbose_name=_('Main ornament'), blank=True, help_text="")
    magazine_ornament = models.ImageField(upload_to='sites/ornaments', verbose_name=_('Magazine ornament'), blank=True, help_text="")
    class_prefix = models.CharField(default="", blank=True, verbose_name=_('Prefix for classes'), max_length=50)

    currency = models.CharField(
        default="₽",
        blank=True,
        max_length=50,
    )

    title = models.CharField(
        max_length=255,
        default="",
        blank=True,
    )

    slogan = models.CharField(
        max_length=255,
        default="",
        blank=True,
    )

    robots = models.TextField(
        default="User-agent: * \n Disallow: /"
    )

    https_robots = models.TextField(
        default="User-agent: * \n Disallow: /"
    )

    phone = models.CharField(default="", blank=True, max_length=100)
    operation_time = models.CharField(default="", blank=True, max_length=100)

    import_id = models.PositiveIntegerField(
        default="0"
    )

    admins = models.ManyToManyField(
        User,
        blank=True
    )

    json = JSONField(
        blank=True,
        null=True
    )

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def get_region(self):
        site = self.site
        try:
            region = Region.objects.filter(sites=site)[:1].get()
        except:
            region = Region.objects.filter(sites=1)[:1].get()

        return region

    def get_admins_emails(self):
        return self.admins.values_list('email', flat=True)

    def get_admins_phones(self):
        return UserProfile.objects.filter(user__in=self.admins.all()).values_list('phone', flat=True)

    def __unicode__(self):
        return self.site.name
