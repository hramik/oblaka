# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from events.views import *

urlpatterns = [

    url(r'^send_event$', send_event, name='send_event'),
    url(r'^(?P<session>.*)$', events, name='events'),

]
