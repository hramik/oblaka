# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response, redirect
from datetime import datetime, timedelta
from django.views.decorators.csrf import csrf_exempt
from urlparse import urlparse, parse_qs, parse_qsl
import json

from django.http import HttpResponse, JsonResponse

from land.models import LandingPage, LandingPagePart
from events.models import Event


def events(request, session=None):
    land_kwargs = {}

    landingpage = LandingPage.on_site.get()
    is_author = True if request.user == landingpage.user or request.user.is_superuser or landingpage.sandbox else False

    land_kwargs['landingpage'] = landingpage.id
    # list_display = ('pub_date', 'type', 'landingpage', 'landingpagepart', 'category', 'event', 'label', 'additional', 'user', 'session')


    if session:
        land_kwargs['session'] = session

    land_kwargs['pub_date__date'] = datetime.today()
    if "yesterday" in request.GET:
        land_kwargs['pub_date__date'] = datetime.today() - timedelta(days=1)

    if "date" in request.GET:
        land_kwargs['pub_date__date'] = datetime.datetime.strptime(request.GET["date"], "%d.%m.%Y").date()

    if "category" in request.GET:
        land_kwargs['category'] = request.GET["category"]

    if "event" in request.GET:
        land_kwargs['event'] = request.GET["event"]

    if "label" in request.GET:
        land_kwargs['event'] = request.GET["label"]

    if "session" in request.GET:
        land_kwargs['event'] = request.GET["session"]

    if "id" in request.GET:
        land_kwargs['landingpagepart__id'] = request.GET["id"]

    if "number" in request.GET:
        number = request.GET["number"]
    else:
        number = 100

    events = Event.objects.filter(**land_kwargs)[:number]

    return render_to_response('land/events/events.html', {
        'events': events,
        'landingpage': landingpage,
        'is_author': is_author,
    })



@csrf_exempt
def send_event(request):
    '''
    Send Event
    :param request:
    :return:
    '''
    result = False

    try:
        # json_data = json.loads(request.body)
        json_data = dict(parse_qsl(request.body))
        landingpage, landingpagepart = None, None
        landingpage_id, landingpagepart_id, user_id = 0, 0, 0

        post_data = request.POST
        get_data = request.GET

        try:
            landingpagepart = LandingPagePart.objects.get(id=json_data['landingpagepart'])
            landingpagepart_id = landingpagepart.id
            landingpage = landingpagepart.landingpage.all()[:1].get()
            landingpage_id = landingpage.id
        except:
            pass

        try:
            if not landingpagepart:
                landingpage = LandingPage.objects.get(id=json_data['landingpage'])
                landingpage_id = landingpage.id
        except:
            pass

        if request.user.id == None:
            user = None
        else:
            user = request.user
            user_id = user.id

        if "additional" in json_data:
            additional = json_data["additional"].encode('utf-8')
        else:
            additional = ""

        if json_data["category"].encode('utf-8') == 'Slideshow':
            pass

        event = Event(
            type=json_data["type"].encode('utf-8') if 'type' in json_data else "",
            landingpage=landingpage_id,
            landingpage_name=landingpage.title if landingpage else "",
            landingpagepart=landingpagepart_id,
            landingpagepart_name=landingpagepart.json["title"] if landingpagepart and 'title' in landingpagepart.json else "",
            category=json_data["category"].encode('utf-8') if 'category' in json_data else "",
            event=json_data["event"].encode('utf-8') if 'event' in json_data else "",
            label=json_data["label"].encode('utf-8') if 'label' in json_data else "",
            additional=additional,
            session=request.session.session_key,
            json=json_data,
            user=user_id,
            meta=json.dumps(request.META, default=lambda o: None),
            cookies=json.dumps(request.COOKIES, default=lambda o: None),
            ga=request.COOKIES['_ga'] if '_ga' in request.COOKIES else '',
            ym_uid=request.COOKIES['_ym_uid'] if '_ym_uid' in request.COOKIES else '',
        )

        event.save()

        result = True
    except Exception, e:
        print Exception, e

    return HttpResponse(result)
