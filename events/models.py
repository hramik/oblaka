# -*- coding: utf-8 -*-

import datetime, sys, os

from django.db import models
from django.contrib.auth.models import User

from django.contrib.sites.models import Site

from django.contrib.postgres.fields import JSONField


class Event(models.Model):

    event = models.CharField(
        max_length=255,
        default="",
        blank=True,
        db_index=True,
    )

    label = models.CharField(
        max_length=255,
        default="",
        blank=True,
    )

    additional = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True,

    )


    session = models.CharField(
        max_length=255,
        default="",
        blank=True
    )


    json = JSONField(
        # load_kwargs={'object_pairs_hook': collections.OrderedDict},
        blank=True,
        default={},
        null=True,
    )

    session = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    def __unicode__(self):
        return "%s" % (self.id,)

    def landingpage_info(self):
        return "%s. %s" % (self.landingpage, self.landingpage_name)

    def landingpagepart_info(self):
        return "%s. %s" % (self.landingpagepart, self.landingpagepart_name)

    class Meta:
        ordering = ["-pub_date"]
