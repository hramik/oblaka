__author__ = 'hramik'
from django.contrib import admin

from events.models import *


class EventAdmin(admin.ModelAdmin):
    list_display = ('pub_date', 'type', 'landingpage_info', 'landingpagepart_info', 'category', 'event', 'label', 'additional', 'user', 'session')
    list_filter = ('type', 'landingpage', 'category', 'event', 'pub_date')
    search_fields = ('session', 'label', 'landingpagepart__title')


admin.site.register(Event, EventAdmin)
