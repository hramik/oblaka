import datetime, sys, os
from django.utils.encoding import force_unicode

from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager

from django.db import models
from django.utils.translation import gettext_lazy as _
from sorl.thumbnail import ImageField
#from markitup.fields import MarkupField
from PIL import Image
from mptt.models import MPTTModel, TreeForeignKey



class MenuGroup(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=50)
    desc = models.TextField(blank=True)
    def __unicode__(self):
        return self.title


class Menu(MPTTModel):
    menu_group = models.ForeignKey(MenuGroup)
    parent = TreeForeignKey('self', blank=True, null=True, verbose_name="Parent", related_name='child')
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length=50)
    link = models.CharField(max_length=200, default="")
    desc = models.TextField(blank=True)
    image = models.ImageField(upload_to='menu_images/', blank=True)
    position = models.PositiveSmallIntegerField("Position")
    publish = models.BooleanField(verbose_name="Publish", default=True)


    sites = models.ManyToManyField(Site)
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title
    class Meta:
        ordering = ['position']
    class MPTTMeta:
        order_insertion_by = ['position']


