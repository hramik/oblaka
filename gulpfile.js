//load plugins
var gulp = require('gulp'),
    compass = require('gulp-compass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    livereload = require('gulp-livereload'),
    plumber = require('gulp-plumber'),
    path = require('path');

//the title and icon that will be used for the Grunt notifications
var notifyInfo = {
    title: 'Gulp',
    icon: path.join(__dirname, 'gulp.png')
};

//error notification settings for plumber
var plumberErrorHandler = {
    errorHandler: notify.onError({
        title: notifyInfo.title,
        icon: notifyInfo.icon,
        message: "Error: <%= error.message %>"
    })
};

//styles
gulp.task('styles', function () {
    return gulp.src(['static/sass/*.scss'])
        .pipe(plumber(plumberErrorHandler))
        .pipe(compass({
            css: 'static/stylesheets',
            sass: 'static/sass',
            image: 'static/images'
        }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest('static/stylesheets'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('static/stylesheets'));
});

//scripts
gulp.task('scripts', function () {
    return gulp.src(['static/js/oblaka.js', 'static/js/hram_selectors.js', 'static/js/angular/*.js'])
        .pipe(plumber(plumberErrorHandler))
        .pipe(concat('main.js'))
        .pipe(gulp.dest('static/js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('static/js'));
});

//watch
gulp.task('live', function () {
    livereload.listen();

    //watch .scss files
    gulp.watch('static/sass/**/*.scss', ['styles']);

    //watch .js files
    //gulp.watch(['static/js/oblaka.js', 'static/js/hram_selectors.js', 'static/js/angular/*.js'], ['scripts']);

    //reload when a template file, the minified css, or the minified js file changes
    gulp.watch(['templates/**/*.html', 'static/stylesheets/*.min.css', 'oblaka/settings.py'], function (event) {
        gulp.src(event.path)
            .pipe(plumber())
            .pipe(livereload())
            .pipe(notify({
                    title: notifyInfo.title,
                    icon: notifyInfo.icon,
                    message: event.path.replace(__dirname, '').replace(/\\/g, '/') + ' was ' + event.type + ' and reloaded'
                })
            );
    });
});