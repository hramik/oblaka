# -*- coding: utf-8 -*-
__author__ = 'm13v246'

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse


urlpatterns = patterns('',
                       url(r'^3d/regions.php$', 'room.views.regions', name="room_regions"),
                       url(r'^3d/enter.php$', 'room.views.enter', name="room_enter"),
                       url(r'^3d/register.php$', 'room.views.register', name="room_enter"),

)

