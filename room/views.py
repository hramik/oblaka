# -*- coding: utf-8 -*-
__author__ = 'm13v246'
from tri.models import Region
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import (login as auth_login)

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.loader import get_template

from django.core.exceptions import ValidationError
from django.core.validators import validate_email

from da_mailer.helper import *


def regions(request, ):
    """
    :return: XML list of regions
    """
    regions = Region.objects.all()
    template = "regions.xml"
    return render_to_response(template, {'regions': regions})


def enter(request, ):
    """
    :param request:
    :return: Auth error message or user id
    """
    template = "enter.txt"
    login = request.REQUEST.get('login', False)
    password = request.REQUEST.get('password', False)

    if login and password:
        user = authenticate(username=login, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return render_to_response(template, {'data': user.id})

    data = u'Неверный логин или пароль.'
    return render_to_response(template, {'data': data})


def register(request, ):
    """
    :param request:
    :return: Register user or return error message
    """
    template = "register.txt"
    email = request.REQUEST.get('email', False)
    region = request.REQUEST.get('city', False)
    try:
        validate_email(email)
    except ValidationError as e:
        data = u'Проверьте правильность написания адреса электронной почты.'
        return render_to_response(template, {'data': data})

    try:
        Region.objects.get(id=region)
    except Exception as e:
        data = u'Неверно указан регион.'
        return render_to_response(template, {'data': data})

    user_exist = User.objects.filter(email=email).count()
    if not user_exist:
        random_password = User.objects.make_random_password()
        user = User.objects.create_user(username=email, email=email, password=random_password)
        user.userprofile.city = Region.objects.get(id=region).name
        user.save()

        data = {
            'user': user,
            'userprofile': user.userprofile,
            'password': random_password,
        }

        DaMailer.send_mail_to_address(template="da_mailer/recover_password.html", subject="Создание учетной записи на \
                            сайте http://www.3tn.ru", data=data, email=email)
        return render_to_response(template, {'data': user.id})

    else:
        data = u'Указанный адрес электронной почты уже используется.'
        return render_to_response(template, {'data': data})

    return render_to_response(template, {'data': user})

