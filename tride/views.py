# -*- coding: utf-8 -*-

import os
import re

from django.db import models
from django.db.models import Q, F

from django.shortcuts import get_object_or_404

import json
import datetime
import random
from django.contrib.auth.decorators import login_required

from itertools import chain
#from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response, redirect
from django.template.loader import render_to_string

from django.contrib.sites.models import get_current_site
from django.core.files import File
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from ipgeo.models import Range
from django.db.models import Max
import base64

from djangular.views.crud import NgCRUDView
from pprint import pprint
import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.http import HttpResponse
from django.template import RequestContext
from django.views.generic import DetailView, TemplateView, ListView
from django.utils.translation import gettext_lazy as _
from django.db.models import Sum

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers

from django.utils import simplejson
import json
from bs4 import BeautifulSoup
import urllib
import urllib2
from urlparse import urlparse
from django.core.files import File
from django.core.files.base import ContentFile

from products.models import *

from array import *
from django.conf import settings

from django.contrib.sites.models import Site

from tri.models import News, Gallery, Page, PagePart, City, Region, SalePlace, MainLink, LoginForm, RegisterForm, Article, UserProfile, Save3d

from da_mailer.helper import *


SENDER = 'info@oblaka.com.ua'
RECEPIENTS = ['hramik@gmail.com']
# RECEPIENTS = ['hramik@gmail.com']
BCC = ['hramik@gmail.com']

QUESTIONS_PER_PAGE = 10
NEWS_PER_PAGE = 20

PROJECT_DIR = os.path.dirname(__file__)
location = lambda x: os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', x)


def enter(request):
    '''
    Login in system
    '''
    if request.method == 'POST':
        user = authenticate(username=request.POST['login'], password=request.POST['password'])
        if user is not None:
            if user.is_active:
                print("User is valid, active and authenticated")
                auth_login(request, user)
                uid = user.id
            else:
                print("The password is valid, but the account has been disabled!")
                uid = 0
        else:
            print("The username and password were incorrect.")
            uid = False

    return HttpResponse(uid)


def register(request):
    '''
    Register in system
    '''
    result = ''
    if request.method == 'POST':
        try:
            User.objects.get(email=request.POST.get('email'))
            result = 'Этот e-mail уже используется.'
        except:
            random_password = User.objects.make_random_password()
            user = User.objects.create_user(username=request.POST['email'], email=request.POST['email'],
                                            password=random_password)


            user.first_name = request.POST['email']
            # user.last_name = request.POST['last_name']
            user.last_name = request.POST['email']

            user.save()

            user_profile = user.get_profile()
            user_profile.city = Region.objects.get(id=request.POST['city']).name
            user_profile.save()
            user_profile.publish_last_question(request)

            user = authenticate(username=request.POST['email'], password=random_password)
            auth_login(request, user)
            result = user.id
            data = {
                'user': user,
                'userprofile': user.userprofile,
                'password': random_password,
            }

            DaMailer.send_mail_to_address(template="da_mailer/recover_password.html", subject="Создание учетной записи \
                            на сайте http://www.3tn.ru", data=data, email=request.POST['email'])

    # return redirect(result)
    return HttpResponse(result)


def app(request, id=False):
    '''
    App
    '''
    print "ID: %s" % id

    try:
        save_3d = Save3d.objects.filter(id=id)[0]
    except Exception, e:
        save_3d = False

    return render_to_response('tride/3d.html', {
        'save_3d': save_3d,
    },
                              context_instance=RequestContext(request))


def load(request):
    '''
    Load plans in app
    '''

    saves_3d = Save3d.objects.filter(user=request.user)

    output = ""
    try:
        output = '<root>'

        for save_3d in saves_3d:
            output += '<item id="%s" saveName="%s" preview="%s" data="%s"/>' % (save_3d.id, save_3d.title.replace('"',"'"), save_3d.photo.url, save_3d.save_data_filename.url)

        output += '</root>'
    except Exception, e:
        print "%s %s", (Exception, e)

    # print "3D load output: %s" % output

    return HttpResponse(output)


def regions(request):
    '''
    Get regions
    '''
    # TODO: по ходу у нас в профиле нету такой строки — регион. Добавить?
    #         $options .= '<item regionName="'.$c->name.'" regionId="'.$c->tid.'"/>';

    regions = Region.objects.all()

    output = ""
    try:
        output = '<root>'

        for region in regions:
            if region.name != 'None':
                output += '<item regionName="%s" regionId="%s"/>' % (region.name, region.id)

        output += '</root>'

    except Exception, e:
        print "%s %s", (Exception, e)

    return HttpResponse(output)


def userdata(request):
    '''
    Load plans in app
    '''

    print 'Load'

    return HttpResponse('0')


def delete_tride(request):
    '''
    Delete user save
    '''
    result = '0'
    vars = json.loads(request.body)
    if vars['id']:
        try:
            Save3d.objects.filter(id=vars['id']).delete()
            result = '1'
        except Exception, e:
            print "%s %s", (Exception, e)

    return HttpResponse(result)


def save(request):
    '''
    Save plan
    '''

    print request.POST

    try:
        user = User.objects.get(id=request.POST.get('userId'))
        if request.POST.get('overwrite') == 'true':
            save3d, create = Save3d.objects.get_or_create(user=user, title=request.POST.get('name'))
        else:
            save3d = Save3d(user=user, title=request.POST.get('name'))


        # Data
        name = "%s-%s-%s.dat" % (user.id, datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"), str(random.randint(100, 999)))
        try:
            content = ContentFile(request.POST.get('data'))
            data = request.REQUEST.get('data', None)
            save3d.save_data_filename.save(name, content, save=True)
            soup = BeautifulSoup(request.POST.get('data'), "lxml", from_encoding='utf-8')
            objects = soup.select('wallable')
            site = SiteInfo.objects.select_related().get(site__domain=request.META['HTTP_HOST']).site

            # print 'site', site
            for object in objects:
                # print 'nid: ', object.get('refid')
                # print 'prods: ', Product.objects.get(nid=object.get('refid'), sites=site)
                save3d.products.add(Product.objects.get(nid=object.get('refid'), sites=site))
            file_name = os.path.join(settings.MEDIA_ROOT, 'saves3d',  '%s' % name)
            handle = open(file_name, 'w')
            handle.write(data)
            handle.close()

        except Exception, e:
            print "data_file %s %s" % (Exception, e)

        #Preview
        name = "%s-%s-%s.png" % (user.id, datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"), str(random.randint(100, 999)))
        try:
            content = ContentFile(request.POST.get('preview').decode("base64"))
            save3d.photo.save(name, content, save=True)
        except Exception, e:
            print "photo %s %s" % (Exception, e)

        save3d.save()
        result = "success"
    except Exception, e:
        result = "0"

    return HttpResponse(result)


