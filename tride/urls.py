# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse
#from oscar.app import application
# Uncomment the next two lines to enable the admin:

from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from tastypie.api import Api
from tri.api.resources import QuestionResource

# from django.conf.urls import handler404, handler500

from django.contrib import admin
from tri.views import questionViewSet, additionalPhotoViewSet
from products.models import ProductEntity
from tri.djangular_views import *

from django.views.generic import TemplateView

from tri import views


admin.autodiscover()

urlpatterns = patterns('',

                       url(r'^3d/triton.html$', TemplateView.as_view(template_name="triton_ua/pages/plan_in_3d.html")),

                       # url(r'^3d$', TemplateView.as_view(template_name="tride/3d.html")),

                       url(r'^3d$', 'tride.views.app', name='app'),
                       url(r'^3d/(?P<id>[1-9]+)$', 'tride.views.app', name='app'),

                       url(r'^media/3dpr/enter.php$', 'tride.views.enter', name='enter'),

                       url(r'^media/3dpr/register.php$', 'tride.views.register', name='register'),

                       url(r'^media/3dpr/regions.php$', 'tride.views.regions', name='regions'),

                       url(r'^media/3dpr/load.php$', 'tride.views.load', name='load'),

                       url(r'^media/3dpr/save.php$', 'tride.views.save', name='save'),

                       url(r'^media/3dpr/userdata$', 'tride.views.userdata', name='userdata'),

                       url(r'^delete_tride$', 'tride.views.delete_tride', name='delete_tride'),


                       url(r'^3d/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT + '/3dpr/',
                       }),

                       url(r'^resources/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT + '/3dpr/resources/',
                       }),

                       url(r'^Models/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT + '/3dpr/Models/',
                       }),
)

