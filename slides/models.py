__author__ = 'hramik'

import datetime, sys, os
from django.utils.encoding import force_unicode
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager

from products.models import Product

from django.db import models
from django.utils.translation import gettext_lazy as _
from sorl.thumbnail import ImageField
from PIL import Image

TYPE_CHOICES = (
    ('M', 'on main'),
    ('T', 'on top'),
    ('B', 'big on top'),
    )

class Slide(models.Model):

    img = models.ImageField(
        upload_to='slides',
        verbose_name=_('Image'),
        blank=True,
        help_text=""
    )

    additional_img = models.ImageField(
        upload_to='slides',
        verbose_name=_('Additioanl Image'),
        blank=True,
        help_text=""
    )

    active = models.BooleanField(
        verbose_name="Active",
        default=True
    )

    message = models.TextField(
        verbose_name=_("Text"),
        blank=True
    )

    link = models.CharField(
        max_length=255,
        blank=True
    )

    link_text = models.CharField(
        max_length=255,
        verbose_name=_("Link text"),
        default="",
        blank=True
    )

    type = models.CharField(
        max_length=1,
        choices=TYPE_CHOICES,
        blank=True
    )

    position = models.IntegerField(
        default=0
    )

    product = models.ForeignKey(
        Product,
        blank=True,
        null=True,
    )

    product_style = models.CharField(
        max_length=255,
        blank=True,
        default="top: 60px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.3);",
    )

    sites = models.ManyToManyField(Site)
    on_site = CurrentSiteManager()

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])



class SlideVideo(models.Model):
    VIDEO_TYPES = (
        ('youtube', 'youtube'),
        ('mp4', 'mp4'),
        ('webm', 'webm'),
    )
    slide = models.ForeignKey(
        Slide,
        related_name='video'
    )

    video_url = models.CharField(max_length=500, verbose_name=_("Video URL"), default='')

    video_type = models.CharField(max_length=20, default='youtube', choices=VIDEO_TYPES)

    position = models.PositiveSmallIntegerField("Position")

    class Meta:
        ordering = ['position']