__author__ = 'm13v246'

from django.db import models
from django.contrib.auth.decorators import login_required
from tri.models import UserProfile
from django.contrib.auth.models import User

# from django.http import HttpResponse
# from django.shortcuts import redirect
import re

from yandex_money.api import Wallet, ExternalPayment
from oblaka import settings

class Yandex_Money_Api():

    token = '410012684464205.D18BC7A842B21DE82472C0FD68D04B8BE7C2061C5D722BC9AAC6A6301B017EB130FC3CB008CF3CBE778C1F5676E84BFD0F651F5F32D715BA8C28825E728692F593CB592EF6F4937F147D6FF23F50C6620F9C8CDAC9700A9FB896EE08076FD9139C119BD9FE8405F00C1D43CCDFD0B1154DCC70DF69707445AD579DF5DB482CD1'

    @classmethod
    def make_credit_card_payment(self, amount='', comment=''):
        result = ExternalPayment.get_instance_id(client_id=settings.YANDEX_MONEY_SCID)
        # print result
        instance_id = result['instance_id']
        print instance_id
        payment_options = {
            'pattern_id': 'p2p',
            'amount_due': '1',
            'message': comment,
            'to': settings.YANDEX_MONEY_ACCOUNT
        }
        payment_object = ExternalPayment(instance_id)
        request_result = payment_object.request(payment_options)
        if request_result['status'] == 'success':
            process_options = {
                'request_id': request_result['request_id'],
                'instance_id': instance_id,
                'ext_auth_success_uri': 'http://www.3tn.ru/payment_success',
                'ext_auth_fail_uri': 'http://www.3tn.ru/payment_failure',
                'request_token': 'false'
            }

            payment_result = payment_object.process(process_options)
            if payment_result['status'] == 'ext_auth_required':
                # uri = "%s?paymentType=%s&cps_context_id=%s" % (payment_result['acs_uri'], payment_result['acs_params']['paymentType'], payment_result['acs_params']['cps_context_id'])
                uri = "%s?paymentType=%s&cps_context_id=%s" % ('https://money.yandex.ru/eshop.xml', payment_result['acs_params']['paymentType'], payment_result['acs_params']['cps_context_id'])
                return uri
            else:
                return False

        # payment_object.request(payment_options)

        pass


    # def send_to_yandex_money(self, yandex_money_account='', amount='', comment='', message=''):
    @classmethod
    def send_to_yandex_money(self, user='', amount='', comment='', message=''):
        # user = User.objects.get(username=user)

        yandex_money_account = user.userprofile.yandex_money_account

        if not yandex_money_account:
            return False


        label = 'Label'
        try:
            wallet_object = Wallet(self.token)
            request_options = {
                "pattern_id": "p2p",
                "to": yandex_money_account,
                "amount_due": amount,
                "comment": comment,
                "message": message,
                "label": label,
            }

            request_result = wallet_object.request_payment(request_options)

            if request_result['status'] == 'success':
                process_payment = wallet_object.process_payment({
                    "request_id": request_result['request_id'],
                })
                return process_payment
            else:
                return request_result

        except Exception, e:
            print Exception, e

    @classmethod
    def send_to_phone_number(self, user='', amount='', comment='', message=''):

        # user = User.objects.get(username=user)
        print user
        phone = user.userprofile.phone
        phone = re.sub('[^\d]+', '', phone)
        if not phone:
            return False

        print phone

        label = 'Label'
        try:
            wallet_object = Wallet(self.token)
            request_options = {
                "pattern_id": "phone-topup",
                "phone-number": phone,
                "amount": amount,
                "comment": comment,
                "message": message,
                "label": label,
            }

            request_result = wallet_object.request_payment(request_options)

            if request_result['status'] == 'success':
                process_payment = wallet_object.process_payment({
                    "request_id": request_result['request_id'],
                })
                print process_payment
                return process_payment
            else:
                print request_result
                return request_result


        except Exception, e:
            print Exception, e
