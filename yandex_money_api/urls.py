payment_success__author__ = 'm13v246'

from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
                       url(r'^payments/pay/(?P<user_id>[0-9]+)$', 'yandex_money_api.views.pay', name="pay"),
                       # url(r'^testpay$', 'yandex_money_api.views.testpay', name='testpay'),
                       url(r'^success-payment$', 'yandex_money_api.views.payment_success', name='payment_success'),
                       url(r'^fail-payment$', 'yandex_money_api.views.payment_failure', name='payment_failure'),
                       url(r'^check-payment$', 'yandex_money_api.views.check_payment', name='check-payment'),
                       url(r'^aviso-payment$', 'yandex_money_api.views.aviso_payment', name='aviso-payment'),
                       # url(r'^aviso-payment$', 'yandex_money_api.views.aviso_payment', name='aviso-payment'),
)