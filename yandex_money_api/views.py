__author__ = 'm13v246'
# -*- coding: utf-8 -*-


from yandex_money_api.models import *
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.template import RequestContext

import time
from django.utils import timezone
import pytz
import locale
from hashlib import md5
from datetime import datetime
from tri.models import Company
from products.models import Order
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo
from da_mailer.helper import *
from sms_api.models import *


import logging

logger = logging.getLogger(__name__)


def testpay(request):
    '''
    TEST FORM FOR Ya PAY
    '''
    # result = True
    if 'amount' in request.POST:
        uri = Yandex_Money_Api.make_credit_card_payment(amount=request.POST['amount'], comment='test payment')
        return redirect(uri)
        # return HttpResponse(uri)
    else:
        # result = '<h3>Payment</h3><form method="post" action="/testpay"><input type="text" name="amount"/><input type="submit" name="Оплатить"/></form>'
        logger.info('Show payment form.')
        order_number = 'test-%s' % int(time.time())
        result = '''<form action="https://demomoney.yandex.ru/eshop.xml" method="post">
                    <!-- Обязательные поля -->
                    <input name="shopId" value="%s" type="hidden"/>
                    <input name="scid" value="%s" type="hidden"/>
                    Сумма:<br/>
                    <input name="sum" value="" type="text"><br/>
                    <input name="customerNumber" value="%s" type="hidden"/>

                    <!-- Необязательные поля -->
                    <!--input name="shopArticleId" value="333" type="hidden"/-->
                    Способ оплаты:<br/>
                    <select name="paymentType">
                        <option value="PC">Оплата из кошелька в Яндекс.Деньгах.</option>
                        <option value="AC">Оплата с произвольной банковской карты.</option>
                        <option value="MC">Платеж со счета мобильного телефона.</option>
                        <option value="GP">Оплата наличными через кассы и терминалы.</option>
                        <option value="WM">Оплата из кошелька в системе WebMoney.</option>
                        <option value="SB">Оплата через Сбербанк: оплата по SMS или Сбербанк Онлайн.</option>
                        <option value="MP">Оплата через мобильный терминал (mPOS).</option>
                        <option value="AB">Оплата через Альфа-Клик.</option>
                        <option value="МА">Оплата через MasterPass.</option>
                        <option value="PB">Оплата через Промсвязьбанк.</option>
                    </select>
                    <br/>

                    <!--input name="paymentType" value="AC" type="radio"/-->

                    <input name="orderNumber" value="%s" type="hidden"/>
                    Номер телефона:<br/>
                    <input name="cps_phone" value="" type="text"/><br/>
                    Адрес электронной почты:<br/>
                    <input name="cps_email" value="" type="text"/><br/>

                    <input type="submit" value="Заплатить"/>
                    </form> ''' % (settings.YANDEX_MONEY_SHOP_ID, settings.YANDEX_MONEY_SCID, request.user, order_number)
        # success-payment
        return HttpResponse(result)


def payment_success(request):
    '''
    SUCCESSEFUL PAYMENT ENDPOINT
    '''

    cd = request.GET
    try:
        if 'action' in cd and cd['action'] == 'PaymentSuccess' or True:
            if 'orderNumber' in cd and 'customerNumber' in cd and 'orderSumAmount' in cd:
            # if 'orderNumber' in cd and Order.objects.filter(id=cd['orderNumber']).count():
                user_id = cd['customerNumber'].split('/')[0]
                order = Order.objects.get_or_none(id=cd['orderNumber'].replace('-add', ''), user__id=user_id, price=int(locale.atof(cd['orderSumAmount'])))
                if not order:
                    return redirect('/')
                # order = Order.objects.get(id=cd['orderNumber'])
                order_site = order.site
                site = Site.objects.get_current()
                if order_site == site:
                    # template for payed order here
                    return render_to_response('yandex_money_api/order_payed.html', {
                        'order': order,
                        'message': 'Ваш заказ успешно оплачен',
                    }, context_instance=RequestContext(request))
                else:
                    # redirect to working site
                    url = 'http://%s/success-payment?%s' % (order_site, request.META["QUERY_STRING"])
                    return redirect(url)
            else:
                result = request.META["QUERY_STRING"]
                return HttpResponse(result)
        else:
            result = 'Something went wrong..'
            return HttpResponse(result)

    except Exception, e:
            logger.info("SUCCESS ERROR: %s %s" % (Exception, e))

    result = '<h3>Платеж успешно завершен</h3>'
    result += request.META["QUERY_STRING"]

    return HttpResponse(result)
    # return redirect('/')


def payment_failure(request):
    cd = request.GET

    try:
        if 'orderNumber' in cd and 'customerNumber' in cd and 'orderSumAmount' in cd:
        # if 'orderNumber' in cd and Order.objects.filter(id=cd['orderNumber']).count():
            user_id = cd['customerNumber'].split('/')[0]
            order = Order.objects.get_or_none(id=cd['orderNumber'].replace('-add', ''), user__id=user_id, price=int(locale.atof(cd['orderSumAmount'])))
            if not order:
                return redirect('/')
            # order = Order.objects.get(id=cd['orderNumber'])
            order_site = order.site
            site = Site.objects.get_current()
            if order_site == site:
                # template for payed order here
                return render_to_response('yandex_money_api/order_payed.html', {
                    'order': order,
                    'message': 'Произошла ошибка. Заказ не оформлен',
                }, context_instance=RequestContext(request))
            else:
                # redirect to working site
                url = 'http://%s/fail-payment?%s' % (order_site, request.META["QUERY_STRING"])
                return redirect(url)
        else:
            result = request.META["QUERY_STRING"]
            return HttpResponse(result)

    except Exception, e:
            logger.info("FAILURE ERROR: %s %s" % (Exception, e))

    return HttpResponse(result)


def check_payment(request):
    '''
    CHECK ORDER DATA
    '''
    cd = request.POST

    # default false answer
    code = 1
    logger.info('Check_payment request: %s' % cd)
    if not check_md5(cd):  # не совпал hash, возвращаем ошибку
        logger.info('check_payment code 1.')
    else:
        order_exists = Order.objects.filter(id=cd['orderNumber'].replace('-add', '')).count()
        # order_exists = Order.objects.filter(id=cd['orderNumber'], price=int(locale.atof(cd['orderSumAmount']))).count()
        logger.info('check_payment order_exists value %s.' % order_exists)
        if order_exists:
            code = 0
            order = Order.objects.get(id=cd['orderNumber'].replace('-add', ''))
            # order = Order.objects.get(id=cd['orderNumber'], price=int(locale.atof(cd['orderSumAmount'])))
            if '-add' in cd['orderNumber']:
                order.status = 'CO'
                order.additional_pay_date = datetime.datetime.now()
            else:
                order.status = 'PA'
                order.pay_date = datetime.datetime.now()
            try:
                order.pay_agent = Company.objects.get(id='22801')
            except:
                pass
            order.save()
            logger.info('check_payment code 0. changing status to PA')
        else:
            logger.info('check_payment order not exists %s %s %s.' % (cd['orderNumber'], cd['customerNumber'], cd['orderSumAmount']))

    xml = '''<?xml version="1.0" encoding="UTF-8"?>
             <checkOrderResponse performedDatetime="%s" code="%s"
             invoiceId="%s" shopId="%s"/>''' % (datetime.datetime.now(tz=pytz.timezone('Europe/Moscow')).isoformat(), code, cd['invoiceId'], settings.YANDEX_MONEY_SHOP_ID)
    logger.info('check_payment code 1.')

    return HttpResponse(xml, content_type="application/xml")


def aviso_payment(request):
    '''
    PAYMENT NOTICE
    '''
    cd = request.POST
    logger.info('avizo payment start')

    if not check_md5(cd):  # не совпал hash, возвращаем ошибку
        code = 1
    else:
        code = 0
        if '-add' in cd['orderNumber']:
            order = Order.objects.get(id=cd['orderNumber'].replace('-add', ''))
            order.additional_invoice_id = cd['invoiceId']
            order.additional_order_sum_amount = cd['orderSumAmount']
            order.save()
        else:
            order = Order.objects.get(id=cd['orderNumber'])
            order.invoice_id = cd['invoiceId']
            order.order_sum_amount = cd['orderSumAmount']
            order.save()

        logger.info('avizo payment finish')

        # admins_emails = order.site.get_admins_emails()
        admins_phones = SiteInfo.objects.get(site=order.site).get_admins_phones()

        # DaMailer.send_mail_to_addresses(template='products/email/payment_order.email.html', subject=u'Заказ на %s оплачен' % (order.site.name,), data=order, emails=admins_emails)

        # DaMailer.send_mail_to_address(template='products/email/customer_order.email.html', subject=u'Ваш %sзаказ на %s' % (adjective, order.site.name), data=order, email=order.email)

        Sms_Api.send_message_to_phone_number(phone_list=admins_phones, message="Payment %s for order IM-%s on %s is done" % (cd['orderSumAmount'], str(order.id), order.site,))
    xml = '''<?xml version="1.0" encoding="UTF-8"?>
                <paymentAvisoResponse
                    performedDatetime="%s"
                    code="%s"
                    invoiceId="%s"
                    shopId="%s"/>''' % (datetime.datetime.now(tz=pytz.timezone('Europe/Moscow')).isoformat(), 0, cd['invoiceId'], settings.YANDEX_MONEY_SHOP_ID)


    return HttpResponse(xml, content_type="application/xml")


def make_md5(cd):
    """
    action;orderSumAmount;orderSumCurrencyPaycash;orderSumBankPaycash;shopId;invoiceId;customerNumber;shopPassword
    """
    params = [cd['action'],
              str(cd['orderSumAmount']),
              str(cd['orderSumCurrencyPaycash']),
              str(cd['orderSumBankPaycash']),
              str(cd['shopId']),
              str(cd['invoiceId']),
              cd['customerNumber'],
              settings.YANDEX_MONEY_SHOP_PASSWORD]
    s = str(';'.join(params))
    return md5(s).hexdigest().upper()


def check_md5(cd):
    return make_md5(cd) == cd['md5']
    # return 1
