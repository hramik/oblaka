# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from django.contrib.auth.models import User


from da_mailer.constants import TEST_RECEPIENT, SENDER, BCC



class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'

    def handle(self, *args, **options):

        test_mail_body = get_template('da_mailer/test.email.html').render(
            Context({
                'content': u'Тестер шмейстер, трутота!',
            })
        )
        subject = u'Triton test email subject'

        mail = EmailMultiAlternatives(
            subject,
            test_mail_body,
            SENDER,
            TEST_RECEPIENT,
            BCC
        )

        mail.attach_alternative(test_mail_body, "text/html")

        try:
            # mail.send()
            print('Successfully send test email')
        except Exception, e:
            print(Exception, e)

