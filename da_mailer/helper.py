# -*- coding: utf-8 -*-

# TODO:
# Поместить в заголовок письма вот такую строчку:
# List-Unsubscribe: <mailto:rm-0bxg2hykazs74dxau3ua1cq2cxdkebs@e.victoriassecret.com>
# Письмо на указанный адрес должно отписывать данного конкретного пользователя от подписки.

# send_notification_mail
# send_mail_to_admin
# send_mail_to_super_admin
# send_mail_to_author
# send_mail_to_user
# send_mail_to_group
# send_subscribe_mail

__author__ = 'hramik'

import datetime, sys, os
from django.utils.encoding import force_unicode

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models

from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template

from da_mailer.constants import *
from da_mailer.models import *
from django.contrib.auth.models import User
import time
import thread
import threading


class EmailThread(threading.Thread):
    def __init__(self, subject, body, from_email, recipient_list, fail_silently, html):
        self.subject = subject
        self.body = body
        self.recipient_list = recipient_list
        self.from_email = from_email
        self.fail_silently = fail_silently
        self.html = html
        threading.Thread.__init__(self)

    def run (self):
        msg = EmailMultiAlternatives(self.subject, self.body, self.from_email, self.recipient_list)
        if self.html:
            msg.attach_alternative(self.html, "text/html")
        msg.send(self.fail_silently)

def send_thread_mail(subject, body, from_email, recipient_list, fail_silently=False, html=None, *args, **kwargs):
    EmailThread(subject, body, from_email, recipient_list, fail_silently, html).start()

class DaMailer(models.Model):
    pass

    @classmethod
    def send_notification_mail(cls, name, data=False):
        '''
        Send email to admins and author of parent node
        '''
        cls.send_mail_to_admin(name, data)
        cls.send_mail_to_author(name, data)
        return True

    @classmethod
    def send_mail_to_admin(cls, template, subject=False, data=False):
        '''
        Send email to all admins
        '''
        notification_mail_type = None
        try:
            notification_mail_type = EmailNotificationType.objects.get(name=template)
            subject = notification_mail_type.admin_subject
            template_name = "admin_%s" % notification_mail_type.name
        except:
            template_name = template

        ADMINS = User.objects.filter(is_superuser=True).values_list('email', flat=True)

        mail_body = get_template("da_mailer/%s.mail.html" % template_name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            ADMINS,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    @classmethod
    def send_mail_to_address(cls, template=False, subject=False, data=False, email=False):
        '''
        Send email to email address
        '''
        notification_mail_type = None
        template_name = template

        mail_body = get_template("%s" % template_name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            (email,),
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            thread.start_new_thread(mail.send, (False,))
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    @classmethod
    def send_mail_to_addresses(cls, template=False, subject=False, data=False, emails=False, files=False):
        '''
        Send email to a few email addresses
        '''
        notification_mail_type = None
        template_name = template

        mail_body = get_template("%s" % template_name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            emails,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        if files:
            for file in files:
                mail.attach_file(file)

        try:
            mail.send()
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    @classmethod
    def send_mail_to_super_admin(cls, template, subject=False, data=False):
        '''
        Send email to super admins
        '''
        notification_mail_type = None
        try:
            notification_mail_type = EmailNotificationType.objects.get(name=template)
            subject = notification_mail_type.admin_subject
            template_name = "admin_%s.mail.html" % notification_mail_type.name
        except:
            template_name = template

        ADMINS = User.objects.filter(is_superuser=True).values_list('email', flat=True)

        mail_body = get_template("%s" % template_name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            SUPER_ADMINS,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    @classmethod
    def send_mail_to_author(cls, name, data=False):
        '''
        Send email to parent node author
        '''
        notification_mail_type = EmailNotificationType.objects.get(name=name)
        AUTHOR = User.objects.filter(id = data.user.id, is_superuser=False).values_list('email', flat=True)

        mail_body = get_template("da_mailer/author_%s.mail.html" % notification_mail_type.name).render(
            Context({
                'data': data,
                'type': notification_mail_type,
            })
        )
        subject = notification_mail_type.user_subject

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            TEST_RECEPIENT,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)


    @classmethod
    def send_mail_to_user(cls, template, subject, user, data=False):
        '''
        Send email to user
        '''
        user_email = [user.email, ]

        mail_body = get_template("da_mailer/%s.mail.html" % template).render(
            Context({
                'data': data,
                'user': user,
            })
        )
        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            # TEST_RECEPIENT,
            user_email,
            BCC
        )
        mail.attach_alternative(mail_body, "text/html")
        # print 'sending mail'
        try:
            result = mail.send(fail_silently=False)
            return ('Successfully send email to user')
        except Exception, e:
            return (Exception, e)

    class Meta():
        abstract = True


    @classmethod
    def send_mail_to_group(cls, template, subject, group_name, data=False):
        '''
        Send email to user
        '''

        recepients = list(User.objects.filter(groups__name = group_name).values_list('email', flat=True))

        mail_body = get_template("da_mailer/%s.mail.html" % template).render(
            Context({
                'data': data,
            })
        )
        subject = subject

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            recepients,
            BCC
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send()
            return ('Successfully send email to user')
        except Exception, e:
            return (Exception, e)

    class Meta():
        abstract = True

    @classmethod
    def send_subscribe_mail_node(cls, name='subscribe_node', user=False, data=False):
        '''
        Send email to parent node author
        '''

        mail_body = get_template("da_mailer/subscribe_node.mail.html").render(
            Context({
                'data': data,
                'user': user
            })
        )
        subject = "Новый материал: %s «%s»" % (data.node_type, data.title)

        mail = EmailMultiAlternatives(
            subject,
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            [user.email]
        )

        mail.attach_alternative(mail_body, "text/html")

        try:
            mail.send(fail_silently=True)
            return ('Successfully send email to admin')
        except Exception, e:
            return (Exception, e)

    class Meta():
        abstract = True

    @classmethod
    def send_subscribe_mail(cls, subject='Рассылка Triton', emails=False, data={'title': 'Рассылка компании Triton', 'body': "<p>Информационное сообщение</p>"}, template='subscribe_default.html'):
        '''
        Send email to list of subscribers via template
        '''
        total = 0
        subtotal = 0
        rate = 14.0
        per = 1.0
        allowance = rate
        last_check = time.time()

        template = "da_mailer/%s" %(template)
        for email in emails:

            mail_body = get_template(template).render(
                Context({
                    'data': data,
                    'email': email
                })
            )

            mail = EmailMultiAlternatives(
                subject,
                mail_body,
                settings.DEFAULT_FROM_EMAIL,
                [email]
            )

            mail.attach_alternative(mail_body, "text/html")

            current = time.time()
            time_passed = current - last_check
            try:
                # thread.start_new_thread(mail.send, (False,))
                subtotal += 1
                total += 1
                # if(total > 5000):
                    # send_thread_mail(subject=subject, body='', from_email=settings.DEFAULT_FROM_EMAIL, recipient_list=[email], fail_silently=False, html=mail_body)
                mail.send(fail_silently=False)
                if subtotal == 100:
                    print total
                    subtotal = 0
                print "%s %s" % (email,time_passed)
                # mail.send(False)
                # time.sleep(1)
                # return ('Successfully send email to admin')
            except Exception, e:
                print 'Something wrong %s' % e
                time.sleep(1)
                # return (Exception, e)

            last_check = current
            allowance += time_passed * (rate / per)
            if allowance > rate:
                allowance = rate
            if allowance < 1.0:
                print "%s" % (allowance)
                # if total > 5000:
                time.sleep(1)
            else:
                allowance -= 1.0
        print total


    class Meta():
        abstract = True

    @classmethod
    def send_test_mail(cls, to='admin@3tn.ru', mail_body='TEST BODY'):
        '''
        Send email to user
        '''
        user_email = [to, ]
        print 'Go'
        mail = EmailMultiAlternatives(
            'TEST',
            mail_body,
            settings.DEFAULT_FROM_EMAIL,
            # TEST_RECEPIENT,
            user_email,
            BCC
        )
        mail.attach_alternative(mail_body, "text/html")
        # print 'sending mail'
        print 'Go 2'
        try:
            print 'Go 3'
            result = mail.send(False)
            print 'Ok'
            return ('Successfully send TEST email to user')
        except Exception, e:
            print 'Error'
            return (Exception, e)

    class Meta():
        abstract = True