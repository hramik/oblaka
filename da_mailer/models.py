# -*- coding: utf-8 -*-

__author__ = 'hramik'

import datetime, sys, os
from django.utils.encoding import force_unicode

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models

from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from autoslug import AutoSlugField

from da_mailer.constants import TEST_RECEPIENT, SENDER, BCC


class EmailNotificationType(models.Model):
    '''
    Types of email notifications
    '''

    name = models.CharField(
        max_length=254,
        verbose_name=u'Название',
    )

    admin_subject = models.CharField(
        max_length=255,
        default=""
    )

    user_subject = models.CharField(
        max_length=255,
        default=""
    )

    slug = AutoSlugField(
        populate_from=('name'),
        always_update=True,
        # editable=True,
        verbose_name=u'Идентификатор для url',
        help_text=u'Если оставить пустым, то сгенерируется автоматически.',
    )

    def __unicode__(self):
        return self.name


class Subscription(models.Model):
    name = models.CharField(
        max_length=254,
        verbose_name=u'Название',
    )


    desc = models.TextField(
        verbose_name=u"Desc",
        blank=True,
        null=True,
        default=''
    )

    slug = AutoSlugField(
        populate_from='name',
        always_update=False,
        # editable=True,
        null=True,
        blank=True,
        verbose_name=u'Идентификатор для url',
        help_text=u'Если оставить пустым, то сгенерируется автоматически.',
    )

    def __unicode__(self):
        return self.name


class SubscriptionPost(models.Model):
    subscription = models.ForeignKey(Subscription)

    title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name=u'Заголовок',
    )

    body = models.TextField(
        verbose_name=u"Body",
        default=''
    )

    slug = AutoSlugField(
        unique_with=('id', 'title'),
        always_update=False,
        # editable=True,
        null=True,
        blank=True,
        verbose_name=u'Идентификатор для url',
        help_text=u'Если оставить пустым, то сгенерируется автоматически.',
    )

    create_date = models.DateTimeField(
        auto_now_add=True,
        # editable=True,
        verbose_name=u'Дата создания',
    )

    def __unicode__(self):
        return self.title


class Subsriber(models.Model):
    subsribtion = models.ManyToManyField(Subscription)

    class Meta():
        abstract = True