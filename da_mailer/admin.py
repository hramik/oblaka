# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.contrib import admin

from mptt.admin import MPTTModelAdmin
from sorl.thumbnail.admin import AdminImageMixin
from django.contrib.admin import BooleanFieldListFilter

from da_mailer.models import *

admin.site.register(Subscription)
admin.site.register(SubscriptionPost)
admin.site.register(EmailNotificationType)
