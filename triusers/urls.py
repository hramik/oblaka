# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse


urlpatterns = patterns('',
                       url(r'^dealer_request$', 'triusers.views.dealer_request', name="dealer_request"),
                       url(r'^repres_request$', 'triusers.views.repres_request', name="repres_request"),
                       url(r'^seller_request$', 'triusers.views.seller_request', name="seller_request"),
                       url(r'^report_request$', 'triusers.views.report_request', name="report_request"),

                       url(r'^advuser/activate/(?P<user_type>[\w]+)/(?P<id>[0-9]+)$', 'triusers.views.activate_advuser', name="activate_advuser"),
                       url(r'^advuser/activate_seller/(?P<user_type>[\w]+)/(?P<id>[0-9]+)/(?P<fee_value>[0-9]+)$', 'triusers.views.activate_seller', name="activate_seller"),
                       url(r'^advuser/deactivate/(?P<user_type>[\w]+)/(?P<id>[0-9]+)$', 'triusers.views.deactivate_advuser', name="deactivate_advuser"),
                       url(r'^advuser/delete/(?P<user_type>[\w]+)/(?P<id>[0-9]+)$', 'triusers.views.delete_advuser', name="deactivate_advuser"),

                       url(r'^advuser/approve_report/(?P<id>[0-9]+)$', 'triusers.views.approve_report', name="approve_report"),
)
