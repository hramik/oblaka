# -*- coding: utf-8 -*-
__author__ = 'hramik'

import datetime, sys, os, re
from django.utils.encoding import force_unicode

from django.db import models
from django.forms import ModelForm
from django.db import models
from django import forms

from django.forms.models import inlineformset_factory, formset_factory


from triusers.models import *


class DealerRegisterForm(ModelForm):
    '''
    Form for user registration
    '''


    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.first_name', 'ng-initial': 'True', 'required': 'True'}),
        label='Имя',
        required=True,
    )

    second_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.second_name ', 'ng-initial': 'True', 'required': 'True'}),
        label='Отчество',
        required=True,
    )

    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.last_name ', 'ng-initial': 'True', 'required': 'True'}),
        label='Фамилия',
        required=True,
    )

    company = forms.ModelChoiceField(queryset=Company.objects.filter(company_type__slug="distributor"),
                                      to_field_name="slug",
                                      # initial=Delivery.on_site.filter(publish=True)[0],
                                      widget=forms.Select(attrs={'class': '',  'ng-model': 'register.company', 'required': 'True', 'ng-initial': 'True'}),
                                      label="Компания",
                                      required=True)



    # region = forms.ModelMultipleChoiceField(
    #     widget=forms.SelectMultiple(
    #         attrs={
    #             'class': '',
    #             'ng-model': 'dealer.region',
    #         },
    #     ),
    #     #choices=list(Region.objects.all().exclude(slug='none').values_list('name', 'name')),
    #     label = u"Регион",
    #     queryset = Region.objects.all(),
    #     required = True
    # )
    #
    #
    # dealer_point_name = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'dealer.dealer_point_name', 'ng-initial': 'True', 'required': 'True'}),
    #     label='Название дилерского центра'
    # )
    #
    # dealer_point_address = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'register.dealer_point_address', 'ng-initial': 'True', 'required': 'False'}),
    #     label='Адрес дилерского центра'
    # )

    personal_data_confirmed = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'ng-model': 'register.personal_data_confirmed ', 'ng-initial': 'True', 'required': 'False'}),
        label=u'Согласен на передачу персональных данных (<a href="/soglasie" target="blank">Ознакомиться</a>)'
    )

    class Meta:
        model = DealerProfile
        fields = ('first_name', 'second_name', 'last_name', 'company',)


class DealerPhoneForm(ModelForm):
    '''
    Form for user registration
    '''

    phone = forms.CharField(
        widget=forms.TextInput(attrs={'required': 'True'}),
        label='Телефон'
    )

    class Meta:
        model = DealerProfilePointPhone
        fields = ('phone',)

# DealerPhoneFormSet = inlineformset_factory(
#     parent_model = DealerProfile,
#     model = DealerProfilePointPhone,
#     form = DealerPhoneForm,
#     extra=1,
    # can_delete = False
# )


DealerPhoneFormSet = formset_factory(DealerPhoneForm,
    # form = DealerPhoneForm,
    extra=1,
    can_delete = False
)


class RepresRegisterForm(ModelForm):
    '''
    Form for user registration
    '''

    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.first_name', 'ng-initial': 'True', 'required': 'True'}),
        label='Имя',
        required=True,
    )

    second_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.second_name ', 'ng-initial': 'True', 'required': 'True'}),
        label='Отчество',
        required=True,
    )

    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.last_name ', 'ng-initial': 'True', 'required': 'True'}),
        label='Фамилия',
        required=True,
    )

    region = forms.ModelChoiceField(
        widget=forms.Select(
            attrs={
                'class': '',
                'ng-model': 'repres.region',
            },
        ),
        #choices=list(Region.objects.all().exclude(slug='none').values_list('name', 'name')),
        label = u"Регион",
        queryset = Region.objects.filter(id__in = DealerProfile.objects.all().values_list('company', flat=True)),
        required = True
    )

    # dealer = forms.ModelChoiceField(
    #     widget=forms.Select(
    #         attrs={
    #             'class': '',
    #             'ng-model': 'repres.dealer',
    #         },
    #     ),
    #     queryset = DealerProfile.objects.all().filter(active='True'),
    # )

#    dealer = forms.ChoiceField(
#        widget=forms.Select(
#            attrs={
#                'class': '',
#                'ng-model': 'repres.dealer',
#            },
#        ),
#        #choices=list(DealerProfile.objects.all().exclude(active='False').values_list('dealer_point_name', 'dealer_point_name')),
#        choices=list(DealerProfile.objects.all().filter(active='True').values_list('dealer_point_name','dealer_point_name')),
#        label=u"Дилер",
#        required=True
#    )



    personal_data_confirmed = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'ng-model': 'register.personal_data_confirmed ', 'ng-initial': 'True', 'required': 'False'}),
        label=u'Согласен на передачу персональных данных (<a href="/soglasie" target="blank">Ознакомиться</a>)'
    )


    class Meta:
        model = RepresProfile
        fields = ('first_name', 'second_name', 'last_name',  'region', 'personal_data_confirmed',)



class SellerRegisterForm(ModelForm):
    '''
    Form for user registration
    '''

    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.first_name', 'ng-initial': 'True', 'required': 'True'}),
        label='Имя',
        required=True,
    )

    second_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.second_name ', 'ng-initial': 'True', 'required': 'True'}),
        label='Отчество',
        required=True,
    )

    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.last_name ', 'ng-initial': 'True', 'required': 'True'}),
        label='Фамилия',
        required=True,
    )


    region = forms.ModelChoiceField(
        widget=forms.Select(
            attrs={
                'class': '',
                'ng-model': 'seller.region',
            },
        ),
        queryset = Region.objects.filter(id__in = RepresProfile.objects.all().values_list('region', flat=True)),
        label = u'Регион',
        required = True,
    )

    sellerCity = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'seller.sellerCity', 'ng-initial': 'True', 'required': 'True'}),
        label='Город'
    )


    position = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'seller.position', 'ng-initial': 'True', 'required': 'False'}),
        label=u'Укажите занимаемую должность'
    )




    award_method = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': '',
                'ng-model': 'seller.award_method',
            },
        ),
        choices=SellerProfile.AWARD_METHOD_CHOICES,
        label=u"Способ получения вознаграждения",
        required=True
    )

    yandex_money_account = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'seller.yandex_money_account', 'ng-initial': 'True', 'ng-show': 'seller.award_method=="yandex_money";', 'ng-required': 'seller.award_method=="yandex_money";'}),
        label=u'Укажите Яндекс аккаунт',
        required=False,
    )

    phone = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'seller.phone', 'ng-initial': 'True', 'ng-show': 'seller.award_method=="phone";', 'ng-required': 'seller.award_method=="phone";'}),
        label=u'Укажите телефон',
        required=False,

    )

    personal_data_confirmed = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'ng-model': 'register.personal_data_confirmed ', 'ng-initial': 'True', 'required': 'False'}),
        label=u'Согласен на передачу персональных данных (<a href="/soglasie" target="blank">Ознакомиться</a>)',
    )

    def get_exist_regions(self):
        repres = RepresProfile.objects.all().values_list('region', flat=True)
        regions = Region.objects.filter(id__in=repres)
        return repres

    class Meta:
        model = SellerProfile
        fields = ('first_name', 'second_name', 'last_name', 'region', 'sellerCity', 'position', 'award_method', 'yandex_money_account', 'phone', 'personal_data_confirmed', )


class SellerPaymentMethodForm(ModelForm):
    '''
    Seller payment method in register
    '''

    yandex_money_account = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'seller.yandex_money_account', 'ng-initial': 'True'}),
        label=u'Укажите Яндекс аккаунт',
        required=False,
    )

    phone = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'seller.phone', 'ng-initial': 'True'}),
        label=u'Укажите телефон',
        required=False,
    )

    class Meta:
        model = UserProfile
        fields = ('yandex_money_account', 'phone')



class SellerReportForm(ModelForm):
    '''
    Saller report form
    '''
    # product = forms.ModelChoiceField(
    #    widget=forms.Select(
    #        attrs={
    #            'class': '',
    #            'ng-model': 'sellerreport.product',
    #            'required': 'True'
    #        },
    #    ),
    #    queryset = Product.objects.filter(),
    #    label=u"Продукт",
    #    required = True,
    # )

    product = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'seller.product', 'ng-initial': 'True', 'required': 'True'}),
        label=u'Укажите проданную продукцию',
        required=True,
    )

    price = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'seller.price', 'ng-initial': 'True', 'required': 'True'}),
        label=u'Укажите цену',
        required=True,
    )

    file  = forms.FileField(
        widget=forms.FileInput(attrs={'ng-model': 'seller.file'}),
        required = False,
        label = u'Фотоотчет',
    )

    file1  = forms.FileField(
        widget=forms.FileInput(attrs={'ng-model': 'seller.file1'}),
        required = False,
        label = u'Фотоотчет',
    )

    file2  = forms.FileField(
        widget=forms.FileInput(attrs={'ng-model': 'seller.file2'}),
        required = False,
        label = u'Фотоотчет',
    )

    class Meta:
        model = SellerReport
        fields = ('product', 'price', 'file', 'file1', 'file2')
