# -*- coding: utf-8 -*-

import os
import re

from django.db import models
from django.db.models import Q, F

from django.shortcuts import get_object_or_404

import json
import datetime
import random
import logging
import thread

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

from itertools import chain
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.template import RequestContext

from seo.models import *
from node.models import SEONode, NodeType

from triusers.forms import *
from tri.models import UserProfile
from da_mailer.helper import *
from constructors.models import FreelanceProfile


from sms_api.models import *
from yandex_money_api.models import *
from transliterate import translit, get_available_language_codes

@login_required
def dealer_request(request, ):
    # price_form = GetPriceForm()

    if request.method == 'POST':
        dealer_form = DealerRegisterForm(request.POST)
        # dealer_phone_from = DealerPhoneFormSet(request.POST)

        print dealer_form

        if dealer_form.is_valid():
            # mail = DaMailer.send_mail_to_admin('get_dealer_price', data=price_form.clean())
            # try:

            df = dealer_form.save(commit=False)
            user_profile = UserProfile.objects.get(user=request.user)
            df.user_profile = user_profile

            df.save()
            dealer_form.save_m2m()

            clean_form = dealer_form.clean()

            user_profile.second_name = clean_form.get('second_name')
            user_profile.save()

            user = request.user
            user.first_name = clean_form.get('first_name')
            user.last_name = clean_form.get('last_name')
            user.save()

            message = u"Заявка принята. <br />В ближайшее время наш супервайзер свяжется с вами."

            # # df.dealer_phone = None
            # if dealer_phone_from.is_valid():
            #     for form in dealer_phone_from.cleaned_data:
            #         # print "PHONE", form.get('phone')
            #         dealer_phone = DealerProfilePointPhone(phone = form.get('phone'), dealer_profile = df)
            #         dealer_phone.save()
            #         df.dealer_phone = dealer_phone
            #     pass

            thread.start_new_thread(DaMailer.send_mail_to_admin,('admin_new_dealer_notification', 'Заявка на получение статуса Дилер', df))
            # DaMailer.send_mail_to_user(template = 'new_dealer_notification', subject=u'Заявка на получение статуса Дилер', data = df, user = user)
            user = User.objects.get(username='nevariuss')
            thread.start_new_thread(Sms_Api.send_message,(user, "Poluchena Zayavka na poluchenie statusa Diler. Voidite v kabinet, chtoby podtverdit' http://3tn.ru/profile"))


        else:
            message = dealer_form.errors
            print dealer_form.errors
            pass





    return redirect(request.META.get('HTTP_REFERER'))

@login_required
def repres_request(request):
    '''
    TODO: at DaMailer.send_mail_to_user user should be parent dealer_profile.user
    '''

    # price_form = GetPriceForm()

    if request.method == 'POST':
        repres_form = RepresRegisterForm(request.POST)

        if repres_form.is_valid():
            # mail = DaMailer.send_mail_to_admin('get_dealer_price', data=price_form.clean())
            # try:

            # clear_form = repres_form.clean()

            df = repres_form.save(commit=False)
            user_profile = UserProfile.objects.get(user=request.user)
            df.user_profile = user_profile
            #df.dealer = DealerProfile.objects.get(dealer_point_name=clear_form.dealer_point_name)
            # df.user_profile = request.user.get_profile()
            df.save()

            repres_form.save_m2m()


            clean_form = repres_form.clean()

            user_profile.second_name = clean_form.get('second_name')
            user_profile.save()

            user = request.user
            user.first_name = clean_form.get('first_name')
            user.last_name = clean_form.get('last_name')
            user.save()

            message = u"Заявка принята. <br />Ожидайте подтвеждения от Вашего дилера."

            clean_form = repres_form.clean()
            user = DealerProfile.objects.filter(region = clean_form.get('region'))[:1].get().user_profile.user

            # DaMailer.send_mail_to_admin(template = 'new_repres_notification', subject=u'Заявка на получение статуса Торгового представителя', data=df)

            thread.start_new_thread(DaMailer.send_mail_to_user,('new_repres_notification', u'Заявка на получение статуса Торгового представителя', df, user))
            thread.start_new_thread(Sms_Api.send_message,(user, "Poluchena Zayavka na poluchenie statusa Torgpred. Voidite v kabinet, chtoby podtverdit' http://3tn.ru/profile"))

        else:
            message = repres_form.errors
            pass



    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def seller_request(request):
    # price_form = GetPriceForm()

    print "request"
    if request.method == 'POST':
        seller_form = SellerRegisterForm(request.POST)

        if seller_form.is_valid():
            # mail = DaMailer.send_mail_to_admin('get_dealer_price', data=price_form.clean())
            # try:

            clean_form = seller_form.clean()

            df = seller_form.save(commit=False)
            user_profile = UserProfile.objects.get(user = request.user)
            df.user_profile = user_profile

            # change phone
            # print clean_form.get('phone')
            if clean_form.get('phone') != '':
                user_profile.phone = clean_form.get('phone')

            # change yandex
            if clean_form.get('yandex_money_account') != '':
                user_profile.yandex_money_account = clean_form.get('yandex_money_account')

            try:
                df.repres = RepresProfile.objects.filter(region = clean_form.get('region'))[:1].get()
                df.repres_first_name = df.repres.user_profile.user.first_name
                df.repres_last_name = df.repres.user_profile.user.last_name

            except Exception, e:
                print Exception, e
            # df.user_profile = request.user.get_profile()
            df.save()
            user_profile.save()

            user_profile.second_name = clean_form.get('second_name')
            user_profile.save()

            user = request.user
            user.first_name = clean_form.get('first_name')
            user.last_name = clean_form.get('last_name')
            user.save()


            user =  df.repres.user_profile.user
            DaMailer.send_mail_to_admin(template = 'new_seller_notification', subject=u'Заявка на получение статуса Продавец', data=df)
            # thread.start_new_thread(DaMailer.send_mail_to_admin,('new_seller_notification', u'Заявка на получение статуса Продавец', df))
            DaMailer.send_mail_to_user(template = 'new_seller_notification', subject=u'Заявка на получение статуса Продавец', data=df, user=user)
            # thread.start_new_thread(DaMailer.send_mail_to_user,('new_seller_notification', u'Заявка на получение статуса Продавец', df, user))

            thread.start_new_thread(Sms_Api.send_message, (user, "Poluchena Zayavka na poluchenie statusa Prodavetz. Voidite v kabinet, chtoby podtverdit' http://3tn.ru/profile"))



            message = u"Заявка принята. <br />Ожидайте подтвеждения от Вашего торгового представителя."
        else:
            message = seller_form.errors
            pass



    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def report_request(request):
    # price_form = GetPriceForm()

    print request

    if request.method == 'POST':
        report_form = SellerReportForm(request.POST, request.FILES)

        if report_form.is_valid():



            print request.FILES

            rf = report_form.save(commit=False)
            rf.seller = SellerProfile.objects.get(user_profile__user = request.user)
            rf.save()

            try:
                seller = SellerProfile.objects.get(user_profile=request.user.userprofile)
                repres = seller.get_repres()

                # Sending new report mail notification to head representative of seller
                subject = u'Новый отчет о продаже от %s' % seller.user_profile.user.username
                # print '--'
                # print rf.price
                # print seller.fee_value
                # print '--'
                rf.payout = round(int(rf.price)/100*int(seller.fee_value), 0)


                DaMailer.send_mail_to_user('new_report_request', subject, data=rf, user=repres.user_profile.user)
                # thread.start_new_thread(DaMailer.send_mail_to_user,('new_report_request', subject, rf, repres.user_profile.user))

                # Sending new report sms notification to head representative of seller
                # print 'go sms'
                thread.start_new_thread(Sms_Api.send_message,(repres.user_profile.user, "Novaya prodazha ot %s: %s - %s rub. K vyplate %i rub. Voidite v kabinet, chtoby podtverdit' http://3tn.ru/profile" % (seller.user_profile.user.username, translit(rf.product,  'ru', reversed=True), rf.price, rf.payout)))

                # print repres.user_profile.user
            except Exception, e:
                print Exception, e

            message = u"Заявка принята. <br />Ожидайте подтвеждения от Вашего торгового представителя."
        else:
            message = report_form.errors
            pass



    return redirect(request.META.get('HTTP_REFERER'))



@login_required
def approve_report(request, id):
    result = False
    try:
        report = SellerReport.objects.get(id=id)

        report.approve = True
        report.approve_date = datetime.datetime.now()
        report.save()

        # def send_to_phone_number(self, user='', amount='', comment='', message=''):

        amount = round(int(report.price)/100*int(report.seller.fee_value), 0)

        if report.seller.award_method == 'phone':

            Yandex_Money_Api.send_to_phone_number(report.seller.user_profile.user, amount)

            subject=u'На счет вашего мобильного телефона переведено %s руб' % (amount)
            message = 'Вход в личный кабинет http://3tn.ru/profile'
            DaMailer.send_mail_to_user('new_payment_report', subject, data=message, user=report.seller.user_profile.user)
            # thread.start_new_thread(DaMailer.send_mail_to_user, ('new_payment_report', subject, message, report.seller.user_profile.user))
            thread.start_new_thread(Sms_Api.send_message,(report.seller.user_profile.user, 'Na schet vashego telefona perevedeno %s rub. za prodazhu.' % (amount) ))

        elif report.seller.award_method == 'yandex_money':

            Yandex_Money_Api.send_to_yandex_money(report.seller.user_profile.user, amount)

            subject=u'На ваш счет Яндекс-деньги переведено %s руб' % (amount)
            message = 'Вход в личный кабинет http://3tn.ru/profile'
            DaMailer.send_mail_to_user('new_payment_report', subject, data=message, user=report.seller.user_profile.user)
            # thread.start_new_thread(DaMailer.send_mail_to_user, ('new_payment_report', subject, message, report.seller.user_profile.user))
            thread.start_new_thread(Sms_Api.send_message,(report.seller.user_profile.user, 'Na vash schet Yandex-dengi perevedeno %s rub. za prodazhu' % (amount)))


        result = True
    except:
        pass

    return HttpResponse(result)

@login_required
def activate_advuser(request, user_type, id):
    result = False
    try:
        user_advanced_profile = None

        if user_type == 'dealer':
            user_advanced_profile = DealerProfile.objects.get(id=id)
            profile = 'Dealer'
            profile_rus = 'Дилер'
        elif user_type == 'repres':
            user_advanced_profile = RepresProfile.objects.get(id=id)
            profile = 'Torgpred'
            profile_rus = 'Торговый представитель'
        elif user_type == 'seller':
            user_advanced_profile = SellerProfile.objects.get(id=id)
            profile = 'Prodavetz'
            profile_rus = 'Продавец'
        elif user_type == 'freelancer':
            user_advanced_profile = FreelanceProfile.objects.get(id=id)

        user_advanced_profile.active = True
        user_advanced_profile.save()



        user = user_advanced_profile.user_profile.user
        user.groups.add(Group.objects.get(name=user_type))
        user.save()

        if profile:
            data = 'Войдите в <a href="http://3tn.ru/profile">личный кабинет</a> для дальнейших операций'
            # print 'go adv user activation'
            # print user
            thread.start_new_thread(DaMailer.send_mail_to_user,('adv_user_activation_notification', u'Заявка на получение статуса %s подтверждена' % (profile_rus), user, data))
            # print 'go adv user sms'
            thread.start_new_thread(Sms_Api.send_message,(user, "Zayavka na poluchenie statusa %s podtverzhdena. Voidite v kabinet http://3tn.ru/profile" % (profile)))

        result = True
    except:
        pass

    return HttpResponse(result)

@login_required
def activate_seller(request, user_type, id, fee_value):
    result = False
    try:
        # user_advanced_profile = None

        # if user_type == 'seller':

        user_advanced_profile = SellerProfile.objects.get(id=id)
        profile = 'Prodavetz'
        profile_rus = 'Продавец'

        user_advanced_profile.active = True
        user_advanced_profile.fee_value = fee_value
        user_advanced_profile.save()

        user = user_advanced_profile.user_profile.user
        user.groups.add(Group.objects.get(name=user_type))
        user.save()

        # if profile:
        data = u'Ваше вознаграждение составит %s%% . Войдите в <a href="http://3tn.ru/profile">личный кабинет</a> для дальнейших операций' % (fee_value)

        DaMailer.send_mail_to_user( 'adv_user_activation_notification', subject=u'Заявка на получение статуса %s подтверждена' % (profile_rus), user=user, data=data)
        # thread.start_new_thread(DaMailer.send_mail_to_user,( 'adv_user_activation_notification', u'Заявка на получение статуса %s подтверждена' % (profile_rus), user, data))

        thread.start_new_thread(Sms_Api.send_message, (user, 'Zayavka na poluchenie statusa %s podtverzhdena. Voznagazhdenie %s%% . Voidite v kabinet http://3tn.ru/profile' % (profile, fee_value)))

        result = True
    except:
        pass

    return HttpResponse(result)

@login_required
def deactivate_advuser(request, user_type, id):
    result = False
    try:
        user_advanced_profile = None

        if user_type == 'dealer':
            user_advanced_profile = DealerProfile.objects.get(id=id)
        elif user_type == 'repres':
            user_advanced_profile = RepresProfile.objects.get(id=id)
        elif user_type == 'seller':
            user_advanced_profile = SellerProfile.objects.get(id=id)
        elif user_type == 'freelancer':
            user_advanced_profile = FreelanceProfile.objects.get(id=id)

        user_advanced_profile.active = False
        user_advanced_profile.save()
        result = True

        user = user_advanced_profile.user_profile.user
        user.groups.remove(Group.objects.get(name=user_type))
        user.save()

    except:
        pass

    return HttpResponse(result)

@login_required
def delete_advuser(request, user_type, id):
    result = False
    try:
        user_advanced_profile = None

        if user_type == 'dealer':
            user_advanced_profile = DealerProfile.objects.get(id=id)
        elif user_type == 'repres':
            user_advanced_profile = RepresProfile.objects.get(id=id)
        elif user_type == 'seller':
            user_advanced_profile = SellerProfile.objects.get(id=id)
        elif user_type == 'freelancer':
            user_advanced_profile = FreelanceProfile.objects.get(id=id)



        user = user_advanced_profile.user_profile.user
        user.groups.remove(Group.objects.get(name=user_type))
        user.save()


        user_advanced_profile.delete()
        result = True

    except:
        pass

    return HttpResponse(result)

