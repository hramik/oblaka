# -*- coding: utf-8 -*-
__author__ = 'm13v246'

from django.db import models
# from tri.models import *
# from products.models import Product
from tri.models import UserProfile, Company, Region, Phone


class AdvancedUserProfileFields(models.Model):
    '''
    Abstract class for Tri Users Profiles
    '''

    class Meta():
        abstract = True

    user_profile = models.OneToOneField(
        UserProfile,
    )

    position = models.CharField(
        max_length=50,
        default="",
        blank=True
    )

    active = models.BooleanField(
        default=False,
    )

    inn = models.CharField(
        max_length=15,
        default="",
        blank=True,
        verbose_name='ИНН',
    )

    passport = models.CharField(
        max_length=15,
        default="",
        blank=True,
        verbose_name='Номер паспорта',
    )

    passport_organization = models.CharField(
        max_length=250,
        default="",
        blank=True,
        verbose_name='Паспорт выдан',
    )

    registration_address = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name='Адрес регистрации',
    )

    agree_personal_data = models.BooleanField(
        default=False,
    )

    def __unicode__(self):
        return self.user_profile.user.username


class DealerProfile(AdvancedUserProfileFields):
    company = models.ForeignKey(
        Company,
        default=1
    )

    # region = models.ManyToManyField(
    #     Region
    # )
    #
    # dealer_point_name = models.CharField(
    #     max_length=50,
    #     default="",
    #     blank=True
    # )
    #
    # dealer_point_address = models.CharField(
    #     max_length=50,
    #     default="",
    #     help_text=u'Адрес дилерского центра',
    #     blank=True
    # )

    def __unicode__(self):
        return self.company.title


class DealerProfilePointPhone(Phone):
    dealer_profile = models.ForeignKey(
        DealerProfile,
        blank=True,
        null=True,
        related_name='phones'
    )


class RepresProfile(AdvancedUserProfileFields):
    region = models.ForeignKey(
        Region
    )
    # TODO: delete this
    # dealer = models.ForeignKey(
    # DealerProfile
    # )

    def __unicode__(self):
        return self.user_profile.user.username


class SellerProfile(AdvancedUserProfileFields):
    '''
    Seller profiler for Motivation System
    '''

    AWARD_METHOD_CHOICES = (
        ('phone', u'На мобильный'),
        ('yandex_money', u'На счет в Yandex-кошелек'),
        ('ccard', u'На банковскую карту'),
    )

    region = models.ForeignKey(
        Region
    )

    sellerCity = models.CharField(
        max_length=50,
        default="",
        blank=True,
    )

    award_method = models.CharField(
        max_length=50,
        default="",
        blank=True,
        choices=AWARD_METHOD_CHOICES
    )

    repres = models.ForeignKey(
        RepresProfile,
        blank=True,
        null=True,
    )

    # комиссионное вознаграждение
    fee_value = models.PositiveSmallIntegerField(
        default=0,
    )



    def get_repres(self):
        return RepresProfile.objects.filter(region=self.region)[:1].get()


class SellerReport(models.Model):
    '''
    Seller report about sell
    '''
    seller = models.ForeignKey(
        SellerProfile
    )

    # product = models.ForeignKey(
    # Product
    # )

    product = models.CharField(
        max_length=255,
        default="",
    )

    price = models.CharField(
        max_length=20,
        default="",
    )

    file = models.FileField(
        upload_to='seller_report_files',
        default='',
        blank=True
    )

    file1 = models.FileField(
        upload_to='seller_report_files',
        default='',
        blank=True
    )

    file2 = models.FileField(
        upload_to='seller_report_files',
        default='',
        blank=True
    )

    approve = models.BooleanField(
        default=False
    )

    pub_date = models.DateTimeField(
        verbose_name=u'Дата добавления',
        auto_now_add=True,
    )
    # pub_date.editable = True

    approve_date = models.DateTimeField(
        verbose_name=u'Дата подтверждения',
        blank=True,
        null=True
    )

    class Meta:
        ordering = ['-pub_date']
        permissions = (("approve_sellerreport", "Can approve seller report"),)






