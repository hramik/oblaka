# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.contrib import admin

from django.db import models

from mptt.admin import MPTTModelAdmin
from sorl.thumbnail.admin import AdminImageMixin
from django.contrib.admin import BooleanFieldListFilter

from tri.models import *
from slides.models import Slide
from node.models import *
from ticka.models import Ticka
from products.models import Category, Product, Delivery, Payment, Order, OrderItem, OptionsGroup, Option, OptionPrice
from products.models import ProductComment, ProductType, ProductEntity, CategoryType, Category, ProductCategory, \
    Component, Visor
from custom_sites.models import SiteInfo
from callback.models import Callback
from questions.models import Question
from seo.models import PathSEO

from menu.models import Menu, MenuGroup
from tri.admin_filters import *
from tri.admin_actions import *

from triusers.models import *


class DealerPointPhoneInline(admin.TabularInline):
    model = DealerProfilePointPhone
    extra = 0


class DealerProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('user_profile',)
    list_display = ('company', 'user_profile')
    inlines = (DealerPointPhoneInline,)


class RepresProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('user_profile',)
    # list_display = ('dealer_point_name', 'user')
    # inlines = (DealerPointPhoneInline,)


class SellerProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('user_profile', 'repres')


class SellerReportAdmin(admin.ModelAdmin):
    list_display = ('seller', 'product', 'price', 'file', 'approve', 'pub_date', 'approve_date')


admin.site.register(DealerProfile, DealerProfileAdmin)
admin.site.register(RepresProfile, RepresProfileAdmin)
admin.site.register(SellerProfile, SellerProfileAdmin)

admin.site.register(DealerProfilePointPhone)
admin.site.register(SellerReport, SellerReportAdmin)
