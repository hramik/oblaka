__author__ = 'hramik'

import urllib
import urllib2
from custom_sites.models import SiteInfo

def calltouchEvent(object, call_value, comment):
    result = False
    try:
        site = SiteInfo.on_site.select_related().get()
        if site.json["client_api_id"]:
            calltouch_api_href = 'http://api.calltouch.ru/calls-service/RestAPI/requests/orders/register/'
            data = urllib.urlencode({'clientApiId': site.json["client_api_id"], # 1127245625ct7a7d9749897b14a3bd29aee41544e268
                                     'requestNumber': object.id,
                                     'fio': object.name,
                                     'phoneNumber': object.phone,
                                     'email': object.email,
                                     'personalPhone': 'true',
                                     'orderComment': comment,
                                     'sessionId': call_value
                                     })

            req = urllib2.Request(calltouch_api_href, data=data)
            req.add_header('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8')
            r = urllib2.urlopen(req)
            result = True
    except Exception, e:
        result = "%s %s" % (Exception, e)
    return result
