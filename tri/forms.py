# -*- coding: utf-8 -*-

import datetime, sys, os, re
from django.utils.encoding import force_unicode

from django.db import models
from django.forms import ModelForm
from django.db import models
from django import forms

from seo.models import *
from tri.models import *

class GetPriceForm(ModelForm):
    '''
    Get prices for dealers
    '''

    name = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'price.name',
                'required': 'True',
            }
        ),
        label=u"Имя",
        required=True
    )

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
                'class': '',
                'ng-model': 'price.email',
            }
        ),
        label=u"Емейл",
        required=False
    )

    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'price.phone',
                'required': 'True',
            }
        ),
        label=u"Телефон",
        required=True
    )

    region = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': '',
                'ng-model': 'price.region',
            },
        ),

        # choices=list(Region.objects.all().exclude(slug='none').values_list('name', 'name')),
        label=u"Регион",
        required=True
    )

    organization = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'price.organization',
            }
        ),
        label=u"Организация",
        required=False
    )

    activity = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'price.activity',
            }
        ),
        label=u"Вид деятельности",
        required=False
    )

    class Meta:
        model = SEOFields
        fields = ('name', 'email', 'phone', 'region', 'organization', 'activity')


class SolutionOrderForm(ModelForm):
    '''
    Set order for design solution in magazine
    '''
    body = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': '',
                'ng-model': 'solution.body',
                'required': 'True',
            }
        ),
        label=u"Пожелания",
        required=True
    )

    location = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'solution.location',
                'required': 'True',
            }
        ),
        label=u"Область/республика/край, город/населённый пункт",
        required=True
    )


    house_type = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': '',
                'ng-model': 'solution.house_type',
                'required': 'True',
            },
        ),
        choices=list(HOUSE_TYPES_CHOICES),
        label=u"Тип дома",
        required=True
    )


    series = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'solution.series',
            }
        ),
        label=u"Серия",
        required=False,
        help_text = "Укажите её по возможности. Эта информация поможет  дизайнеру оценить технические нюансы вашей квартиры."
    )

    height = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'solution.height',
            }
        ),
        label=u"Высота потолков (см)",
        required=False
    )

    transfer = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'solution.transfer',
            }
        ),
        label=u"Возможен ли перенос сантехнического оборудования",
        required=False
    )

    counters = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'solution.counters',
            }
        ),
        label=u"Требуется ли установка счётчиков воды",
        required=False
    )

    rude_filters = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'solution.rude_filters',
            }
        ),
        label=u"Требуется ли установка фильтров грубой очистки",
        required=False
    )

    thin_filters = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'solution.thin_filters',
            }
        ),
        label=u"Требуется ли установка фильтров тонкой очистки",
        required=False
    )

    living_people = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'solution.living_people',
            }
        ),
        label=u"Количество проживающих людей (чел)",
        required=False
    )

    old_man_interests = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'solution.old_man_interests',
            }
        ),
        label=u"Учитывать ли интересы пожилых людей?",
        required=False
    )

    children_interests = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'solution.children_interests',
            }
        ),
        label=u"Учитывать ли интересы маленьких детей?",
        required=False
    )

    bath_need = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'solution.bath_need',
            }
        ),
        label=u"Готовы ли отказаться от ванны?",
        required=False
    )


    hydromassage_need = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': '',
                'ng-model': 'solution.hydromassage_need',
            },
        ),
        choices=list(YES_NO_DONTKNOW_CHOICES),
        label=u"Нужен ли вам гидромассаж?",
        required=False
    )

    second_washbasin = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'solution.second_washbasin',
            }
        ),
        label=u"Полезен ли для вас второй умывальник?",
        required=False
    )

    storage_places = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': '',
                'ng-model': 'solution.storage_places',
            },
        ),
        choices=list(STORAGE_PLACE_CHOICE),
        label=u"Мест для хранения должно быть:",
        required=False
    )

    color_preferences = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'solution.color_preferences',
            }
        ),
        label=u"Цветовые предпочтения",
        required=False,
        help_text="Приложите, пожалуйста, к этому письму следующие файлы: <br/>"
                  "1.  План помещения <br/>"
                  "Внимание! Кроме размеров, на плане необходимо показать:<br/>"
                  "- Стояки ХВ и ГВ (холодной и горячей воды), канализации<br/>"
                  "- Тип подключения полотенцесушителя (стояк ГВ или отопления; электросеть)<br/>"
                  "- Счётчики и фильтры (если есть)<br/>"
                  "2. Фото помещения. Пожалуйста, фотографий с разных ракурсов  сделайте побольше!"
                  ""
    )





    class Meta:
        model = SolutionOrder
        fields = ('body', 'location', 'house_type', 'series', 'height', 'transfer', 'counters', 'rude_filters', 'thin_filters', 'living_people', 'old_man_interests', 'children_interests', 'bath_need', 'hydromassage_need', 'second_washbasin', 'storage_places', 'color_preferences')


from products.models import ORDER_STATUS_CHOICES

class OrdersFilterForm(ModelForm):
    '''
    Filter orders
    '''

    status = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'ui search selection dropdown',
                'multiple': '',
                # 'ng-model': 'orders.status',
            },
        ),
        choices=tuple(((u'', "все"),) + ORDER_STATUS_CHOICES),
        label=u"Статус",
        required=False,
    )

    site = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'ui search  dropdown',
                'multiple': '',
                # 'ng-model': 'orders.site',
            },
        ),
        # choices=tuple(((u'', "все"),) + tuple(Site.objects.all().values_list('name', 'domain'))),
        label=u"Сайт",
        required=False
    )

    id = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'ui input',
                # 'ng-model': 'orders.site',
            },
        ),
        label=u"id",
        required=False
    )

    start_data = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'ui input',
                'placeholder': "2016-05-21"
                # 'ng-model': 'orders.site',
            },
        ),
        label=u"C",
        required=False
    )

    end_data = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': 'ui input',
                'placeholder': "2016-05-21"
                # 'ng-model': 'orders.site',
            },
        ),
        label=u"По",
        required=False
    )

    class Meta:
        model = SEOFields
        fields = ('id', 'status', 'site', 'start_data', 'end_data')
