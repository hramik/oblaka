# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

SITE_ID = 8

urlpatterns = patterns('',
                       url(r'^map-1.html$', 'tri.views.site_structure', {'site_id': SITE_ID}, name='site_structure'),

                       url(r'^vanny-akrilovye-magazin-ufa$', 'tri.views.products', {'entity': 'bath', 'site_id': SITE_ID}, name='baths'),
                       url(r'^asimmetrichnyie-vannyi.html$', 'tri.views.products', {'entity': 'bath', 'cats': ('asimmetrichnie',), 'site_id': SITE_ID}, name='asimetric_baths'),
                       url(r'^pryamyie-vannyi.html$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='pryamougolnyie_baths'),
                       # url(r'^uglovyie-vannyi.html$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='uglovie_baths'),
                       url(r'^uglovyie-vannyi.html$', 'tri.views.products', {'entity': 'bath', 'cats': ('uglovie',), 'site_id': SITE_ID}, name='uglovie_baths'),
                       url(r'^standartnyie-vannyi.html$', 'tri.views.products', {'entity': 'bath', 'cats': ('standart',), 'site_id': SITE_ID}, name='standart_baths'),

                       url(r'^mebel-dlya-vannyih-komnat.html$', 'tri.views.products', {'entity': 'mebel', 'site_id': SITE_ID}, name='mebel'),
                       url(r'^mebel-dlya-vannyih-komnat/zerkala.html$', 'tri.views.products', {'entity': 'mebel', 'cats': ('mirrors',), 'site_id': SITE_ID},name='mebel_mirrors'),
                       url(r'^mebel-dlya-vannyih-komnat/shkafyi.html$', 'tri.views.products', {'entity': 'mebel', 'cats': ('selfs',), 'site_id': SITE_ID},name='mebel_selfs'),
                       url(r'^mebel-dlya-vannyih-komnat/penalyi.html$', 'tri.views.products', {'entity': 'mebel', 'cats': ('penals',), 'site_id': SITE_ID},name='mebel_penals'),
                       url(r'^mebel-dlya-vannyih-komnat/tumbyi.html$', 'tri.views.products', {'entity': 'mebel', 'cats': ('sinks',), 'site_id': SITE_ID},name='mebel_sinks'),
                       url(r'^mebel-dlya-vannyih-komnat/tumbyi-podvesnye.html$', 'tri.views.products', {'entity': 'mebel', 'cats': ('hang_sinks',), 'site_id': SITE_ID},name='mebel_hang_sinks'),
                       url(r'^mebel-dlya-vannyih-komnat/komodyi.html$', 'tri.views.products', {'entity': 'mebel', 'cats': ('commodes',), 'site_id': SITE_ID},name='mebel_commodes'),
                       url(r'^mebel-dlya-vannyih-komnat/tumbyi-napolnyie.html$', 'tri.views.products', {'entity': 'mebel', 'cats': ('tumb',), 'site_id': SITE_ID},name='mebel_tumb'),

                       url(r'^smesiteli.html$', 'tri.views.products', {'entity': 'mixer', 'site_id': SITE_ID}, name='mixers'),

                       url(r'^dushevyie-kabinyi.html$', 'tri.views.products', {'entity': 'bathcab', 'site_id': SITE_ID}, name='bathcabs'),
                       url(r'^common$', 'tri.views.list', {'slug_in': ('news', 'advices'), 'site_id': SITE_ID}, name='news'),
                       url(r'^questions-ufa$', 'tri.views.questions', name='questions'),

)



