# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

SITE_ID = 20

urlpatterns = patterns('',
                       url(r'^vanny-akrilovye-eburg$', 'tri.views.products', {'entity': 'bath', 'site_id': SITE_ID}, name='baths'),
                       url(r'^asimmetrichnyie-vannyi$', 'tri.views.products', {'entity': 'bath', 'cats': ('asimmetrichnie',), 'site_id': SITE_ID}, name='asimetric_baths'),
                       url(r'^pryamougolnyie-vannyi$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='pryamougolnyie_baths'),
                       url(r'^uglovyie-vannyi$', 'tri.views.products', {'entity': 'bath', 'cats': ('uglovie',), 'site_id': SITE_ID}, name='uglovie_baths'),
                       # url(r'^$', 'tri.views.products', {'entity': 'bath', 'cats': ('standart',), 'site_id': SITE_ID}, name='standart_baths'),

                       url(r'^mebel-dlya-vannoy-komnatyi$', 'tri.views.products', {'entity': 'mebel', 'site_id': SITE_ID}, name='mebel'),
                       url(r'^mebel-serii-diana$', 'tri.views.products', {'entity': 'mebel', 'cats': ('diana',), 'site_id': SITE_ID},name='mebel_diana'),
                       url(r'^mebel-serii-kristi$', 'tri.views.products', {'entity': 'mebel', 'cats': ('kristi',), 'site_id': SITE_ID},name='mebel_kristi'),
                       url(r'^mebel-serii-nika$', 'tri.views.products', {'entity': 'mebel', 'cats': ('nika',), 'site_id': SITE_ID},name='mebel_nika'),
                       url(r'^mebel-serii-eko$', 'tri.views.products', {'entity': 'mebel', 'cats': ('eko',), 'site_id': SITE_ID},name='mebel_eko'),
                       url(r'^mebel-serii-lira$', 'tri.views.products', {'entity': 'mebel', 'cats': ('lira',), 'site_id': SITE_ID},name='mebel_lira'),
                       # url(r'^$', 'tri.views.products', {'entity': 'mebel', 'cats': ('eko-wood',), 'site_id': SITE_ID},name='mebel_eko-wood'),
                       url(r'^mebel-serii-mirta$', 'tri.views.products', {'entity': 'mebel', 'cats': ('mirta',), 'site_id': SITE_ID},name='mebel_mirta'),

                       url(r'^smesiteli$', 'tri.views.products', {'entity': 'mixer', 'site_id': SITE_ID}, name='mixers'),

                       url(r'^dushevyie-kabinyi$', 'tri.views.products', {'entity': 'bathcab', 'site_id': SITE_ID}, name='bathcabs'),

                       url(r'^news$', 'tri.views.list', {'slug_in': ('news', 'advices'), 'site_id': SITE_ID}, name='news'),
                       url(r'^sitemap$', 'tri.views.site_structure', {'site_id': SITE_ID}, name='site_structure'),
                       url(r'^questions-eburg$', 'tri.views.questions', name='questions'),



                       )



