# -*- coding: utf-8 -*-
__author__ = 'hramik'

from ipgeo.models import Range
from django.contrib.gis.geoip import GeoIP
from products.forms import *
from custom_sites.models import SiteInfo
from callback.models import CallbackForm
from menu.models import Menu
from tri.constants import *
from django.contrib.sites.shortcuts import get_current_site
from da_mailer.helper import *


def user_processor(request):
    # Current User and his profile
    current_user = request.user
    try:
        user_profile = current_user.get_profile()
    except:
        user_profile = False

    # Login/Register/Recovery Forms
    login_form = LoginForm(initial={
        'username': '',
        'password': '',
    })

    recovery_form = RecoveryForm(initial={
        'email': '',
    })

    register_form = RegisterForm(initial={
        'username': '',
        'email': '',
        'password': '',
        'confirm_password': '',
    })

    # IS TEST SESSION
    is_test = False
    if current_user.is_superuser:
        is_test = True

    if request.META['REMOTE_ADDR'] == '127.0.0.1':
        is_test = True

    return {
        'current_user': current_user,
        'user_profile': user_profile,
        'login_form': login_form,
        'recovery_form': recovery_form,
        'register_form': register_form,
        'is_test': is_test,
    }


def geo_processor(request):
    current_site = SiteInfo.on_site.get()
    current_site.clear_json = json.dumps(current_site.json)
    current_ip = request.META['REMOTE_ADDR']
    current_host = request.META['HTTP_HOST']

    g = GeoIP()


    if not '127.0.0.1' in current_host and not current_host in current_site.site.domain:
        # if not current_host in current_site.site.domain:
        data = {
            'current_site': current_site.site.domain,
            'current_host': current_host,
            'get_current_site': get_current_site(request),
            'get_host': request.get_host(),
            'current_ip': current_ip,
            'path': request.META['PATH_INFO'],
            'date': datetime.datetime.now(),
            'site_id': settings.SITE_ID,
        }

        # DaMailer.send_mail_to_address(template="da_mailer/hosts.html",
        #                               subject="Несовпадение хоста", data=data,
        #                               email='rpp31@ya.ru')

    if '127.0.0.1' in current_host:
        current_ip = '82.208.100.10'  # Нижний
        current_ip = '62.182.208.10'  # Владивосток
        # current_ip = '176.192.253.29'  # Серега
        # current_ip = '188.186.65.122' #Тюмень
        # current_ip = '46.44.54.12' #Москва

    rang_region = None
    current_region = None
    no_region = False

    # if current_site.is_shop and not current_site.site.domain == 'www.triton3tn.ru':
    #     current_region = current_site.get_region()
    # else:
    #     try:
    #         # rang = Range.objects.find(current_ip)
    #         geo_city = g.city(current_ip)
    #         rang_region = geo_city['region']
    #         current_region = Region.objects.filter(name__contains=rang_region)[:1].get()
    #     except Exception as e:
    #         # If not recognize region — set moscow region as current
    #         current_ip_ex = '79.120.31.237'
    #         geo_city = g.city(current_ip_ex)
    #         rang_region = geo_city['region']
    #         current_region = Region.objects.filter(name__contains=rang_region)[:1].get()
    #         no_region = True

    # if current_site.site.domain in ('www.triton3tn.ru', '3tn.ru') and not "Москва" in current_region.name:
    #     current_site.is_shop = False
    #
    # if current_site.site.domain in ('www.triton3tn.ru', '3tn.ru') and request.user_agent.is_bot:
    #     current_site.is_shop = False

    return {
        'current_site': current_site,
        'current_region': current_region,
        'no_region': no_region,
        'rang_region': rang_region,
        'current_ip': current_ip,
        'current_host': current_host,
    }


def shop_processor(request):
    cart_ids = None
    cart = False

    try:
        cart_session = request.session["cart"]
        if not cart_session == []:
            cart = Product.on_site.filter(id__in=cart_session)
            cart_ids = str(cart.values_list('id', flat=True)).strip('[]').replace('L', '').replace(' ', '')
    except Exception, e:
        pass

    return {
        'cart': cart,
        'cart_ids': cart_ids,
        'quick_order_form': OrderForm(),
        'discounts': Discount.objects.filter(publish=True),
    }


def menu_processor(request):
    # Different menus for different site parts
    if "/magazine" in request.path:
        main_menu = Menu.on_site.filter(menu_group__slug='magazine_menu', publish=1)
    else:
        main_menu = Menu.on_site.filter(menu_group__slug='main_menu', publish=1)

    try:
        current_menu = Menu.on_site.select_related().get(link=request.path)
    except Exception, e:
        current_menu = False

    return {
        'main_menu': main_menu,
        'current_menu': current_menu,
    }


def triton_processor(request):
    current_site = SiteInfo.on_site.select_related().get().site

    #  Regions and saleplaces for map
    all_sale_places = cache.get('all_sale_places')
    if all_sale_places is None:
        all_sale_places = SalePlace.objects.filter(publish=True)
        cache.set('all_sale_places', all_sale_places)

    sale_places = cache.get('sale_places::%s' % (current_site.domain,))
    if sale_places is None:
        sale_places = SalePlace.on_site.filter(publish=True)
        cache.set('sale_places::%s' % (current_site.domain,), sale_places)

    regions = cache.get('regions')
    if regions is None:
        regions = Region.objects.all().exclude(slug='none')
        cache.set('regions', regions)

    # Form for callback
    callback_form = CallbackForm()

    # Comparsion
    try:
        compare_session = request.session["comparsion"]
        comparsion = Product.on_site.filter(id__in=compare_session)
        comparsion_ids = str(comparsion.values_list('id', flat=True)).strip('[]').replace('L', '').replace(' ', '')
    except Exception, e:
        comparsion = False
        comparsion_ids = None

    # Favorite
    try:
        favorite_session = request.session["favorite"]
        favorite = Product.on_site.filter(id__in=favorite_session)
        favorite_ids = str(favorite.values_list('id', flat=True)).strip('[]').replace('L', '').replace(' ', '')
    except Exception, e:
        favorite = False
        favorite_ids = None

    # Comments likes
    try:
        comment_likes = CommentLike.objects.filter(user=request.user).values_list('comment__id', flat=True)
    except Exception, e:
        comment_likes = False

    return {
        'comparsion': comparsion,
        'comparsion_ids': comparsion_ids,
        'favorite': favorite,
        'favorite_ids': favorite_ids,
        'path': request.path,
        'callback_form': callback_form,
        'sale_places': sale_places,
        'all_sale_places': all_sale_places,
        'comment_likes': comment_likes,
        'regions': regions,
        'dealer_code': DEALER_CODE,
        'freelancer_code': FREELANCER_CODE,
        'request': request,

    }
