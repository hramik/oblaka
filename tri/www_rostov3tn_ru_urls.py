# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

SITE_ID = 38

urlpatterns = patterns('',
                       url(r'^map-1.html$', 'tri.views.site_structure', {'site_id': SITE_ID}, name='site_structure'),

                       url(r'^asimmetrichnyie-akrilovyie-vannyi.html$', 'tri.views.products', {'entity': 'bath', 'cats': ('asimmetrichnie',), 'site_id': SITE_ID},
                           name='asimetric_baths'),
                       url(r'^pryamougolnyie-vannyi.html$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='pryamougolnyie_baths'),
                       url(r'^uglovyie-vannyi.html$', 'tri.views.products', {'entity': 'bath', 'cats': ('uglovie',), 'site_id': SITE_ID}, name='uglovie_baths'),

                       url(r'^mebel-dlya-vannyih-komnat.html$', 'tri.views.products', {'entity': 'mebel', 'site_id': SITE_ID}, name='mebel'),
                       url(r'^smesiteli.html$', 'tri.views.products', {'entity': 'mixer', 'site_id': SITE_ID}, name='mixers'),
                       url(r'^dushevyie-kabinyi.html$', 'tri.views.products', {'entity': 'bathcab', 'site_id': SITE_ID}, name='bathcabs'),

                       url(r'^akrilovie_vanny_s_gidromassagem$', 'tri.views.products', {'entity': 'bath', 'site_id': SITE_ID}, name='baths'),

                       url(r'^common/index.html$', 'tri.views.list', {'slug_in': ('news', 'advices'), 'site_id': SITE_ID}, name='news'),

                       url(r'^questions-rostov$', 'tri.views.questions', name='questions'),



                       )



