# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.contrib import admin
from  django.contrib.contenttypes.admin import GenericTabularInline, GenericStackedInline

from django.db import models

from mptt.admin import MPTTModelAdmin
from sorl.thumbnail.admin import AdminImageMixin
from django.contrib.admin import BooleanFieldListFilter

from django.utils.html import format_html
from django.core.urlresolvers import reverse

from tri.models import *
from slides.models import *
from node.models import *
from ticka.models import Ticka
from products.models import Category, Product, Delivery, Payment, Order, OrderItem, OptionsGroup, Option, OptionPrice
from products.models import ProductComment, ProductType, ProductEntity, CategoryType, Category, ProductCategory, \
    Component, Visor
from custom_sites.models import SiteInfo
from callback.models import Callback
from questions.models import Question
from seo.models import PathSEO
from da_spy.models import *

from menu.models import Menu, MenuGroup
from tri.admin_filters import *
from tri.admin_actions import *

from actions import export_as_csv_action

import pprint
from django.contrib.sessions.models import Session


class SKUInline(GenericTabularInline):
    fields = ("sku", "weight", "provider_price", "price")
    # fields = ("price", "position")
    model = SKU
    extra = 0


class AdditionalFilesInline(admin.TabularInline):
    model = AdditionalFile
    extra = 0
    sortable_field_name = "position"





class AdditionalPhotoInline(AdminImageMixin, admin.TabularInline):
    model = AdditionalPhoto
    extra = 0
    sortable_field_name = "position"


class CommentAdditionalPhotoInline(AdminImageMixin, admin.TabularInline):
    model = CommentAdditionalPhoto
    extra = 0
    sortable_field_name = "position"


class AdditionalVideosInline(AdminImageMixin, admin.TabularInline):
    model = AdditionalVideos
    extra = 0
    sortable_field_name = "position"


class SlideVideoInline(AdminImageMixin, admin.TabularInline):
    model = SlideVideo
    extra = 0
    sortable_field_name = "position"


class WorkTimeInline(admin.TabularInline):
    model = WorkTime
    extra = 0


class CommentInline(admin.TabularInline):
    # fields = ('body',)
    model = Comment
    extra = 0


class VisorInline(admin.TabularInline):
    model = Visor
    extra = 0


class PagePartInline(admin.TabularInline):
    model = PagePart
    extra = 0
    sortable_field_name = "position"


class TagAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug')
    search_fields = ('name', 'slug')


class PagePartAdmin(admin.ModelAdmin):
    pass
    # fields = ('title', 'sites', 'body')
    # list_display = ('title')


class NodeTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug', 'in_subscribe')


class PageAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                ('node_type', 'send_do_subscribe', 'publish'), ('photo', 'show_photo', 'color'), 'sites', 'tags', 'related_nodes', 'related_products',
                ('user', 'author', 'email'))
        }),
        ('Content', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('title', 'second_title'), ('h1', 'h2'), 'teaser', 'body', 'body_clear')
        }),
        ('SEO', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('dot_slug', 'page_title', 'meta_desc', 'meta_keywords', 'noindex')
        }),
        (None, {
            'fields': ('pub_date',)
        }),
    )
    list_display = ('title', 'pub_date', 'teaser', 'dot_slug', 'nid', 'node_type', 'sites_names')
    list_filter = ('tags', 'node_type', 'sites')
    inlines = [PagePartInline, AdditionalPhotoInline, AdditionalFilesInline]
    filter_horizontal = ('sites', 'tags', 'related_nodes')
    search_fields = ('title', 'dot_slug')

    def save_model(self, request, obj, form, change):
        obj.edit_user = request.user.username
        super(PageAdmin, self).save_model(request, obj, form, change)


class SolutionOrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'pub_date', 'node_type', 'sites_names')
    # list_filter = ('tags', 'node_type')
    inlines = [AdditionalPhotoInline, AdditionalFilesInline]
    filter_horizontal = ('sites', 'tags', 'related_nodes')
    list_editable = ('node_type',)
    # search_fields = ('title', 'dot_slug')


class NodeAdmin(admin.ModelAdmin):
    '''
    Simple admin just nodes
    '''
    list_display = ('id', 'nid', 'title', 'teaser', 'dot_slug', 'nid', 'node_type', 'edit_date', 'edit_user', 'dot_slug',)
    list_filter = ('tags', 'node_type')
    filter_horizontal = ('tags',)
    search_fields = ('nid',)

    def save_model(self, request, obj, form, change):
        obj.edit_user = request.user.username
        super(NodeAdmin, self).save_model(request, obj, form, change)


class SEONodeAdmin(admin.ModelAdmin):
    '''
    Admin all nodes for SEO
    '''
    fieldsets = (

        ('Content', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('title', 'second_title'), ('h1', 'h2'), 'teaser', 'body', 'body_clear')
        }),
        ('SEO', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('dot_slug', 'region_dot_slug', 'noindex'), 'page_title', 'meta_desc', 'meta_keywords', ('priority', 'changefreq'))
        }),
        ('Tags', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('tags',)
        }),
        ('User', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('user', 'edit_user'), ('author', 'email'))
        }),
        (None, {
            'fields': (('photo', 'show_photo'), 'pub_date', 'node_type', 'publish', 'last_comment_date', 'sites')
        }),
    )
    list_display = ('id', 'publish', 'title', 'h1', 'page_title', 'slug_with_url', 'is_teaser', 'is_body', 'node_type', 'edit_date',
                    'pub_date',
                    'last_comment_date',
                    'editor',
                    'is_h1', 'is_h2', 'is_page_title', 'is_meta_desc', 'is_meta_keywords', 'is_comments',
                    'is_images_alt', 'is_images_title', 'sites_names')
    list_display_links = ('id',)
    # list_editable = ('title', 'h1', 'page_title')

    list_filter = ('sites', TypeProductFilter, TagsFilter, 'node_type', TeaserFilter, BodyFilter, H1Filter, H2Filter,
                   PageTitleFilter,
                   MetaDescFilter, MetaKeywordsFilter, CommentsFilter, 'edit_user', AltFilter, TitleFilter)
    filter_horizontal = ('tags', 'sites')
    search_fields = ('nid', 'title', 'dot_slug')
    inlines = (AdditionalPhotoInline,)
    ordering = ('-edit_date',)

    def save_model(self, request, obj, form, change):
        obj.edit_user = request.user.username
        super(SEONodeAdmin, self).save_model(request, obj, form, change)


class TickaAdmin(admin.ModelAdmin):
    # fields = ('title', 'sites', 'meta_desc', 'teaser', 'body', 'slug')
    list_display = ('title', 'status')
    # inlines = [CommentInline, AdditionalFilesInline]
    inlines = [CommentInline, AdditionalFilesInline]
    filter_horizontal = ('assign_to', 'tags')
    list_filter = ('status', 'assign_to', 'tags')


class ArticleAdmin(admin.ModelAdmin):
    pass
    inlines = [AdditionalPhotoInline]


class GalleryPhotoInline(admin.TabularInline):
    model = GalleryPhoto
    extra = 10
    sortable_field_name = "position"


class GalleryAdmin(admin.ModelAdmin):
    pass
    # fields = ('lawyer', 'question', 'user_name', 'user_email')
    inlines = [GalleryPhotoInline]


class AdAdmin(admin.ModelAdmin):
    list_display = ('img', 'active', 'type', 'position', 'sites_names')
    list_editable = ('active', 'position', 'type')
    fields = ('img', 'additional_img', 'active', 'message', 'link', 'link_text', 'type', 'position', 'product', 'product_style', 'sites')
    # raw_id_fields = ("product",)
    list_filter = ('active', 'type', 'sites')
    search_fields = ('img',)
    filter_horizontal = ('sites',)
    actions = [activate, deactivate]
    inlines = (SlideVideoInline,)

    def render_change_form(self, request, context, *args, **kwargs):
        context['adminform'].form.fields['product'].queryset = Product.on_site.all()
        return super(AdAdmin, self).render_change_form(request, context, args, kwargs)


class NewsAdmin(admin.ModelAdmin):
    fields = (('title', 'page_title', 'pub_date'), 'photo', 'sites', 'status', 'teaser', 'body', 'dot_slug')
    list_display = ('title', 'pub_date', 'teaser', 'dot_slug')
    # inlines = [AdditionalPhotoInline]
    filter_horizontal = ('sites',)


class CityAdmin(admin.ModelAdmin):
    pass


class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'phone', 'site', 'map_coords', 'map_zoom')


class SalePlacesAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_region', 'city', 'phone', 'email', 'map_coords', 'map_zoom', 'sites_names', 'body', 'position')
    list_filter = ('city', 'sites')
    search_fields = ('title', 'city')
    inlines = (AdditionalPhotoInline, WorkTimeInline)
    filter_horizontal = ('sites',)
    list_editable = ('position',)

    def save_model(self, request, obj, form, change):
        obj.edit_user = request.user.username
        super(SalePlacesAdmin, self).save_model(request, obj, form, change)


class DealerAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_region', 'city', 'phone', 'email', 'sites_names')
    list_filter = ('city', 'sites')
    search_fields = ('title', 'city')
    inlines = [AdditionalPhotoInline]
    filter_horizontal = ('sites',)

    def add_view(self, request, form_url="", extra_context=None):
        data = request.GET.copy()
        data['node_type'] = NodeType.objects.get(slug='dealer')
        request.GET = data
        return super(DealerAdmin, self).add_view(request, form_url="", extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        obj.edit_user = request.user.username
        super(DealerAdmin, self).save_model(request, obj, form, change)


#
# class CategoryAdmin(MPTTModelAdmin):
#     pass


class BathStyleCatAdmin(admin.ModelAdmin):
    pass


class BathFormCatAdmin(admin.ModelAdmin):
    pass


class BathMaterialAdmin(admin.ModelAdmin):
    pass


class ProductTypeAdmin(admin.ModelAdmin):
    pass


class ProductColorAdmin(admin.ModelAdmin):
    pass


class ProductEntityAdmin(admin.ModelAdmin):
    pass


class ProductCaltegoryAdmin(admin.ModelAdmin):
    # pass
    list_display = ('title', 'category_type', 'category_type_second_title', 'dot_slug', 'slug')
    list_filter = ('category_type',)
    ordering = ('category_type',)


class CategoryAdmin(admin.ModelAdmin):
    # pass
    list_display = ('title', 'category_type', 'category_type_second_title', 'dot_slug', 'slug')
    list_filter = ('category_type',)
    ordering = ('category_type',)


class CategoriesInline(admin.StackedInline):
    fields = ('title', 'photo', 'show_photo', 'body', 'slug', 'dot_slug', 'publish', 'position', 'import_date')
    # exclude = ('h1', 'second_title', 'type', 'teaser', 'body_clear', 'tags', 'user')
    model = Category
    extra = 0
    sortable_field_name = "position"
    fk_name = "category_type"


class CategoryTypeAdmin(admin.ModelAdmin):
    list_display = ('title', 'second_title', 'dot_slug')
    inlines = (CategoriesInline,)


class DeliveryAdmin(admin.ModelAdmin):
    filter_horizontal = ('sites',)
    list_display = ('title', 'price', 'sites_names', 'position')
    list_filter = ('sites',)
    list_editable = ('position',)


class PaymentAdmin(admin.ModelAdmin):
    list_display = ('title', 'test', 'publish')
    list_editable = ('test', 'publish')


class PrepayAdmin(admin.ModelAdmin):
    list_display = ('title', 'test', 'publish')
    list_editable = ('test', 'publish')


class ProductCommentAdmin(admin.ModelAdmin):
    search_fields = ('node',)
    list_display = ('node', 'body', 'pub_date', 'publish')
    list_filter = ('publish', 'pub_date')


class ProductCommentInline(admin.TabularInline):
    model = ProductComment
    extra = 0


# class ProductAdmin(admin.ModelAdmin):
# fields = (('title', 'page_title', 'sku'), ('category', 'popular'), ('price'), 'meta_desc', 'teaser', 'body', 'slug')
# list_display = ('title', 'teaser', 'slug')
#    inlines = [AdditionalPhotoInline]


class OptionsInline(admin.TabularInline):
    model = Option
    extra = 0


class OptionsAdmin(admin.ModelAdmin):
    list_display = ("name", 'position', 'slug', 'for_dealer', 'none_price', 'requeries', 'override')
    list_editable = ('position', 'for_dealer', 'none_price', 'requeries', 'override')
    search_fields = ('name',)
    exclude = ('exclude_group',)
    filter_horizontal = ('exclude', 'dependent', 'dependent_group')


class ComplectationAdmin(admin.ModelAdmin):
    filter_horizontal = ('options',)


class OptionPriceAdmin(admin.ModelAdmin):
    # pass
    list_display = ('id', 'position', 'price', 'product', 'option', 'sku_names', 'sites_names')
    search_fields = ('sku', 'product')
    list_filter = (SiteOptionsFilter, 'product__entity', SiteProductsOptionsFilter,)
    fields = ("product", "option", "price", 'position', 'import_id')

    # list_per_page = 10
    inlines = [SKUInline, ]


class OptionPriceInline(admin.TabularInline):
    fields = ("option", "price", "position", 'import_id', 'action', 'discount', 'not_change', 'selflink',)
    # fields = ("price", "position")
    readonly_fields = ('selflink',)
    # list_display_links = ('id', )
    model = OptionPrice
    extra = 0
    sortable_field_name = "position"
    # filter_horizontal = ('sites',)
    show_change_link = True



class OptionsGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    inlines = [OptionsInline]


class ProductAdmin(admin.ModelAdmin):
    # fields = (('title', 'page_title', 'sku'),  ('price'), 'meta_desc', 'teaser', 'body', 'slug', 'length', 'width', 'height', 'depth', 'value')
    fieldsets = (
        (None, {
            'fields': (
                ('nid', 'publish'), 'sites', ('entity', 'type'), ('popular', 'position', 'hydromassage'), ('price', 'overide_sku_price', 'soon'),
                ('action', 'discount', 'not_change'),
                ('photo', 'char_photo', 'base_complectation'), ('tn_alias', 'model_file'),
                ('provider', 'country', 'manufacture', 'seria'), 'import_date')
        }),
        ('Content', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('title', 'second_title'), 'teaser', 'body', 'body_clear')
        }),
        ('SEO', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('dot_slug', 'page_title', 'meta_desc', 'meta_keywords', 'noindex')
        }),
        ('Categories', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('category',)
            # 'fields': ('category', ('style', 'form'), ('material', 'color'))
        }),
        ('Tags', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('tags',)
        }),
        ('Chars', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('length', 'width'), ('height', 'depth'), ('value', 'weight'))
        }),
        ('User', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('user', ('author', 'email'))
        }),
        ('Json', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('json',)
        }),

    )
    list_display = ('title', 'id', 'popular', 'position', 'teaser', 'edit_date', 'editor', 'dot_slug', 'sites_names', 'publish', 'skus_names', 'price', 'dot_slug', 'tn_alias', 'copy')
    list_editable = ('publish', 'popular', 'position')
    # list_display_links = ('title', 'id')
    inlines = [SKUInline, OptionPriceInline, VisorInline, AdditionalPhotoInline, AdditionalVideosInline, AdditionalFilesInline, ProductCommentInline]
    search_fields = ('title', 'id', 'nid')
    list_filter = ('sites', 'entity', 'type', 'category', ModelFilter, FormFilter, 'edit_user', 'popular', 'action')
    filter_horizontal = ('category', 'tags', 'sites')
    ordering = ('-edit_date',)

    # list_per_page = 20
    # list_per_page = 50
    # list_select_related = ('category_type', 'category')

    def add_view(self, request, form_url="", extra_context=None):
        data = request.GET.copy()
        data['node_type'] = NodeType.objects.get(slug='product')
        request.GET = data
        return super(ProductAdmin, self).add_view(request, form_url="", extra_context=extra_context)

    def save_model(self, request, obj, form, change):
        obj.edit_user = request.user.username
        super(ProductAdmin, self).save_model(request, obj, form, change)


class SKUProductAdmin(ProductAdmin):
    # fields = (('title', 'page_title', 'sku'),  ('price'), 'meta_desc', 'teaser', 'body', 'slug', 'length', 'width', 'height', 'depth', 'value')
    fieldsets = ProductAdmin.fieldsets
    list_display = ('title', 'sku',)
    list_editable = ('sku',)
    search_fields = ('title', 'sku')
    # list_select_related = ('category_type', 'category')


class ImportProductAdmin(ProductAdmin):
    # fields = (('title', 'page_title', 'sku'),  ('price'), 'meta_desc', 'teaser', 'body', 'slug', 'length', 'width', 'height', 'depth', 'value')
    fieldsets = ProductAdmin.fieldsets
    list_display = ('import_id', 'import_date', 'dot_slug', 'sku', 'title', 'price', 'sites_names')
    list_editable = ('sku', 'price')
    search_fields = ('title', 'sku')
    list_display_links = ('title',)
    # date_hierarchy = 'import_date'
    list_filter = ('import_id', 'import_date', 'sites', 'entity', 'type', 'category',)

    # list_select_related = ('category_type', 'category')


class ImportOptionPriceAdmin(admin.ModelAdmin):
    # pass
    list_display = ('product', 'import_id', 'import_date', 'sku', 'group', 'option', 'price', 'product', 'sites_names')
    list_editable = ('sku', 'price')
    search_fields = ('sku', 'option')
    # date_hierarchy = 'import_date'
    list_filter = ('import_id', 'import_date', 'product', 'product__entity', 'product__sites', 'import_id')
    ordering = ('product', 'option__position')
    # filter_horizontal = ('sites',)
    list_display_links = ('option', 'product')
    list_per_page = 100
    # list_max_show_all = 1000
    list_select_related = True
    actions = [export_as_csv_action("CSV Export", fields=['product', 'option', 'sku', 'price', 'group'])]


class SiteInfoAdmin(admin.ModelAdmin):
    filter_horizontal = ('admins',)

    # fields = (('title', 'page_title', 'sku'), ('category', 'popular'), ('price'), 'meta_desc', 'teaser', 'body', 'slug')
    # list_display = ('site_name', 'site_domen')
    # inlines = [AdditionalPhotoInline]


class CallbackAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'call_time', 'email', 'comment', 'status', 'pub_date')
    list_filter = ('status',)


class OrderItemsAdmin(admin.ModelAdmin):
    pass
    filter_horizontal = ('options',)
    # raw_id_fields = ("options",)
    list_display = ('id', 'order', 'product', 'options_list', 'price')
    list_filter = ('order',)
    # inlines = [OptionPriceInline]


class SKUAdmin(admin.ModelAdmin):
    list_filter = ('sites',)
    list_display = ('sku', 'sites_names', 'price', 'weight')
    search_fields = ('sku',)
    filter_horizontal = ('sites',)


class OrderItemsInline(admin.TabularInline):
    model = OrderItem
    # filter_horizontal = ('options',)
    readonly_fields = ('id', 'product', 'options', 'order_item_options', 'price')
    # list_display = ('name', 'phone', 'call_time', 'email')
    extra = 0
    # sortable_field_name = "position"


class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'im_id', 'invoice_id', 'pub_date', 'name', 'email', 'address', 'phone', 'delivery', 'pay_type', 'additional_pay_date', 'comment', 'get_order_items', 'status', 'price', 'order_sum_amount',
        'prepay', 'site', 'test',
        'on_site')
    inlines = [OrderItemsInline]
    list_filter = ('site', 'status', 'delivery', 'pay_type')


class MenuGroupAdmin(admin.ModelAdmin):
    pass


class MenuAdmin(MPTTModelAdmin):
    list_display = ('title', 'slug', 'link', 'image', 'position', 'menu_group', 'publish')
    list_filter = ('menu_group', 'level', 'title')
    filter_horizontal = ('sites',)


class MainLinkAdmin(admin.ModelAdmin):
    filter_horizontal = ('sites',)


class QuestionAdmin(admin.ModelAdmin):
    # fields = ('body',)
    list_display = ('id', 'pub_date', 'last_comment_date', 'body', 'user', 'author', 'publish', 'is_answer')
    # inlines = [AdditionalPhotoInline, CommentInline]
    inlines = [AdditionalPhotoInline]
    filter_horizontal = ('tags', 'sites')
    list_filter = ('publish',)
    search_fields = ('title',)

    def is_answer(self, obj):
        is_answer_return = False
        if obj.comment_set.all()[:1]:
            is_answer_return = True
        return is_answer_return

    is_answer.short_description = 'Answer'
    is_answer.boolean = True

    # {% for answer in question.comment_set.all|slice:":1" %}


class CommentsAdmin(admin.ModelAdmin):
    list_display = ('id', 'cid', 'import_id', 'pub_date', 'add_date', 'body', 'user', 'author', 'publish', 'node')
    inlines = [CommentAdditionalPhotoInline]
    search_fields = ('node__id', 'id', 'cid', 'body')
    list_filter = ('import_id',)


class Save3dAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'nid', 'pub_date', 'add_date', 'user', 'publish', 'dot_slug')
    search_fields = ('node__id', 'id', 'nid')
    list_filter = ('import_id',)
    filter_horizontal = ('products',)


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'id', 'uid', 'city', 'date_joined')
    search_fields = ('id', 'uid', 'user__email', 'user__username')
    filter_horizontal = ('subscribtions', 'sites')


class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return pprint.pformat(obj.get_decoded()).replace('\n', '<br>\n')

    _session_data.allow_tags = True
    list_display = ['session_key', '_session_data', 'expire_date']
    readonly_fields = ['_session_data']
    exclude = ['session_data']
    date_hierarchy = 'expire_date'


class PathSEOAdmin(admin.ModelAdmin):
    list_display = ('page_title', 'path', 'slug', 'sites')
    list_filter = ('sites',)
    search_fields = ('slug', 'page_title', 'path')


class LegalAdmin(admin.ModelAdmin):
    list_display = ('title', 'inn',)
    list_filter = ('title',)
    search_fields = ('title',)
    fields = ("title", "inn", 'address',)


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('title', 'company_type', 'company_legal', 'email', 'phone', 'slug')
    list_filter = ('company_type', 'regions')
    search_fields = ('title',)
    fields = ("title", "company_type", 'company_legal', 'address', 'index', 'region', 'country', 'city', 'street',
              'street_abbr', 'house', 'house_korpus', 'office', 'str',
              'contact',
              'phone',
              'email',
              'regions',
              'contacts')
    filter_horizontal = ('regions', 'contacts')


# admin.site.register(News, NewsAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(NodeType, NodeTypeAdmin)
admin.site.register(PagePart, PagePartAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Gallery, GalleryAdmin)
admin.site.register(Slide, AdAdmin)
admin.site.register(Country)
admin.site.register(Region, RegionAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(SalePlace, SalePlacesAdmin)

admin.site.register(Product, ProductAdmin)
admin.site.register(SKUProduct, SKUProductAdmin)
admin.site.register(ImportProduct, ImportProductAdmin)
# admin.site.register(Component)
# admin.site.register(Category, CategoryAdmin)
admin.site.register(ProductType, ProductTypeAdmin)
admin.site.register(ProductEntity, ProductEntityAdmin)
admin.site.register(ProductCategory, ProductCaltegoryAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(CategoryType, CategoryTypeAdmin)
admin.site.register(ProductComment, ProductCommentAdmin)
admin.site.register(OptionsGroup, OptionsGroupAdmin)
admin.site.register(OptionPrice, OptionPriceAdmin)
admin.site.register(Option, OptionsAdmin)
admin.site.register(Complectation, ComplectationAdmin)
admin.site.register(ImportOptionPrice, ImportOptionPriceAdmin)

admin.site.register(MenuGroup, MenuGroupAdmin)
admin.site.register(Menu, MenuAdmin)

admin.site.register(Delivery, DeliveryAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Prepay, PrepayAdmin)
admin.site.register(SiteInfo, SiteInfoAdmin)
admin.site.register(MainLink, MainLinkAdmin)

admin.site.register(Callback, CallbackAdmin)

admin.site.register(Order, OrderAdmin)
admin.site.register(Discount)
admin.site.register(OrderItem, OrderItemsAdmin)

admin.site.register(Tag, TagAdmin)
admin.site.register(Ticka, TickaAdmin)

admin.site.register(Question, QuestionAdmin)
admin.site.register(Comment, CommentsAdmin)

admin.site.register(Node, NodeAdmin)
admin.site.register(SEONode, SEONodeAdmin)

admin.site.register(Test)
admin.site.register(CommentLike)

admin.site.register(NodeSubscribe)

admin.site.register(Save3d, Save3dAdmin)

admin.site.register(UserProfile, UserProfileAdmin)

admin.site.register(PathSEO, PathSEOAdmin)
admin.site.register(BaseComplectation)

admin.site.register(SKU, SKUAdmin)

admin.site.register(Dealer, DealerAdmin)

admin.site.register(Session, SessionAdmin)

admin.site.register(Legal, LegalAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(CompanyType)

admin.site.register(SolutionOrder, SolutionOrderAdmin)
admin.site.register(OrderItemOption)
admin.site.register(OrderLog)

admin.site.register(Log)
