# -*- coding: utf-8 -*-

import os
import re

from django.db import models
from django.db.models import Q, F

import json
import datetime
import random

from itertools import chain
#from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response, redirect
from django.template.loader import render_to_string

from django.contrib.sites.shortcuts import get_current_site
from django.core.files import File
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from ipgeo.models import Range
from django.db.models import Max

from djangular.views.crud import NgCRUDView
from pprint import pprint
import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.http import HttpResponse
from django.template import RequestContext
from django.views.generic import DetailView, TemplateView, ListView
from django.utils.translation import gettext_lazy as _
from django.db.models import Sum

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers

# from django.utils import simplejson
import json
import urllib
import urllib2
from urlparse import urlparse
from django.core.files import File
from django.core.files.base import ContentFile


from array import *
from django.conf import settings

from django.contrib.sites.models import Site

from tri.models import News, Gallery, Page, PagePart, City, Region, SalePlace, MainLink, LoginForm, RegisterForm, Article, UserProfile
from slides.models import Slide
from node.models import Node, Tag, CommentForm, AnonimousCommentForm, AdditionalPhoto, Comment, CommentLike, CommentAdditionalPhoto, NodeType
from products.models import Category, Product, Product, ProductEntity, ProductType, ProductCategory, Component
from products.models import ProductComment, ProductCommentForm, Option, OptionPrice, Order, OrderItem, CategoryType
from custom_sites.models import SiteInfo
from callback.models import Callback, CallbackForm
from menu.models import Menu, MenuGroup
from questions.models import Question, QuestionForm, AnonimousQuestionForm


class TagsCRUDView(NgCRUDView):
    model = Tag

class NodeCRUDView(NgCRUDView):
    model = Node