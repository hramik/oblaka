# -*- coding: utf-8 -*-

__author__ = 'hramik'
from django import template
#from django.template.defaultfilters import simple_tag
import logging
import os
import os.path

import re
from sorl.thumbnail import get_thumbnail
from django import template
from django.template.defaultfilters import stringfilter
from django.template import Context, Template
from oblaka.settings import location
from pytils import numeral, dt


register = template.Library()
# location = lambda x: os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', x)

@register.filter(name='multiply')
def multiply(value, arg):
    return int(value) * int(arg)

@register.simple_tag(takes_context=True)
def render(context, value):
    return Template(value).render(context)

@register.simple_tag
def get_verbose_name(object):
    return object.model._meta.verbose_name

@register.filter(name='zip')
def zip_lists(a, b):
    return zip(a, b)

@register.filter(name='minus')
def minus(count):
    return count-1

@register.filter(name='is_publish')
def is_publish(objects):
    logging.debug(objects)
    return objects.filter(publish=True)

@register.filter(name='format')
def format(value, arg):
    """
    Alters default filter "stringformat" to not add the % at the front,
    so the variable can be placed anywhere in the string.
    """
    try:
        if value:
            return (unicode(arg)) % value
        else:
            return u''
    except (ValueError, TypeError):
        return u''

@register.filter(name='filename')
def filename(value):
    return os.path.basename(value.file.name)

@register.filter(name='strip')
def strip(value):
    text = re.sub("[^a-z0-9\.\,]+","", value, flags=re.IGNORECASE)
    return text

@register.filter(name='hide')
def hide(value):
    phone_text = re.sub("[\d\-\(\)\+]{7,18}","<span class='hide'>телефон</span>", value, flags=re.IGNORECASE)
    text = re.sub("[\w\.\_\-\d]+@[\w\_\-\d]+.[\w\.\_\-\d]+","<span class='hide'>емейл</span>", phone_text, flags=re.IGNORECASE)
    return text

@register.filter(name='by_option_slug')
def by_option_slug(items, slug):
    # print value
    try:
        result = items.get(option__slug = slug)
    except:
        result = False
    return result

@register.filter(name='delivery_sum')
def delivery_sum(delivery_operations):
    result = 0
    for delivery in delivery_operations:
        try:
            result += float(delivery.delivery_price)
        except:
            pass
    return result

@register.filter(name='images_insert')
def images_insert(value, images):
    # search  = args.split(args[0])[1]
    # replace = args.split(args[0])[2]
    for image in images:
        im = get_thumbnail(image.photo, '1120x500', crop='center', quality=80)
        if image.hover_photo:
            hover_im = get_thumbnail(image.hover_photo, '1120x500', crop='center', quality=80)
            value = value.replace("{{ insert }}", '<div class="field_img" style="width: 1120px; height: 500px;" ng-mouseenter="hover=1;" ng-mouseleave="hover=0;" ng-controller="imgCtr"><img src="%s" alt="%s" title="%s" ng-hide="hover" /><img src="%s" ng-show="hover" /><label ng-show="hover">%s</label><label ng-hide="hover">%s</label></div>'% (im.url,image.alt,image.title, hover_im.url, image.hover_title, image.title), 1)
        else:
            value = value.replace("{{ insert }}", '<div class="field_img" style="width: 1120px; height: 500px;"><img src="%s" alt="%s" title="%s"/></div>'% (im.url,image.alt,image.title), 1)

    return value


@register.filter(name='price')
def price(value):
    try:
        return "{:,}".format(int(float(value))).replace(',', ' ')
    except:
        return 0


@register.filter(name='price_with_dime')
def price_with_dime(value):
    try:
        return "{:,}".format(int(float(value))).replace(',', ' ') + ",00"
    except:
        return 0


@register.filter(name='backslash')
def backslash(value):
    try:
        return value.replace('"', '\"')
    except:
        return 0

@register.filter(name='class_value')
def class_value(value):
    try:
        return value.replace('.', '_')
    except:
        return 0

@register.filter(name='option_video_exists')
def option_video_exists(file_path):
    return os.path.isfile(location("media/optionvideos/%s.mp4" % file_path))

@register.filter(name='option_image_exists')
def option_image_exists(file_path):
    return os.path.isfile(location("media/optionvideos/%s.jpg" % file_path))

@register.filter(name='get_delivery_price')
def get_delivery_price(order, delivery):
    return order.get_delivery_price(delivery)

@register.filter
def keyvalue(dict, key):
    return dict[key]

@register.filter
def int_in_word(value):
    return numeral.in_words(int(float(value)))

@register.filter
def order_product(product):
    if product.product:
        return product.product
    else:
        return product

@register.filter
def str_ftime(value):
    return dt.ru_strftime(u"%d %B %Y", value, inflected=True)
