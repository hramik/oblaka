# -*- coding: utf-8 -*-

import os
import re
from django.db import models
from django.db.models import Q, F
import operator
from django.utils import timezone
import pytz
from hashlib import md5
import time as mytime
from datetime import datetime
# from datetime import time
# from datetime import datetime, timedelta
from django.utils.html import strip_tags
from django.core.cache import cache
from django.views.decorators.cache import cache_page
from django.db import transaction
# from xml.etree import ElementTree
from xml.dom import minidom
from bs4 import BeautifulSoup
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.http import Http404
from django.contrib.sites.shortcuts import get_current_site
from django.utils.html import normalize_newlines, linebreaks
import json
import random
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
import copy
from itertools import chain
# from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response, redirect, render
from django.template.loader import render_to_string
from urlparse import urlparse
from django.core.files import File
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from django.db.models import Max
from django.db.models import F
# from ipgeo.models import Range
# from djangular.views.crud import NgCRUDView
from pprint import pprint
import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.template import RequestContext
from django.views.generic import DetailView, TemplateView, ListView
from django.utils.translation import gettext_lazy as _
from django.db.models import Sum
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
import json
import urllib
import urllib2
from urlparse import urlparse
from django.core.files import File
from django.core.files.base import ContentFile
from integrations import *
# REST Framework
from rest_framework import viewsets
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework.response import Response
from tri.serializers import *
from rest_framework.views import APIView
from array import *
from django.conf import settings
from django.contrib.sites.models import Site
from tri.models import *
from slides.models import Slide
from node.models import *
from products.models import *
from products.forms import *
from callback.models import Callback, CallbackForm
from menu.models import Menu, MenuGroup
from questions.models import Question, QuestionForm, AnonimousQuestionForm
from tri import helper
from tri.forms import *
from da_mailer.helper import *
from operator import attrgetter
from tri.constants import *
from triusers.forms import *
from tri.templatetags import hramik_tags
from dbfpy import dbf
from sms_api.models import *
from yandex_money_api.models import *
from validate_email import validate_email

SENDER = 'info@oblaka.com.ua'
RECEPIENTS = ['hramik@gmail.com']
# RECEPIENTS = ['hramik@gmail.com']
BCC = ['hramik@gmail.com']

QUESTIONS_PER_PAGE = 10
NEWS_PER_PAGE = 20
THREE_PER_PAGE = 9

PROJECT_DIR = os.path.dirname(__file__)
location = lambda x: os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', x)

new_sites = ['www.3tn.ru', 'www.moscow3tn.ru', 'www.rostov3tn.ru', 'www.orel3tn.ru', 'www.kavkaz3tn.ru', 'www.ufa3tn.ru',
             'www.orenburg3tn.ru', 'www.saratov3tn.ru', 'www.kazan3tn.ru', 'www.volgograd3tn.ru',
             'www.stavropol3tn.ru', 'www.krasnodar3tn.ru', 'www.samara3tn.ru',
             'www.nnovgorod3tn.ru', 'www.bryansk3tn.ru', 'www.belgorod3tn.ru', 'www.kirov3tn.ru', 'www.izhevsk3tn.ru',
             'www.arhangelsk3tn.ru', 'www.yaroslavl3tn.ru', 'tver3tn.ru', 'belarus.3tn.by', 'www.ryazan3tn.ru',
             'www.piter3tn.ru', 'www.tula3tn.ru', 'www.novosib3tn.ru', 'www.chelyabinsk3tn.ru', 'www.barnaul3tn.ru',
             'www.kemerovo3tn.ru', 'www.krasnoyarsk3tn.ru', 'www.omsk3tn.ru ', 'www.eburg3tn.ru', 'www.perm3tn.ru',
             'www.kurgan3tn.ru', 'www.tyumen3tn.ru', 'www.3tn.by', 'www.triton3tn.ru']

import json


# @csrf_protect
def send_callback(request):
    result = False
    if request.method == 'POST':
        callback_form_json = json.loads(request.body)
        callback_form = CallbackForm(callback_form_json['callback'])

        if callback_form.is_valid():
            callback = callback_form.save(commit=False)
            current_site = SiteInfo.on_site.select_related().get()
            callback.site = current_site.site
            callback.save()

            admins_emails = current_site.get_admins_emails()
            admins_phones = current_site.get_admins_phones()

            product_text = ""

            try:
                if callback_form_json['callback']['product_id'] != '0':
                    cart = Product.objects.filter(id=callback_form_json['callback']['product_id'])

                    for product in cart:
                        product_text += product.title
                        try:
                            options_session = [int(s) for s in
                                               request.session[str(product.id) + "_options"]]
                            if options_session:
                                product_text += " c опциями: "
                                for option_id in options_session:
                                    option = OptionPrice.objects.select_related().get(id=option_id)
                                    product_text += "%s, " % option.option.name

                        except Exception, e:
                            options_session = False

                    callback.comment += product_text
                    callback.save()

            except Exception, e:
                print Exception, e

            if not callback.name == "TEST":
                DaMailer.send_mail_to_addresses(template='oblaka/email/callback.email.html',
                                                subject=u'Заявка на обратный звонок c %s %s' % (current_site.site.domain, product_text), data=callback,
                                                emails=admins_emails)

                Sms_Api.send_message_to_phone_number(phone_list=admins_phones,
                                                     message="New callback order to %s %s. %s" % (callback.phone, product_text, current_site.site.domain))
                #     CALLTOUCH
                try:
                    if callback_form_json['product_id'] == '0':
                        calltouchEvent(callback, callback_form_json['call_value'], u'Оформил заказ Обратного Звонока')
                    else:
                        calltouchEvent(callback, callback_form_json['call_value'], u'Оформил заказ Консультации')
                except Exception, e:
                    calltouchEvent(callback, callback_form_json['call_value'], u'Оформил заказ Обратного Звонока')
                    print Exception, e

            result = True

    return HttpResponse(result)


##  @csrf_protect
def change_callback_status(request):
    result = False
    response_data = {}

    if request.method == 'POST':
        try:
            callback_json = json.loads(request.body)

            callback = Callback.objects.select_related().get(id=callback_json['callback_id'])
            callback.status = callback_json['status']
            callback.manager_comment = callback_json['manager_comment']

            if callback.call_date == None:
                callback.call_date = datetime.now()

            callback.save()
        except Exception, e:
            print Exception, e

        result = callback.status

    response_data['result'] = result

    # return HttpResponse(json.dumps(response_data), content_type="application/json")
    return HttpResponse(result)


# @csrf_protect
def send_comment(request):
    result = False

    if request.method == 'POST':
        comment_form = CommentForm(json.loads(request.body))
        if comment_form.is_valid():
            print 'valid'
            comment_j = json.loads(request.body)
            c_node = Node.objects.get(id=comment_j["node"])
            c_form = comment_form.save(commit=False)
            c_form.node = c_node
            if request.user.is_authenticated():
                c_form.user = request.user
                c_form.publish = True
            else:
                c_form.publish = False
            c_form.save()

            # c_node.publish = True
            # c_node.last_comment_date = now()
            # c_node.save()

            for photo_url in comment_j["additionalphoto"]:
                print photo_url
                photo = CommentAdditionalPhoto(comment=c_form,
                                               photo=photo_url.replace('/media/', ''))
                c_form.commentadditionalphoto_set.add(photo)

                # mail = DaMailer.send_notification_mail('new_comment', data=c_form)

    if request.user.is_authenticated():
        return render_to_response('questions/parts/comment_base.html', {
            'comment': c_form,
            'class': 'comment',
        },
                                  context_instance=RequestContext(request))
    else:
        return HttpResponse("anonym")


# @csrf_protect
def send_product_comment(request):
    result = False

    if request.method == 'POST':
        print 'chuck'
        comment_form = ProductCommentForm(json.loads(request.body))
        if comment_form.is_valid():
            print 'valid'
            comment_j = json.loads(request.body)
            c_node = Product.objects.get(id=comment_j["node"])
            c_form = comment_form.save(commit=False)
            c_form.node = c_node
            c_form.user = request.user
            c_form.save()
            result = True
            client_mail_body = get_template('triton_ua/email/comment.email.html').render(
                Context({
                    'form': comment_form.cleaned_data,
                    'product': c_node,
                    'comment': c_form,
                })
            )
            subject = _('New comment')
            mail = EmailMultiAlternatives(
                subject,
                client_mail_body,
                SENDER,
                RECEPIENTS,
                BCC
            )
            mail.attach_alternative(client_mail_body, "text/html")
            # mail.send()
            result = True

    if request.user.is_authenticated():
        return render_to_response(
            'triton_ua/blocks/products_block/parts/product_comment_base.html', {
                'comment': c_form,
                # 'class': 'comment',
            },
            context_instance=RequestContext(request))
    else:
        return HttpResponse("anonym")


def send_order(request):
    result = False

    cart = []
    response_data = {}
    cart_session = []
    options_sessions = []
    cart_sum_end = ""
    product_text = ""
    order_item = None
    discount_cart_sum = 0

    if request.method == 'POST':
        print request.body
        order_form_body = json.loads(request.body)
        order_form = OrderForm(order_form_body['order'])
        order_form_json = order_form_body['order']

        if order_form.is_valid():

            try:
                if order_form_json['product_id'] != '0':
                    cart = Product.objects.filter(id=order_form_json['product_id'])
                    # price = cart[:1].get().price
                    cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum = get_price(
                        request, (order_form_json['product_id'],))

                else:
                    # cart_session = request.session["cart"]
                    # cart = Product.objects.filter(id__in=cart_session)
                    cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum = get_price(
                        request)

                order = order_form.save()

                order_user = User.objects.filter(email=order.email)
                if order_user.count() > 0:
                    user = order_user[:1].select_related().get()
                else:
                    user = User(username=order.email, email=order.email)
                    user.save()

                try:
                    order.ip = request.META['REMOTE_ADDR']
                except Exception, e:
                    pass

                order.user = user
                order.price = discount_cart_sum
                order.discount = discount
                order.cart_sum_end = cart_sum_end
                order.discount_value = discount_value
                order.site = SiteInfo.on_site.select_related().get().site

                order.comment = "Связаться с получателем за 2 часа \r\n%s" % order.comment

                order.save()

                data = order_form.clean()

                for product in cart:
                    order_item = OrderItem(order=order, product=product, price=product.price)
                    order_item.save()
                    product_text += product.title
                    try:
                        options_session = request.session[str(product.id) + "_options"]
                        options_price = 0
                        product_text += " c опциями: "
                        for option_price in options_session:
                            order_item_option_price = OptionPrice.objects.select_related().get(id=option_price)
                            order_item.options.add(order_item_option_price)
                            # options_price += int(order_item_option_price.price)

                            order_item_option = OrderItemOption(option=order_item_option_price.option, price=order_item_option_price.price)
                            order_item_option.save()
                            order_item.order_item_options.add(order_item_option)

                            product_text += "%s, " % order_item_option_price.option.name

                        # order_item.price = int(order_item.price) + int(options_price)
                        order_item.price = product.price
                        # order.price = product.price
                        order_item.save()

                        request.session[str(product.id) + "_options"] = []

                    except Exception, e:
                        options_session = False

                # if order_form_json['prepay'] == 'prepay_30':
                #     order.prepay = "%s = %s" % (order.prepay, str(int(order.price * 0.3)))

                # SEND NOTIFICATIONS
                if 'TEST' in order.comment:
                    adjective = u'TEST'
                    order.test = True
                else:
                    adjective = ''

                order.hash = User.objects.make_random_password(length=20)

                order.save()

                admins_emails = SiteInfo.on_site.select_related().get().get_admins_emails()
                admins_phones = SiteInfo.on_site.select_related().get().get_admins_phones()

                if not 'TEST' in order.comment:
                    DaMailer.send_mail_to_addresses(template='products/email/order.email.html',
                                                    subject=u'Новый %sзаказ на %s' % (
                                                        adjective, order.site.name), data=order,
                                                    emails=admins_emails)
                    DaMailer.send_mail_to_address(template='products/email/customer_order.email.html',
                                                  subject=u'Ваш %sзаказ на %s' % (
                                                      adjective, order.site.name), data=order,
                                                  email=order.email)

                    Sms_Api.send_message_to_phone_number(phone_list=admins_phones,
                                                         message="New %sorder on %s to %s" % (
                                                             adjective,
                                                             # SiteInfo.on_site.select_related().get().site.domain,
                                                             order.site.name,
                                                             product_text))

                    #     CALLTOUCH
                    if not order_form_json['product_id'] == '0':
                        calltouchEvent(order, order_form_body['call_value'], u'Оформил Покупку в 1 клик')
                    else:
                        calltouchEvent(order, order_form_body['call_value'], u'Оформил Заказ')

                result = True

                if order_form_json['product_id'] == '0':
                    request.session["cart"] = []

                    # # SEND TO YANDEX
                    # if len(order.pay_type.slug) == 2:
                    #     result = 'yandex'
                    #     response_data['order_number'] = order.id
                    #     response_data['payment_type'] = order.pay_type.slug

                    # return redirect('https://demomoney.yandex.ru/eshop.xml', permanent=True)

            except Exception, e:
                print "ORDER_ERR", sys.exc_traceback.tb_lineno, Exception, e
                cart_session = []
                cart = False

    response_data['result'] = result
    response_data['customer_number'] = request.user.id

    return HttpResponse(json.dumps(response_data), content_type="application/json")


def save_order(request):
    result = False

    cart = []
    response_data = {}
    cart_session = []
    options_sessions = []
    cart_sum_end = ""
    product_text = ""
    order_item = None
    discount_cart_sum = 0

    try:
        if request.method == 'POST':
            pay_form = PayForm(json.loads(request.body))
            pay_form_json = json.loads(request.body)
            # print pay_form_json

            order = Order.objects.select_related().get(id=pay_form_json['order_id'])

            try:
                delivery = Delivery.objects.select_related().get(slug=pay_form_json['pay_form']['delivery'])
                order.delivery = delivery
                order.delivery_sum = order.delivery.price
            except Exception, e:
                print Exception, e

            try:
                order.custom_delivery_sum = pay_form_json['pay_form']['custom_delivery_sum']
            except Exception, e:
                print Exception, e

            try:
                order.pay_type = Payment.objects.select_related().get(slug=pay_form_json['pay_form']['pay_type'])
            except Exception, e:
                print Exception, e

            try:
                order.prepay = Prepay.objects.select_related().get(slug=pay_form_json['pay_form']['prepay'])
            except Exception, e:
                print Exception, e

            try:
                order.carrier = Company.objects.select_related().get(slug=pay_form_json['pay_form']['carrier'])
            except Exception, e:
                print Exception, e

            try:
                order.distributor = Company.objects.select_related().get(
                    slug=pay_form_json['pay_form']['distributor'])
            except Exception, e:
                print Exception, e

            order.carrier_number = pay_form_json['pay_form']['carrier_number']
            order.need_cargo_service = pay_form_json['pay_form']['need_cargo_service']

            try:
                order.delivery_date = datetime.strptime(pay_form_json['pay_form']['delivery_date'], '%d.%m.%Y').strftime('%Y-%m-%d')
            except Exception, e:
                print Exception, e

            try:
                order.delivery_pickup_time_period = pay_form_json['pay_form']['delivery_pickup_time_period']
            except Exception, e:
                print Exception, e

            try:
                order.name = pay_form_json['order_form']['name']
            except Exception, e:
                print Exception, e

            try:
                order.address = pay_form_json['order_form']['address']
            except Exception, e:
                print Exception, e

            try:
                order.version = pay_form_json['pay_form']['version']
            except Exception, e:
                print Exception, e

            try:
                order.country = pay_form_json['order_form']['country']
            except Exception, e:
                print Exception, e

            try:
                order.index = pay_form_json['order_form']['index']
            except Exception, e:
                print Exception, e

            print order.index

            try:
                order.region = pay_form_json['order_form']['region']
            except Exception, e:
                print Exception, e

            try:
                order.city = pay_form_json['order_form']['city']
            except Exception, e:
                print Exception, e

            try:
                order.street_abbr = pay_form_json['order_form']['street_abbr']
            except Exception, e:
                print Exception, e

            try:
                order.street = pay_form_json['order_form']['street']
            except Exception, e:
                print Exception, e

            try:
                order.house = pay_form_json['order_form']['house']
            except Exception, e:
                print Exception, e

            try:
                order.phone = pay_form_json['order_form']['phone']
            except Exception, e:
                print Exception, e

            try:
                order.email = pay_form_json['order_form']['email']
            except Exception, e:
                print Exception, e

            try:
                order.comment = pay_form_json['order_form']['comment']
            except Exception, e:
                print Exception, e

            try:
                order.destination = pay_form_json['order_form']['destination']
            except Exception, e:
                print Exception, e

            if order.status == 'NO' and request.user.is_staff:
                order.status = "SA"

            order.save()
            # print pay_form_json['delivery']

            result = True
    except Exception, e:
        result = "%s %s" % (Exception, e)

    response_data['result'] = result

    return HttpResponse(json.dumps(response_data), content_type="application/json")


def change_status(request):
    result = False
    response_data = {}

    if request.method == 'POST':
        pay_form_json = json.loads(request.body)
        print pay_form_json

        order = Order.objects.select_related().get(id=pay_form_json['order_id'])
        order.status = pay_form_json['status']

        order.save()

        result = True

    response_data['result'] = result

    return HttpResponse(json.dumps(response_data), content_type="application/json")


def send_quick_order_form(request):
    result = False

    cart = []
    cart_session = []
    cart_sum_end = ""

    if request.method == 'POST':
        quick_order_form = OrderForm(json.loads(request.body))
        if quick_order_form.is_valid():
            qo = quick_order_form.save()
            qo_j = json.loads(request.body)
            # quick order
            try:
                c_node = Product.objects.select_related().get(id=qo_j["product"])
                # c_node = Product.objects.get(id=qo_j["product"])
                order_item = OrderItem(order=qo, product=c_node)
                order_item.save()
                result = True
                email_template = "quick_order"
                subject = _('New quick order')
                cart = False
            # usual order
            except Exception, e:
                try:
                    cart_session = request.session["cart"]
                    cart = Product.objects.filter(id__in=cart_session)
                    options_sessions = []
                    cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum = get_price(
                        request)
                    for product in cart:
                        order_item = OrderItem(order=qo, product=product)
                        order_item.save()
                        try:
                            options_session = [int(s) for s in
                                               request.session[str(product.id) + "_options"]]
                            for option_price in options_session:
                                order_item_option_price = OptionPrice.objects.select_related().get(id=option_price)
                                order_item.options.add(order_item_option_price)
                        except Exception, e:
                            options_session = False
                except Exception, e:
                    cart_session = []
                    cart = False
                c_node = False
                email_template = "order"
                subject = _('New order')
                result = True

            client_mail_body = get_template(
                'products/email/' + email_template + '.email.html').render(
                Context({
                    'form': quick_order_form.cleaned_data,
                    'product': c_node,
                    'cart': cart,
                    'order': qo,
                    'cart_sum_end': cart_sum_end,
                    # 'options_sessions': options_sessions,
                })
            )
            mail = EmailMultiAlternatives(
                subject,
                client_mail_body,
                SENDER,
                RECEPIENTS,
                BCC
            )
            mail.attach_alternative(client_mail_body, "text/html")
            mail.send()
            result = True

    return HttpResponse(result)


def set_city(request, city_slug):
    # print city_slug
    current_city = City.objects.select_related().get(slug=city_slug)
    request.session["city"] = current_city
    return HttpResponse(city_slug)


def saleplaces_menu(request, region_id):
    saleplaces = SalePlace.objects.filter(city__region__id=region_id, publish=True)
    # return HttpResponse(saleplaces)
    # data = serializers.serialize('json', saleplaces)
    jsonstring = render_to_string('triton_ua/json/saleplaces_menu.html', {
        'saleplaces': saleplaces,
    },
                                  context_instance=RequestContext(request))
    return HttpResponse(jsonstring, mimetype='application/json')


# return HttpResponse(data, mimetype='application/json')
# return render_to_response('oblaka/blocks/saleplaces_menu.html', {
# 'saleplaces': saleplaces,
# },
# context_instance=RequestContext(request, processors=[]))


class NewsDetailView(DetailView):
    model = News


import random


def get_option(request, slug, ajax=False):
    try:
        option = Option.objects.select_related().get(slug=slug)
    except:
        option = OptionsGroup.objects.select_related().get(slug=slug)

    if ajax:
        template_name = 'products/blocks/clear_option.html'
    else:
        template_name = 'products/option.html'

    random_num = random.randint(0, 1000)
    print random_num

    return render_to_response(template_name, {
        'option': option,
        'random': random_num,
        'ajax': ajax,
    },
                              context_instance=RequestContext(request))


def base_complectation(request, id):
    product = Product.objects.select_related().get(id=id)

    random_num = random.randint(0, 1000)

    option = product.base_complectation
    option.name = u"Базовая комплектация"

    return render_to_response('products/blocks/clear_option.html', {
        'option': option,
        'random': random_num,
    },
                              context_instance=RequestContext(request))


# @transaction.non_atomic_requests
def node(request, slug, entity=None, new=None, options=None):
    kwargs = {}
    current_site = SiteInfo.on_site.get()
    # current_site = current_site_info

    # return_page = cache.get('page::%s::%s::mobile_%s::is_shop_%s' % (current_site.site.domain, slug, request.user_agent.is_mobile, current_site.is_shop))
    # if return_page and not request.user.is_staff:
    #     return return_page

    try:
        redirect_node = SEONode.on_site.filter(region_dot_slug=slug)[:1].get()
        return redirect('/%s' % redirect_node.dot_slug, permanent=True)
    except Exception, e:
        print Exception, e

    kwargs['dot_slug'] = slug

    if not request.user.is_staff:
        kwargs['publish'] = True

    try:
        page = SEONode.on_site.filter(**kwargs)[:1].get()
        try:
            pageparts = cache.get('pageparts::%s::%s::is_shop_%s' % (current_site.site.domain, slug, current_site.is_shop))
            if pageparts is None:
                pageparts = PagePart.on_site.filter(page=page)
                cache.set('pageparts::%s::%s::is_shop_%s' % (current_site.site.domain, slug, current_site.is_shop), pageparts)
        except:
            pageparts = None

        if page.node_type.slug == 'save_3d':
            page = Save3d.objects.filter(dot_slug=slug)[:1].get()

        if page.node_type.slug in ('advices', 'article', 'news', 'action', 'solution',
                                   'solution_order',) and current_site.site.domain == 'www.triton3tn.ru':
            return magazine(request, slug)

        if page.node_type.slug in ('advices', 'article', 'news', 'action', 'solution',
                                   'solution_order',) and not current_site.site.domain == 'www.triton3tn.ru':
            try:
                request.breadcrumbs([
                    (_('%s') % ('Новости и Акции'), current_site.json['front']['news']['link']),
                ])
            except:
                pass

        if page.node_type.slug in ('solution_order',):
            return solution_order(request, slug)

        if page.node_type.slug != 'product':
            return_page = render_to_response('triton_ua/pages/page.html', {
                'single_page': True,
                'page': page,
                'page_title': page.page_title if page.page_title else page.title,
                'meta_keywords': page.meta_keywords,
                'meta_desc': page.meta_desc,
                'pageparts': pageparts,
            },
                                             context_instance=RequestContext(request))

            if not request.user.is_staff:
                cache.set('page::%s::%s::mobile_%s::is_shop_%s' % (current_site.site.domain, slug, request.user_agent.is_mobile, current_site.is_shop), return_page)
            return return_page
    except Exception, e:
        pass

    product = False
    comment_form = CommentForm()

    # Product with cache
    product = cache.get('product::%s::%s' % (current_site.site.domain, slug))
    if product is None:
        try:
            product = Product.on_site.get(**kwargs)
            cache.set('product::%s::%s' % (current_site.site.domain, slug), product)
        except(Exception, e):
            raise Http404

    # Comments with cache
    comments = cache.get('comments::%s::%s' % (current_site.site.domain, slug))
    if comments is None:
        comments = Comment.objects.filter(node=product, publish=True)
        cache.set('comments::%s::%s' % (current_site.site.domain, slug), comments)

    # Product options with cache
    try:
        product_options = cache.get('product_options::%s::%s' % (current_site.site.domain, slug))
        print 'from cache'
        if product_options is None:
            product_options = OptionPrice.objects.filter(product=product)
            cache.set('product_options::%s::%s' % (current_site.site.domain, slug), product_options)
    except Exception, e:
        product_options = False

    # Same size with cache
    if product.entity.slug == 'mebel':
        same_size_products = cache.get('same_size_products_mebel::%s::is_shop_%s' % (slug, current_site.is_shop))
        if same_size_products is None:
            same_size_products = Product.on_site.filter(~Q(id=product.id), entity=product.entity, publish=True,
                                                        price__gt="0",
                                                        width__lte=(int(product.width) + 100),
                                                        width__gte=(int(product.width) - 100))[:10]
            cache.set('same_size_products_mebel::%s::is_shop_%s' % (slug, current_site.is_shop), same_size_products)
    elif product.entity.slug in ['bath', 'bathcab']:
        same_size_products = cache.get('same_size_products_bath::%s::is_shop_%s' % (slug, current_site.is_shop))
        if same_size_products is None:
            same_size_products = Product.on_site.filter(~Q(id=product.id), entity=product.entity, publish=True,
                                                        price__gt="0",
                                                        length__lte=(int(product.length) + 100),
                                                        length__gte=(int(product.length) - 100))[:10]
            cache.set('same_size_products_bath::%s::is_shop_%s' % (slug, current_site.is_shop), same_size_products)
    else:
        same_size_products = None

    # Same category with cache
    if product.entity.slug == 'mebel':
        same_category_products = cache.get('same_category_products_mebel::%s::%s::is_shop_%s' % (current_site.site.domain, slug, current_site.is_shop))
        if same_category_products is None:
            same_category_products = Product.on_site.filter(~Q(id=product.id), entity=product.entity, publish=True,
                                                            price__gt="0",
                                                            category__in=product.category.all,
                                                            category__category_type__slug='type')[:10]
            cache.set('same_category_products_mebel::%s::%s::is_shop_%s' % (current_site.site.domain, slug, current_site.is_shop), same_category_products)

    else:
        same_category_products = cache.get('same_category_products::%s::%s::is_shop_%s' % (current_site.site.domain, slug, current_site.is_shop))
        if same_category_products is None:
            same_category_products = Product.on_site.filter(~Q(id=product.id), entity=product.entity, publish=True,
                                                            price__gt="0",
                                                            category__in=product.category.all,
                                                            category__category_type__slug='style')[:10]
            cache.set('same_category_products::%s::%s::is_shop_%s' % (current_site.site.domain, slug, current_site.is_shop), same_category_products)

    try:
        product_history = request.session["product_history"]

        if (product.id in product_history):
            product_history.remove(product.id)

        product_history.append(product.id)

        if (len(product_history) > 20):
            product_history.pop(0)

        request.session["product_history"] = product_history

    except Exception, e:
        print e
        request.session["product_history"] = [product.id]

    if product.action:
        request.breadcrumbs('Акции', '/action')

    else:
        if product.entity.slug == 'mebel':
            request.breadcrumbs(_('%s') % ('Продукция'), Menu.on_site.select_related().get(slug="production").link)

            request.breadcrumbs(_('%s') % (product.entity.page_title),
                                Menu.on_site.select_related().get(slug=product.entity.slug).link)
        else:
            request.breadcrumbs(_('%s') % ('Продукция'), Menu.on_site.select_related().get(slug="production").link)

            try:
                request.breadcrumbs(_('%s') % (product.entity.page_title),
                                Menu.on_site.select_related().get(slug="%ss" % product.entity.slug).link)
            except:
                pass

    if product.action:
        try:
            options_session_name = "%d_options" % product.id

            options = []
            for option in product.optionprices.filter(action=True):
                if (not option.id in options):
                    options.append(option.id)
            request.session[options_session_name] = options
        except Exception, e:
            print Exception, e

    page_title = product.page_title if product.page_title else product.title

    # Active tab by default
    part = None
    if 'part' in request.GET:
        part = request.GET['part']

    options_get = None
    if 'options' in request.GET:
        if request.GET['options'] == 'clear':
            clear_all_option_from_session(request, str(product.id))
        else:
            try:
                complectation = Complectation.objects.get(slug=request.GET['options'])
                options_slugs = complectation.options.values_list('slug', flat=True)
            except:
                options_slugs = request.GET['options'].split('-')

            options_get = OptionPrice.objects.filter(product=product, option__slug__in=options_slugs).values_list('id', flat=True)
            clear_all_option_from_session(request, str(product.id))
            request.session[str(product.id) + "_options"] = options_get
            page_title = "%s | %s | %s" % (product.title, product.entity.title, (", ").join(Option.objects.filter(slug__in=request.GET['options'].split('-')).values_list('name', flat=True)))

    # product_solutions = cache.get('last_news_list::%s::%s' % (current_site.domain, product.id))
    # if product_solutions is None:
    #     product_solutions = Page.on_site.filter(node_type__slug__in=['news', 'advices', 'article'], publish=True).order_by('-pub_date')[:3]
    #     cache.set('last_news_list::%s::%s' % (current_site.domain, product.id), product_solutions)

    product_solutions = Page.objects.filter(node_type__slug__in=['news', 'advices', 'article'], publish=True, related_products__regex=r'(,|^)' + str(product.nid) + '(,|$)').order_by('-pub_date')

    product_actions = Product.objects.filter(nid=product.nid, action=True, publish=True, sites__in=product.sites.all())
    if product_actions:
        for product_action in product_actions:
            options_session_name = "%d_options" % product_action.id

            product_action_options = []
            for product_action_option in product_action.optionprices.filter(action=True):
                if (not product_action_option.id in product_action_options):
                    product_action_options.append(product_action_option.id)
            request.session[options_session_name] = product_action_options
            product_action.cart_session, product_action.cart, product_action.cart_all_sum_end, product_action.discount, product_action.discount_value, product_action.discount_cart_sum = get_price(
                request, (product_action.id,))

    try:
        options_session = [int(s) for s in request.session[str(product.id) + "_options"]]
    except Exception, e:
        options_session = False

    complectations = Complectation.objects.all()

    visor_frames = {}
    try:
        for visor_set in product.visor_set.all():
            visor_dir = os.path.join(settings.MEDIA_ROOT, 'visor', '%s' % visor_set)
            # path, dirs, files = os.walk(visor_dir).next()
            files_list = os.listdir(visor_dir)
            visor_files_count = len(files_list)
            visor_frames[visor_set] = visor_files_count
    except Exception, e:
        print 'visor', Exception, e


    return_page = render_to_response('products/detail.html', {
        'single_page': True,
        'product': product,
        'product_solutions': product_solutions,
        'node': product,
        'page_title': page_title,
        'meta_keywords': product.meta_keywords,
        'meta_desc': product.meta_desc,
        'comments': comments,
        'comment_form': comment_form,
        'product_options': product_options,
        'options_session': options_session,
        'options_get': options_get,
        'same_size_products': same_size_products,
        'same_category_products': same_category_products,
        'visor_path': location("media/visor/%s" % product.nid),
        'visor_frames': visor_frames,
        'part': part,
        'product_history': get_history(request),
        'complectations': complectations,
        'product_actions': product_actions,
    },
                                     context_instance=RequestContext(request))

    if not request.user.is_staff:
        cache.set('page::%s::%s::mobile_%s::is_shop_%s' % (current_site.site.domain, slug, request.user_agent.is_mobile, current_site.is_shop), return_page)


    return return_page


# GET PRODUCT OPTIONS
def product_options(request, id):
    try:
        product = Product.objects.select_related().get(id=id)
        product_options = OptionPrice.objects.filter(product=product)
        try:
            options_session = [int(s) for s in request.session[str(id) + "_options"]]
        except Exception, e:
            options_session = False
    except Exception, e:
        product_options = False

    complectations = Complectation.objects.all()

    return render_to_response('products/blocks/options.html', {
        'product_options': product_options,
        'options_session': options_session,
        'complectations': complectations,
        'product': product,

    },
                              context_instance=RequestContext(request))


# GET PRODUCT OPTIONS FOR OREDR
def product_order_options(request, order_id, id):
    product = None
    order_item = None
    order = None

    try:
        order_item = OrderItem.objects.select_related().get(order=order_id, product_id=id)
        order = order_item.order

        product = order_item.product
        product_options = OptionPrice.objects.filter(product=product)

        # try:
        #     options_session = [int(s) for s in request.session[str(id) + "_options"]]
        # except Exception, e:
        #     options_session = False
    except Exception, e:
        product_options = False

    complectations = Complectation.objects.all()

    return render_to_response('products/blocks/options.html', {
        'product_options': product_options,
        'options_session': order_item.options_ids(),
        'complectations': complectations,
        'product': product,
        'order': order,

    },
                              context_instance=RequestContext(request))


def for_tema(request):
    products = Product.on_site.all()
    output = ""

    for product in products:
        output += "%s;%s<br>" % (product.nid, product.title)

    return HttpResponse(output)


def cart(request):
    cart = None
    is_hydromassage = 'false'
    try:
        cart_session = request.session["cart"]
        cart = Product.on_site.filter(id__in=cart_session)

        for cart_item in cart:
            cart_item.options = []
            if cart_item.hydromassage:
                is_hydromassage = 'true'
            try:
                options_session = request.session[str(cart_item.id) + "_options"]
                cart_item.options = OptionPrice.objects.filter(id__in=options_session,
                                                               product__id__in=cart_session)
            except Exception, e:
                print Exception, e

    except Exception, e:
        print Exception, e

    return render_to_response('products/cart.html', {
        'cart_with_options': cart,
        'is_hydromassage': is_hydromassage,
        'YANDEX_MONEY_SHOP_ID': settings.YANDEX_MONEY_SHOP_ID,
        'YANDEX_MONEY_SCID': settings.YANDEX_MONEY_SCID,
    },
                              context_instance=RequestContext(request))


@staff_member_required
def backoffice(request, active_tab='orders'):
    kwargs = {}
    sum = {}
    ORDER_PAGE_COUNT = 20
    site = ""
    status = ""

    user = request.user
    user_profile = user.userprofile

    request.GET = request.GET.copy()

    if user_profile.sites:
        kwargs['site__in'] = user_profile.sites.all()

    if 'status' in request.GET and not request.GET['status'] == '':
        kwargs['status__in'] = request.GET.getlist('status')
        status = request.GET.getlist('status')

    if 'site' in request.GET and not request.GET['site'] == '':
        kwargs['site__domain__in'] = request.GET.getlist('site')
        site = request.GET['site']

    if 'id' in request.GET and not request.GET['id'] == '':
        kwargs['id'] = request.GET['id']
        id = request.GET['id']

    if 'start_data' in request.GET and not request.GET['start_data'] == '':
        kwargs['pub_date__gt'] = request.GET['start_data']
    else:
        start_data = (datetime.today() - timedelta(days=30)).strftime("%Y-%m-%d")
        request.GET.update({"start_data": start_data})

    if 'end_data' in request.GET and not request.GET['end_data'] == '':
        kwargs['pub_date__lt'] = request.GET['end_data']
        end_data = request.GET['end_data']
    else:
        end_data = datetime.today().strftime("%Y-%m-%d")
        request.GET.update({"end_data": end_data})

    filter_form = OrdersFilterForm(request.GET)

    start, end = helper.get_start_end_pages(ORDER_PAGE_COUNT, request)

    callbacks = None
    orders = None

    if not request.GET.get('page'):
        orders = Order.objects.filter(**kwargs).order_by('-id')[start:end]
        callbacks = Callback.objects.filter(**kwargs).order_by('-id')[start:end]
        sum = Order.objects.filter(**kwargs).aggregate(Sum('cart_sum_end'), Sum('price'), Sum('delivery_sum'), Sum('order_sum_amount'), Sum('additional_order_sum_amount'), Sum('bonus'))

    if active_tab == 'orders' and request.GET.get('page'):
        orders = Order.objects.filter(**kwargs).order_by('-id')[start:end]

    if active_tab == 'callbacks' and request.GET.get('page'):
        callbacks = Callback.objects.filter(**kwargs).order_by('-id')[start:end]

    if request.GET.get('page'):
        page = int(request.GET.get('page'))
    else:
        page = 1

    # Choose response url for page or ajax callback
    if request.GET.get('page'):
        response_url = 'products/backoffice/%s_list.html' % active_tab
    else:
        response_url = 'products/backoffice/backoffice.html'

    return render_to_response(response_url, {
        'active_tab': active_tab,
        'callbacks': callbacks,
        'orders': orders,
        'filter_form': filter_form,
        # 'order_count': orders.count(),
        'page': page,
        'status': status,
        'site': site,
        'sum': sum,
        # 'pages_count': range(1, orders.count() / ORDER_PAGE_COUNT),
    },
                              context_instance=RequestContext(request))


@staff_member_required
def callbacks(request):
    kwargs = {}
    ORDER_PAGE_COUNT = 20
    site = ""
    status = ""

    user = request.user
    user_profile = user.userprofile

    if user_profile.sites:
        kwargs['site__in'] = user_profile.sites.all()

    if 'status' in request.GET and not request.GET['status'] == '':
        kwargs['status'] = request.GET['status']
        status = request.GET['status']

    if 'site' in request.GET and not request.GET['site'] == '':
        kwargs['site__domain'] = request.GET['site']
        site = request.GET['site']

    filter_form = OrdersFilterForm(request.POST)

    start, end = helper.get_start_end_pages(ORDER_PAGE_COUNT, request)
    callbacks = Callback.objects.filter(**kwargs).order_by('-id')

    if request.GET.get('page'):
        page = int(request.GET.get('page'))
    else:
        page = 1

    return render_to_response('products/backoffice/callbacks.html', {
        'callbacks': callbacks[start:end],
        'filter_form': filter_form,
        'callbacks_count': callbacks.count(),
        'page': page,
        'status': status,
        'site': site,
        'pages_count': range(1, callbacks.count() / ORDER_PAGE_COUNT),
    },
                              context_instance=RequestContext(request))


def order(request, hash, message=None, pay=None):
    cart = None
    is_hydromassage = False
    order = None
    pay_form = None
    order_user = None
    try:
        order = Order.objects.select_related().get(hash=hash)

        pay_form_initial = {}
        order_form_initial = {}

        try:
            pay_form_initial.update({"prepay": order.prepay.slug})
        except:
            pass

        try:
            pay_form_initial.update({"custom_delivery_sum": order.custom_delivery_sum})
        except:
            pass

        try:
            pay_form_initial.update({"delivery": order.delivery.slug})
        except:
            pass

        try:
            pay_form_initial.update({"pay_type": order.pay_type.slug})
        except:
            pass

        try:
            pay_form_initial.update({"carrier": order.carrier.slug})
        except:
            pass

        try:
            pay_form_initial.update({"distributor": order.distributor.slug})
        except:
            pass

        try:
            pay_form_initial.update({"carrier_number": order.carrier_number})
        except:
            pass

        try:
            pay_form_initial.update({"need_cargo_service": order.need_cargo_service})
        except:
            pass

        try:
            pay_form_initial.update({"delivery_date": order.delivery_date})
        except:
            pass

        try:
            print "DEL", order.delivery_pickup_time_period
            pay_form_initial.update({"delivery_pickup_time_period": order.delivery_pickup_time_period})
        except Exception, e:
            pass

        try:
            pay_form_initial.update({"version": order.version})
        except:
            pass

        try:
            order_form_initial.update({"name": order.name})
        except:
            pass

        try:
            order_form_initial.update({"address": order.address})
        except:
            pass

        try:
            order_form_initial.update({"country": order.country})
        except:
            pass

        try:
            order_form_initial.update({"city": order.city})
        except:
            pass

        try:
            order_form_initial.update({"index": order.index})
        except:
            pass

        try:
            order_form_initial.update({"region": order.region})
        except:
            pass

        try:
            order_form_initial.update({"street_abbr": order.street_abbr})
        except:
            pass

        try:
            order_form_initial.update({"street": order.street})
        except:
            pass

        try:
            order_form_initial.update({"house": order.house})
        except:
            pass

        try:
            order_form_initial.update({"phone": order.phone})
        except:
            pass

        try:
            order_form_initial.update({"email": order.email})
        except:
            pass

        try:
            order_form_initial.update({"destination": order.destination})
        except:
            pass

        try:
            order_form_initial.update({"comment": order.comment})
        except:
            pass

        print pay_form_initial

        pay_form = PayForm(initial=pay_form_initial)
        order_form = OrderForm(initial=order_form_initial)

        # pay_form.fields['pay_type'].initial = order.pay_type.slug
        # pay_form.fields['prepay'].initial = order.prepay.slug
        delivery_queryset = None
        if request.user.is_staff:
            delivery_queryset = Delivery.on_site.filter(publish=True)
            delivery_queryset.filter(slug='custom').update(price=order.custom_delivery_sum)
            # for delivery in delivery_queryset:
            #     if delivery.price == '0':
            #         delivery.price = order.delivery_sum
            pay_form.fields['delivery'].queryset = delivery_queryset
        else:
            delivery_queryset = Delivery.on_site.filter(publish=True,
                                                        id=order.delivery.id)
            delivery_queryset.filter(slug='custom').update(price=order.custom_delivery_sum)
            pay_form.fields['delivery'].queryset = delivery_queryset
            pay_form.fields['delivery'].label = u"Способ доставки"
            pay_form.fields['prepay'].queryset = Prepay.objects.filter(id=order.prepay.id)
            pay_form.fields['prepay'].label = u"Размер предоплаты"

        for cart_item in cart:
            cart_item.in_cart_options = []
            if cart_item.hydromassage:
                is_hydromassage = True

        order.user = User.objects.filter(email=order.email)[:1].get()

        order.set_delivery_price()

        # cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum = get_price(request, order)
        # print cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum

    except Exception, e:
        print Exception, e


        # LOAD ORDER IN SESSION
        # if not 'order' in request.session:
        #     request.session["order"] = []
        #
        # if request.user.is_superuser and request.session["order"] == []:
        #     request.session["cart"] = []
        #     request.session["order"] = []
        #     request.session["order"] = str(order.id)
        #
        #     cart = request.session["cart"]
        #     for orderitem in order.order_items.all():
        #         product_id = orderitem.product.id
        #         cart.append(product_id)
        #
        #         options_session_name = "%d_options" % product_id
        #         request.session[options_session_name] = []
        #         options = request.session[options_session_name]
        #         for option in orderitem.options.all():
        #             options.append(option.id)

    #       YANDEZ PAY FIELDS

    #         <input name="shopId" value="{{ YANDEX_MONEY_SHOP_ID }}" type="hidden"/>
    #         <input name="scid" value="{{ YANDEX_MONEY_SCID }}" type="hidden"/>
    #         <input name="sum" id="order_sum" value="{? global.cart_prepay_sum ?}"
    #                ng-init="global.pay_price='{{ order.price }}';" type="hidden"><br/>
    #         <input name="customerNumber" value="{{ order.user.id }}/{{ order.id }}" id="customer_number" type="hidden"/>
    #         <input name="orderNumber" id="order_number" type="hidden" ng-init="order_id='{{ order.id }}'" value="{{ order.id }}"/>
    #         <input name="paymentType" id="payment_type" type="hidden" value="{? pay_form.pay_type ?}"/>
    #         <input name="cps_phone" id="cps_phone" type="hidden" value="{{ order.phone }}"/>
    #         <input name="cps_email" id="cps_email" type="hidden" value="{{ order.email }}"/>

    add_products = cache.get('add_products::%s::all' % (order.site.domain,))
    if add_products is None:
        baths = Product.on_site.filter(entity__slug='bath').order_by('length')
        add_products = Product.objects.filter(sites=order.site.id)[:5]
        cache.set('add_products::%s::all' % (order.site.domain,), add_products)

    if pay or 'pay' in request.GET:
        return render_to_response('products/pay.html', {
            'is_order': True,
            'cart_with_options': order.order_items.all(),
            'order': order,
            'order_statuses': ORDER_STATUS_CHOICES,
            'pay_form': pay_form,
            'order_form': order_form,
            'is_hydromassage': is_hydromassage,
            'YANDEX_MONEY_SHOP_ID': settings.YANDEX_MONEY_SHOP_ID,
            'YANDEX_MONEY_SCID': settings.YANDEX_MONEY_SCID,
            'message': message,
            'deliveries': delivery_queryset,
            'payments': Payment.objects.filter(publish=True),
            'prepays': Prepay.objects.filter(publish=True),
            'noindex': True,
        },
                                  context_instance=RequestContext(request))

    return render_to_response('products/cart.html', {
        'is_order': True,
        'cart_with_options': order.order_items.all(),
        'order': order,
        'order_statuses': ORDER_STATUS_CHOICES,
        'pay_form': pay_form,
        'order_form': order_form,
        'is_hydromassage': is_hydromassage,
        'YANDEX_MONEY_SHOP_ID': settings.YANDEX_MONEY_SHOP_ID,
        'YANDEX_MONEY_SCID': settings.YANDEX_MONEY_SCID,
        'message': message,
        'deliveries': delivery_queryset,
        'payments': Payment.objects.filter(publish=True),
        'prepays': Prepay.objects.filter(publish=True),
        'add_products': add_products,
        'noindex': True,
    },
                              context_instance=RequestContext(request))


def add_products(request, order_id, entity):
    current_site = SiteInfo.on_site.select_related().get().site
    order = Order.objects.select_related().get(id=order_id)

    add_products = cache.get('add_products::%s::%s' % (current_site.domain, entity,))
    if add_products is None:
        add_products = Product.on_site.filter(entity__slug=entity)
        cache.set('add_products::%s::%s' % (current_site.domain, entity), add_products)

    return render_to_response('products/list.html', {
        'order': order,
        'products': add_products,
        'without_filter': True,
        'class': 'add_products',
        'add_to_order': True,
        'is_search': True,
        'nocache': True
    },
                              context_instance=RequestContext(request))


def order_list(request, order_id):
    order = Order.objects.select_related().get(id=order_id)

    return render_to_response('products/blocks/cart_block.html', {
        'is_order': True,
        'cart': order.order_items.all(),
        'order': order,
    },
                              context_instance=RequestContext(request))


def n_order(request, hash, message=None):
    cart = None
    is_hydromassage = False
    order = None
    pay_form = None
    order_user = None
    try:
        order = Order.objects.select_related().get(hash=hash)

        pay_form_initial = {}
        order_form_initial = {}

        try:
            pay_form_initial.update({"prepay": order.prepay.slug})
        except:
            pass

        try:
            pay_form_initial.update({"delivery": order.delivery.slug})
        except:
            pass

        try:
            pay_form_initial.update({"pay_type": order.pay_type.slug})
        except:
            pass

        try:
            pay_form_initial.update({"carrier": order.carrier.slug})
        except:
            pass

        try:
            pay_form_initial.update({"distributor": order.distributor.slug})
        except:
            pass

        try:
            pay_form_initial.update({"carrier_number": order.carrier_number})
        except:
            pass

        try:
            pay_form_initial.update({"need_cargo_service": order.need_cargo_service})
        except:
            pass

        try:
            pay_form_initial.update({"delivery_date": order.delivery_date})
        except:
            pass

        try:
            print "DEL", order.delivery_pickup_time_period
            pay_form_initial.update({"delivery_pickup_time_period": order.delivery_pickup_time_period})
        except Exception, e:
            pass

        try:
            pay_form_initial.update({"version": order.version})
        except:
            pass

        try:
            order_form_initial.update({"name": order.name})
        except:
            pass

        try:
            order_form_initial.update({"address": order.address})
        except:
            pass

        try:
            order_form_initial.update({"country": order.country})
        except:
            pass

        try:
            order_form_initial.update({"city": order.city})
        except:
            pass

        try:
            order_form_initial.update({"index": order.index})
        except:
            pass

        try:
            order_form_initial.update({"region": order.region})
        except:
            pass

        try:
            order_form_initial.update({"street_abbr": order.street_abbr})
        except:
            pass

        try:
            order_form_initial.update({"street": order.street})
        except:
            pass

        try:
            order_form_initial.update({"house": order.house})
        except:
            pass

        try:
            order_form_initial.update({"phone": order.phone})
        except:
            pass

        try:
            order_form_initial.update({"email": order.email})
        except:
            pass

        try:
            order_form_initial.update({"comment": order.comment})
        except:
            pass

        print pay_form_initial

        pay_form = PayForm(initial=pay_form_initial)
        order_form = OrderForm(initial=order_form_initial)

        # pay_form.fields['pay_type'].initial = order.pay_type.slug
        # pay_form.fields['prepay'].initial = order.prepay.slug
        if request.user.is_staff:
            pay_form.fields['delivery'].queryset = Delivery.on_site.filter(publish=True)
        else:
            pay_form.fields['delivery'].queryset = Delivery.on_site.filter(publish=True,
                                                                           id=order.delivery.id)
            pay_form.fields['delivery'].label = u"Способ доставки"
            pay_form.fields['prepay'].queryset = Prepay.objects.filter(id=order.prepay.id)
            pay_form.fields['prepay'].label = u"Размер предоплаты"

        for cart_item in cart:
            cart_item.in_cart_options = []
            if cart_item.hydromassage:
                is_hydromassage = True

        order.user = User.objects.filter(email=order.email)[:1].get()

        # cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum = get_price(request, order)
        # print cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum

    except Exception, e:
        print Exception, e

    # # LOAD ORDER IN SESSION
    # if request.user.is_superuser:
    #     request.session["cart"] = []
    #     request.session["order"] = []
    #     request.session["order"] = str(order.id)
    #
    #     cart = request.session["cart"]
    #     for orderitem in order.order_items.all():
    #         product_id = orderitem.product.id
    #         cart.append(product_id)
    #
    #         options_session_name = "%d_options" % product_id
    #         request.session[options_session_name] = []
    #         options = request.session[options_session_name]
    #         for option in orderitem.options.all():
    #             options.append(option.id)

    return render_to_response('products/cart.html', {
        'is_order': True,
        'cart_with_options': order.order_items.all(),
        'order': order,
        'order_statuses': ORDER_STATUS_CHOICES,
        'pay_form': pay_form,
        'order_form': order_form,
        'is_hydromassage': is_hydromassage,
        'YANDEX_MONEY_SHOP_ID': settings.YANDEX_MONEY_SHOP_ID,
        'YANDEX_MONEY_SCID': settings.YANDEX_MONEY_SCID,
        'message': message,
    },
                              context_instance=RequestContext(request))


def delivery(request):
    deliveries = Delivery.on_site.filter(publish=True)

    return render_to_response('triton_ua/pages/delivery.html', {
        'deliveries': zip(deliveries[0:1], deliveries[1:2]),
    },
                              context_instance=RequestContext(request))


def deliveries(request):
    deliveries = Delivery.on_site.filter(publish=True)

    return HttpResponse(deliveries)


def to_prepay(request, order_id):
    result = False
    try:
        order = Order.objects.select_related().get(id=order_id)
        order.status = "PR"
        order.save()

        DaMailer.send_mail_to_address(template='products/email/customer_pay.email.html', subject=u'Оплата вашего заказа на 3tn', data=order, email=order.email)
        result = True

    except Exception, e:
        print Exception, e

    return HttpResponse(result)
    # return redirect(request.META.get('HTTP_REFERER'))


import ydbf

def printorder(request, hash):

    order = Order.objects.select_related().get(hash=hash)

    return render_to_response('products/printorder.html', {
        'order': order,
        'noindex': True,
    },
                              context_instance=RequestContext(request))

def to_diler(request, order_id):
    '''
    SCHEMA = [
            ("EDIZM", "C", 2, 0),  — текстовое поле, заполнять единицей измерения номенклатуры
            ("VIDNOM", "C", 20, 0),  —  текстовое поле, заполнять видом номенклатуры (товар, материал и т.д.)
            ("POSTAV", "C", 100, 0),  —  название поставщика
            ("IDPOSTAV", "C", 15, 0),  —  код поставщика
            ("IDPOSTAV", "N", 15, 2),  — прайс (стоимость) номенклатуры поставщика



            ("NACH", "N", 1, 0),  — Индекс товара. Инкрементируется в начальной строке каждого нового товара
            ("IDDOC", "C", 13, 0), —  ID заказа
            ("DATE", "D", 8, 0), — Дата заказа
            ("SUM_DOST", "N", 15, 2), — Стоимость доставки
            ("DATE_OTG", "D", 8, 0), — Дата отгрузки
            ("DATE_OPL", "D", 8, 0),
            ("POKUP", "C", 50, 0),
            ("ADRDOST", "C", 100, 0),
            ("PERNAME", "C", 100, 0),
            ("PERINN", "C", 22, 0),
            ("PLANAME", "C", 00, 0),
            ("PLAINN", "C", 22, 0),
            ("IDPEREV", "C", 20, 0),
            ("DOPOLN", "C", 200, 0),
            ("NAME", "C", 100, 0),
            ("IDNAME", "C", 20, 0),
            ("NAMEHAR", "C", 200, 0),
            ("KOL", "N", 15, 2),
            ("CENA", "N", 15, 2),
            ("SUMMA", "N", 15, 2),
            ("NDS", "N", 15, 2),
            ("KOMM", "C", 200, 0),
            ("SUM_NAL", "N", 15, 2),
        ]
    '''
    result = False
    try:
        t_order = Order.objects.select_related().get(id=order_id)

        t_order.status = "EX"
        t_order.save()
        price_sum = 0
        price_diff = 0

        if 'triton3tn' in t_order.site.domain:
            SCHEMA = [
                ("EDIZM", "C", 5, 0),
                ("VIDNOM", "C", 20, 0),
                ("POSTAV", "C", 100, 0),
                ("IDPOSTAV", "C", 15, 0),
                ("CENAPOST", "N", 15, 2),
                ("NACH", "N", 1, 0),
                ("IDDOC", "C", 13, 0),
                ("DATE", "D", 8, 0),
                ("SUM_DOST", "N", 15, 2),
                ("DATE_OTG", "D", 8, 0),
                ("DATE_OPL", "D", 8, 0),
                ("POKUP", "C", 100, 0),
                ("ADRDOST", "C", 100, 0),
                ("PERNAME", "C", 100, 0),
                ("PERINN", "C", 22, 0),
                ("PLANAME", "C", 00, 0),
                ("PLAINN", "C", 22, 0),
                ("IDPEREV", "C", 20, 0),
                ("DOPOLN", "C", 200, 0),
                ("NAME", "C", 100, 0),
                ("IDNAME", "C", 20, 0),
                ("NAMEHAR", "C", 200, 0),
                ("KOL", "N", 15, 2),
                ("CENA", "N", 15, 2),
                ("SUMMA", "N", 15, 2),
                ("NDS", "N", 15, 2),
                ("KOMM", "C", 200, 0),
                ("SUM_NAL", "N", 15, 2),
            ]
        else:
            SCHEMA = [
                ("NACH", "N", 1, 0),
                ("IDDOC", "C", 13, 0),
                ("DATE", "D", 8, 0),
                ("SUM_DOST", "N", 15, 2),
                ("DATE_OTG", "D", 8, 0),
                ("DATE_OPL", "D", 8, 0),
                ("POKUP", "C", 100, 0),
                ("ADRDOST", "C", 100, 0),
                ("PERNAME", "C", 100, 0),
                ("PERINN", "C", 22, 0),
                ("PLANAME", "C", 00, 0),
                ("PLAINN", "C", 22, 0),
                ("IDPEREV", "C", 20, 0),
                ("DOPOLN", "C", 200, 0),
                ("NAME", "C", 100, 0),
                ("IDNAME", "C", 20, 0),
                ("NAMEHAR", "C", 200, 0),
                ("KOL", "N", 15, 2),
                ("CENA", "N", 15, 2),
                ("SUMMA", "N", 15, 2),
                ("NDS", "N", 15, 2),
                ("KOMM", "C", 200, 0),
                ("SUM_NAL", "N", 15, 2),
            ]
        #
        # db = dbf.Dbf("dbfile.dbf", new=True)
        # db.addField(*SCHEMA)

        dbf = ydbf.open('media/orders/%s.dbf' % t_order.im_id_wo_version(), 'w', SCHEMA, encoding='cp866')

        data = []

        oreder_len = len(t_order.order_items.all()) - 1
        last = False

        for i, orderitem in enumerate(t_order.order_items.all()):
            if i == 0:
                nach = 1
            else:
                nach = 0

            if i == oreder_len:
                last = True
                # data.append({
                #     'NACH': i, 'IDDOC': str(order.im_id()), 'DATE': date(order.pub_date.year, order.pub_date.month, order.pub_date.day), 'SUM_DOST': float(order.delivery.price), 'DATE_OTG': date(order.delivery_date.year, order.delivery_date.month, order.delivery_date.day), 'DATE_OPL': date(order.pay_date.year, order.pay_date.month, order.pay_date.day), 'POKUP': str(order.name), 'ADRDOST': str(order.address), 'PERNAME': str(order.carrier.company_legal.title), 'PERINN': str(order.carrier.company_legal.inn), 'PLANAME': str(order.pay_agent.company_legal.title), 'PLAINN': str(order.pay_agent.company_legal.inn), 'IDPEREV': str(order.carrier_number), 'DOPOLN': str(''), 'NAME': str(orderitem.product.title), 'IDNAME': str(orderitem.product.skus_with_price()), 'NAMEHAR': str(orderitem.options_list()), 'KOL': 1, 'CENA': float(order.price), 'SUMMA': float(order.price), 'NDS': 0, 'KOMM': str(order.comment)
                # })

            orderitem_len = len(orderitem.product.skus_with_price()) - 1
            for j, orderitemsku in enumerate(orderitem.product.skus_with_price()):

                price_sum += float(orderitemsku.discount_price(t_order.discount))

                options_list = ''
                price = orderitemsku.discount_price(t_order.discount)
                provider_price = float(0 if orderitemsku.provider_price == "" else float(orderitemsku.provider_price))


                if j == 0:
                    options_price, options_price_sum, options_provider_price = 0, 0, 0
                    for option in orderitem.non_dealer_options():
                        options_price += float(option.discount_price(t_order.discount))
                        option_sku = option.skus.all()[:1].get()
                        options_provider_price += 0 if option_sku.provider_price == "" else float(option_sku.provider_price)

                    for option in orderitem.options.all():
                        options_price_sum += float(option.discount_price(t_order.discount))

                    price_sum += float(options_price_sum)
                    price += float(options_price)
                    provider_price += float(options_provider_price)
                    options_list = orderitem.non_dealer_options_list()
                else:
                    nach = 0

                if j == orderitem_len and last:
                    price_diff = float(t_order.price) - float(price_sum)
                    price += float(price_diff)

                try:
                    delivery_date = date(t_order.delivery_date.year,
                                         t_order.delivery_date.month,
                                         t_order.delivery_date.day)
                except:
                    delivery_date = ''

                try:
                    pay_date = date(t_order.pay_date.year, t_order.pay_date.month, t_order.pay_date.day)
                except:
                    pay_date = ''

                try:
                    pay_agent_title = t_order.pay_agent.legal.title
                    pay_agent_inn = t_order.pay_agent.legal.inn
                except:
                    pay_agent_title = ''
                    pay_agent_inn = ''

                try:
                    pub_date = date(t_order.pub_date.year, t_order.pub_date.month, t_order.pub_date.day)
                except:
                    pub_date = ''

                # data.append({
                #     'NACH': nach, 'IDDOC': str(t_order.im_id()),
                #     'DATE': pub_date, 'SUM_DOST': float(t_order.delivery_sum), 'DATE_OTG': delivery_date, 'DATE_OPL': pay_date, 'POKUP': str(t_order.name), 'ADRDOST': str(t_order.get_address()),
                #     'PERNAME': str(t_order.carrier.company_legal.title), 'PERINN': str(t_order.carrier.company_legal.inn), 'PLANAME': str(pay_agent_title), 'PLAINN': str(pay_agent_inn),
                #     'IDPEREV': str(t_order.carrier_number), 'DOPOLN': str(''), 'NAME': str(orderitem.product.title), 'IDNAME': str(orderitemsku.sku), 'NAMEHAR': options_list, 'KOL': 1,
                #     'CENA': float("{0:.2f}".format(price)), 'SUMMA': float("{0:.2f}".format(price)), 'NDS': 0, 'KOMM': str(t_order.comment),
                #     'SUM_NAL': float(float(t_order.price) - t_order.prepay_sum() + float(t_order.delivery_sum))
                # })

                orderitemsku_sku = orderitemsku.sku

                try:
                    for j, option in enumerate(orderitem.override_options()):
                        if option.option.override:
                            orderitemsku_sku = option.skus.all()[:1].get().sku
                except Exception, e:
                    print Exception, e

                if 'triton3tn' in t_order.site.domain:
                    data.append({
                        'EDIZM': 'шт', 'VIDNOM': 'товар', 'POSTAV': orderitem.product.provider.title, 'IDPOSTAV': str(orderitem.product.provider.id), 'CENAPOST': float("{0:.2f}".format(
                        provider_price)),
                        'NACH': nach, 'IDDOC': str(t_order.im_id()),
                        'DATE': pub_date, 'SUM_DOST': float(t_order.delivery_sum), 'DATE_OTG': delivery_date, 'DATE_OPL': pay_date, 'POKUP': str(t_order.name), 'ADRDOST': str(t_order.get_address()),
                        'PERNAME': str(t_order.carrier.legal.title), 'PERINN': str(t_order.carrier.legal.inn), 'PLANAME': str(pay_agent_title), 'PLAINN': str(pay_agent_inn),
                        'IDPEREV': str(t_order.carrier_number), 'DOPOLN': str(''), 'NAME': str(orderitem.product.title), 'IDNAME': str(orderitemsku_sku), 'NAMEHAR': options_list, 'KOL': 1,
                        'CENA': float("{0:.2f}".format(price)), 'SUMMA': float("{0:.2f}".format(price)), 'NDS': 0, 'KOMM': str(t_order.comment),
                        'SUM_NAL': float(t_order.additional_sum())
                    })
                else:
                    data.append({
                        'NACH': nach, 'IDDOC': str(t_order.im_id()),
                        'DATE': pub_date, 'SUM_DOST': float(t_order.delivery_sum), 'DATE_OTG': delivery_date, 'DATE_OPL': pay_date, 'POKUP': str(t_order.name), 'ADRDOST': str(t_order.get_address()),
                        'PERNAME': str(t_order.carrier.legal.title), 'PERINN': str(t_order.carrier.legal.inn), 'PLANAME': str(pay_agent_title), 'PLAINN': str(pay_agent_inn),
                        'IDPEREV': str(t_order.carrier_number), 'DOPOLN': str(''), 'NAME': str(orderitem.product.title), 'IDNAME': str(orderitemsku_sku), 'NAMEHAR': options_list, 'KOL': 1,
                        'CENA': float("{0:.2f}".format(price)), 'SUMMA': float("{0:.2f}".format(price)), 'NDS': 0, 'KOMM': str(t_order.comment),
                        'SUM_NAL': float(t_order.additional_sum())
                    })

            try:
                for j, option in enumerate(orderitem.dealer_options()):

                    if not option.option.group.slug == 'none':
                        option_name = "%s %s" % (option.option.group.name, option.option.name)
                    else:
                        option_name = "%s" % (option.option.name,)

                    option_sku = option.skus.all()[:1].get()

                    if 'triton3tn' in t_order.site.domain:
                        data.append({
                            'EDIZM': 'шт', 'VIDNOM': 'товар', 'POSTAV': orderitem.product.provider.title, 'IDPOSTAV': str(orderitem.product.provider.id),
                            'CENAPOST': float("{0:.2f}".format(0 if option_sku.provider_price == "" else float(option_sku.provider_price))),
                            'NACH': "0", 'IDDOC': str(t_order.im_id()), 'DATE': pub_date, 'SUM_DOST': float(t_order.delivery_sum), 'DATE_OTG': delivery_date, 'DATE_OPL': pay_date,
                            'POKUP': str(t_order.name), 'ADRDOST': str(t_order.get_address()), 'PERNAME': str(t_order.carrier.legal.title), 'PERINN': str(t_order.carrier.legal.inn),
                            'PLANAME': str(pay_agent_title), 'PLAINN': str(pay_agent_inn), 'IDPEREV': str(t_order.carrier_number), 'DOPOLN': str(''), 'NAME': option_name,
                            'IDNAME': str(option.skus.all()[:1].get().sku),
                            'NAMEHAR': "", 'KOL': 1, 'CENA': float(option.discount_price(t_order.discount)), 'SUMMA': float(option.discount_price(t_order.discount)), 'NDS': 0,
                            'KOMM': str(t_order.comment), 'SUM_NAL': float(t_order.additional_sum())
                        })
                    else:
                        data.append({
                            'NACH': "0", 'IDDOC': str(t_order.im_id()), 'DATE': pub_date, 'SUM_DOST': float(t_order.delivery_sum), 'DATE_OTG': delivery_date, 'DATE_OPL': pay_date,
                            'POKUP': str(t_order.name), 'ADRDOST': str(t_order.get_address()), 'PERNAME': str(t_order.carrier.legal.title), 'PERINN': str(t_order.carrier.legal.inn),
                            'PLANAME': str(pay_agent_title), 'PLAINN': str(pay_agent_inn), 'IDPEREV': str(t_order.carrier_number), 'DOPOLN': str(''), 'NAME': option_name,
                            'IDNAME': str(option.skus.all()[:1].get().sku),
                            'NAMEHAR': "", 'KOL': 1, 'CENA': float(option.discount_price(t_order.discount)), 'SUMMA': float(option.discount_price(t_order.discount)), 'NDS': 0,
                            'KOMM': str(t_order.comment), 'SUM_NAL': float(t_order.additional_sum())
                        })
            except Exception, e:
                print Exception, e

        # result = data


        dbf.write(data)
        # print dbf
        # dbf.close()

        if '127.0.0.1' in request.META['HTTP_HOST']:
            phones = []
            emails = [settings.ADMINS[0][1]]
        else:
            emails = ['hramik@gmail.com', 'nomatter@3tn.org', t_order.distributor.email]
            phones = ['+380955491821', t_order.distributor.phone]

        files = ['media/orders/%s.dbf' % t_order.im_id_wo_version(), ]

        DaMailer.send_mail_to_addresses(template='products/email/execution_order.email.html',
                                        subject=u'Заказ на выполнение', data=t_order,
                                        emails=emails,
                                        files=files)

        Sms_Api.send_message_to_phone_number(phone_list=phones,
                                             message="Zakaz na ispolnenie %s" % t_order.im_id())

        result = True
    except Exception, e:

        print "DBF: ", Exception, e
        result = "DBF: ", Exception, e

    return HttpResponse(result)
    # return redirect(request.META.get('HTTP_REFERER'))
    # return order(request, hash=t_order.hash, message=u"Выгрузка отправлена диллеру")


from dpd.models import *


def to_dpd(request, order_id):
    result = False
    try:
        order = Order.objects.select_related().get(id=order_id)

        # order.status = "SH"
        # order.save()

        # try:
        #     contact = order.distributor.contacts.all()[:1].get()
        # except Exception, e:
        #     result = u"DPD: Не назначены пользователи для данного дистрибьютора. ", Exception, e
        #     return HttpResponse(result)
        #

        if order.additional_sum() == 0:
            npp_value = order.prepay_sum()
        else:
            npp_value = order.additional_sum()

        dpd_service = DpdService()

        # try:
        #     contactFio = "%s %s" % (contact.first_name, contact.last_name)
        # except Exception, e:
        #     contactFio = ""


        headerArray = {
            'datePickup': order.delivery_date.isoformat(),  # Дата приёма груза
            'senderAddress': {  # Адрес приёма груза
                'name': order.distributor.legal.title,  # Название
                # отправителя/получателя. В
                # случае, когда адрес
                # приёма/доставки –
                # это магазин, филиал компании, дилерский центр и т.п., в эту строку пишется
                # его название. Если доставка осуществляется физическому лицу,
                # то пишется  Ф.И.О получателя.
                'countryName': order.distributor.country,  # Название страны
                'city': order.distributor.city,  # Город
                'street': order.distributor.street,  # Улица (формат ФИАС)
                'streetAbbr': order.distributor.street_abbr,
                'index': order.distributor.index,  # Город
                'region': order.distributor.region,
                # Сокращения типа улицы (ул,
                # пр-т, б-р и т.д.)
                'house': order.distributor.house,  # Дом
                'houseKorpus': order.distributor.house_korpus,
                'str': order.distributor.str,
                'office': order.distributor.office,
                'contactFio': order.distributor.contact,
                # Контактное лицо
                'contactPhone': order.distributor.phone,  # Контактный телефон
                'instructions': order.comment,  # Комментарий для курьера
            },
            'pickupTimePeriod': order.get_delivery_pickup_time_period_display(),
            # Интервал времени приёма груза. Доступные для выбора интервалы приёма
            # 9-18 – в любое время с 09:00 до 18:00 (вариант по умолчанию);
            # 9-13 – с 09:00 до 13:00;
            # 13-18 – с 13:00 до 18:00.

        }

        # print order.order_items.all().values_list('product__id', flat=True)
        # print Product.objects.filter(id__in=order.order_items.all().values_list('product__id', flat=True))

        # order.order_items.aggregate(Sum('product__weight'))['product__weight__sum']

        # print order.prepay.factor, float(order.prepay.factor)

        # if order.prepay.factor == 1:
        #     npp_value = str(int(float(order.price) - (float(order.price) * float(order.prepay.factor))))
        # else:
        #     npp_value = str(int(float(order.price) - (float(order.price) * float(order.prepay.factor))) + float(order.delivery_sum))




        orderDetails = {
            'orderNumberInternal': order.im_id(),  # Номер заказа в информационной системе клиента
            'serviceCode': 'ECN',  # Код услуги DPD (dpdServicesArray)
            'serviceVariant': u'ДД',  # ДТ – от двери отправителя до терминала DPD;
            # ТД – от терминала DPD до двери получателя; ТТ – от терминала DPD до терминала DPD.
            'cargoNumPack': order.order_items.count(),  # Количество грузомест (посылок) в отправке
            'cargoWeight': order.weight(),  # Вес отправки, кг
            'cargoValue': str(int(float(order.price) + float(order.delivery_sum))),  # Сумма объявленной ценности, руб.
            'cargoCategory': ', '.join(order.order_items.all().values_list('product__entity__title', flat=True).distinct()),  # Содержимое отправки
            'cargoRegistered': False,
            # Ценный груз. Внутреннее вложение, включенное в перечень товаров, требующих
            # дополнительных мер безопасности, снижающих риск его утери или повреждения при перевозке.
            # Перечень товаров, относимых к категории «Ценный груз»:
            # 1. Мобильные телефоны
            # 2. Ноутбуки, планшеты
            'receiverAddress': {
                'name': order.name,
                # Название отправителя/получателя. В случае, когда адрес приёма/доставки –
                # это магазин, филиал компании, дилерский центр и т.п., в эту строку пишется
                # его название. Если доставка осуществляется физическому лицу,
                # то пишется  Ф.И.О получателя.
                'countryName': order.country,  # Название страны
                'index': order.index,  # Город
                'region': order.region,  # Город
                'city': order.city,  # Город
                'street': order.street,  # Улица (формат ФИАС)
                'streetAbbr': order.street_abbr,  # Сокращения типа улицы (ул, пр-т, б-р и т.д.)
                'house': order.house,  # Дом
                'contactFio': order.name,  # Контактное лицо
                'contactPhone': order.phone,  # Контактный телефон
                'instructions': order.comment,  # Комментарий дял курьера
            },
            'extraService': [
                {
                    'esCode': u'НПП',
                    'param': {
                        'name': 'sum_npp',
                        'value': npp_value
                    }
                }
            ]
        }

        if order.need_cargo_service:
            orderDetails['extraService'].append({
                'esCode': u'ПРД',  # нужны погрузочно-разгруз. работы
            })

        result = orderDetails
        dpd_result = dpd_service.create_order(orderDetails, headerArray)

        print dpd_result

        try:
            if dpd_result[0].errorMessage:
                result = dpd_result[0].errorMessage
            else:
                order.carrier_number = dpd_result[0].orderNum
                order.save()
                result = u'Заказ создан'
        except:
            if dpd_result.errorMessage:
                result = dpd_result.errorMessage
            else:
                order.carrier_number = dpd_result.orderNum
                order.save()
                result = u'Заказ создан'

    except Exception, e:
        print "DPD: ", Exception, e
        result = "DPD: ", Exception, e

    return HttpResponse(result)
    # return redirect(request.META.get('HTTP_REFERER'))
    # return order(request, hash=t_order.hash, message=u"Выгрузка отправлена диллеру")


def set_agree_make_review(request, value):
    if value == 'true':
        request.session["agree_make_review"] = True
    else:
        request.session["agree_make_review"] = False

    result = get_cart(request)

    return HttpResponse(result)


def get_cart(request):
    discount, discount_value, discount_cart_sum = 0, 0, 0
    try:
        cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum = get_price(
            request)
        cart_count = cart.count()
    except Exception, e:
        print Exception, e
        cart_session = ""
        cart = []
        cart_sum_end = 0
        cart_count = 0

    agree_make_review = 'false'

    try:
        if request.session["agree_make_review"] == True:
            agree_make_review = 'true'
    except:
        pass

    jsonstring = render_to_string('products/json/cart.html', {
        'cart': cart,
        'discount': discount,
        'cart_count': cart_count,
        'cart_sum': hramik_tags.price(cart_sum_end),
        'cart_sum_clear': cart_sum_end,
        'discount_value': hramik_tags.price(discount_value),
        'discount_cart_sum_clear': discount_cart_sum,
        'discount_cart_sum': hramik_tags.price(discount_cart_sum),
        'agree_make_review': agree_make_review,

    })
    return HttpResponse(jsonstring, mimetype='application/json')


def get_order(request, order_id):
    discount, discount_value, discount_cart_sum = 0, 0, 0

    cart_session = ""
    cart = []
    cart_sum_end = 0
    cart_count = 0

    try:
        order = Order.objects.get(id=order_id)
        cart_sum_end, discount, discount_value, discount_cart_sum, order = get_order_price(request, order)
        # order.discount = discount
        # order.save()
    except Exception, e:
        print Exception, e

    jsonstring = render_to_string('products/json/order.html', {
        'cart': cart,
        'order': order,
        'discount': discount,
        'cart_count': cart_count,
        'cart_sum': hramik_tags.price(cart_sum_end),
        'cart_sum_clear': cart_sum_end,
        'discount_value': hramik_tags.price(discount_value),
        'discount_cart_sum_clear': discount_cart_sum,
        'discount_cart_sum': hramik_tags.price(discount_cart_sum),
        'discount_cart_sum_with_delivery': hramik_tags.price(discount_cart_sum + int(float(order.delivery_sum))),
        'agree_make_review': "",

    })
    return HttpResponse(jsonstring, mimetype='application/json')


#  GET PRICE CURRENT PRODUCT
def get_current_price(request, id, order_id=None):
    print id, order_id

    discount, discount_value, discount_cart_sum, cart_all_sum_end = 0, 0, 0, 0
    try:
        cart_session, cart, cart_all_sum_end, discount, discount_value, discount_cart_sum = get_price(request, (id,))
    except Exception, e:
        print Exception, e
        cart_all_sum_end = 0

    jsonstring = render_to_string('products/json/cart.html', {
        # 'cart': cart,
        'discount': discount,
        'cart_sum': hramik_tags.price(cart_all_sum_end),
        'cart_sum_clear': cart_all_sum_end,
        'discount_value': hramik_tags.price(discount_value),
        'discount_cart_sum': hramik_tags.price(discount_cart_sum),

    })
    return HttpResponse(jsonstring, mimetype='application/json')


def add_to_cart(request, id, order_id=None):
    result = False
    try:
        cart = request.session["cart"]
        if (not id in cart):
            cart.append(id)

        request.session["cart"] = cart
        result = True
    except Exception, e:
        print e
        request.session["cart"] = [id]
        result = True

    # ADD TO ORDER
    try:
        if not order_id:
            order_id = request.session["order"]
        order = Order.objects.select_related().get(id=order_id)
        product = Product.objects.select_related().get(id=id)
        order_item = OrderItem(order=order, product=product, price=product.price)
        order_item.save()

        if order_id:
            cart_session = order.order_items.all().values_list('product__id', flat=True)
            cart_sum_end, discount, discount_value, discount_cart_sum = get_order_price(request, order)
        else:
            cart_session = request.session["cart"]
            cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum = get_price(request, cart_session)

        order.price = discount_cart_sum
        order.discount = discount
        order.cart_sum_end = cart_sum_end
        order.discount_value = discount_value
        order.save()

    except Exception, e:
        print Exception, e

    return HttpResponse(result)


def getsession(request, product_id):
    # request.session['%s_options' % product_id] = []
    from django.utils import simplejson
    json_stuff = simplejson.dumps(request.session['%s_options' % product_id])
    return HttpResponse(json_stuff, content_type="application/json")
    # return HttpResponse(request.session['15784_options'])


def add_option_to_session(request, id, product_id, active, order_id=None):
    options_session_name = product_id + "_options"

    result = False
    if not order_id:
        try:
            options = request.session[options_session_name]
            if (not id in options and active == 'true'):
                result = "ADD_OPTION"
                options.append(id)
            elif (id in options and active == 'false'):
                options.remove(id)
                result = "DELETE_OPTION"
            else:
                pass

            request.session[options_session_name] = options
            result = result
        except Exception, e:
            print e
            if active == 'true':
                request.session[options_session_name] = [id]
                result = "CHANGE_OPTION"

    try:
        if not order_id:
            order_id = request.session["order"]

        order = Order.objects.select_related().get(id=order_id)
        product = Product.objects.select_related().get(id=product_id)
        order_item = OrderItem.objects.select_related().get(order=order, product=product)

        if active == 'true':
            order_item_option_price = OptionPrice.objects.select_related().get(id=id)

            order_item.options.add(order_item_option_price)

            order_item_option = OrderItemOption(option=order_item_option_price.option, price=order_item_option_price.price)

            order_item_option.save()
            order_item.order_item_options.add(order_item_option)

            # order_item.price = int(order_item.price) + int(order_item_option_price.price)
        else:
            order_item_option_price = OptionPrice.objects.select_related().get(id=id)
            order_item.options.remove(order_item_option_price)

            order_item_option = order_item.order_item_options.filter(option=order_item_option_price.option).get()
            order_item.order_item_options.remove(order_item_option)

            order_item_option.delete()

        order_item.save()

        if order_id:
            cart_session = order.order_items.all().values_list('product__id', flat=True)
            cart_sum_end, discount, discount_value, discount_cart_sum = get_order_price(request, order)
        else:
            cart_session = request.session["cart"]
            cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum = get_price(request, cart_session)

        order.price = discount_cart_sum
        order.discount = discount
        order.cart_sum_end = cart_sum_end
        order.discount_value = discount_value
        order.save()

        order_item.order_item_options.all().delete()
        for option_price in order_item.options.all():
            order_item_option = OrderItemOption(option=option_price.option, price=option_price.price)
            order_item_option.save()
            order_item.order_item_options.add(order_item_option)

        result = True

    except Exception, e:
        print "ORDER OPTIONS", Exception, e

    return HttpResponse(result)


def clear_all_option_from_session(request, product_id):
    options_session_name = product_id + "_options"
    try:
        request.session[options_session_name] = []
        result = True
    except Exception, e:
        result = False

    return HttpResponse(result)


def load_options_complecation(request, product_id, complectation_id):
    options_session_name = product_id + "_options"
    try:
        complectation = Complectation.objects.get(id=complectation_id)

        request.session[options_session_name] = []
        options = []
        # for option in complectation.options.all():
        #     option_price = OptionPrice.objects.get(product__id = product_id, option__id = option.id)
        #     # add_option_to_session(request, option_price.id, product_id, 'true')
        #     options.append(option_price.id)

        options = OptionPrice.objects.filter(product__id=product_id, option__in=complectation.options.all()).values_list('id', flat=True)
        clear_all_option_from_session(request, str(product_id))
        request.session[options_session_name] = options

        result = True
    except Exception, e:
        result = False

    return HttpResponse(result)


def delete_from_cart(request, id, order_id=None):
    result = False
    try:
        cart = request.session["cart"]
        if (id in cart):
            cart.remove(id)
            result = True
            request.session["cart"] = cart
    except Exception, e:
        print Exception, e
        result = False

    # DELETE FROM EXISTING ORDER
    try:
        if not order_id:
            order_id = request.session["order"]
        OrderItem.objects.select_related().filter(order__id=order_id, product__id=id).delete()

        if order_id:
            order = Order.objects.select_related().get(id=order_id)
            cart_session = order.order_items.all().values_list('product__id', flat=True)

            cart_sum_end, discount, discount_value, discount_cart_sum = get_order_price(request, order)

            order.price = discount_cart_sum
            order.discount = discount
            order.cart_sum_end = cart_sum_end
            order.discount_value = discount_value
            order.save()
            result = True
        else:
            cart_session = request.session["cart"]

    except Exception, e:
        print Exception, e

    return HttpResponse(result)


def clear_cart(request):
    result = False
    try:
        request.session["cart"] = []
        request.session["order"] = []
        result = True
    except Exception, e:
        result = False

    if request.user.is_staff:
        # return redirect(request.META.get('HTTP_REFERER'))
        return redirect("/")
    else:
        return redirect("/orders")


# COMPARSION

def add_to_comparsion(request, id):
    result = False
    try:
        comparsion = request.session["comparsion"]
        if (not id in comparsion):
            comparsion.append(id)

        request.session["comparsion"] = comparsion
        result = True
    except Exception, e:
        print e
        request.session["comparsion"] = [id]
        result = True

    return HttpResponse(result)


def delete_from_comparsion(request, id):
    result = False
    try:
        comparsion = request.session["comparsion"]
        if (id in comparsion):
            comparsion.remove(id)
            result = True
            request.session["comparsion"] = comparsion
    except Exception, e:
        print e
        result = False

    return HttpResponse(result)


def clear_comparsion(request):
    result = False
    try:
        request.session["comparsion"] = []
        result = True
    except Exception, e:
        result = False

    return HttpResponse(result)


# FAVORITES

def add_to_favorite(request, id):
    result = False
    try:
        favorite = request.session["favorite"]
        if (not id in favorite):
            favorite.append(id)

        request.session["favorite"] = favorite
        result = True
    except Exception, e:
        print e
        request.session["favorite"] = [id]
        result = True

    return HttpResponse(result)


def delete_from_favorite(request, id):
    result = False
    try:
        favorite = request.session["favorite"]
        if (id in favorite):
            favorite.remove(id)
            result = True
            request.session["favorite"] = favorite
    except Exception, e:
        print e
        result = False

    return HttpResponse(result)


def clear_favorite(request):
    result = False
    try:
        request.session["favorite"] = []
        result = True
    except Exception, e:
        result = False

    return HttpResponse(result)


def change_price(request, nid):
    result = False
    try:
        vars = json.loads(request.body)
        price = vars['price']
        sites = vars['sites'].split(',')
        print sites, vars['sites']
        sites_objects = Site.objects.filter(domain__in=sites)
        print sites_objects

        products = Product.objects.filter(nid=nid, sites__domain__in=sites).update(price=price)
        print products
        # if product.price != price:
        # product.price=price
        # product.save()
        # else:
        # result = False
        result = True

    except Exception, e:
        print Exception, e
        result = False

    return HttpResponse(result)


def change_option_price(request, id):
    result = False
    try:
        vars = json.loads(request.body)
        price = vars['price']
        sites = vars['sites'].split(',')
        option_price = OptionPrice.objects.select_related().get(id=id)
        option = option_price.option
        product = option_price.product
        region_products = Product.objects.filter(nid=product.nid, sites__domain__in=sites)
        print id, option_price.id, option.name, products, price, sites
        for region_product in region_products:
            options = OptionPrice.objects.filter(option=option, product=region_product,
                                                 product__sites__domain__in=sites).update(
                price=price, overide_sku_price=True)
            print options

        # if option_price.price != price:
        # option_price.price=price
        # option_price.save()
        # else:
        # result = False
        result = True

    except Exception, e:
        print Exception, e
        result = False

    return HttpResponse(result)


def change_sku(request, id):
    result = False
    try:
        vars = json.loads(request.body)
        sku = vars['sku']
        product = Product.objects.select_related().get(id=id)
        if product.sku != sku:
            product.sku = sku
            product.save()
            result = True
        else:
            result = False
    except Exception, e:
        print Exception, e
        result = False

    return HttpResponse(result)


def clone_product(request, id):
    try:
        result = False

        from_site = '3tn.ru'
        site = Site.objects.select_related().get(name=from_site)

        production_product = Product.objects.select_related().get(id=id)
        region_product = Product(nid=production_product.nid)
        region_product.save()

        # region_product.sites.add(site)
        # region_product.save()

        region_product.title = production_product.title.encode('utf-8')
        region_product.sku = production_product.sku.encode('utf-8')
        region_product.popular = production_product.popular

        # json = JSONField(blank=True)
        region_product.tn_id = production_product.tn_id
        region_product.tn_alias = production_product.tn_alias

        region_product.slug = production_product.slug
        region_product.dot_slug = production_product.dot_slug

        region_product.body = production_product.body
        region_product.teaser = production_product.teaser

        region_product.h1 = production_product.h1
        region_product.h2 = production_product.h2
        region_product.page_title = production_product.page_title
        region_product.meta_desc = production_product.meta_desc
        region_product.meta_keywords = production_product.meta_keywords

        region_product.price = 0

        # TODO: add tags

        if not region_product.visor_set.all():
            for visor in production_product.visor_set.all():
                try:
                    visor = Visor.objects.get_or_create(product=region_product, name=visor.name)
                except Exception, e:
                    print "visor_err", Exception, e
                    pass

        region_product.entity = production_product.entity
        region_product.type = production_product.type

        for category in production_product.category.all():
            region_product.category.add(category)

        region_product.char_photo = production_product.char_photo
        region_product.photo = production_product.photo

        '''
        SKUS
        '''

        for sku in production_product.skus.all():
            try:
                new_sku = SKU.objects.get_or_create(content_type=sku.content_type,
                                                    object_id=region_product.id,
                                                    content_object=region_product, sku=sku)
            except:
                pass

        '''
        PHOTOS
        '''

        for photo in production_product.additionalphoto_set.all():
            try:
                photo = AdditionalPhoto.objects.get_or_create(node=region_product,
                                                              position=photo.position,
                                                              photo=photo.photo, alt=photo.alt,
                                                              title=photo.title)
            except:
                pass

        '''
        VIDEO
        '''

        for video in production_product.additionalvideos_set.all():
            try:
                video = AdditionalVideos.objects.get_or_create(node=region_product,
                                                               position=video.position,
                                                               video_url=video.video_url,
                                                               video_poster=video.video_poster,
                                                               video_type=video.video_type)
            except:
                pass

        region_product.length = production_product.length
        region_product.width = production_product.width
        region_product.height = production_product.height
        region_product.depth = production_product.depth
        region_product.value = production_product.value
        region_product.hydromassage = production_product.hydromassage

        region_product.save()

        result = True
    except Exception, e:
        print Exception, e

    return redirect(reverse('admin:products_product_change', args=[region_product.id]))


def comparsion(request):
    try:
        comparsion_session = request.session["comparsion"]
        comparsion = Product.objects.filter(id__in=comparsion_session)
    except:
        comparsion = None

    return render_to_response('products/comparsion.html', {
        'comparsion': comparsion,
    },
                              context_instance=RequestContext(request))


def staticpage(request, slug):
    page = Page.on_site.select_related().get(dot_slug=slug)
    pageparts = PagePart.on_site.filter(page=page)

    return render_to_response('triton_ua/pages/page.html', {
        'page': page,
        'pageparts': pageparts,
    },
                              context_instance=RequestContext(request))


def component(request, slug):
    try:
        page = SEONode.on_site.select_related().get(dot_slug="accessories/%s" % slug)
    except:
        raise Http404

    request.breadcrumbs([
        (_('%s') % ('Комплектующие'), '/hardware.php'),
    ])

    return render_to_response('triton_ua/pages/page.html', {
        'page': page,
        'colorbox': True,
    },
                              context_instance=RequestContext(request))


def not_found(request):
    return render_to_response('triton_ua/pages/not_found.html', {},
                              context_instance=RequestContext(request))


# def handler404(request):
# response = render_to_response('not_found', {},
# context_instance=RequestContext(request))
# response.status_code = 404
# return response
#
#
# def handler500(request):
# response = render_to_response('not_found', {},
# context_instance=RequestContext(request))
# response.status_code = 500
# return response

def get_history(request):
    # Product History
    try:
        product_history = request.session["product_history"][-5:]

        # TODO: sorted by product_history
        # product_history = [product_history_n[id] for id in product_history]

        clauses = ' '.join(['WHEN node_node.id=%s THEN %s' % (pk, i) for i, pk in enumerate(product_history)])
        ordering = 'CASE %s END' % clauses
        product_history = Product.on_site.filter(id__in=product_history).extra(select={'ordering': ordering}, order_by=('ordering',))

    except Exception, e:
        product_history = False

    return product_history


# @cache_page(60 * 15)
def front(request):
    current_site = SiteInfo.on_site.get()
    # return_page = cache.get('front::%s::mobile_%s::is_shop_%s' % (current_site.site.domain, request.user_agent.is_mobile, current_site.is_shop))
    # if return_page and not request.user.is_staff:
    #     return return_page

    if current_site.id == 82:
        return render_to_response('zavatarro/zavatarro.html', {
        },
                                  context_instance=RequestContext(request))

    # TODO: Чо это и што с этим делать?
    if "cid3tn" in request.GET:
        slug = Product.on_site.select_related().get(nid=request.GET['cid3tn']).dot_slug
        return redirect('/%s' % slug)

    # Baths with cache
    baths = cache.get('baths::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,))
    if baths is None:
        baths = Product.on_site.filter(entity__slug='bath', publish=True).order_by('length')
        cache.set('baths::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,), baths)

    # Popular baths with cache
    popular_baths = cache.get('popular_baths::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,))
    if popular_baths is None:
        popular_baths = Product.on_site.filter(popular=True, publish=True)
        cache.set('popular_baths::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,), popular_baths)

    # Slides with cache
    slides = cache.get('slides::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,))
    if slides is None:
        slides = Slide.on_site.filter(active=True, type__in=("M", "B")).order_by('position')
        cache.set('slides::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,), slides)

    # Main links with cache
    main_links = cache.get('main_links::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,))
    if main_links is None:
        main_links = MainLink.on_site.all()[:3]
        cache.set('main_links::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,), main_links)

    # Last news with cache
    last_news_list = cache.get('last_news_list::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,))
    if last_news_list is None:
        last_news_list = Page.on_site.filter(node_type__slug__in=['news', 'advices', 'article'], publish=True).order_by('-pub_date')[:3]
        cache.set('last_news_list::%s::main::is_shop_%s' % (current_site.site.domain, current_site.is_shop,), last_news_list)

    return_page = render_to_response('triton_ua/pages/front.html', {
        'front': True,
        'slides': slides,
        'main_links': main_links,
        'last_news_list': last_news_list,
        'baths': baths,
        'popular_baths': popular_baths,
        'product_history': get_history(request),
        'cache_string': 'front::%s::mobile_%s::is_shop_%s' % (current_site.site.domain, request.user_agent.is_mobile, current_site.is_shop)
    },
                                     context_instance=RequestContext(request))

    if not request.user.is_staff:
        cache.set('front::%s::mobile_%s::is_shop_%s' % (current_site.site.domain, request.user_agent.is_mobile, current_site.is_shop), return_page)
    return return_page


# @cache_page(60 * 15)
def to_front_test(request):
    return HttpResponse('<a href="/front_test">front_test</a>')


def front_test(request):
    message = 'koko'

    baths = Product.on_site.filter(entity__slug='bath').order_by('length')
    popular_baths = Product.on_site.filter(popular=True)

    baths_cat = baths.values_list('category', flat=True)
    baths_categories_ids = ProductCategory.objects.filter(id__in=set(baths_cat)).values_list(
        'category_type', flat=True)
    baths_types = CategoryType.objects.filter(id__in=set(baths_categories_ids))

    main_links = MainLink.on_site.all()[:3]

    return render_to_response('triton_ua/pages/test.html', {
        'popular_baths': popular_baths,
        'main_links': main_links,
        'baths': baths,
        'baths_types': baths_types,
        'front': True,
        # 'furnitures': furnitures,
        # 'mebel_types': mebel_types,
        # 'bathscabs': bathscabs,
        'message': message,
    },
                              context_instance=RequestContext(request))


def empty(request):
    return render_to_response('triton_ua/pages/empty.html', {
    },
                              context_instance=RequestContext(request))


def visor(request):
    try:
        visor = Visor.objects.filter(name=request.GET.get('visor_url'))[:1].get()
    except:
        product = Product.objects.filter(nid=request.GET.get('visor_url'))[:1].get()
        visor = product.visor_set.all()[:1].get()

    return render_to_response('triton_ua/blocks/visor.html', {
        # 'product': product,
        'visor_url': visor.name,
        'width': '800',
        'height': '600',
        'gal': request.GET.get('gal'),
        'item': request.GET.get('item'),
    },
                              context_instance=RequestContext(request))


def getthreedmodel(request):
    product = None

    try:
        if request.POST:
            product = Product.objects.filter(nid=request.POST['id'])[:1].get()

        if request.GET:
            product = Product.objects.filter(nid=request.GET['id'])[:1].get()
    except Exception, e:
        pass

    return render_to_response('triton_ua/blocks/visor_button.html', {
        'product': product,
        'width': '830',
        'height': '710',
    },
                              context_instance=RequestContext(request))


def getprices(request, entity=None):
    try:
        dot_slug = request.META['HTTP_REFERER']
        dot_slug = re.sub(r'^http://.*?/', '', dot_slug)
        # if dot_slug == '3d' or re.match('.*?www.moscow3tn.ru.*', request.META['HTTP_REFERER']):
        if dot_slug == '3d' or dot_slug == 'media/3dpr/Triton.swf':
            dot_slug = False
    except:
        dot_slug = False

    # now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    # file_name = os.path.join(settings.MEDIA_ROOT, 'logs', 'getprices-%s' % (now))
    # handle=open(file_name, 'w+')
    # handle.write(str(request))
    # if dot_slug:
    #     handle.write(dot_slug)
    # handle.close()

    products, product = None, None
    site_domain = 'www.3tn.ru'
    kwargs = {}

    if request.GET:
        if not request.GET['site'] in new_sites:
            site_domain = request.GET['site'].replace("3tn", ".3tn").replace("www.", "")
        else:
            site_domain = request.GET['site']

    if request.POST:
        if not request.POST['site'] in new_sites:
            site_domain = request.POST['site'].replace("3tn", ".3tn").replace("www.", "")
        else:
            site_domain = request.GET['site']

    # print site_domain, entity

    try:
        site = Site.objects.select_related().get(domain=site_domain)
        kwargs['sites'] = site
        if dot_slug:
            kwargs['dot_slug'] = dot_slug
        if entity:
            kwargs['entity__slug'] = entity

        products = Product.objects.filter(**kwargs)
        # print products
        # product = Product.objects.filter(sites=site, sku='Н0000020145')[:1].get()
    except:
        pass

    try:
        if not dot_slug:
            jsonstring = get_json_cache(site_domain)
            if jsonstring:
                return HttpResponse(jsonstring, content_type='application/json; charset=utf-8')
    except:
        pass

    jsonstring = render_to_string('triton_ua/json/prices.html', {
        'products': products,
        # 'product': product,
    },
                                  context_instance=RequestContext(request))
    try:
        if not dot_slug:
            put_json_cache(jsonstring, site_domain)
    except:
        pass

    return HttpResponse(jsonstring, content_type='application/json; charset=utf-8')


def get_json_cache(site_domain):
    # Cache time to live in seconds
    cache_ttl = 86400
    file_name = os.path.join(settings.MEDIA_ROOT, 'json_cache', '%s.prices.json' % site_domain)
    up_to_date = False
    try:
        now = mytime.time()
        modify_time = os.path.getmtime(file_name)
        if now - modify_time < cache_ttl:
            up_to_date = True
    except Exception, e:
        print Exception, e
    if up_to_date and os.path.isfile(file_name):
        try:
            handler = open(file_name)
            jsonstring = handler.read()
            return jsonstring
        except:
            return False
    return False


def put_json_cache(jsonstring, site_domain):
    jsonstring = re.sub('[\n\r]', '', jsonstring)
    jsonstring = re.sub('\s+', ' ', jsonstring)
    file_name = os.path.join(settings.MEDIA_ROOT, 'json_cache', '%s.prices.json' % site_domain)
    handle = open(file_name, 'w+')
    handle.write(jsonstring)
    handle.close()
    return


@login_required
def clear_cache_page(request):
    cache.clear()
    return render_to_response('triton_ua/pages/empty.html', {
        'message': u'Кешу капец'
    },
                              context_instance=RequestContext(request))


def search(request):
    try:
        query = request.GET['text']
    except Exception, e:
        query = ""

    search_baths = Product.on_site.filter(title__icontains=query.encode('utf-8'))

    return render_to_response('triton_ua/pages/search.html', {
        'search_baths': search_baths,
        'query': query
    },
                              context_instance=RequestContext(request))


def dataexport(request, slug):
    return redirect('http://old.3tn.ru/dataexport/%s' % slug)


def pages(request):
    pages = Page.on_site.all()

    return render_to_response('triton_ua/pages/pages.html', {
        'pages': pages,
    },
                              context_instance=RequestContext(request))


def robots(request):
    current_site = SiteInfo.on_site.select_related().get()

    print request.META['SERVER_PORT']

    if not request.META['SERVER_PORT'] == '80':
        return HttpResponse(current_site.https_robots, content_type="text/plain")
    else:
        return HttpResponse(current_site.robots, content_type="text/plain")


def style(request):
    return render_to_response('triton_ua/css/style.html', {
    },
                              context_instance=RequestContext(request))


def logout(request):
    try:
        auth_logout(request)
    except KeyError:
        pass

    if request.META.get('HTTP_REFERER') and urlparse(
            request.META.get('HTTP_REFERER')).path == '/profile':
        return redirect(reverse('tri:front'))
    else:
        return redirect(request.META.get('HTTP_REFERER'))


def check_login(request):
    if request.user.is_authenticated():
        result = "<div class='username'>%s</div>" % request.user.username
    else:
        result = False
    return HttpResponse(result)


def multi_login(request):
    url = 'http://www.3tn.ru/check_login'
    result = urllib2.urlopen(url).read()
    print result

    return HttpResponse(result)


@login_required
def profile(request, qr=None):
    change_user_form = RegisterForm()

    repres = RepresProfile.objects.all().values_list('region', flat=True)
    regions = Region.objects.filter(id__in=repres)
    print "REPRES", regions

    if request.GET:
        qr = request.GET.get('qr')

    dealers = None
    repress = None
    sellers = None
    reports = None
    projects = None
    freelancers = None

    dealer_form = None
    dealer_phones_form = None
    repres_form = None
    seller_form = None
    report_form = None
    question_form = None
    comment_form = None
    node_subscribe_form = None

    user_questions = None
    user_saves = None

    freelancer_form = None

    if qr == DEALER_CODE:
        if not DealerProfile.objects.filter(
                user_profile=request.user.userprofile) and not RepresProfile.objects.filter(
            user_profile=request.user.userprofile) and not SellerProfile.objects.filter(
            user_profile=request.user.userprofile):
            dealer_form = DealerRegisterForm(
                initial={
                    'first_name': request.user.first_name,
                    'second_name': request.user.userprofile.second_name,
                    'last_name': request.user.last_name,
                }
            )

            # dealer_phones_form = DealerPhoneFormSet()

            repres_form = RepresRegisterForm(
                initial={
                    'first_name': request.user.first_name,
                    'second_name': request.user.userprofile.second_name,
                    'last_name': request.user.last_name,
                }
            )

            seller_form = SellerRegisterForm(
                initial={
                    'first_name': request.user.first_name,
                    'second_name': request.user.userprofile.second_name,
                    'last_name': request.user.last_name,
                    'phone': request.user.userprofile.phone,
                    'yandex_money_account': request.user.userprofile.yandex_money_account,
                }
            )

    if qr == FREELANCER_CODE:
        if not FreelanceProfile.objects.filter(user_profile=request.user.userprofile):
            print "FREELANCER_CODE"
            freelancer_form = FreelancerRegisterForm(
                initial={
                    'first_name': request.user.first_name,
                    'second_name': request.user.userprofile.second_name,
                    'last_name': request.user.last_name,
                }
            )

    # Dealer list for superuser
    if request.user.is_superuser:
        dealers = DealerProfile.objects.all()
        repress = RepresProfile.objects.all()
        sellers = SellerProfile.objects.all()

    # if request.user.is_superuser or request.user.userprofile.dealerprofile.active:

    if request.user.has_perm('constructors.view_freelancer'):
        freelancers = FreelanceProfile.objects.all()

    if request.user.has_perm('constructors.view_project'):
        projects = Project.objects.own_projects(request.user)

    if 'dealer' in request.user.groups.values_list('name', flat=True):
        try:
            dealer = DealerProfile.objects.get(user_profile__user=request.user)
            repress = RepresProfile.objects.filter(region__in=dealer.company.region.all())
            sellers = SellerProfile.objects.filter(region__in=dealer.company.region.all())
        except:
            pass

    if 'repres' in request.user.groups.values_list('name', flat=True):
        repres = RepresProfile.objects.get(user_profile__user=request.user)
        sellers = SellerProfile.objects.filter(repres=repres)
        try:
            reports = SellerReport.objects.filter(seller__in=sellers)
        except:
            pass

    if 'seller' in request.user.groups.values_list('name', flat=True):
        seller = SellerProfile.objects.get(user_profile__user=request.user)
        report_form = SellerReportForm()
        try:
            reports = SellerReport.objects.filter(seller=seller)
        except:
            pass

    if request.user.is_authenticated():
        user_profile = request.user.userprofile

        change_user_form = EditProfileForm(
            initial={
                'username': request.user.username,
                'email': request.user.email,
                'city': user_profile.city,
                'phone': user_profile.phone,
                'first_name': request.user.first_name,
                'last_name': request.user.last_name
            }
        )

        node_subscribe_form = NodeSubscribeForm(
            initial={
                'node_types': NodeSubscribe.objects.filter(user=request.user).values_list(
                    'node_type__slug', flat=True)
            }
        )

        print request.user.userprofile.subscribtions.all().values_list('slug', flat=True)

        subscribe_form = SubscribeForm(
            initial={
                # 'subscribe': req.objects.filter(user=request.user).values_list('slug', flat=True)
                'subscribe': request.user.userprofile.subscribtions.all().values_list('slug',
                                                                                      flat=True)
            }
        )

        user_questions = Question.objects.filter(user=request.user).order_by('-last_comment_date')
        user_saves = Save3d.objects.filter(user=request.user).order_by('-pub_date')

        question_form = QuestionForm()
        comment_form = CommentForm()

    # print change_user_form

    return render_to_response('triton_ua/accounts/profile.html', {
        'change_user_form': change_user_form,
        'user_questions': user_questions,
        'user_saves': user_saves,
        'question_form': question_form,
        'comment_form': comment_form,
        'node_subscribe_form': node_subscribe_form,
        'subscribe_form': subscribe_form,
        'report_form': report_form,
        'qr': qr,
        'dealer_form': dealer_form,
        'dealer_phones_form': dealer_phones_form,
        'repres_form': repres_form,
        'seller_form': seller_form,
        'freelancer_form': freelancer_form,
        'dealers': dealers,
        'repress': repress,
        'sellers': sellers,
        'reports': reports,
        'freelancers': freelancers,
        'projects': projects,
        'advanced_profiles': ('dealer', 'repres', 'seller')
    },
                              context_instance=RequestContext(request))


@login_required
def another_profile(request, username):
    projects = []

    user_profile = UserProfile.objects.get(user__username=username)

    if request.user.has_perm('constructors.view_all_projects'):
        try:
            projects = Project.objects.user_projects(username)
        except:
            pass

    if request.user.has_perm('constructors.view_freelancer'):
        try:
            freelancer = FreelanceProfile.objects.get(user_profile=user_profile)
        except:
            freelancer = None

    return render_to_response('triton_ua/accounts/another_profile.html', {
        'a_user_profile': user_profile,
        'projects': projects,
        'freelancer': freelancer,
    },
                              context_instance=RequestContext(request))


def perimeter(request):
    if request.GET and 'ring' in request.GET and request.GET['ring'] == 'I8vu3UQuwK3DKLF1T1DNPlLqZ':
        seo_nodes = SEONode.objects.all()
        seo_nodes.update(h1="", h2="", page_title="", meta_desc="", meta_keywords="", top_text="")
        for seo_node in seo_nodes:
            seo_node.dot_slug = seo_node.id
            seo_node.save()

    return HttpResponse('--------- PERIMETER DONE ---------')


def login(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            if user.is_active:
                print("User is valid, active and authenticated")
                auth_login(request, user)
            else:
                print("The password is valid, but the account has been disabled!")
        else:
            print("The username and password were incorrect.")

    try:
        user_profile = request.user.get_profile()
        user_profile.publish_last_question(request)
    except Exception, e:
        pass

    if request.META.get('HTTP_REFERER') and urlparse(
            request.META.get('HTTP_REFERER')).path != '/login':
        print request.META.get('HTTP_REFERER')

        return redirect(request.META.get('HTTP_REFERER'))
    elif request.META.get('HTTP_REFERER') and urlparse(
            request.META.get('HTTP_REFERER')).path == '/login':
        return redirect(reverse('tri:profile'))
    else:
        return render_to_response('triton_ua/accounts/login.html', {
        },
                                  context_instance=RequestContext(request))


def recovery_password(request):
    message = None

    if request.method == "POST":
        try:
            user = User.objects.filter(email=request.POST["email"])[:1].get()
            print user
            password = User.objects.make_random_password()
            print password
            user.set_password(password)
            user.save()

            data = {
                'user': user,
                'userprofile': user.userprofile,
                'password': password,
            }

            DaMailer.send_mail_to_address(template="da_mailer/recover_password.html",
                                          subject="Восстановление пароля", data=data,
                                          email=request.POST["email"])

            message = "Пароль и инструкции высланы на <b>%s</b>" % request.POST["email"]

        except Exception, e:
            message = "Ваш имейл не найден"
            print Exception, e

    return render_to_response('triton_ua/accounts/login.html', {
        "message": message,
    },
                              context_instance=RequestContext(request))


def register(request):
    if request.method == 'POST':
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                user_profile = user.get_profile()
                user_profile.publish_last_question(request)
            else:
                print("The password is valid, but the account has been disabled!")
        else:
            user = User.objects.create_user(username=request.POST['username'],
                                            email=request.POST['email'],
                                            password=request.POST['password'])

            # user.phone = request.POST['phone']
            #
            # user.first_name = request.POST['first_name']
            #
            # user.last_name = request.POST['last_name']

            user.save()

            user_profile = user.get_profile()
            # user_profile.city = request.POST['city']
            if 'subscribe' in request.POST:
                user_profile.subscribtions.add(Subscription.objects.get(slug='common'))

            # user_profile.city = request.POST['city']
            user_profile.save()
            user_profile.publish_last_question(request)

            user = authenticate(username=request.POST['username'],
                                password=request.POST['password'])

            auth_login(request, user)

    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def edit_profile(request):
    if request.method == 'POST':
        user = request.user
        user.username = request.POST['username']
        user.email = request.POST['email']
        if request.POST['password']:
            user.set_password(request.POST['password'])

        if 'first_name' in request.POST:
            user.first_name = request.POST['first_name']

        if 'last_name' in request.POST:
            user.last_name = request.POST['last_name']

        user.save()

        # Edit User Profile Fields
        user_profile = user.userprofile

        if 'city' in request.POST:
            user_profile.city = request.POST['city']

        if 'phone' in request.POST:
            user_profile.phone = request.POST['phone']

        user_profile.save()

    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def profile_change_avatar(request):
    if request.method == 'POST':
        user = request.user
        user_profile = user.get_profile()

        try:
            os.mkdir(location("media/avatars"))
            result = location("media/avatars")
        except Exception, e:
            result = "dir"

        try:
            # filedest = os.path.abspath(os.path.dirname(__name__))+'/media/upload_files/'+request.FILES['file'].name
            new_image_name = str(request.user.id) + '-' + datetime.now().strftime(
                "%Y-%m-%d-%H-%M-%S") + str(
                random.randint(100, 999)) + os.path.splitext(request.FILES[
                                                                 'file']
                                                             .name)[1]
            print new_image_name

            filedest = location("media/avatars/" + new_image_name)
            result_filedest = '/media/avatars/' + new_image_name
            avatar_dest = 'avatars/' + new_image_name
            handle_uploaded_file(request.FILES['file'], filedest)
            result = result_filedest

            user_profile.avatar = avatar_dest
            user_profile.save()
        except Exception, e:
            # result='file'
            print Exception, e
            pass

    return HttpResponse(result)


@login_required
def node_subscribe(request):
    if request.method == 'POST':
        user = request.user
        node_types = request.POST.getlist('node_types')
        NodeSubscribe.objects.filter(user=user).delete()
        for node_type in node_types:
            node_subscribe, create = NodeSubscribe.objects.get_or_create(user=user,
                                                                         node_type=NodeType.objects.get(
                                                                             slug=node_type))

    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def subscribe(request):
    if request.method == 'POST':
        user = request.user
        subscribes = request.POST.getlist('subscribe')
        print subscribes
        user.userprofile.subscribtions = []
        for subscribe_slug in subscribes:
            user.userprofile.subscribtions.add(Subscription.objects.get(slug=subscribe_slug))

    return redirect(request.META.get('HTTP_REFERER'))


@login_required
def last_articles(request):
    last_articles = Page.objects.filter(sites=1,
                                        node_type__slug__in=('advices', 'article', 'news'),
                                        publish=True).order_by('-pub_date')[
                    request.GET["start"]:request.GET["end"]]

    output = ""

    for article in last_articles:
        output += "%s <br>" % article

    return HttpResponse(output)


# @login_required
def send_subscribe(subscribe_slug='common'):
    # user_emails = UserProfile.objects.filter(subscribtions__slug=subscribe_slug, user__is_active=True).values_list('user__email', flat=True)
    user_emails = ['nevariuss@gmail.com', 'nomatter@3tn.org', 'hramik@gmail.com']
    # user_emails = ['hramik@gmail.com']
    # user_emails = ['rpp31@yandex.ru ']


    # user_emails = UserProfile.objects.filter(subscribtions__slug=subscribe_slug, user__is_active=True, user__is_superuser=True).values_list('user__email', flat=True)
    # user_emails = UserProfile.objects.filter(user__is_active=True, user__is_staff=True).values_list('user__email', flat=True)

    last_articles = Page.objects.filter(sites=1,
                                        node_type__slug__in=('advices', 'article', 'news'),
                                        publish=True).order_by('-pub_date')[0:3]
    # print user_emails.count()
    # return

    DaMailer.send_subscribe_mail(subject='Новости журнала Triton', emails=user_emails,
                                 template='subscribe-1.html', data=last_articles)
    return HttpResponse(True)


def show_json(request):
    return render_to_response('triton_ua/pages/json.html', {
    },
                              context_instance=RequestContext(request))


def import_products(request, import_site='this', arg=''):
    # Definition current site domain and link for doenload json data
    if import_site == 'this':
        import_site = get_current_site(request).domain
    current_site = get_current_site(request)

    json_download_link = 'http://www.3tn.ru/dataexport/prices?site=' + import_site + '&nocache=1'
    json_file_name = 'media/docs/' + current_site.domain + '.json'
    # json_file_name = 'media/docs/test.json'

    logging.debug(json_download_link)

    # Create json file for this site
    if arg == 'update' or arg == 'new':
        data = urllib.urlopen(json_download_link)
        with open(location(json_file_name), 'w') as f:
            myfile = File(f)
            myfile.write(data.read())

    json_data = open(location(json_file_name))
    json_load = json.load(json_data)

    for item in json_load:
        json_product = json_load[item]
        # if product['entity'] == 'bath':
        logging.debug(json_product['name'])
        try:
            print 'try'
            product = Product.on_site.get(tn_alias=json_product["alias"])
            print product
            # product_details, created_product_details = ProductDetails.objects.get_or_create(product=product, sites=current_site)
            product.json = json_product
            product.json_save()
        # product.save(current_site=current_site, product_details=product_details)
        except Product.DoesNotExist:
            print 'does_not_exist'
            product = Product(title=json_product["name"],
                              tn_alias=json_product["alias"], sites=current_site)
            print product, current_site
            # product_details, created_product_details = ProductDetails.objects.get_or_create(product=product, sites=current_site)
            product.save()
            product.json = json_product
            product.json_save()
            # product.save(current_site=current_site, product_details=product_details)

    return render_to_response('triton_ua/pages/import_products.html', {
        'data': "True",
    },
                              context_instance=RequestContext(request))


def import_users(request):
    pass


def import_accessories(request):
    json_download_link = 'http://www.3tn.ru/dataexport/accessories'
    json_file_name = 'media/docs/accessories.json'

    # Create json file for this site
    data = urllib.urlopen(json_download_link)
    with open(location(json_file_name), 'w') as f:
        myfile = File(f)
        myfile.write(data.read())

    json_data = open(location(json_file_name))
    json_load = json.load(json_data)

    for item in json_load:
        json_a = json_load[item]

        component_slug = json_a['url'].replace('accessories/', '')
        component, created = Component.objects.get_or_create(dot_slug=component_slug)

        try:
            component.title = json_a['title']
        except:
            pass

        try:
            component.body = json_a['body']['ru'][0]['safe_value']
        except:
            pass

        # PHOTO
        try:
            if not component.photo:
                img_url = json_a["field_image"]['ru'][0]['uri'].replace('public://',
                                                                        'http://www.3tn.ru/sites/default/files/')
                name = urlparse(img_url).path.split('/')[-1]
                content = ContentFile(urllib2.urlopen(img_url).read())
                component.photo.save(name, content, save=True)
        except:
            a_p_error = 'main_photo'

        component.save()

    return render_to_response('triton_ua/pages/import_products.html', {
        'data': "True",
    },
                              context_instance=RequestContext(request))


# def import_advices(request):
# json_download_link = 'http://www.3tn.ru/dataexport/advices'
# json_file_name = 'media/docs/advices.json'
#
# # Create json file for this site
#     # data = urllib.urlopen(json_download_link)
#     # with open(location(json_file_name), 'w') as f:
#     #     myfile = File(f)
#     #     myfile.write(data.read())
#
#     json_data = open(location(json_file_name))
#     json_load = json.load(json_data)
#
#     for item in json_load:
#         json_a = json_load[item]
#
#         advice, create = Page.on_site.get_or_create(nid=json_a['nid'], title=json_a['title'])
#         # advice = Page.objects.filter(nid=json_a['nid'], title=json_a['title'])
#         # advice.delete()
#         print advice
#
#         try:
#             advice.dot_slug = json_a['link']
#         except:
#             print 'link'
#             pass
#
#         try:
#             advice.nid=json_a['nid']
#         except:
#             print 'nid'
#             pass
#
#         # try:
#         #     advice.title = json_a['title']
#         # except:
#         #     print 'title'
#         #     pass
#
#         try:
#             advice_body = json_a['body']
#             # images_re = re.compile(r'<img[ ]+src=\"([a-z0-9_\/]+\.\w+)\"[a-z0-9\" =\:]*\>')
#             # advice_images = images_re.findall(advice_body)
#             # advice_body = images_re.sub('', advice_body)
#
#             advice_body = advice_body.replace('src="', 'src="/static/import')
#             advice.body = advice_body
#         except:
#             print 'body'
#             pass
#
#         try:
#             advice.page_title = json_a['page_title']
#         except:
#             print 'page_title'
#             pass
#
#         try:
#             advice.meta_desc = json_a['meta_description']
#         except:
#             print 'meta_desc'
#             pass
#
#         try:
#             advice.meta_keywords = json_a['meta_keywords']
#         except:
#             print 'meta_keywords'
#             pass
#
#         try:
#             tag = Tag.objects.get(slug='advices')
#             advice.tags.add(tag)
#             # print tag, advice.tags
#         except Exception, e:
#             print Exception, e, 'tag'
#             pass
#
#         try:
#             advice.sites.add(get_current_site(request))
#         except:
#             print 'sites'
#             pass
#
#
#         # PHOTO
#         try:
#             if not advice.photo:
#                 img_url = json_a["image"]
#                 name = urlparse(img_url).path.split('/')[-1]
#                 content = ContentFile(urllib2.urlopen(img_url).read())
#                 advice.photo.save(name, content, save=True)
#         except:
#             a_p_error = 'main_photo'
#
#         advice.save()
#
#     return render_to_response('oblaka/pages/import_products.html', {
#         'data': "True",
#     },
#                               context_instance=RequestContext(request))

def import_pages(request, type="news"):
    json_download_link = 'http://www.3tn.ru/dataexport/' + type
    json_file_name = 'media/docs/' + type + '.json'

    data = urllib.urlopen(json_download_link)
    with open(location(json_file_name), 'w') as f:
        myfile = File(f)
        myfile.write(data.read())

    json_data = open(location(json_file_name))
    json_load = json.load(json_data)

    pages = []
    index = 0

    for item in json_load:
        json_a = json_load[item]

        page, create = Page.on_site.get_or_create(nid=json_a['nid'], title=json_a['title'])
        # advice = Page.objects.filter(nid=json_a['nid'], title=json_a['title'])
        # advice.delete()
        print page

        try:
            page.dot_slug = json_a['link']
        except:
            print 'link'
            pass

        try:
            page.nid = json_a['nid']
        except:
            print 'nid'
            pass

        try:
            page_body = json_a['body']
            # images_re = re.compile(r'<img[ ]+src=\"([a-z0-9_\/]+\.\w+)\"[a-z0-9\" =\:]*\>')
            # advice_images = images_re.findall(advice_body)
            # advice_body = images_re.sub('', advice_body)

            page_body = page_body.replace('src="', 'src="/media/import')
            page.body = page_body
        except:
            print 'body'
            pass

        try:
            page.page_title = json_a['title']
        except:
            print 'page_title'
            pass

        try:
            page_type, create = NodeType.objects.get_or_create(slug=type)
            page.node_type = page_type
        except:
            print 'page_type'
            pass

        try:
            page.meta_desc = json_a['meta_description']
        except:
            print 'meta_desc'
            pass

        try:
            page.meta_keywords = json_a['meta_keywords']
        except:
            print 'meta_keywords'
            pass

        try:
            page.pub_date = fromtimestamp(int(json_a['created']))
        except:
            print "pub_date"
            pass

        try:
            print type
            tag = Tag.objects.get(slug=type)
            page.tags.add(tag)
        except Exception, e:
            print Exception, e, 'tag'
            pass

        try:
            page.sites.add(get_current_site(request))
        except:
            print 'sites'
            pass

        # PHOTO
        try:
            if not page.photo:
                img_url = json_a["image"]
                name = urlparse(img_url).path.split('/')[-1]
                content = ContentFile(urllib2.urlopen(img_url).read())
                page.photo.save(name, content, save=True)
        except:
            a_p_error = 'main_photo'

        page.save()
        pages.append(page)

    return render_to_response('triton_ua/pages/import_products.html', {
        'data': pages,
    },
                              context_instance=RequestContext(request))


def import_dealers(request):
    json_download_link = 'http://www.3tn.ru/dataexport/dealers'
    json_file_name = 'media/docs/dealers.json'

    result = []

    # Create json file for this site
    data = urllib.urlopen(json_download_link)
    with open(location(json_file_name), 'w') as f:
        myfile = File(f)
        myfile.write(data.read())

    json_data = open(location(json_file_name))
    json_load = json.load(json_data)

    for item in json_load:
        json_d = json_load[item]

        try:
            site, create = Site.objects.get_or_create(domain=json_d['site'])
            site.name = json_d['site']
            site.save()

        except Exception, e:
            pass

        try:
            region, created = Region.objects.get_or_create(name=json_d['region'])

            if json_d['regionCoord']:
                region.map_coords = json_d['regionCoord']

            if json_d['regionZoom']:
                region.map_zoom = json_d['regionZoom']

            try:
                region.phone = json_d['regionPhone']
            except:
                result.append("%s phone" % json_d['nid'])

            try:
                region.site = json_d['regionUrl']
            except:
                result.append("%s site" % json_d['nid'])

            region.save()
        except Exception, e:
            result.append("%s not save" % json_d['nid'])
            pass

        try:
            city, region = City.objects.get_or_create(region=region, name=json_d['city'])
        except Exception, e:
            print 'region', json_d['nid']
            pass

        saleplace, created = SalePlace.objects.get_or_create(nid=json_d['nid'], city=city)
        #
        if json_d['site']:
            saleplace.site_url = json_d['site']
            try:
                saleplace.sites.add(site)
            except Exception, e:
                pass

        # saleplace.dealer = False

        saleplace.title = json_d['name']
        saleplace.body = json_d['body']
        if json_d['address']:
            saleplace.address = json_d['address']

        if json_d['coord']:
            saleplace.map_coords = json_d['coord']

        if json_d['zoom']:
            saleplace.map_zoom = json_d['zoom']

        if json_d['email']:
            saleplace.email = json_d['email']

        if json_d['phone']:
            saleplace.phone = json_d['phone']

        saleplace.save()

    return render_to_response('triton_ua/pages/import_products.html', {
        'data': "True",
    },
                              context_instance=RequestContext(request))


def import_comments(request):
    pass


def check_comments(request):
    json_download_link = 'http://www.3tn.ru/dataexport/comments'
    json_file_name = 'media/docs/comments.json'

    # Create json file for this site
    # data = urllib.urlopen(json_download_link)
    # with open(location(json_file_name), 'w') as f:
    #     myfile = File(f)
    #     myfile.write(data.read())

    json_data = open(location(json_file_name))
    json_load = json.load(json_data)
    # import_id = Comment.objects.all().aggregate(Max('import_id')) + 1

    none_comment_sum = 0
    comment_sum = 0
    questions_sum = 0

    for item in json_load:
        json_c = json_load[item]
        try:
            comment_node = Node.objects.get(nid=json_c['nid'])
        except Exception, e:
            comment_node = Node.objects.get(slug='homeless_comments')

        try:
            comment_user = UserProfile.objects.get(uid=json_c['uid']).user
            comment = Comment.objects.get(cid=json_c['cid'], user=comment_user)
            comment_sum += 1
            none_comment = False
        except Exception, e:
            none_comment = True
            pass

        try:
            comment_user = UserProfile.objects.get(uid=json_c['uid']).user
            comment_question = Question.objects.get(user=comment_user,
                                                    pub_date=fromtimestamp(
                                                        int(json_c['date'])),
                                                    body=json_c['body'].replace(
                                                        '/sites/all/libraries/tinymce/jscripts/tiny_mce/plugins/emotions/img/',
                                                        '/static/images/smiles/'))
            none_question = False
            questions_sum += 1
        except Exception, e:
            none_question = True
            pass

        if none_comment and none_question:
            none_comment_sum += 1

        print 'none_comment_sum: ', none_comment_sum

    print 'NONE COMMENT: ', none_comment_sum
    print 'COMMENTS: ', comment_sum
    print 'QUESTIONS: ', questions_sum
    print "LENGHT", len(json_load)

    return render_to_response('triton_ua/pages/import_products.html', {
        'data': "True",
    },
                              context_instance=RequestContext(request))


def import_forum(request):
    json_download_link = 'http://www.3tn.ru/dataexport/forums'
    json_file_name = 'media/docs/forum.json'

    # Create json file for this site
    data = urllib.urlopen(json_download_link)
    with open(location(json_file_name), 'w') as f:
        myfile = File(f)
        myfile.write(data.read())

    json_data = open(location(json_file_name))
    json_load = json.load(json_data)

    for item in json_load:
        json_f = json_load[item]
        try:
            comment_node = Node.objects.get(nid=json_f['nid'])
        # comment_user = UserProfile.objects.get(uid=json_c['uid']).user
        # comment, create = Comment.objects.get_or_create(cid=json_c['cid'], node=comment_node, user=comment_user)
        # comment.body = json_c['body']
        # comment.pub_date = fromtimestamp(int(json_c['date']))
        # comment.save()

        except Exception, e:
            print Exception, e
            pass

    return render_to_response('triton_ua/pages/import_products.html', {
        'data': "True",
    },
                              context_instance=RequestContext(request))


def set_comment_date(request):
    nodes = Node.objects.all()

    for node in nodes:
        try:
            last_comment = Comment.objects.filter(node=node).order_by("-pub_date")[0]
            node.last_comment_date = last_comment.pub_date
            node.save()
        except Exception, e:
            print Exception, e
            node.last_comment_date = node.pub_date
            node.save()

    return render_to_response('triton_ua/pages/import_products.html', {
        'data': "True",
    },
                              context_instance=RequestContext(request))


from functools import wraps


def mail_office_confirmation(func):
    return HttpResponse(
        "postoffice-958f6dbd9f8deb850d05b59c22857565521cb3eb30e851517fc2427f8c703045")


def youtube_confirmation(func):
    return HttpResponse("google-site-verification: google83988dd02d5012b9.html")


def current_site_or_404(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if get_current_site(args).id == kwargs["site_id"]:
            func(*args, **kwargs)
        else:
            return False

    return wrapper


# @cache_page(60 * 60)
# @current_site_or_404
def products(request, entity=None, cats=None, site_id=1):
    if PathSEO.objects.filter(sites=get_current_site(request), path=request.path).count() == 0:
        # pass
        raise Http404

    kwargs = {}
    current_site = SiteInfo.on_site.select_related().get()

    entity = ProductEntity.objects.get(slug=entity)
    kwargs['entity'] = entity
    # kwargs['action'] = False

    if cats and not 'clear' in request.GET:
        kwargs['category__slug__in'] = cats

    products_cache_cats = "none"
    if 'cats' in request.GET and not 'clear' in request.GET:
        cats = request.GET['cats'].split(',')
        products_cache_cats = request.GET['cats']
        if Category.objects.filter(slug__in=cats).count() > 0:
            kwargs['category__slug__in'] = cats
        else:
            cats = False

    return_page = cache.get('products::%s::%s::%s::is_shop_%s' % (current_site.site.domain, entity.slug, products_cache_cats, current_site.is_shop))
    if return_page and not request.user.is_staff:
        return return_page

    products_all = cache.get('products_all:%s:%s::is_shop_%s' % (entity.slug, current_site.site.domain, current_site.is_shop,))
    if products_all is None:
        products_all = Product.on_site.filter(entity=entity, publish=True, price__gt="0")
        cache.set('products_all:%s:%s::is_shop_%s' % (entity.slug, current_site.site.domain, current_site.is_shop,), products_all)

    products = cache.get('products:%s:%s:%s::is_shop_%s' % (entity.slug, current_site.site.domain, cats, current_site.is_shop))

    if products is None:
        if entity.slug == 'mebel':
            products = Product.on_site.filter(**kwargs).order_by('position', 'title', 'width')
        elif entity.slug == 'bath' or entity.slug == 'bathcab':
            products = Product.on_site.filter(**kwargs).order_by('position', 'length')
        else:
            products = products_all.filter(**kwargs)

        cache.set('products:%s:%s:%s::is_shop_%s' % (entity.slug, current_site.site.domain, cats, current_site.is_shop), products)

    products_cat = products_all.values_list('category', flat=True)
    product_categories_ids = ProductCategory.objects.filter(id__in=set(products_cat)).values_list(
        'category_type', flat=True)

    categories_types = CategoryType.objects.filter(id__in=set(product_categories_ids))

    request.breadcrumbs([
        (_('%s') % ('Продукция'), Menu.on_site.get(slug="production").link),
    ])

    filter_form = BathFilterForm()
    # if entity.slug == 'bath':
    #     filter_form = BathFilterForm()

    is_action = False

    for product in products:
        if product.action:
            is_action = True
            options_session_name = "%d_options" % product.id

            options = []
            for option in product.optionprices.filter(action=True):
                if (not option.id in options):
                    options.append(option.id)
            request.session[options_session_name] = options
            product.cart_session, product.cart, product.cart_all_sum_end, product.discount, product.discount_value, product.discount_cart_sum = get_price(request, (product.id,))

    if cats:
        cache_cats = " ".join(cats)
    else:
        cache_cats = "none"

    if 'clear' in request.GET:
        template = 'products/list.html'
    else:
        template = 'oblaka/pages/products.html'

    return_page = render_to_response(template, {
        'products': products,
        'entity': entity,
        'categories_types': categories_types,
        'cats': cats,
        'cache_cats': cache_cats,
        'filter_form': filter_form,
        'is_action': is_action,
        'clear': 'clear_%s' % str('clear' in request.GET),

    },
                                     context_instance=RequestContext(request))

    # if not request.user.is_staff:
    #     cache.set('products::%s::%s::%s' % (current_site.domain, entity.slug, products_cache_cats), return_page)
    return return_page


# @cache_page(60 * 60)
# @current_site_or_404
def action_products(request, entity=None, cats=None, site_id=1):
    if PathSEO.objects.filter(sites=get_current_site(request), path=request.path).count() == 0:
        # pass
        raise Http404

    kwargs = {}
    current_site = SiteInfo.on_site.select_related().get()

    if entity:
        entity = ProductEntity.objects.get(slug=entity)
        kwargs['entity'] = entity

    kwargs['action'] = True

    products = cache.get('products_all:%s:%s::is_shop_%s' % ("action", current_site.site.domain, current_site.is_shop,))

    if products is None:
        products = Product.on_site.filter(**kwargs)
        cache.set('products_all:%s:%s::is_shop_%s' % ("action", current_site.site.domain, current_site.is_shop,), products)

    request.breadcrumbs([
        (_('%s') % ('Aкции'), Menu.on_site.get(slug="production").link),
    ])

    for product in products:
        options_session_name = "%d_options" % product.id

        options = []
        for option in product.optionprices.all():
            if (not option.id in options):
                options.append(option.id)
        request.session[options_session_name] = options
        product.cart_session, product.cart, product.cart_all_sum_end, product.discount, product.discount_value, product.discount_cart_sum = get_price(request, (product.id,))

    return render_to_response('triton_ua/pages/action_products.html', {
        'products': products,
    },
                              context_instance=RequestContext(request))


# @cache_page(60 * 15)
def components(request):
    components = SEONode.on_site.filter(node_type=NodeType.objects.get(slug='component'))

    return render_to_response('triton_ua/pages/components.html', {
        'components': components,
    },
                              context_instance=RequestContext(request))


def advices(request):
    advices = Page.on_site.filter(tags__slug='advices').order_by('-pub_date')

    return render_to_response('triton_ua/pages/list.html', {
        'items': advices,
        'title': 'Советы покупателям',
    },
                              context_instance=RequestContext(request))


def dealers(request):
    dealers = Dealer.objects.all()

    request.breadcrumbs([
        (_('%s') % ('Оптовикам'), '/opt.php'),
    ])

    return render_to_response('triton_ua/pages/dealers.html', {
        'dealers': dealers,
    },
                              context_instance=RequestContext(request))


def get_dealer_price(request):
    message = False

    price_form = GetPriceForm()

    logging.debug(price_form)

    if request.method == 'POST':
        price_form = GetPriceForm(request.POST)

        if price_form.is_valid():
            mail = DaMailer.send_mail_to_super_admin('da_mailer/admin_get_dealer_price.mail.html',
                                                     subject='Запрос на прайс для дилера',
                                                     data=price_form.clean())
            message = u"Спасибо за заявку. <brbr />В ближайшее время наш менеджер свяжется с вами."
            price_form = GetPriceForm(initial={
                'region': u'Москва и Московская область',
            })
        else:
            pass
            # message = price_form.errors

    # price_form.fields['region'].queryset = Region.objects.all()

    request.breadcrumbs([
        (_('%s') % ('Оптовикам'), '/opt.php'),
    ])

    return render_to_response('triton_ua/pages/get_dealer_price.html', {
        'price_form': price_form,
        'message': message,
    },
                              context_instance=RequestContext(request))


# def actions(request):
#     advices = Page.on_site.filter(tags__slug='actions')
#
#     return render_to_response('oblaka/pages/list.html', {
#         'items': advices,
#         'title': 'Акции'
#     },
#                               context_instance=RequestContext(request))

# @cache_page(60 * 60)
def site_structure(request, site_id=1):
    # if not get_current_site(request).id == site_id:
    #     raise Http404

    if PathSEO.objects.filter(sites=get_current_site(request), path=request.path).count() == 0:
        raise Http404

    pages = Page.on_site.filter(dot_slug__gt='').exclude(node_type__slug='fake_page').order_by(
        'node_type')
    products = Product.on_site.all().order_by('-entity', )

    baths = Product.on_site.filter(entity__slug='bath')
    mebel = Product.on_site.filter(entity__slug='mebel')
    bathcabs = Product.on_site.filter(entity__slug='bathcab')

    questions = Question.on_site.all()

    items = sorted(
        chain(pages, products),
        key=attrgetter('node_type'))

    return render_to_response('triton_ua/pages/site_structure.html', {
        'items': items,
        'products': products,
        'baths': sorted(baths, key=get_form),
        'mebel': sorted(mebel, key=get_type),
        'bathcabs': bathcabs,
        'pages': pages,
        'questions': questions,
        'title': u'Структура сайта'
    },
                              context_instance=RequestContext(request))


def get_form(item):
    '''
    Get form slug of product
    '''
    form = ''
    for category in item.category.all():
        if category.category_type.slug == 'form':
            form = category.slug

    return form


def get_type(item):
    '''
    Get form slug of product
    '''
    form = ''
    for category in item.category.all():
        if category.category_type.slug == 'type':
            form = category.slug

    return form


#  TODO: Delete
# def actions_callbacks(request):
#     start, end = helper.get_start_end_pages(NEWS_PER_PAGE, request)
#
#     actions_callbacks = Page.on_site.filter(node_type__slug__in=['action_callback']).order_by('-pub_date')[start:end]
#
#     if request.GET.get('page'):
#         response_url = 'oblaka/pages/helper/clear_list.html'
#     else:
#         response_url = 'oblaka/pages/list.html'
#
#     return render_to_response(response_url, {
#         'items': actions_callbacks,
#         'title': 'Отзывы об акциях',
#     },
#                               context_instance=RequestContext(request))



def contacts(request):
    return render_to_response('triton_ua/pages/contacts.html', {
    },
                              context_instance=RequestContext(request))


def post_comment(request, new_comment):
    if request.session.get('has_commented', False):
        return HttpResponse("You've already commented.")

    # TODO: What is comments there?
    c = Comment(comment=new_comment)
    c.save()

    request.session['has_commented'] = True

    return HttpResponse('Thanks for your comment!')


def news_detail(request, slug):
    '''
	Одна новость
	'''
    news = News.on_site.get(dot_slug=slug)

    return render_to_response('triton_ua/pages/page.html', {
        'page': news,
    },
                              context_instance=RequestContext(request))


# @cache_page(60 * 15)
def list(request, slug_in="", site_id=1, page=1):
    '''
	Список новостей (постарничный)
	'''

    if PathSEO.objects.filter(sites=get_current_site(request), path=request.path).count() == 0:
        raise Http404

    start, end = helper.get_start_end_pages(THREE_PER_PAGE, request)

    items = Page.on_site.filter(node_type__slug__in=slug_in).order_by('-pub_date')[start:end]

    if not items:
        return HttpResponse("end")

    if request.GET.get('page'):
        response_url = 'oblaka/magazine/list.html'
    else:
        response_url = 'oblaka/pages/three_list.html'

    return render_to_response(response_url, {
        'items': items,
        'next_page': page + 1,
    },
                              context_instance=RequestContext(request))


# @cache_page(60 * 15)
def magazine_list(request, slug_in=""):
    '''
	List like magazine
	'''

    start, end = helper.get_start_end_pages(THREE_PER_PAGE, request)

    items = Page.on_site.filter(node_type__slug__in=slug_in).order_by('-pub_date')[start:end]

    if not items:
        return HttpResponse("end")

    if request.GET.get('page'):
        response_url = 'oblaka/magazine/list.html'
    else:
        response_url = 'oblaka/magazine/part.html'

    return render_to_response(response_url, {
        'items': items
    },
                              context_instance=RequestContext(request))


# @staff_member_required
def magazine(request, slug=None):
    '''
	Magazine
	'''

    if not get_current_site(request).id in (1, 86):
        raise Http404

    current_site_info = SiteInfo.on_site.select_related().get()
    current_site = current_site_info.site

    current_article = None

    solution_order = None
    order_solutions = None

    request.breadcrumbs([
        (_('%s') % ('Журнал'), '/magazine'),
    ])

    start, end = helper.get_start_end_pages(THREE_PER_PAGE, request)

    if not slug:
        # Last articles cache
        last_article = cache.get('last_article::magazine')
        if last_article is None:
            last_article = Page.on_site.filter(node_type__slug__in=('advices', 'article', 'news'), publish=True).order_by('-pub_date')[:1].get()
            cache.set('last_article::magazine', last_article)

        # Advices with cache
        advices = cache.get('advices::magazine')
        if advices is None:
            advices = Page.on_site.filter(node_type__slug__in=('advices',), publish=True).order_by('-pub_date')[:3]
            cache.set('advices::magazine', advices)

        # People design with cache
        people_design = cache.get('people_design::magazine')
        if people_design is None:
            people_design = Page.on_site.filter(node_type__slug__in=('article',), tags__slug__in=('people_design',), publish=True).order_by('-pub_date')[:3]
            cache.set('people_design::magazine', people_design)

        # Home with cache
        home = cache.get('home::magazine')
        if home is None:
            home = Page.on_site.filter(node_type__slug__in=('article',), tags__slug__in=('home_sweet_home',), publish=True).order_by('-pub_date')[:3]
            cache.set('home::magazine', home)

        # Solutions with cache
        solutions = cache.get('solutions::magazine')
        if solutions is None:
            solutions = Page.on_site.filter(tags__slug__in=('solutions',), publish=True).order_by('-pub_date')[:3]
            cache.set('solutions::magazine', solutions)

        # Solutions with cache
        news = cache.get('news::magazine')
        if news is None:
            news = Page.on_site.filter(node_type__slug__in=('news',), publish=True).order_by('-pub_date')[:3]
            cache.set('news::magazine', news)

        # Actions with cache
        actions = cache.get('actions::magazine')
        if actions is None:
            actions = Page.on_site.filter(node_type__slug__in=('action',)).order_by('-pub_date')[:3]
            cache.set('actions::magazine', actions)
    else:
        last_article = None
        advices = None
        people_design = None
        home = None
        solutions = None
        news = None
        actions = None

    rewrite_main_menu = Menu.on_site.filter(menu_group__slug='magazine_menu', publish=1)

    try:
        current_article = SEONode.objects.get(dot_slug=slug)
        try:
            solution_order_id = current_article.related_nodes.all()[:1].get()

            # solution_order = cache.get('solution_order::%s' % (solution_order_id, ))
            # order_solutions = cache.get('order_solutions::%s' % (solution_order_id, ))

            # if solution_order is None:
            solution_order = SolutionOrder.objects.get(id=solution_order_id.id)
            order_solutions = Page.objects.filter(~Q(id=current_article.id),
                                                  tags__slug__in=('solutions',),
                                                  related_nodes=solution_order,
                                                  publish=True).order_by('-pub_date')

            # cache.set('solution_order::%s' % (solution_order_id, ), solution_order)
            # cache.set('order_solutions::%s' % (solution_order_id, ), order_solutions)

        except Exception, e:
            print Exception, e
    except Exception, e:
        print Exception, e

    try:
        current_article = SolutionOrder.objects.get(dot_slug=slug)
        order_solutions = Page.objects.filter(tags__slug__in=('solutions',),
                                              related_nodes=current_article,
                                              publish=True).order_by('-pub_date')
    except:
        pass

    request.main_menu_name = 'magazine_menu'

    solution_order_form = SolutionOrderForm()
    comment_form = CommentForm()

    try:
        related_products = cache.get('related_products::magazine::%s::%s' % (current_article.id, current_site.domain))
        if related_products is None:
            # related_products = None
            related_products = Product.on_site.filter(nid__in=current_article.related_products.split(','))
            cache.set('related_products::magazine::%s::%s' % (current_article.id, current_site.domain), related_products)
    except:
        related_products = None

    print current_article

    return_page = render_to_response('triton_ua/magazine/main.html', {
        'last_article': last_article,
        'advices': advices,
        'current_article': current_article,
        'related_products': related_products,
        'home': home,
        'news': news,
        'solutions': solutions,
        'people_design': people_design,
        'solution_order_form': solution_order_form,
        'comment_form': comment_form,
        'rewrite_main_menu': rewrite_main_menu,
        'actions': actions,
        'solution_order': solution_order,
        'order_solutions': order_solutions,
    },
                                     context_instance=RequestContext(request))

    if not request.user.is_staff:
        cache.set('page::%s::%s::mobile_%s' % (current_site.domain, slug, request.user_agent.is_mobile), return_page)

    return return_page


# @staff_member_required
def solution_order(request, slug=None):
    '''
	Magazine
	'''

    current_site_info = SiteInfo.on_site.select_related().get()
    current_site = current_site_info.site

    request.breadcrumbs([
        (_('%s') % ('Журнал'), '/magazine'),
    ])

    start, end = helper.get_start_end_pages(THREE_PER_PAGE, request)

    last_article = Page.on_site.filter(
        node_type__slug__in=('advices', 'article', 'news')).order_by('-pub_date')[:1].get()
    advices = Page.on_site.filter(node_type__slug__in=('advices',)).order_by('-pub_date')[:3]
    people_design = Page.on_site.filter(node_type__slug__in=('article',),
                                        tags__slug__in=('people_design',)).order_by('-pub_date')[
                    :3]
    home = Page.on_site.filter(node_type__slug__in=('article',),
                               tags__slug__in=('home_sweet_home',)).order_by('-pub_date')[:3]
    solutions = Page.on_site.filter(tags__slug__in=('solutions',)).order_by('-pub_date')[:3]
    news = Page.on_site.filter(node_type__slug__in=('news',)).order_by('-pub_date')[:3]
    actions = Page.on_site.filter(node_type__slug__in=('action',)).order_by('-pub_date')[:3]

    rewrite_main_menu = Menu.on_site.filter(menu_group__slug='magazine_menu', publish=1)

    try:
        current_article = SolutionOrder.objects.get(dot_slug=slug)
    except:
        current_article = None

    request.main_menu_name = 'magazine_menu'

    solution_order_form = SolutionOrderForm()
    comment_form = CommentForm()

    return_page = render_to_response('triton_ua/magazine/main.html', {
        'last_article': last_article,
        'advices': advices,
        'current_article': current_article,
        'home': home,
        'news': news,
        'solutions': solutions,
        'people_design': people_design,
        'solution_order_form': solution_order_form,
        'comment_form': comment_form,
        'rewrite_main_menu': rewrite_main_menu,
        'actions': actions,
    },
                                     context_instance=RequestContext(request))

    if not request.user.is_staff:
        cache.set('page::%s::%s::mobile_%s' % (current_site.domain, slug), return_page)

    return return_page


# @staff_member_required
def magazine_part(request, part_slug):
    if not get_current_site(request).id in (1, 86):
        raise Http404

    request.breadcrumbs([
        (_('%s') % ('Журнал'), '/magazine'),
    ])

    start, end = helper.get_start_end_pages(THREE_PER_PAGE, request)
    last_item = None
    response_url = 'oblaka/magazine/part.html'

    try:
        part = NodeType.objects.get(slug=part_slug)
        # last_article = Page.objects.filter(node_type__slug=part_slug).order_by('-pub_date')[:1].get()
        items = SEONode.on_site.filter(node_type__slug=part_slug, publish=True).order_by('-pub_date')[start:end]
        print items.count()
    except:
        part = Tag.objects.get(slug=part_slug)
        # last_article = Page.objects.filter(node_type__slug='article', tags__slug=part_slug).order_by('-pub_date')[:1].get()
        items = SEONode.on_site.filter(
            node_type__slug__in=('article', 'advices', 'solution_order'), tags__slug=part_slug,
            publish=True).order_by('-pub_date')[start:end]

    if part_slug in ('solutions', 'people_design', 'solution_order'):
        author = True
    else:
        author = False

    if request.GET.get('page'):
        response_url = 'oblaka/magazine/list.html'
    else:
        response_url = 'oblaka/magazine/part.html'
        last_item = items[:1].get()

    solution_order_form = SolutionOrderForm()
    comment_form = CommentForm()

    rewrite_main_menu = Menu.on_site.filter(menu_group__slug='magazine_menu', publish=1)

    return render_to_response(response_url, {
        'part': part,
        'last_item': last_item,
        'items': items,
        'solution_order_form': solution_order_form,
        'comment_form': comment_form,
        'author': author,
        'rewrite_main_menu': rewrite_main_menu,
    },
                              context_instance=RequestContext(request))


# @staff_member_required
def magazine_article(request, id):
    '''
	Magazine
	'''

    request.breadcrumbs([
        (_('%s') % ('Журнал'), '/magazine'),
    ])

    if id == '0':
        article = Page.on_site.get(dot_slug='create_new')
        editable = True
    else:
        article = Page.on_site.get(id=id)
        editable = False

    comment_form = CommentForm()
    solution_order_form = SolutionOrderForm()

    related_products = Product.objects.select_related().filter(id__ib=article.related_products.split())

    return render_to_response('triton_ua/magazine/article.html', {
        'editable': editable,
        'item': article,
        'related_products': related_products,
        'width': "1120",
        'height': "500",
        'geometry': "1120x500",
        'with_body': 'True',
        'comment_form': comment_form,
        'solution_order_form': solution_order_form,
    },
                              context_instance=RequestContext(request))


# @staff_member_required
def save_article(request, id=None):
    '''
	Save article
	'''

    result = False

    if request.method == 'POST':
        article_fields = json.loads(request.body)
        print article_fields['title']

        if id:
            article = Page.objects.get(id=id)

            article.title = str(article_fields['title'])
            article.teaser = article_fields['teaser']
            article.body = article_fields['body']
            article.user = request.user
        else:
            article = Page(
                title=article_fields['title'],
                teaser=article_fields['teaser'],
                body=article_fields['body'],
                node_type=NodeType.objects.get(slug='article'),
                user=request.user
            )

        article.save()
        article.tags.add(Tag.objects.get(slug='people_design'))

        result = True

    return HttpResponse(result)


# @staff_member_required
def send_solution_order(request):
    '''
	Save solution order
	'''
    result = False

    if request.method == 'POST':
        order_form = SolutionOrderForm(json.loads(request.body))
        order_form_json = json.loads(request.body)

        if order_form.is_valid():
            order = order_form.save()
            order.sites.add(Site.objects.get(domain="www.3tn.ru"))
            order.sites.add(Site.objects.get(domain="www.triton3tn.ru"))

            for index, photo_url in enumerate(order_form_json["additionalphoto"]):
                photo = AdditionalPhoto(node=order, photo=photo_url.replace('/media/', ''))
                order.additionalphoto_set.add(photo)

            DaMailer.send_mail_to_addresses(template='oblaka/email/solution_order.email.html',
                                            subject=u'Новый заказ на готовое решение', data=order,
                                            emails=['hramik@gmail.com', 'elryzhkova@gmail.com',
                                                    "nomatter@3tn.org"])
            result = True
        else:
            print order_form.errors

    print result
    return HttpResponse(result)


def question(request, id):
    '''
	Один вопрос
	'''
    kwargs = {}
    user = request.user

    kwargs['id'] = id

    if not user.is_staff:
        kwargs["publish"] = True

    try:
        question = Question.on_site.get(**kwargs)
    except Exception, e:
        return render_to_response('triton_ua/pages/not_permission.html', {
        },
                                  context_instance=RequestContext(request))

    if request.user.is_authenticated():
        question_form = QuestionForm()
        comment_form = CommentForm()
    else:
        question_form = AnonimousQuestionForm()
        comment_form = AnonimousCommentForm()

    questions_path = PathSEO.on_site.get(slug='questions').path

    request.breadcrumbs([
        (_('%s') % ('Форум'), questions_path),
    ])

    return render_to_response('questions/pages/question.html', {
        'single_page': True,
        'question': question,
        'questions_path': questions_path,
        'question_form': question_form,
        'comment_form': comment_form,
    },
                              context_instance=RequestContext(request))


def homeless_comments(request, tag=False, page=1):
    '''
	Comments without question
	'''
    questions = Question.objects.filter(slug='homeless_comments').order_by('id')
    last_questions = Question.objects.all().order_by('-id')[0:10]

    return render_to_response('questions/pages/homeless_questions.html', {
        'questions': questions,
        'last_questions': last_questions,
    },
                              context_instance=RequestContext(request))


def questions(request, tag=False, page=1):
    '''
	Вопросы-ответы (с продукцией)
	'''

    current_site = SiteInfo.on_site.select_related().get()

    if PathSEO.objects.filter(sites=get_current_site(request), path=request.path).count() == 0:
        raise Http404

    kwargs = {}
    user = request.user

    if not user.is_staff:
        kwargs["publish"] = True

    # kwargs["node_type__slug__in"] = ['product', 'question']
    kwargs["dot_slug__gt"] = ''

    tag = request.GET.get('tag')

    if request.GET.get('comments'):
        kwargs["last_comment_date"] = F('pub_date')
        kwargs["node_type__slug__in"] = ['question']

    if tag:
        if tag == 'my':
            kwargs["user"] = user
            current_tag = False
        elif tag == 'unpubliched':
            current_tag = False
        elif tag == 'uncommented':
            current_tag = False
        else:
            kwargs["tags__slug"] = tag
            current_tag = Tag.objects.get(slug=tag)
    else:
        current_tag = False

    # Сортировка по параметру
    order_by = '-last_comment_date'
    if request.method == 'GET' and request.GET.get('order_by'):
        order_by = request.GET.get('order_by')

    # Nodes
    start, end = helper.get_start_end_pages(QUESTIONS_PER_PAGE, request)
    nodes = Node.objects.filter(**kwargs).exclude(slug='homeless_comments').exclude(
        node_type__slug__in=['solution_order', 'solution', 'article', 'project', 'product',
                             'fake_page', 'advices', 'news', 'static_page'],
        last_comment_date=F('pub_date')).order_by(order_by)[start:end]
    print nodes

    if tag == 'unpubliched':
        nodes = Node.objects.filter(**kwargs).filter(publish=False).exclude(
            slug='homeless_comments')

    if tag == 'uncommented':
        nodes = Node.objects.filter(node_type__slug__in=('question',), last_comment_date=F('pub_date')).exclude(pub_date__lt=datetime(2014, 1, 1)).exclude(slug='homeless_comments')[start:end]

    unpublished_nodes = 0
    uncommented_nodes = 0
    if not request.GET.get('page'):
        kwargs["node_type__slug__in"] = ['question']
        # kwargs["pub_date"]
        all_nodes = Node.objects.filter(**kwargs).exclude(slug='homeless_comments')
        unpublished_nodes = all_nodes.filter(publish=False)
        uncommented_nodes = all_nodes.filter(last_comment_date=F('pub_date')).exclude(pub_date__lt=datetime(2014, 1, 1))

    # Tags
    tags = Tag.showed_objects.all()
    all_tags = Tag.objects.all()

    if 'tag' in current_site.json:
        site_tags = Tag.objects.filter(name__in=(current_site.site.domain, current_site.json['tag']))
    else:
        site_tags = Tag.objects.filter(name__in=(current_site.site.domain,))

    if request.user.is_authenticated():
        question_form = QuestionForm()
        comment_form = CommentForm()
    else:
        question_form = QuestionForm()
        comment_form = False

    # Last questions for manage homeless questions
    last_questions = Question.objects.all().order_by('-id')[0:10]

    if not nodes:
        return HttpResponse("end")

    # Choose response url for page or ajax callback
    if request.GET.get('page'):
        response_url = 'questions/parts/questions_view.html'
    else:
        response_url = 'questions/pages/questions.html'

    return render_to_response(response_url, {
        'questions': nodes,
        'question_form': question_form,
        'comment_form': comment_form,
        'tags': tags,
        'all_tags': all_tags,
        'site_tags': site_tags,
        'current_tag': current_tag,
        'tag': tag,
        'next_page': page + 1,
        'last_questions': last_questions,
        'unpublished_nodes': unpublished_nodes,
        'uncommented_nodes': uncommented_nodes,
    },
                              context_instance=RequestContext(request))


@api_view(['GET'])
def hello_world(request):
    return Response({"message": "Hello, world!"})


class OrderDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """

    def get_object(self, pk):
        try:
            return Order.objects.get(pk=pk)
        except Order.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        order = self.get_object(pk)
        serializer = OrdersSerializer(order)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        task = self.get_object(pk)

        try:
            task.title = request.data['title']
        except Exception, e:
            print Exception, e

        try:
            task.body = request.data['body']
        except Exception, e:
            print Exception, e

        try:
            task.project = Project.objects.get(id=request.data['project_id'])
        except Exception, e:
            print Exception, e

        try:
            task.status = request.data['status']
        except Exception, e:
            print Exception, e

        try:
            task.priority = request.data['priority']
        except Exception, e:
            print Exception, e

        try:
            task.start_date = request.data['start_date']
        except Exception, e:
            print Exception, e

        try:
            task.end_date = request.data['end_date']
        except:
            pass

        try:
            task.control = request.data['control']
        except:
            pass

        task.save()

        task.performers.clear()
        for performer_id in request.data['performers_ids']:
            try:
                task.performers.add(UserProfile.objects.get(user__id=performer_id))
            except Exception, e:
                print Exception, e

        serializer = TasksSerializer(task, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        task = self.get_object(pk)
        task.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class OrdersViewSet(viewsets.ModelViewSet):
    serializer_class = OrdersSerializer

    def get_queryset(self):
        kwargs = {}

        # queryset = Order.objects.all()

        kwargs['status'] = self.request.query_params.get('status', None)
        print kwargs['status']
        queryset = Order.objects.filter(**kwargs)

        return queryset


class questionViewSet(viewsets.ModelViewSet):
    queryset = Question.on_site.filter(publish=True).order_by('-pub_date')
    serializer_class = QuestionSerializer


# paginate_by = 5



class additionalPhotoViewSet(viewsets.ModelViewSet):
    queryset = AdditionalPhoto.objects.all()
    serializer_class = AdditioanalPhotoSerializer
    paginate_by = 5


class OrderItemViewSet(viewsets.ModelViewSet):
    queryset = OrderItem.objects.all()[:5]
    serializer_class = OrderItemSerializer
    paginate_by = 10


class questionList(generics.ListCreateAPIView):
    queryset = Question.on_site.filter(publish=True).order_by('-pub_date')[:5]
    serializer_class = QuestionSerializer
    paginate_by = 5


def handle_uploaded_file(file, filedest):
    with open(filedest, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)


# @csrf_protect
def image_upload(request):
    if request.method == 'POST':
        print location("media/upload_files")
        try:
            os.mkdir(location("media/upload_files"))
        # result=location("media/upload_files")
        except Exception, e:
            result = "dir"

        try:
            # filedest = os.path.abspath(os.path.dirname(__name__))+'/media/upload_files/'+request.FILES['file'].name
            new_image_name = datetime.now().strftime("%Y-%m-%d-%H-%M-%S") + str(
                random.randint(100, 999)) + \
                             os.path.splitext(request.FILES['file'].name)[1]
            print new_image_name

            filedest = location("media/upload_files/" + new_image_name)
            result_filedest = '/media/upload_files/' + new_image_name
            print filedest
            handle_uploaded_file(request.FILES['file'], filedest)
            result = result_filedest
        except Exception, e:
            # result='file'
            print Exception, e
            pass

    return HttpResponse(result)


# @csrf_protect
def send_question(request):
    result = False

    if request.method == 'POST':
        question_form = QuestionForm(json.loads(request.body))

        if question_form.is_valid():
            question_j = json.loads(request.body)
            c_form = question_form.save(commit=False)
            if request.user.is_authenticated():
                c_form.user = request.user
                c_form.edit_user = request.user.username
            else:
                c_form.user = None
                c_form.edit_user = "none"

            c_form.publish = False
            # c_form.send_do_subscribe = True
            c_form.save()
            c_form.dot_slug = "question-%s" % c_form.id

            body = strip_tags(c_form.body).replace('&nbsp;', ' ')
            c_form.title = body[:300]
            c_form.h1 = c_form.title
            c_form.page_title = c_form.title

            c_form.save()
            c_form.sites.add(get_current_site(request))

            for photo_url in question_j["additionalphoto"]:
                print photo_url
                photo = AdditionalPhoto(node=c_form, photo=photo_url.replace('/media/', ''))
                c_form.additionalphoto_set.add(photo)

            # mail = DaMailer.send_mail_to_admin('new_question', data=c_form)

            questions = Question.on_site.filter(id=c_form.id)

        if request.user.is_authenticated():
            comment_form = CommentForm()

            return render_to_response('questions/parts/questions_view.html', {
                'questions': questions,
                'comment_form': comment_form
                # 'tags': tags,
                # 'current_tag': current_tag
            },
                                      context_instance=RequestContext(request))
        else:
            comment_form = AnonimousCommentForm()

            request.session["last_question"] = c_form.id
            response_data = {}
            response_data['user'] = 'anonym'
            response_data['qid'] = c_form.id
            return HttpResponse(json.dumps(response_data), content_type="application/json")


# @csrf_protect
def send_que(request):
    result = False

    question_form = QuestionForm(request.GET)

    c_form = question_form.save(commit=False)
    c_form.sites = get_current_site(request)
    if request.user.is_authenticated():
        c_form.user = request.user
        c_form.edit_user = request.user.username
    else:
        c_form.publish = False
        c_form.user = None
        c_form.edit_user = "none"

    # c_form.send_do_subscribe = True
    c_form.save()
    c_form.dot_slug = "question-%s" % c_form.id

    body = strip_tags(c_form.body).replace('&nbsp;', ' ')
    c_form.title = body[:300]
    c_form.h1 = c_form.title
    c_form.page_title = c_form.title

    c_form.save()

    # # for photo_url in question_j["additionalphoto"]:
    # #     print photo_url
    # #     photo = AdditionalPhoto(node=c_form, photo=photo_url.replace('/media/', ''))
    # #     c_form.additionalphoto_set.add(photo)
    #
    # mail = DaMailer.send_mail_to_admin('new_question', data=c_form)

    questions = Question.on_site.filter(id=c_form.id)

    comment_form = CommentForm()

    return render_to_response('triton_ua/pages/empty.html', {
        # 'questions': questions,
        # 'comment_form': comment_form
        # 'tags': tags,
        # 'current_tag': current_tag
    },
                              context_instance=RequestContext(request))


def get_price(request, cart_session=None):
    '''
    Count common price (products + options)
    :param request:
    :return:
    '''
    current_site = SiteInfo.on_site.select_related().get()

    if cart_session == None:
        cart_session = request.session["cart"]

    cart = Product.on_site.filter(id__in=cart_session)
    cart_sum = Product.on_site.filter(id__in=cart_session).aggregate(Sum('price'))

    cart_sum_end = int(cart_sum['price__sum'])
    discount, discount_value, discount_cart_sum = 0, 0, 0

    try:
        for cart_item in cart:
            try:
                options_session = request.session[str(cart_item.id) + "_options"]
                print 'options_session', options_session

                options_sum = OptionPrice.objects.filter(id__in=options_session,
                                                         product__id__in=cart_session).aggregate(
                    Sum('price'))
                cart_sum_end = int(cart_sum_end) + int(options_sum['price__sum'])

            except Exception, e:
                print Exception, e

        discount = 0
        agree_make_review = 'false'

        #  COUNT DISCOUNT FOR SIMPLE PRODUCTS
        if ('not_show_discount' in current_site.json and current_site.json['not_show_discount']) or cart_item.action:
            pass
        else:
            if cart_sum_end > 20000:
                discount = 2

            if cart_sum_end > 45000:
                discount = 4

            try:
                if request.session["agree_make_review"] == True:
                    discount += 1
                    agree_make_review = 'true'
            except Exception, e:
                print Exception, e

        # DISCOUNT FOR ACTION PRODUCT
        if cart_item.action:
            discount = cart_item.discount

        discount_value = cart_sum_end / 100 * discount
        discount_cart_sum = cart_sum_end - discount_value
    except:
        pass

    return cart_session, cart, cart_sum_end, discount, discount_value, discount_cart_sum


def get_order_price(request, order):
    '''
    Count order price (products + options)
    :param request:
    :return:
    '''

    current_site = SiteInfo.on_site.select_related().get()

    # cart_sum = order.order_items.aggregate(Sum('price'))
    # cart_sum_end = int(cart_sum['price__sum'])
    cart_sum_end = 0
    discount, discount_value, discount_cart_sum = 0, 0, 0

    try:
        for order_item in order.order_items.all():
            options_price = 0
            try:
                options_price = order_item.order_item_options.aggregate(Sum('price'))['price__sum']
                if not options_price:
                    options_price = 0
            except Exception, e:
                print Exception, e

            product_with_options_price = (int(order_item.price) + int(options_price)) * order_item.count
            cart_sum_end = int(cart_sum_end) + int(product_with_options_price)

        discount = int(order.discount)
        agree_make_review = 'false'

        #  COUNT DISCOUNT FOR SIMPLE PRODUCTS
        # if 'not_show_discount' in current_site.json and current_site.json['not_show_discount']:
        #     pass
        # else:
        #     if cart_sum_end > 20000:
        #         discount = 2
        #
        #     if cart_sum_end > 45000:
        #         discount = 4

        # try:
        #     if request.session["agree_make_review"] == True:
        #         discount += 1
        #         agree_make_review = 'true'
        # except Exception, e:
        #     print Exception, e


        discount_value = cart_sum_end / 100 * discount
        discount_cart_sum = cart_sum_end - discount_value

        if not order.discount == discount:
            order.price = discount_cart_sum
            order.discount = discount
            order.cart_sum_end = cart_sum_end
            order.discount_value = discount_value
            order.save()

    except Exception, e:
        print Exception, e

    return cart_sum_end, discount, discount_value, discount_cart_sum, order


def add_comment_like(request):
    if request.method == 'POST':
        vars = json.loads(request.body)
        comment_like, create = CommentLike.objects.get_or_create(user=request.user,
                                                                 comment=Comment.objects.get(
                                                                     id=vars["comment_id"]))
    return HttpResponse(create)


def remove_comment_like(request):
    if request.method == 'POST':
        vars = json.loads(request.body)
        CommentLike.objects.get(user=request.user,
                                comment=Comment.objects.get(id=vars["comment_id"])).delete()
    return HttpResponse("done")


def comment_to_question(request):
    if request.method == 'POST':
        vars = json.loads(request.body)
        current_site = get_current_site(request)

        comment = Comment.objects.get(id=vars["comment_id"])
        question = Question(title=comment.title, body=comment.body, user=comment.user,
                            pub_date=comment.pub_date,
                            publish=True, sites=current_site)
        question.save()
        comment.delete()

    questions = Question.objects.filter(id=question.id)

    tags = Tag.objects.all()

    if request.user.is_authenticated():
        comment_form = CommentForm()
    else:
        comment_form = AnonimousCommentForm()

    return render_to_response('questions/parts/simple_question_base.html', {
        'question': question,
        'comment_form': comment_form,
        'tags': tags,
    },
                              context_instance=RequestContext(request))


def comment_transfer(request):
    if request.method == 'POST':
        vars = json.loads(request.body)

        comment = Comment.objects.get(id=vars["comment_id"])
        question = Question.objects.get(id=vars["question_id"])
        comment.node = question
        comment.save()

    return render_to_response('questions/parts/questions_view.html', {
        'questions': questions,
    },
                              context_instance=RequestContext(request))



    # CACHE SIGNALS
    #
    #     @receiver(post_save, sender=Page)
    #     def on_change(instance, **kwargs):
    #         cache.set('article::%(id)d' % {'id': instance.id}, instance)
    #
    #     @receiver(pre_delete, sender=Article)
    #     def on_delete(instance, **kwargs):
    #         cache.delete('article::%(id)d' % {'id': instance.id})
