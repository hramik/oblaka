# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse
# from oscar.app import application
# Uncomment the next two lines to enable the admin:

from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from tastypie.api import Api
from tri.api.resources import QuestionResource
from django.conf.urls import handler400, handler500
# from django.conf.urls import *
from django.conf.urls import handler404

from django.contrib import admin
from tri.views import questionViewSet, additionalPhotoViewSet, OrdersViewSet, OrderItemViewSet, OrderDetail, not_found
from products.models import ProductEntity
from tri.djangular_views import *
from django.views.generic import TemplateView
# from tri.views import *
from django.contrib.sites.shortcuts import get_current_site

# import xadmin
# xadmin.autodiscover()
# from xadmin.plugins import xversion
# xversion.register_models()

admin.autodiscover()

# v1_api = Api(api_name='v1')
# v1_api.register(QuestionResource())


handler404 = not_found
# handler500 = 'tri.views.not_found'


router = routers.DefaultRouter()
router.register(r'questions', questionViewSet)
router.register(r'photos', additionalPhotoViewSet)
router.register(r'order_item', OrderItemViewSet)
# router.register(r'options', optionsViewSet)
# router.register(r'hello_world', hello_world)

router.register(r'orders', OrdersViewSet, base_name="Orders")

Site.objects.clear_cache()
# site_domain = SiteInfo.on_site.get().site.domain.replace(".", '_')

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'oblaka.views.home', name='home'),
                       # url(r'^oblaka/', include('oblaka.foo.urls')),

                       # url(r'^trapi/order/$', OrderList.as_view()),
                       url(r'^trapi/order/(?P<pk>[0-9]+)$', OrderDetail.as_view()),

                       url(r'^api/', include(router.urls)),

                       url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

                       # url(r'', include('tri.%s_urls' % site_domain, namespace=site_domain)),
                       url(r'', include('tri.www_triton3tn_ru_urls', namespace='triton3tn')),
                       url(r'', include('tri.kavkaz_3tn_ru_urls', namespace='kavkaz')),
                       url(r'', include('tri.kazan_3tn_ru_urls', namespace='kazan')),
                       url(r'', include('tri.www_rostov3tn_ru_urls', namespace='rostov')),
                       url(r'', include('tri.www_orel3tn_ru_urls', namespace='orel')),
                       url(r'', include('tri.www_3tn_ru_urls', namespace='mainsite')),
                       url(r'', include('tri.ufa_3tn_ru_urls', namespace='ufa')),
                       url(r'', include('tri.orenburg_3tn_ru_urls', namespace='orenburg')),
                       url(r'', include('tri.saratov_3tn_ru_urls', namespace='saratov')),
                       url(r'', include('tri.volgograd_3tn_ru_urls', namespace='volgograd')),
                       url(r'', include('tri.stavropol_3tn_ru_urls', namespace='stavropol')),
                       url(r'', include('tri.krasnodar_3tn_ru_urls', namespace='krasnodar')),
                       url(r'', include('tri.nnovgorod_3tn_ru_urls', namespace='nnovgorod')),
                       url(r'', include('tri.samara_3tn_ru_urls', namespace='samara')),
                       url(r'', include('tri.bryansk_3tn_ru_urls', namespace='bryansk')),
                       url(r'', include('tri.belgorod_3tn_ru_urls', namespace='belgorod')),
                       url(r'', include('tri.voronezh_3tn_ru_urls', namespace='voronezh')),
                       url(r'', include('tri.ryazan_3tn_ru_urls', namespace='ryazan')),
                       url(r'', include('tri.penza_3tn_ru_urls', namespace='penza')),
                       url(r'', include('tri.vladimir_3tn_ru_urls', namespace='vladimir')),
                       url(r'', include('tri.tver_3tn_ru_urls', namespace='tver')),
                       url(r'', include('tri.yaroslavl_3tn_ru_urls', namespace='yaroslavl')),
                       url(r'', include('tri.arhangelsk_3tn_ru_urls', namespace='arhangelsk')),
                       url(r'', include('tri.kirov_3tn_ru_urls', namespace='kirov')),
                       url(r'', include('tri.izhevsk_3tn_ru_urls', namespace='izhevsk')),
                       url(r'', include('tri.moscow_3tn_ru_urls', namespace='moscow')),
                       url(r'', include('tri.piter_3tn_ru_urls', namespace='piter')),
                       url(r'', include('tri.tula_3tn_ru_urls', namespace='tula')),
                       url(r'', include('tri.belarus_3tn_ru_urls', namespace='belarus')),
                       url(r'', include('tri.novosib_3tn_ru_urls', namespace='novosib')),
                       url(r'', include('tri.chelyabinsk_3tn_ru_urls', namespace='chelyabinsk')),
                       url(r'', include('tri.barnaul_3tn_ru_urls', namespace='barnaul')),
                       url(r'', include('tri.kemerovo_3tn_ru_urls', namespace='kemerovo')),
                       url(r'', include('tri.krasnoyarsk_3tn_ru_urls', namespace='krasnoyarsk')),
                       url(r'', include('tri.omsk_3tn_ru_urls', namespace='omsk')),
                       url(r'', include('tri.eburg_3tn_ru_urls', namespace='eburg')),
                       url(r'', include('tri.perm_3tn_ru_urls', namespace='perm')),
                       url(r'', include('tri.kurgan_3tn_ru_urls', namespace='kurgan')),
                       url(r'', include('tri.tyumen_3tn_ru_urls', namespace='tyumen')),


                       url(r'^$', 'tri.views.front', name='front'),

                       url(r'^to_front_test$', 'tri.views.to_front_test', name='to_front_test'),
                       url(r'^front_test$', 'tri.views.front_test', name='front_test'),
                       # url(r'^front$', 'tri.views.front', name='front'),


                       url(r'^tyoma$', 'tri.views.for_tema', name='tyoma'),

                       url(r'^empty$', 'tri.views.empty', name='empty'),
                       url(r'^clear_cache$', 'tri.views.clear_cache_page', name='clear_cache_page'),

                       url(r'^not_found$', 'tri.views.not_found', name='not_found'),
                       url(r'^error$', 'tri.views.not_found', name='error'),
                       # url(r'^contacts$', 'tri.views.contacts'),

                       url(r'^pages$', 'tri.views.pages'),

                       url(r'^crud/tags/?$', TagsCRUDView.as_view(), name='tags_crud_view'),
                       url(r'^crud/nodes/?$', NodeCRUDView.as_view(), name='nodes_crud_view'),

                       # Cart part
                       url(r'^add_to_cart/(?P<id>[0-9]+)$', 'tri.views.add_to_cart'),
                       url(r'^delete_from_cart/(?P<id>[0-9]+)$', 'tri.views.delete_from_cart'),

                       url(r'^add_to_order/(?P<order_id>[0-9]+)/(?P<id>[0-9]+)$', 'tri.views.add_to_cart'),
                       url(r'^delete_from_order/(?P<order_id>[0-9]+)/(?P<id>[0-9]+)$', 'tri.views.delete_from_cart'),

                       url(r'^saleplaces_menu/(?P<region_id>[0-9]+)$', 'tri.views.saleplaces_menu'),

                       url(r'^getsession/(?P<product_id>[0-9]+)$', 'tri.views.getsession', name='getsession'),

                       # url(r'^update_cart$', 'tri.views.update_cart'),

                       url(r'^clear_cart$', 'tri.views.clear_cart', name='clear_cart'),

                       url(r'^backoffice$', 'tri.views.backoffice', name='backoffice'),

                       url(r'^cart$', 'tri.views.cart'),
                       url(r'^orders$', 'tri.views.backoffice', {'active_tab': 'orders'}, name='orders'),
                       url(r'^order_list/(?P<order_id>[0-9]+)', 'tri.views.order_list', name='order_list'),
                       url(r'^order(?P<hash>.+)', 'tri.views.order', name='order'),
                       url(r'^pay(?P<hash>.+)', 'tri.views.order', {'pay': True}, name='payorder'),
                       url(r'^n_order(?P<hash>.+)', 'tri.views.n_order', name='n_order'),

                       url(r'^callbacks$', 'tri.views.backoffice', {'active_tab': 'callbacks'}, name='callbacks'),

                       url(r'^delivery', 'tri.views.delivery'),
                       url(r'^deliveries', 'tri.views.deliveries'),

                       url(r'^printorder/(?P<hash>.+)', 'tri.views.printorder', name='printorder'),
                       url(r'^to_prepay/(?P<order_id>[0-9]+)$', 'tri.views.to_prepay', name='to_prepay'),
                       url(r'^to_diler/(?P<order_id>[0-9]+)$', 'tri.views.to_diler', name='to_diler'),
                       url(r'^to_dpd/(?P<order_id>[0-9]+)$', 'tri.views.to_dpd', name='to_dpd'),

                       url(r'^get_cart$', 'tri.views.get_cart'),
                       url(r'^get_order/(?P<order_id>[0-9]+)$', 'tri.views.get_order'),

                       url(r'^set_agree_make_review/(?P<value>.+)$', 'tri.views.set_agree_make_review'),
                       url(r'^get_current_price/(?P<id>[0-9]+)$', 'tri.views.get_current_price', name='get_current_price'),
                       url(r'^get_current_price/(?P<id>[0-9]+)/(?P<order_id>[0-9]+)$', 'tri.views.get_current_price', name='get_current_price_in_order'),

                       # Products part
                       url(r'^comparsion$', 'tri.views.comparsion'),
                       url(r'^clear_comparsion$', 'tri.views.clear_comparsion'),
                       url(r'^add_to_comparsion/(?P<id>[0-9]+)$', 'tri.views.add_to_comparsion'),
                       url(r'^delete_from_copmarsion/(?P<id>[0-9]+)$', 'tri.views.delete_from_comparsion'),
                       url(r'^clear_favorite$', 'tri.views.clear_favorite'),
                       url(r'^add_to_favorite/(?P<id>[0-9]+)$', 'tri.views.add_to_favorite'),
                       url(r'^delete_from_favorite/(?P<id>[0-9]+)$', 'tri.views.delete_from_favorite'),
                       url(r'^change_price/(?P<nid>[0-9]+)$', 'tri.views.change_price'),
                       url(r'^change_option_price/(?P<id>[0-9]+)$', 'tri.views.change_option_price'),
                       url(r'^change_sku/(?P<nid>[0-9]+)$', 'tri.views.change_sku'),

                       url(r'^clone-(?P<id>[0-9]+)$', 'tri.views.clone_product'),

                       url(r'^send_callback$', 'tri.views.send_callback'),
                       url(r'^change_callback_status$', 'tri.views.change_callback_status'),
                       url(r'^send_product_comment$', 'tri.views.send_product_comment'),
                       url(r'^send_comment$', 'tri.views.send_comment'),
                       url(r'^send_order$', 'tri.views.send_order'),
                       url(r'^save_order$', 'tri.views.save_order'),
                       url(r'^change_status$', 'tri.views.change_status'),
                       url(r'^send_quick_order_form$', 'tri.views.send_quick_order_form'),

                       #  Question and comment part
                       url(r'^send_question$', 'tri.views.send_question'),
                       url(r'^send_que$', 'tri.views.send_que'),

                       url(r'^add_comment_like$', 'tri.views.add_comment_like'),
                       url(r'^remove_comment_like$', 'tri.views.remove_comment_like'),
                       url(r'^to_question$', 'tri.views.comment_to_question'),
                       url(r'^comment_transfer$', 'tri.views.comment_transfer'),

                       url(r'^image_upload$', 'tri.views.image_upload'),

                       url(r'^json$', 'tri.views.show_json'),

                       # TODO: перенести в команды все импорты
                       url(r'^import_products$', 'tri.views.import_products'),
                       url(r'^import_products/(?P<import_site>[a-z0-9\.\-_]+)/(?P<arg>[a-zA-Z0-9\-_]+)$',
                           'tri.views.import_products'),
                       url(r'^import_accessories$', 'tri.views.import_accessories'),

                       url(r'^import_dealers$', 'tri.views.import_dealers'),
                       url(r'^import_pages/(?P<type>[a-z]+)$', 'tri.views.import_pages'),

                       url(r'^import_comments$', 'tri.views.import_comments'),
                       url(r'^check_comments$', 'tri.views.check_comments'),
                       url(r'^import_forum$', 'tri.views.import_forum'),

                       # TODO: удалить пути
                       # url(r'^import_users$', 'tri.views.import_users'),
                       # url(r'^import_advices$', 'tri.views.import_advices'),

                       url(r'^set_comment_date$', 'tri.views.set_comment_date'),

                       url(r'^advices$', 'tri.views.list', {'slug_in': ('advices',)}, name='advices'),

                       url(r'^last_articles$', 'tri.views.last_articles', name='last_articles'),

                       url(r'^magazine/get_article/(?P<id>[0-9]+)$', 'tri.views.magazine_article', name='magazine_article'),
                       url(r'^magazine/save_article$', 'tri.views.save_article', name='save_article'),
                       url(r'^magazine/send_solution_order$', 'tri.views.send_solution_order', name='send_solution_order'),
                       url(r'^magazine$', 'tri.views.magazine', name='magazine'),
                       url(r'^magazine_(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.magazine', name='article'),
                       url(r'^magazine/(?P<part_slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.magazine_part', name='magazine_part'),
                       # url(r'^magazine/(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.magazine_article', name='magazine_article'),

                       # TODO: удалить пути
                       # url(r'^news/(?P<slug>[a-zA-Z0-9\-_\.]+)$', 'tri.views.news_detail'),

                       url(r'^homeless_comments$', 'tri.views.homeless_comments', name='homeless_comments'),
                       # url(r'^commentless_comments$', 'tri.views.commentless_comments', name='commentless_comments'),

                       # TODO: удалить пути
                       # url(r'^only_questions', 'tri.views.only_questions', name='only_questions'),
                       # url(r'^only_questions/(?P<tag>[a-zA-Z0-9\-_\.]+)$', 'tri.views.only_questions', name='only_questions'),

                       url(r'^questions/page-(?P<page>[0-9]+)$', 'tri.views.questions'),
                       # url(r'^questions/(?P<tag>[a-zA-Z0-9\-_\.]+)$', 'tri.views.questions', name='questions_tag'),
                       # url(r'^question/(?P<id>[0-9]+)$', 'tri.views.question', name="question_details_slash"),
                       url(r'^question-(?P<id>[0-9]+)$', 'tri.views.question', name="question_details"),

                       url(r'^opt/dilery$', 'tri.views.dealers'),
                       url(r'^opt/prays-list-dlya-optovikov$', 'tri.views.get_dealer_price'),
                       # url(r'^opt/(?P<slug>[a-zA-Z0-9\-_\.]+)$', 'tri.views.node'),

                       url(r'^akc.php$', 'tri.views.list', {'slug_in': ('action',)}, name='actions'),
                       url(r'^akcii-otzyvy$', 'tri.views.list', {'slug_in': ('action_callback',)},
                           name='actions_callbacks'),

                       url(r'^hardware.php$', 'tri.views.components'),
                       url(r'^accessories/(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.component', name='component'),

                       url(r'^products/(?P<entity>[a-zA-Z0-9\-_\.]+)$', 'tri.views.products'),
                       url(r'^add_products/(?P<order_id>[0-9]+)/(?P<entity>[a-zA-Z0-9\-_\.]+)$', 'tri.views.add_products'),
                       url(r'^action$', 'tri.views.action_products', name='action_products'),

                       # OPTIONS URLS
                       url(r'^product_options/(?P<id>[0-9]+)$', 'tri.views.product_options'),
                       url(r'^product_order_options/(?P<order_id>[0-9]+)/(?P<id>[0-9]+)$', 'tri.views.product_order_options'),

                       url(r'^_option_(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.get_option', {'ajax': True}, name='option'),
                       url(r'^_base_complectation_(?P<id>[0-9]+)$', 'tri.views.base_complectation', name='base_complectation_'),
                       url(r'^option_(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.get_option', name='option'),

                       url(r'^add_option_to_session/(?P<id>[0-9]+)/(?P<product_id>[0-9]+)/(?P<active>true|false)$', 'tri.views.add_option_to_session'),
                       url(r'^add_option_to_order/(?P<order_id>[0-9]+)/(?P<id>[0-9]+)/(?P<product_id>[0-9]+)/(?P<active>true|false)$', 'tri.views.add_option_to_session'),

                       url(r'^clear_all_option_from_session/(?P<product_id>[0-9]+)$', 'tri.views.clear_all_option_from_session'),
                       url(r'^load_options_complecation/(?P<product_id>[0-9]+)/(?P<complectation_id>[0-9]+)$', 'tri.views.load_options_complecation'),

                       # url(r'^dataexport/(?P<slug>.+)$', 'tri.views.dataexport'),

                       # TODO: удалить пути
                       # url(r'^baths$', 'tri.views.baths'),
                       # url(r'^product/(?P<slug>[a-zA-Z0-9\-_\.]+)$', 'tri.views.node'),
                       # url(r'^(?P<entity>[a-zA-Z0-9\-_]+)/(?P<slug>[a-zA-Z0-9\-_\.]+)$', 'tri.views.node'),


                       # url(r'^search$', 'tri.views.search'),
                       url(r'^poisk$', 'tri.views.search'),

                       url(r'^visor$', 'tri.views.visor', name='visor'),
                       url(r'^visor/picasa.php$', 'tri.views.visor', name='visor'),

                       url(r'^dataexport/getthreedmodel$', 'tri.views.getthreedmodel', name='getthreedmodel'),
                       url(r'^dataexport_new/getthreedmodel$', 'tri.views.getthreedmodel', name='getthreedmodel'),

                       url(r'^dataexport_new/prices$', 'tri.views.getprices', name='getprices'),
                       url(r'^dataexport_new/prices/(?P<entity>[a-zA-Z0-9\-_\.]+)$', 'tri.views.getprices', name='getprices_by_entity'),

                       url(r'^dataexport_new/baths_prices$', 'tri.views.getprices', {'entity': ('bath',)}, name='baths_prices$'),
                       url(r'^dataexport_new/mebel_prices$', 'tri.views.getprices', {'entity': ('mebel',)}, name='mebel_prices$'),
                       url(r'^dataexport_new/bathcabs_prices$', 'tri.views.getprices', {'entity': ('bathcab',)}, name='bathcabs_prices$'),

                       url(r'^robots\.txt$', 'tri.views.robots', name='robots'),
                       # url(r'^style\.css$', 'tri.views.style', name='style'),

                       url(r'^profile$', 'tri.views.profile', name='profile'),
                       url(r'^profile/(?P<username>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.another_profile', name='another_profile'),
                       # url(r'^users/(?P<username>[a-zA-Z0-9\-_\.\/]+)/$', 'tri.views.another_profile', name='another_profile_user'),

                       url(r'^perimeter$', 'tri.views.perimeter', name='perimeter'),
                       url(r'^login$', 'tri.views.login', name='login'),
                       url(r'^register$', 'tri.views.register', name='register'),
                       url(r'^edit_profile$', 'tri.views.edit_profile', name='edit_profile'),
                       url(r'^profile_change_avatar$', 'tri.views.profile_change_avatar', name='profile_change_avatar'),
                       url(r'^logout$', 'tri.views.logout', name='logout'),
                       url(r'^check_login$', 'tri.views.check_login', name='check_login'),
                       url(r'^multi_login$', 'tri.views.multi_login', name='multi_login'),
                       url(r'^recovery_password$', 'tri.views.recovery_password', name='recovery_password'),

                       url(r'^node_subscribe$', 'tri.views.node_subscribe', name='node_subscribe'),
                       url(r'^subscribe', 'tri.views.subscribe', name='subscribe'),
                       url(r'^send_subscribe', 'tri.views.send_subscribe', name='send_subscribe'),

                       url(r'^sale$', 'tri.views.profile', name='sale_request'),
                       url(r'^r$', 'tri.views.profile', {'qr': '1976'}, name='user_request'),
                       url(r'^dr$', 'tri.views.profile', {'qr': '1976'}, name='dealer_request'),
                       url(r'^rr$', 'tri.views.profile', {'qr': '1976'}, name='repress_request'),
                       url(r'^sr$', 'tri.views.profile', {'qr': '1976'}, name='seller_request'),

                       # (r'^api/', include(v1_api.urls)),

                       # url(r'^$', 'api_root'),
                       # url(r'^api2/questions/$', QuestionList.as_view(), name='question-list'),
                       # url(r'^api2/questions/(?P<pk>\d+)$', QuestionDetail.as_view(), name='question-detail'),

                       )
#
# try:
#
# # print site_domain
# urlpatterns += patterns('',
#                             url(r'', include('tri.%s_urls' % site_domain, namespace=site_domain)),
#     )
# except Exception, e:
#     print Exception, e
