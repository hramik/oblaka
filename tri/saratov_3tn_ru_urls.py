# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

SITE_ID = 78

urlpatterns = patterns('',
                       url(r'^karta-sayta$', 'tri.views.site_structure', {'site_id': SITE_ID}, name='site_structure'),

                       url(r'^akrilovye-vanny-saratov$', 'tri.views.products', {'entity': 'bath', 'site_id': SITE_ID}, name='baths'),
                       url(r'^asimmetrichnye-vanny$', 'tri.views.products', {'entity': 'bath', 'cats': ('asimmetrichnie',), 'site_id': SITE_ID}, name='asimetric_baths'),
                       url(r'^pryamougolnye-vanny$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='pryamougolnyie_baths'),
                       # url(r'^$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='uglovie_baths'),
                       url(r'^uglovye-vanny$', 'tri.views.products', {'entity': 'bath', 'cats': ('uglovie',), 'site_id': SITE_ID}, name='uglovie_baths'),
                       # url(r'^$', 'tri.views.products', {'entity': 'bath', 'cats': ('standart',), 'site_id': SITE_ID}, name='standart_baths'),

                       url(r'^mebel-dlya-vannyh-komnat$', 'tri.views.products', {'entity': 'mebel', 'site_id': SITE_ID}, name='mebel'),
                       url(r'^zerkala$', 'tri.views.products', {'entity': 'mebel', 'cats': ('mirrors',), 'site_id': SITE_ID},name='mebel_mirrors'),
                       url(r'^shkafy$', 'tri.views.products', {'entity': 'mebel', 'cats': ('selfs',), 'site_id': SITE_ID},name='mebel_selfs'),
                       url(r'^penaly$', 'tri.views.products', {'entity': 'mebel', 'cats': ('penals',), 'site_id': SITE_ID},name='mebel_penals'),
                       url(r'^tumbyi-s-rakovinoy$', 'tri.views.products', {'entity': 'mebel', 'cats': ('sinks',), 'site_id': SITE_ID},name='mebel_sinks'),
                       url(r'^tumbyi-s-rakovinoy-podvesnye$', 'tri.views.products', {'entity': 'mebel', 'cats': ('hang_sinks',), 'site_id': SITE_ID},name='mebel_hang_sinks'),
                       url(r'^komody$', 'tri.views.products', {'entity': 'mebel', 'cats': ('commodes',), 'site_id': SITE_ID},name='mebel_commodes'),
                       url(r'^tumbyi-napolnye$', 'tri.views.products', {'entity': 'mebel', 'cats': ('tumb',), 'site_id': SITE_ID},name='mebel_tumb'),

                       url(r'^smesiteli$', 'tri.views.products', {'entity': 'mixer', 'site_id': SITE_ID}, name='mixers'),

                       url(r'^dushevyie-kabiny$', 'tri.views.products', {'entity': 'bathcab', 'site_id': SITE_ID}, name='bathcabs'),

                       url(r'^novosti-kompanii$', 'tri.views.list', {'slug_in': ('news', 'advices'), 'site_id': SITE_ID}, name='news'),
                       url(r'^questions-saratov$', 'tri.views.questions', name='questions'),

)



