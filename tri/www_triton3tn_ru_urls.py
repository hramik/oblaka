# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
                       url(r'^site_structure$', 'tri.views.site_structure', name='site_structure'),

                       url(r'^mebel$', 'tri.views.products', {'entity': 'mebel'}, name='mebel'),
                       url(r'^baths$', 'tri.views.products', {'entity': 'bath'}, name='baths'),
                       url(r'^bathcabs$', 'tri.views.products', {'entity': 'bathcab'}, name='bathcabs'),
                       url(r'^mixers$', 'tri.views.products', {'entity': 'mixer'}, name='mixers'),
                       url(r'^accessories$', 'tri.views.products', {'entity': 'accessorie'}, name='accessories'),



                       url(r'^news$', 'tri.views.list', {'slug_in': ('news', 'advices')}, name='news'),

                       url(r'^questions$', 'tri.views.questions', name='questions'),

                       url(r'^958f6dbd9f8deb850d05b59c22857565521cb3eb30e851517fc2427f8c703045.html$', 'tri.views.mail_office_confirmation', name='mail_office_confirmation'),

                       url(r'^google83988dd02d5012b9.html', 'tri.views.youtube_confirmation', name='youtube_confirmation'),

                       )
