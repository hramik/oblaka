__author__ = 'hramik'

from django.contrib import admin

import xadmin
from xadmin import views
from xadmin.layout import Main, TabHolder, Tab, Fieldset, Row, Col, AppendedText, Side
from xadmin.plugins.inline import Inline
from xadmin.plugins.batch import BatchChangeAction

from mptt.admin import MPTTModelAdmin
from sorl.thumbnail.admin import AdminImageMixin

from tri.models import Page, PagePart, Article, News, Gallery, GalleryPhoto, City, SalePlace
from slides.models import Slide
from node.models import AdditionalFile, AdditionalPhoto, AdditionalVideos
from products.models import Category, Product, ProductDetails, Delivery, Payment, Order, OrderItem, OptionsGroup, Option, OptionPrice
from products.models import ProductStyleCat, ProductFormCat, ProductMaterial, ProductComment, ProductType, ProductColor, ProductEntity, ProductCategory
from custom_sites.models import SiteInfo
from callback.models import Callback
from triusers.models import SellerProfile, DealerProfile, RepresProfile

from menu.models import Menu, MenuGroup


class AdditionalFilesInline(admin.TabularInline):
    model = AdditionalFile
    extra = 3
    sortable_field_name = "position"


class AdditionalPhotoInline(AdminImageMixin, admin.TabularInline):
    model = AdditionalPhoto
    extra = 5
    sortable_field_name = "position"


class AdditionalVideosInline(AdminImageMixin, admin.TabularInline):
    model = AdditionalVideos
    extra = 1
    sortable_field_name = "position"


class PagePartInline(admin.TabularInline):
    model = PagePart
    extra = 0
    sortable_field_name = "position"


class PagePartAdmin(admin.ModelAdmin):
    pass
    #fields = ('title', 'sites', 'body')
    #list_display = ('title')


class PageAdmin(admin.ModelAdmin):
    fields = (('title', 'page_title'), 'sites', 'meta_desc', 'teaser', 'body', 'slug')
    list_display = ('title', 'teaser', 'slug')
    inlines = [PagePartInline, AdditionalPhotoInline]
    filter_horizontal = ('sites',)



class ArticleAdmin(admin.ModelAdmin):
    pass
    inlines = [AdditionalPhotoInline]


class GalleryPhotoInline(admin.TabularInline):
    model = GalleryPhoto
    extra = 10
    sortable_field_name = "position"


class GalleryAdmin(admin.ModelAdmin):
    pass
    #fields = ('lawyer', 'question', 'user_name', 'user_email')
    inlines = [GalleryPhotoInline]


class AdAdmin(admin.ModelAdmin):
    list_display = ('img', 'active', 'type', 'sites_names')
    list_filter = ('active', 'type', 'sites')
    search_fields = ('img', )
    filter_horizontal = ('sites',)


class NewsAdmin(object):
    fields = (('title', 'page_title'), 'sites', 'status', 'teaser', 'body', 'slug')
    list_display = ('title', 'teaser', 'slug')
    inlines = [AdditionalPhotoInline]
    filter_horizontal = ('sites',)


class CityAdmin(admin.ModelAdmin):
    pass


class SalePlacesAdmin(admin.ModelAdmin):
    list_display = ('title', 'city', 'phone', 'map_coords', 'map_zoom', 'sites_names')
    list_filter = ('city', 'sites')
    search_fields = ('title', )
    inlines = [AdditionalPhotoInline]
    filter_horizontal = ('sites',)

class CategoryAdmin(MPTTModelAdmin):
    pass


class BathStyleCatAdmin(admin.ModelAdmin):
    pass


class BathFormCatAdmin(admin.ModelAdmin):
    pass


class BathMaterialAdmin(admin.ModelAdmin):
    pass

class ProductTypeAdmin(admin.ModelAdmin):
    pass


class ProductColorAdmin(admin.ModelAdmin):
    pass


class ProductEntityAdmin(admin.ModelAdmin):
    pass


class ProductCaltegoryAdmin(admin.ModelAdmin):
    pass


class DeliveryAdmin(admin.ModelAdmin):
    pass


class PaymentAdmin(admin.ModelAdmin):
    pass


class ProductCommentAdmin(admin.ModelAdmin):
    search_fields = ('node',)
    list_display = ('node', 'body', 'pub_date', 'publish')
    list_filter = ('publish', 'pub_date')


class ProductCommentInline(admin.TabularInline):
    model = ProductComment
    extra = 0
    #sortable_field_name = "pub_date"


#class ProductAdmin(admin.ModelAdmin):
#    fields = (('title', 'page_title', 'sku'), ('category', 'popular'), ('price'), 'meta_desc', 'teaser', 'body', 'slug')
#    list_display = ('title', 'teaser', 'slug')
#    inlines = [AdditionalPhotoInline]


class OptionsInline(admin.TabularInline):
    model = Option
    extra = 3



class OptionsAdmin(admin.ModelAdmin):
    pass


class OptionPriceAdmin(admin.ModelAdmin):
    list_display = ('product', 'option', 'price')
    search_fields = ('product', 'option')
    #filter_horizontal = ('sites',)


class OptionPriceInline(admin.TabularInline):
    model = OptionPrice
    extra = 3
    #filter_horizontal = ('sites',)


class OptionsGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    inlines = [OptionsInline]


class ProductDetailsAdmin(admin.ModelAdmin):
    pass

class ProductDetailsInline(admin.StackedInline):
    model = ProductDetails
    extra = 1


class ProductAdmin(admin.ModelAdmin):
    #fields = (('title', 'page_title', 'sku'), 'category', ('price'), 'meta_desc', 'teaser', 'body', 'slug', 'length', 'width', 'height', 'depth', 'value')
    fieldsets = (
        (None, {
            'fields': (
                'tn_id', 'sites', ('title', 'second_title'), ('entity', 'type'), ('page_title', 'sku'), ('popular', 'hydromassage'), ('price'),
                'slug')
        }),
        ('Json', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('json',)
        }),
        ('Content', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('teaser', 'body')
        }),
        ('SEO', {
            'classes': ('grp-collapse grp-closed',),
            'fields': ('meta_desc', 'meta_keywords')
        }),
        ('Categories', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('style', 'form'), ('material', 'color'))
        }),
        ('Chars', {
            'classes': ('grp-collapse grp-closed',),
            'fields': (('length', 'width'), ('height', 'depth'), 'value')
        }),
    )
    list_display = ('title', 'teaser', 'style_names', 'material_names', 'slug')
    inlines = [OptionPriceInline, AdditionalPhotoInline, AdditionalVideosInline, ProductCommentInline]
    search_fields = ('title', )
    list_filter = ('entity', 'type')
    #filter_horizontal = ('sites',)



class SiteInfoAdmin(admin.ModelAdmin):
    pass
    #fields = (('title', 'page_title', 'sku'), ('category', 'popular'), ('price'), 'meta_desc', 'teaser', 'body', 'slug')
    #list_display = ('site_name', 'site_domen')
    #inlines = [AdditionalPhotoInline]


class CallbackAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'call_time', 'email', 'comment', 'status')
    list_filter = ('status', )


class OrderItemsAdmin(admin.ModelAdmin):
    pass
    filter_horizontal = ('options',)
    #raw_id_fields = ("options",)
    list_display = ('id', 'product', 'order')
    list_filter = ('order', )
    #inlines = [OptionPriceInline]

class OrderItemsInline(admin.TabularInline):
    model = OrderItem
    filter_horizontal = ('options',)
    #list_display = ('name', 'phone', 'call_time', 'email')
    extra = 0
    #sortable_field_name = "position"


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'status')
    inlines = [OrderItemsInline]


class MenuGroupAdmin(admin.ModelAdmin):
    pass


class MenuAdmin(MPTTModelAdmin):
    list_display = ('title', 'slug', 'link', 'image', 'position', 'menu_group', 'publish')
    list_filter = ('menu_group', 'level', 'title')
    filter_horizontal = ('sites',)



xadmin.site.register(News, NewsAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(PagePart, PagePartAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Gallery, GalleryAdmin)
admin.site.register(Slide, AdAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(SalePlace, SalePlacesAdmin)

admin.site.register(Product, ProductAdmin)
#admin.site.register(Category, CategoryAdmin)
admin.site.register(ProductStyleCat, BathStyleCatAdmin)
admin.site.register(ProductFormCat, BathFormCatAdmin)
admin.site.register(ProductMaterial, BathMaterialAdmin)
admin.site.register(ProductColor, ProductColorAdmin)
admin.site.register(ProductType, ProductTypeAdmin)
admin.site.register(ProductEntity, ProductEntityAdmin)
admin.site.register(ProductCategory, ProductCaltegoryAdmin)
admin.site.register(ProductComment, ProductCommentAdmin)
admin.site.register(OptionsGroup, OptionsGroupAdmin)
admin.site.register(OptionPrice, OptionPriceAdmin)
admin.site.register(Option, OptionsAdmin)

admin.site.register(MenuGroup, MenuGroupAdmin)
admin.site.register(Menu, MenuAdmin)

admin.site.register(Delivery, DeliveryAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(SiteInfo, SiteInfoAdmin)

admin.site.register(Callback, CallbackAdmin)

admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem, OrderItemsAdmin)

admin.site.register(ProductDetails, ProductDetailsAdmin)

admin.site.register(SellerProfile)
admin.site.register(DealerProfile)
admin.site.register(RepresProfile)

