# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

SITE_ID = 30

urlpatterns = patterns('',
                       url(r'^vanni.html$', 'tri.views.products', {'entity': 'bath', 'site_id': SITE_ID}, name='baths'),
                       url(r'^asimmetrichnye-vanny-belarus$', 'tri.views.products', {'entity': 'bath', 'cats': ('asimmetrichnie',), 'site_id': SITE_ID}, name='asimetric_baths'),
                       url(r'^pryamougolnye-vanny-belarus$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='pryamougolnyie_baths'),
                       url(r'^uglovye-vanny-belarus$', 'tri.views.products', {'entity': 'bath', 'cats': ('uglovie',), 'site_id': SITE_ID}, name='uglovie_baths'),
                       url(r'^standartnye-vanny-belarus$', 'tri.views.products', {'entity': 'bath', 'cats': ('standart',), 'site_id': SITE_ID}, name='standart_baths'),

                       url(r'^mebel-dlya-vannyh-komnat-belarus$', 'tri.views.products', {'entity': 'mebel', 'site_id': SITE_ID}, name='mebel'),
                       url(r'^zerkala-belarus$', 'tri.views.products', {'entity': 'mebel', 'cats': ('mirrors',), 'site_id': SITE_ID},name='mebel_mirrors'),
                       url(r'^shkafy-belarus$', 'tri.views.products', {'entity': 'mebel', 'cats': ('selfs',), 'site_id': SITE_ID},name='mebel_selfs'),
                       url(r'^penaly-belarus$', 'tri.views.products', {'entity': 'mebel', 'cats': ('penals',), 'site_id': SITE_ID},name='mebel_penals'),
                       url(r'^tumbyi.html$', 'tri.views.products', {'entity': 'mebel', 'cats': ('sinks',), 'site_id': SITE_ID},name='mebel_sinks'),
                       url(r'^tumby-s-rakovinoy-podvesnye-belarus$', 'tri.views.products', {'entity': 'mebel', 'cats': ('hang_sinks',), 'site_id': SITE_ID},name='mebel_hang_sinks'),
                       url(r'^komody-belarus$', 'tri.views.products', {'entity': 'mebel', 'cats': ('commodes',), 'site_id': SITE_ID},name='mebel_commodes'),
                       url(r'^tumby-napolnye-podvesnye-belarus$', 'tri.views.products', {'entity': 'mebel', 'cats': ('tumb',), 'site_id': SITE_ID},name='mebel_tumb'),

                       url(r'^smesiteli-belarus$', 'tri.views.products', {'entity': 'mixer', 'site_id': SITE_ID}, name='mixers'),

                       url(r'^dushevye-kabiny-belarus$', 'tri.views.products', {'entity': 'bathcab', 'site_id': SITE_ID}, name='bathcabs'),

                       url(r'^map-1.html$', 'tri.views.site_structure', {'site_id': SITE_ID}, name='site_structure'),
                       url(r'^common$', 'tri.views.list', {'slug_in': ('news', 'advices'), 'site_id': SITE_ID}, name='news'),
                       url(r'^questions-belarus$', 'tri.views.questions', name='questions'),

)



