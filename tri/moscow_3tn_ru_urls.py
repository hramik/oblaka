# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

SITE_ID = 58

urlpatterns = patterns('',
                       url(r'^products-vanny$', 'tri.views.products', {'entity': 'bath', 'site_id': SITE_ID}, name='baths'),
                       url(r'^asimmetrichnie$', 'tri.views.products', {'entity': 'bath', 'cats': ('asimmetrichnie',), 'site_id': SITE_ID}, name='asimetric_baths'),
                       url(r'^pryamougolnie$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='pryamougolnyie_baths'),
                       url(r'^uglovie$', 'tri.views.products', {'entity': 'bath', 'cats': ('uglovie',), 'site_id': SITE_ID}, name='uglovie_baths'),


                       url(r'^products-mebel$', 'tri.views.products', {'entity': 'mebel', 'site_id': SITE_ID}, name='mebel'),
                       url(r'^mebel-diana$', 'tri.views.products', {'entity': 'mebel', 'cats': ('diana',), 'site_id': SITE_ID},name='mebel_diana'),
                       url(r'^mebel-kristi$', 'tri.views.products', {'entity': 'mebel', 'cats': ('kristi',), 'site_id': SITE_ID},name='mebel_kristi'),
                       url(r'^mebel-nika$', 'tri.views.products', {'entity': 'mebel', 'cats': ('nika',), 'site_id': SITE_ID},name='mebel_nika'),
                       url(r'^mebel-eko$', 'tri.views.products', {'entity': 'mebel', 'cats': ('eko',), 'site_id': SITE_ID},name='mebel_eko'),
                       url(r'^mebel-lira$', 'tri.views.products', {'entity': 'mebel', 'cats': ('lira',), 'site_id': SITE_ID},name='mebel_lira'),
                       url(r'^mebel-eko_wood$', 'tri.views.products', {'entity': 'mebel', 'cats': ('eko-wood',), 'site_id': SITE_ID},name='mebel_eko-wood'),
                       url(r'^mebel-mirta$', 'tri.views.products', {'entity': 'mebel', 'cats': ('mirta',), 'site_id': SITE_ID},name='mebel_mirta'),

                       url(r'^products-mixers$', 'tri.views.products', {'entity': 'mixer', 'site_id': SITE_ID}, name='mixers'),

                       url(r'^products-bathcab$', 'tri.views.products', {'entity': 'bathcab', 'site_id': SITE_ID}, name='bathcabs'),

                       url(r'^news-triton$', 'tri.views.list', {'slug_in': ('news', 'advices'), 'site_id': SITE_ID}, name='news'),
                       url(r'^struktura$', 'tri.views.site_structure', {'site_id': SITE_ID}, name='site_structure'),
                       url(r'^questions-moscow$', 'tri.views.questions', name='questions'),



                       )



