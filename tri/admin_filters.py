# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.contrib import admin
from products.models import *
from django.db.models import Count
from django.db.models import F

import logging




class PageTitleFilter(admin.SimpleListFilter):
    title = u'Meta Title'

    parameter_name = 'is_page_title'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(page_title__gt='')

        if self.value() == 'false':
            return queryset.filter(page_title='')

class MetaDescFilter(admin.SimpleListFilter):
    title = u'Meta Description'

    parameter_name = 'is_meta_desc'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(meta_desc__gt='')

        if self.value() == 'false':
            return queryset.filter(meta_desc='')

class MetaKeywordsFilter(admin.SimpleListFilter):
    title = u'Meta Keywords'

    parameter_name = 'is_meta_keywords'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(meta_keywords__gt='')

        if self.value() == 'false':
            return queryset.filter(meta_keywords='')

class CommentsFilter(admin.SimpleListFilter):
    title = u'Комментарии'

    parameter_name = 'is_comments'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(last_comment_date__gt=F('pub_date'))

        if self.value() == 'false':
            return queryset.filter(last_comment_date=F('pub_date'))


class AltFilter(admin.SimpleListFilter):
    '''
    Images Alt Filter
    '''
    title = u'Альты'

    parameter_name = 'is_alts'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        not_alt_photos = AdditionalPhoto.objects.filter(alt='').values_list('node__id', flat=True)
        # alt_photos = AdditionalPhoto.objects.filter(alt__gt='').values_list('id', flat=True).distinct()
        logging.debug(AdditionalPhoto.objects.all().count())
        logging.debug(not_alt_photos.count())
        if self.value() == 'true':
            return queryset.exclude(id__in=not_alt_photos)
        if self.value() == 'false':
            return queryset.filter(id__in=not_alt_photos)


class TitleFilter(admin.SimpleListFilter):
    '''
    Images Title Filter
    '''
    title = u'Титлы'

    parameter_name = 'is_titles'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        not_title_photos = AdditionalPhoto.objects.filter(title='').values_list('node__id', flat=True)
        # alt_photos = AdditionalPhoto.objects.filter(alt__gt='').values_list('id', flat=True).distinct()
        logging.debug(AdditionalPhoto.objects.all().count())
        logging.debug(not_title_photos.count())
        if self.value() == 'true':
            return queryset.exclude(id__in=not_title_photos)
        if self.value() == 'false':
            return queryset.filter(id__in=not_title_photos)


class TeaserFilter(admin.SimpleListFilter):
    title = u'Тизер'

    parameter_name = 'is_teaser'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(teaser__gt='')

        if self.value() == 'false':
            return queryset.filter(teaser='')

class BodyFilter(admin.SimpleListFilter):
    title = u'Body'

    parameter_name = 'is_body'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(body__gt='')

        if self.value() == 'false':
            return queryset.filter(body='')

class H1Filter(admin.SimpleListFilter):
    title = u'H1'

    parameter_name = 'is_h1'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(h1__gt='')

        if self.value() == 'false':
            return queryset.filter(h1='')

class H2Filter(admin.SimpleListFilter):
    title = u'H2'

    parameter_name = 'is_h2'

    def lookups(self, request, model_admin):
        return (
            ('true', u'Есть'),
            ('false', u'Нет'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'true':
            return queryset.filter(h2__gt='')

        if self.value() == 'false':
            return queryset.filter(h2='')


class CategoryTypeFilter(admin.SimpleListFilter):
    title = u'Тип категории'

    parameter_name = 'category_type'

    def lookups(self, request, model_admin):
        category_types = CategoryType.objects.all().values_list('slug', 'title')
        print category_types
        return category_types

    def queryset(self, request, queryset):
        return queryset.filter()

class ModelFilter(admin.SimpleListFilter):
    title = u'Модель'

    parameter_name = 'model'

    def lookups(self, request, model_admin):
        category_types = ProductCategory.objects.filter(category_type__slug__in = ('modelline',)).values_list('slug', 'title')
        print category_types
        return category_types

    def queryset(self, request, queryset):
        if self.value():
            print self.value()
            return queryset.filter(category__slug__in=(self.value(),))
        else:
            pass


class FormFilter(admin.SimpleListFilter):
    title = u'Форма'

    parameter_name = 'form'

    def lookups(self, request, model_admin):
        category_types = ProductCategory.objects.filter(category_type__slug__in = ('form',)).values_list('slug', 'title')
        print category_types
        return category_types

    def queryset(self, request, queryset):
        if self.value():
            print self.value()
            return queryset.filter(category__slug__in=(self.value(),))
        else:
            pass

class TypeProductFilter(admin.SimpleListFilter):
    title = u'Тип продукции'

    parameter_name = 'product_type'

    def lookups(self, request, model_admin):
        product_types = Tag.objects.filter(slug__in = ('bath', 'mebel', 'bathcab', 'mixers')).values_list('slug', 'name')
        return product_types

    def queryset(self, request, queryset):
        if self.value():
            print self.value()
            return queryset.filter(tags__slug__in=(self.value(),), node_type__slug='product')
        else:
            pass

class TagsFilter(admin.SimpleListFilter):
    title = u'Тэг'

    parameter_name = 'tag'

    def lookups(self, request, model_admin):
        tags = (('none', 'Нету'),)
        tags = tags + tuple(Tag.objects.all().values_list('slug', 'name'))
        return tags

    def queryset(self, request, queryset):
        if self.value():
            if(self.value() == 'none'):
                return queryset.filter(tags__slug=None)
            else:
                return queryset.filter(tags__slug__in=(self.value(),))
        else:
            pass

class SiteOptionsFilter(admin.SimpleListFilter):
    title = u'Сайт'

    parameter_name = 'option_price_site'

    def lookups(self, request, model_admin):
        option_price_site = Site.objects.all().values_list('domain', 'name')
        return option_price_site

    # def choices(self, cl):
    #     yield {
    #         'selected': self.value() is None,
    #         'query_string': cl.get_query_string({}, [self.parameter_name]),
    #         # 'display': _('All'),
    #     }
    #     for lookup, title in self.lookup_choices:
    #         yield {
    #             'selected': self.value() == lookup,
    #             'query_string': cl.get_query_string({
    #                 self.parameter_name: lookup,
    #             }, []),
    #             'display': title,
    #         }

    def queryset(self, request, queryset):
        if self.value():
            print self.value()
            site = Site.objects.get(domain=self.value())
            return queryset.filter(product__sites=site)
        else:
            pass

class SiteProductsOptionsFilter(admin.SimpleListFilter):
    title = u'Продукция'

    parameter_name = 'option_price_option_product'

    def lookups(self, request, model_admin):
        kwargs = {}

        try:
            kwargs['sites'] = Site.objects.get(domain = request.GET['option_price_site'])
        except:
            pass

        try:
            kwargs['entity'] = ProductEntity.objects.get(id = request.GET['product__entity__category_ptr__exact'])
        except:
            pass

        print kwargs

        option_price_option_product = Product.objects.filter(**kwargs).values_list('id', 'title')
        return option_price_option_product

    def queryset(self, request, queryset):
        if self.value():
            print self.value()
            product = Product.objects.get(id=self.value())
            return queryset.filter(product=product)
        else:
            pass