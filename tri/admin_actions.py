# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.contrib import admin
from products.models import *
from django.db.models import Count
from django.db.models import F

import logging


def make_published(modeladmin, request, queryset):
    queryset.update(publish=True)

make_published.short_description = "Mark nodes as published"

def make_unpublished(modeladmin, request, queryset):
    queryset.update(publish=False)

make_unpublished.short_description = "Mark nodes as unpublished"

def activate(modeladmin, request, queryset):
    queryset.update(active=True)

activate.short_description = "Activate nodes"

def deactivate(modeladmin, request, queryset):
    queryset.update(active=False)

deactivate.short_description = "Deactivate nodes"
