# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

SITE_ID = 30

urlpatterns = patterns('',

                       url(r'^vanny-akrilovye-krasnodar$', 'tri.views.products', {'entity': 'bath', 'site_id': SITE_ID}, name='baths'),
                       url(r'^asimmetrichnye-vanny-krasnodar$', 'tri.views.products', {'entity': 'bath', 'cats': ('asimmetrichnie',), 'site_id': SITE_ID}, name='asimetric_baths'),
                       url(r'^pryamougolnye-vanny-krasnodar$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='pryamougolnyie_baths'),
                       url(r'^uglovye-vanny-krasnodar$', 'tri.views.products', {'entity': 'bath', 'cats': ('uglovie',), 'site_id': SITE_ID}, name='uglovie_baths'),
                       # url(r'^$', 'tri.views.products', {'entity': 'bath', 'cats': ('standart',), 'site_id': SITE_ID}, name='standart_baths'),

                       url(r'^mebel-dlya-vannoy-komnaty.html$', 'tri.views.products', {'entity': 'mebel', 'site_id': SITE_ID}, name='mebel'),
                       url(r'^zerkala-krasnodar$', 'tri.views.products', {'entity': 'mebel', 'cats': ('mirrors',), 'site_id': SITE_ID},name='mebel_mirrors'),
                       url(r'^shkafy-navesnye-krasnodar$', 'tri.views.products', {'entity': 'mebel', 'cats': ('selfs',), 'site_id': SITE_ID},name='mebel_selfs'),
                       url(r'^penaly-krasnodar$', 'tri.views.products', {'entity': 'mebel', 'cats': ('penals',), 'site_id': SITE_ID},name='mebel_penals'),
                       url(r'^komody-krasnodar$', 'tri.views.products', {'entity': 'mebel', 'cats': ('commodes',), 'site_id': SITE_ID},name='mebel_commodes'),
                       url(r'^tumby-s-rakovinoy-napolnye-krasnodar$', 'tri.views.products', {'entity': 'mebel', 'cats': ('sinks',), 'site_id': SITE_ID},name='mebel_sinks'),
                       url(r'^tumby-s-rakovinoy-podvesnye-krasnodar$', 'tri.views.products', {'entity': 'mebel', 'cats': ('hang_sinks',), 'site_id': SITE_ID},name='mebel_hang_sinks'),
                       url(r'^tumby-napolnye-krasnodar$', 'tri.views.products', {'entity': 'mebel', 'cats': ('tumb',), 'site_id': SITE_ID},name='mebel_tumb'),

                       url(r'^smesiteli-krasnodar$', 'tri.views.products', {'entity': 'mixer', 'site_id': SITE_ID}, name='mixers'),

                       url(r'^dushevye-kabiny-krasnodar$', 'tri.views.products', {'entity': 'bathcab', 'site_id': SITE_ID}, name='bathcabs'),

                       url(r'^news$', 'tri.views.list', {'slug_in': ('news', 'advices'), 'site_id': SITE_ID}, name='news'),
                       url(r'^map-krasnodar$', 'tri.views.site_structure', {'site_id': SITE_ID}, name='site_structure'),
                       url(r'^questions-krasnodar$', 'tri.views.questions', name='questions'),
)



