from questions.models import Question
from node.models import Tag, AdditionalPhoto, Comment
from rest_framework import serializers
from sorl.thumbnail import get_thumbnail
from products.models import *

import logging


# class QuestionSerializer(serializers.HyperlinkedModelSerializer):

class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('name', 'slug')

class AdditioanalPhotoSerializer(serializers.HyperlinkedModelSerializer):
    pk = serializers.Field()  # Note: `Field` is an untyped read-only field.
    # photo = serializers.ImageField()
    class Meta:
        model = AdditionalPhoto
        fields = ('pk', 'photo')

class CommentSerializer(serializers.HyperlinkedModelSerializer):
    pk = serializers.Field()  # Note: `Field` is an untyped read-only field.
    # photo = serializers.ImageField()
    user = serializers.RelatedField(read_only=True)
    class Meta:
        model = Comment
        fields = ('pk', 'title', 'body', 'user', 'author', 'email', 'pub_date', 'publish')


class QuestionSerializer(serializers.ModelSerializer):
    pk = serializers.Field()  # Note: `Field` is an untyped read-only field.
    user = serializers.RelatedField(read_only=True)
    tags = TagsSerializer(many=True)

    # additionalphotos_query = AdditionalPhoto.objects.all()
    # print(additionalphotos_query)
    # print(pk)

    additionalphoto_set = AdditioanalPhotoSerializer(many=True)
    comment_set = CommentSerializer(many=True)

    class Meta:
        model = Question
        fields = ('title', 'body', 'tags', 'pub_date', 'pk', 'user', 'author', 'additionalphoto_set', 'comment_set')


class OrderItemProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'title')
        depth = 0

class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        product = OrderItemProductSerializer(read_only=True)

        fields = ('id', 'price', 'product')
        depth = 1

class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        # fields = ('title',)
        order_items = OrderItemSerializer(many=True, read_only=True)


        # fields = ('im_id', 'status', )
        # fields = ('im_id', 'status', 'name', 'test', 'address', 'phone', 'email', 'delivery', 'carrier', 'carrier_number', 'distributor', 'delivery_date', 'prepay', 'pay_type')
        depth = 1