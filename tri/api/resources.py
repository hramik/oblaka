__author__ = 'hramik'

from tastypie import fields, utils
from tastypie.resources import ModelResource, Resource, ALL, ALL_WITH_RELATIONS
from django.contrib.auth.models import User
from tastypie.authorization import DjangoAuthorization

from questions.models import Question
from node.models import Tag


class QuestionResource(ModelResource):
    # title = fields.CharField(attribute='title')
    tags = fields.ManyToManyField('self', 'tags', full=True)

    class Meta:
        queryset = Question.on_site.filter(publish=True).order_by('-pub_date')
        # detail_allowed_methods = ['get', 'post', 'put', 'delete']