# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location

PARSE_URL = 'http://www.3tn.ru'


class Command(BaseCommand):
    args = ''
    help = 'Assign prices to materials'

    # option_list = BaseCommand.option_list + (
    #     make_option('--update',
    #                 action='store_true',
    #                 help='Update file from server'),
    # )

    # @transaction.atomic
    def handle(self, *args, **options):


        try:
            import_site = args[0]
        except:
            import_site = '3tn.ru'

        try:
            sku_row = int(args[1])-1
        except:
            sku_row = 1

        try:
            price_row = int(args[2])-1
        except:
            price_row = 2

        # product_site = Site.objects.get(id=1)
        site = Site.objects.get(name=import_site)
        site_info = SiteInfo.objects.get(site=site)

        import_id = int(site_info.import_id) + 1
        site_info.import_id = import_id
        site_info.save()

        import_date = datetime.datetime.now()

        # if import_site == 'this':
        # import_site = get_current_site(request).domain
        #     current_site = get_current_site(request)
        #
        #
        #
        # json_download_link = 'http://www.3tn.ru/dataexport/prices?site=%s&nocache=1' % import_site
        csv_file_name = 'media/docs/%s.csv' % import_site

        with open(csv_file_name, 'rb') as csvfile:
            csv_data = csv.reader(csvfile, delimiter=';')
            for row in csv_data:
                if row[sku_row]:
                    print row[sku_row]
                    try:
                        price = int(str(row[price_row]).replace(",", ".").replace(" ", "").replace("руб.", ""))
                        # sku_objects = SKU.objects.filter(sku=row[sku_row], sites=site)
                        # for sku_object in sku_objects:
                        #
                        #     try:
                        #         option_object = OptionPrice.objects.get(id=sku_object.object_id)
                        #         option_id = option_object.option_id

                                # if option_id in [1, 2] and sku_object.content_type_id == 50:
                                #     SKU.objects.filter(object_id=sku_object.object_id).update(price=0)
                                # else:
                                #     SKU.objects.filter(object_id=sku_object.object_id).update(price=price)
                                #
                                # option_object.save()
                            #
                            # except Exception, e:
                            #     print Exception, e
                            # print sku_object.sku.encode('utf-8'), 'update as product with price', price
                        SKU.objects.filter(sku=row[sku_row], sites=site).update(price=price)

                    except Exception, e:
                        print Exception, e, row[sku_row], row[price_row]

        # return

        products = Product.objects.filter(sites=site)
        for product in products:
            product.save()

        options = OptionPrice.objects.filter(product__sites=site)
        for option in options:
            option.save()

        '''
        Create dublicates of all products
        '''

        OptionPrice.objects.filter(option_id__in=[1, 2]).update(price=0)

        print('Successfully import/update prices')

                    # Import Product prices
                    # try:
                    #     price = float(str(row[price_row]).replace(",",".").replace(" ","").replace("руб.",""))
                    #     products = Product.objects.filter(sku__contains=row[sku_row], sites = site)
                    #     for product in products:
                    #         if product.import_id != import_id:
                    #             product.price = 0
                    #
                    #         if '/' in product.sku:
                    #             product.price = price
                    #         else:
                    #             sku = product.sku.encode('utf-8')
                    #             sku_count = sku.count(row[sku_row])
                    #             product.price = float(product.price) + (price * sku_count)
                    #
                    #         product.import_id = import_id
                    #         product.import_date = import_date
                    #
                    #         product.save()
                    #         print import_id, product.sku.encode('utf-8'), row[sku_row], price, product.price
                    #
                    #
                    # except Exception, e:
                    #     print Exception, e, row[sku_row], row[price_row]
                    #     pass

                    # Import Options prices
                    # try:
                    #     price = int(str(row[price_row]).replace(",",".").replace(" ","").replace("руб.",""))
                    #     options = OptionPrice.objects.filter(sku__contains=row[sku_row], product__sites = site)
                    #     for option in options:
                    #         if option.import_id != import_id:
                    #             option.price = 0
                    #
                    #         if '/' in option.sku:
                    #             option.price = price
                    #         else:
                    #             sku = option.sku.encode('utf-8')
                    #             sku_count = sku.count(row[sku_row])
                    #             option.price = float(option.price) + (price * sku_count)
                    #
                    #         option.import_id = import_id
                    #         option.import_date = import_date
                    #
                    #         option.save()
                    #         print import_id, option.sku.encode('utf-8'), row[sku_row], price, option.price
                    #
                    #
                    # except Exception, e:
                    #     print Exception, e, row[sku_row], row[price_row]
                    #     pass



from django.db.models import AutoField


def copy_model_instance(obj):
    """Create a copy of a model instance.

    M2M relationships are currently not handled, i.e. they are not
    copied.

    See also Django #4027.
    """
    initial = dict([(f.name, getattr(obj, f.name))
                    for f in obj._meta.fields
                    if not isinstance(f, AutoField) and \
                    not f in obj._meta.parents.values()])
    return obj.__class__(**initial)