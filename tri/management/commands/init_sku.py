# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option


from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice, SKU, Option
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo


from tri.views import location
import csv

PARSE_URL = 'http://www.3tn.ru'

class Command(BaseCommand):
    args = ''
    help = 'Init sku from main site'

    def handle(self, *args, **options):
        count = 0

        SKU.objects.all().delete()
        products = Product.objects.all()
        options = OptionPrice.objects.all()

        for product in products:
            print count

            try:
                pat = re.compile(r'([a-zA-Zа-яА-Я0-9]+)')
                product_skus=re.findall(pat, product.sku.encode('utf-8'))

                for product_sku in product_skus:
                    sku = SKU(content_object=product, sku=product_sku)
                    sku.save()
                    for site in product.sites.all():
                        sku.sites.add(site)
                    sku.save()
                    count += 1
            except Exception, e:
                print "pro", Exception, e
                pass

        for option in options:
            print count

            try:
                pat = re.compile(r'([a-zA-Zа-яА-Я0-9]+)')
                option_skus=re.findall(pat, option.sku.encode('utf-8'))

                for option_sku in option_skus:
                    sku = SKU(content_object=option, sku=option_sku)
                    sku.save()
                    for site in option.product.sites.all():
                        sku.sites.add(site)
                    sku.save()
                    count += 1
            except Exception, e:
                print 'op', Exception, e
                pass



        print('Successfully init %s sku' % count)