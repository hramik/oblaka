# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction


from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice
from django.contrib.sites.models import Site

from tri.views import location

PARSE_URL = 'http://www.3tn.ru'


class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'

    option_list = BaseCommand.option_list + (
        make_option('--update',
                    action='store_true',
                    help='Update file from server'),
    )

    @transaction.atomic
    def handle(self, *args, **options):
        kwargs = {}


        try:
            to_site = args[0]
        except:
            print 'EXIT'
            return False

        try:
            entity = args[1]
            kwargs['entity__slug'] = entity
        except:
            pass


        from_site  = '3tn.ru'
        kwargs['sites__name'] = from_site

        product_site = Site.objects.get(name=from_site)
        site = Site.objects.get(name=to_site)


        # if import_site == 'this':
        # import_site = get_current_site(request).domain
        #     current_site = get_current_site(request)
        #
        #
        #
        json_download_link = 'http://www.3tn.ru/dataexport/prices?site=%s&nocache=1' % to_site
        json_file_name = 'media/docs/%s.json' % to_site

        json_meta_file_name = 'media/docs/%s.meta.json' % to_site

        if options.get('update'):
            # Create json file for this site
            data = urllib.urlopen(json_download_link)
            with open(location(json_file_name), 'w') as f:
                myfile = File(f)
                myfile.write(data.read())

        production_products = Product.objects.filter(**kwargs)
        print production_products

        '''
        Create dublicates of all products
        '''

        for production_product in production_products:
            # production_product.sites = [1]
            try:
                region_product = Product.objects.get(nid=production_product.nid, sites=site)
            except:
                region_product = Product(nid=production_product.nid)
                region_product.save()

                region_product.sites.add(site)
                region_product.save()

            region_product.title = production_product.title.encode('utf-8')
            region_product.sku = production_product.sku.encode('utf-8')
            region_product.popular = production_product.popular

            # json = JSONField(blank=True)
            region_product.tn_id = production_product.tn_id
            region_product.tn_alias = production_product.tn_alias

            region_product.slug = production_product.slug
            region_product.dot_slug = production_product.dot_slug

            region_product.body = production_product.body
            region_product.teaser = production_product.teaser

            region_product.h1 = production_product.h1
            region_product.h2 = production_product.h2
            region_product.page_title = production_product.page_title
            region_product.meta_desc = production_product.meta_desc
            region_product.meta_keywords = production_product.meta_keywords

            region_product.price = 0

            # TODO: add tags

            if not region_product.visor_set.all():
                for visor in production_product.visor_set.all():
                    try:
                        visor = Visor.objects.get_or_create(product=region_product, name=visor.name)
                    except Exception, e:
                        print "visor_err", Exception, e
                        pass

            region_product.entity = production_product.entity
            region_product.type = production_product.type

            for category in production_product.category.all():
                region_product.category.add(category)

            region_product.char_photo = production_product.char_photo
            region_product.photo = production_product.photo

            '''
            PHOTOS
            '''

            for photo in production_product.additionalphoto_set.all():
                try:
                    photo = AdditionalPhoto.objects.get_or_create(node=region_product, position=photo.position, photo=photo.photo, alt=photo.alt, title=photo.title)
                except:
                    pass

            '''
            VIDEO
            '''

            for video in production_product.additionalvideos_set.all():
                try:
                    video = AdditionalVideos.objects.get_or_create(node=region_product, position=video.position, video_url=video.video_url, video_poster=video.video_poster,
                                                                   video_type=video.video_type)
                except:
                    pass



            region_product.length = production_product.length
            region_product.width = production_product.width
            region_product.height = production_product.height
            region_product.depth = production_product.depth
            region_product.value = production_product.value
            region_product.hydromassage = production_product.hydromassage

            region_product.save()
            print region_product.title

            # try:
            #     for product_price in production_product.optionprices.all():
            #         op, created = OptionPrice.objects.get_or_create(product=region_product, option=product_price.option)
            #         op.price = 0
            #         op.position = product_price.position
            #         op.save()
            # except Exception, e:
            #     print Exception, e, "old_options_error"
            #     pass

        '''
        Take info from jsons
        '''

        try:
            # options = args[2]

            json_data = open(location(json_file_name))
            print 'json_data', json_data
            json_load = json.load(json_data)
            print 'json_load', json_load



            for item in json_load:
                json_product = json_load[item]

                try:
                    print json_product["name"].encode('utf-8')
                    product = Product.objects.get(nid=json_product["siteitemid"], sites=site)
                    product.price = json_product["price"]
                    product.save()
                except:
                    pass

                '''
                OPTIONS
                '''
                try:
                    index = 0
                    for option in json_product["options"]:
                        index += 1
                        try:

                            o = Option.objects.get(slug=option["alias"])

                            op, created = OptionPrice.objects.get_or_create(product=product, option=o)
                            op.price = option["price"]
                            # op.sku = production_product.sku.encode('utf-8')
                            # optionprice = product.optionprice_set.get(option=region_option_price.option)
                            # region_option_price.sku = optionprice.sku
                            # region_option_prices = OptionPrice.objects.filter(product__nid=option_price.product.nid, option__slug=option_price.option.slug).update(sku=option_price.sku)

                            op.position = index
                            op.save()

                        except Exception, e:
                            print Exception, e, "none"
                            print "option_error"
                except Exception, e:
                    print Exception, e, "options_error"
                    try:
                        production_product = Product.objects.get(nid=product.nid, sites__id=1)
                        for product_price in production_product.optionprices.all():
                            op, created = OptionPrice.objects.get_or_create(product=product, option=product_price.option)
                            op.price = 0
                            op.position = product_price.position
                            op.save()
                    except Exception, e:
                        print Exception, e, "old_options_error"
                        pass

            json_data = open(location(json_meta_file_name))
            json_load = json.load(json_data)

            for item in json_load:
                json_product = json_load[item]
                print json_product

                try:
                    print json_product["name"].encode('utf-8')
                    product = Product.objects.get(nid=json_product["id"], sites=site)
                    product.teaser = json_product["s_description"]
                    product.body = json_product["description"]

                    product.meta_keywords = json_product["meta_keywords"]
                    product.meta_desc = json_product["meta_description"]
                    product.page_title = json_product["title"]

                    product.dot_slug = json_product["url"]
                    product.slug = json_product["url"]
                    product.title = json_product["name"]
                    product.h1 = json_product["name"]

                    '''
                    PHOTOS
                    '''

                    try:
                        img_url = json_product["thumb_image"]
                        name = "%s-%s-thumb.%s" % (product.dot_slug, to_site, os.path.splitext(img_url)[1])
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        product.photo.save(name, content, save=True)
                    except Exception, e:
                        print 'thumb_image_err %s %s %s' % (img_url, Exception, e)

                    try:
                        photo = AdditionalPhoto(node=product, position=0)
                        img_url = json_product["main_image"]
                        product.additionalphoto_set.all().delete()
                        # name = urlparse(img_url).path.split('/')[-1]
                        name = "%s-%s-main.%s" % (product.dot_slug, to_site, os.path.splitext(img_url)[1])
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        photo.photo.save(name, content, save=True)
                    except Exception, e:
                        print 'main_image_err', Exception, e

                    try:
                        index = 1
                        for image in json_product["images"]:
                            photo = AdditionalPhoto(node=product, position=index)
                            img_url = image
                            # name = urlparse(img_url).path.split('/')[-1]
                            name = "%s-%s-photo-%s.%s" % (product.dot_slug, to_site, str(index), os.path.splitext(img_url)[1])
                            content = ContentFile(urllib2.urlopen(img_url).read())
                            photo.photo.save(name, content, save=True)
                            index += 1
                    except Exception, e:
                        print 'image_err', Exception, e, "images_error"

                    product.save()
                except Exception, e:
                    print "%s %s" % (Exception, e)
                    pass
        except Exception, e:
                    print Exception, e, "common_error"



        #     try:
        #         print 'try'
        #         product = Product.on_site.get(tn_alias=json_product["alias"])
        #         print product
        #         #product_details, created_product_details = ProductDetails.objects.get_or_create(product=product, sites=current_site)
        #         product.json = json_product
        #         product.json_save()
        #     #product.save(current_site=current_site, product_details=product_details)
        #     except Product.DoesNotExist:
        #         print 'does_not_exist'
        #         product = Product(title=json_product["name"],
        #                           tn_alias=json_product["alias"], sites=import_site)
        #         print product, import_site
        #         #product_details, created_product_details = ProductDetails.objects.get_or_create(product=product, sites=current_site)
        #         product.save()
        #         product.json = json_product
        #         product.json_save()


        print('Successfully import/update products')


from django.db.models import AutoField


def copy_model_instance(obj):
    """Create a copy of a model instance.

    M2M relationships are currently not handled, i.e. they are not
    copied.

    See also Django #4027.
    """
    initial = dict([(f.name, getattr(obj, f.name))
                    for f in obj._meta.fields
                    if not isinstance(f, AutoField) and \
                    not f in obj._meta.parents.values()])
    return obj.__class__(**initial)