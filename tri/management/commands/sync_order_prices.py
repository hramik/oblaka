# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node, SEONode
from  tri.models import Page


class Command(BaseCommand):
    args = ''
    help = 'Sync second title between products and clear nodes'

    def handle(self, *args, **options):

        for order in Order.objects.all():
            try:
                for order_item in order.order_items.all():
                    order_item.price = order_item.product.price
                    order_item.order_item_options.all().delete()
                    for option_price in order_item.options.all():
                        order_item_option = OrderItemOption(option=option_price.option, price=option_price.price)
                        order_item_option.save()
                        order_item.order_item_options.add(order_item_option)
                    order_item.save()
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully sync order prices' )
