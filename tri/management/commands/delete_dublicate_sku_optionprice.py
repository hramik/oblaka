# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
from django.db.models import Q

import re
from optparse import OptionParser
from optparse import make_option

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location
import csv


class Command(BaseCommand):
    args = ''
    help = 'Delete duplicate skus on optionprice'

    def handle(self, *args, **options):
        domains = ['www.saratov3tn.ru', 'www.volgograd3tn.ru', 'www.kavkaz3tn.ru', 'www.kazan3tn.ru',
                   'www.kirov3tn.ru', 'www.nnovgorod3tn.ru', 'www.orenburg3tn.ru', 'www.rostov3tn.ru',
                   'www.krasnodar3tn.ru', 'www.samara3tn.ru']

        double_sku_ids = [5, 23, 25, 27, 29, 30, 32, 49, 98, 100]

        for domain in domains:
            print domain
            site = Site.objects.get(domain=domain)
            object_ids = SKU.objects.filter(content_type=50, sites=site).values_list('object_id', flat=True).distinct()
            for object_id in object_ids:
                skus = SKU.objects.filter(content_type=50, object_id=object_id)
                # if skus.__len__() > 3:
                #     print 'object_id (>3)', object_id
                #     if skus[0].sku == skus[1].sku == skus[2].sku == skus[3].sku:
                #         print 'equal, deleting excess skus (2)', skus[0].sku.encode('utf-8'), skus[1].sku.encode('utf-8'), \
                #             skus[2].sku.encode('utf-8'), skus[3].sku.encode('utf-8')
                #         skus[2].delete()
                #         skus[3].delete()
                #         continue
                if skus.__len__() > 1:
                    if skus[0].sku == skus[1].sku:
                        option_id = OptionPrice.objects.get(id=skus[0].object_id).serializable_value('option_id')
                        if option_id not in double_sku_ids:
                            print 'object_id (>1)', object_id
                            print 'option_id', option_id
                            print 'equal, deleting excess skus (1)', skus[0].sku.encode('utf-8'), skus[1].sku.encode('utf-8')
                            # skus[1].delete()
                            continue

        print('Successfully delete dublicate skus in optionprice')
