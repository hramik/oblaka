# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option


from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product

from tri.views import location
import csv
from products.models import *

PARSE_URL = 'http://www.3tn.ru'

class Command(BaseCommand):
    args = ''
    help = 'Import weights to sku'


    option_list = BaseCommand.option_list + (
        make_option('--update',
            action='store_true',
            help='Update file from server'),
    )

    def handle(self, *args, **options):
        csv_file_name = 'media/docs/weight.csv'
        count = 0


        with open(csv_file_name, 'rb') as csvfile:
            csv_data = csv.reader(csvfile, delimiter=';')
            for row in csv_data:

                try:
                    SKU.objects.filter(sku=row[0]).update(weight=row[3].replace(',','.'))
                    print row[0], row[3], row[3].replace(',','.')
                    count += 1
                except Exception, e:
                    print Exception, e
                    pass


        print('Successfully import %s weights' % count)