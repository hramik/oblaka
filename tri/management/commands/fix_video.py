# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo
from django.contrib.contenttypes.models import ContentType

from tri.views import location
import csv

class Command(BaseCommand):
    args = ''
    help = 'Update region sku from main site'

    def handle(self, *args, **options):
        count = 0


        videos = AdditionalVideos.objects.filter(video_url__endswith='webn')

        for video in videos:
            video.video_url = video.video_url.replace('webn', 'webm', 1)
            video.save()



        print('Successfully fix videos')