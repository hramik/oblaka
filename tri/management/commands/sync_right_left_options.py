# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location
from django.conf.urls.static import static
from django.conf import settings
from price_parsing.views import get_page

class Command(BaseCommand):
    args = ''
    help = ''

    # @transaction.atomic
    def handle(self, *args, **options):

        main_site = Site.objects.get(id=1)

        sites = Site.objects.exclude(id=1)

        products = Product.objects.filter(sites=main_site)
        for product in products:

            product = Product.objects.get(id=product.id)


            try:
                product_right = OptionPrice.objects.get(product=product, option__id=1)
                product_left = OptionPrice.objects.get(product=product, option__id=2)

                products_to_change = Product.objects.filter(nid=product.nid)


                for product_to_change in products_to_change:

                    try:
                        product_ch_right = OptionPrice.objects.get(product=product_to_change, option__id=1)
                        product_ch_left = OptionPrice.objects.get(product=product_to_change, option__id=2)

                        print product_ch_right, product_ch_left, product_to_change

                        option_sku = SKU(content_object=product_ch_right, sku=product_right.first_sku())
                        option_sku.save()

                        option_sku = SKU(content_object=product_ch_left, sku=product_left.first_sku())
                        option_sku.save()


                    except Exception, e:
                        print Exception, e

            except Exception, e:
                        print Exception, e

            print product


        print('Check done')

