# -*- coding: utf-8 -*-

__author__ = 'hramik'


from django.core.management.base import BaseCommand, CommandError
from tri.models import Page



class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'

    def handle(self, *args, **options):

        pages = Page.on_site.all()
        print pages
        for page in pages:
            print page.title.encode('utf-8')
            page_body = page.body.replace('src="/static/import', 'src="')
            page.body = page_body
            page.save()

        print('Images url change complete!')
