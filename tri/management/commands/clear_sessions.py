# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from django.contrib.sessions.models import Session
from datetime import datetime

class Command(BaseCommand):
    args = ''
    help = 'Delete sessions older than 1 week'

    def handle(self, *args, **options):
        try:
            objects = Session.objects.filter(expire_date__lte=datetime.now())
            print objects.count()
        except Exception, e:
            print Exception, e