# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node, SEONode
from tri.models import Page


class Command(BaseCommand):
    args = ''
    help = 'Set picasa gallery to product'

    def handle(self, *args, **options):
        nodes = Product.objects.all()

        for node in nodes:
            try:
                # node.h1 = node.title
                # node.h2 = node.second_title
                # node.save()

                try:
                    Visor.objects.filter(product=node).delete()
                except:
                    pass

                if os.path.exists(location("media/visor/%s" % node.nid)):
                    visor = Visor(product = node, name = "%s" % node.nid)
                    visor.save()

                if os.path.exists(location("media/visor/%sb" % node.nid)):
                    visor = Visor(product = node, name = "%sb" % node.nid)
                    visor.save()

                print "%s DONE: %s" % (node.dot_slug, node.json['picasaLink'])
            except Exception, e:
                print '%s %s' % (Exception, node.dot_slug)
                pass

        print('Successfully set picasa gallery' )

