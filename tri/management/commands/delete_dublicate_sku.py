# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
from django.db.models import Q

import re
from optparse import OptionParser
from optparse import make_option

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location
import csv

PARSE_URL = 'http://www.3tn.ru'


class Command(BaseCommand):
    args = ''
    help = 'Update region sku from main site'

    def handle(self, *args, **options):
        count = 0

        options = OptionPrice.objects.filter(option__id__in=(1,2)).values_list('id', flat=True)
        skus = SKU.objects.filter(object_id__in=options)
        for sku in skus:

            dubl_skus = SKU.objects.filter(object_id=sku.object_id, sku=sku.sku)

            if dubl_skus.count() > 1:
                # dubl_skus.exclude(id=sku.id).delete()
                print sku.sku, sku.object_id

        print('Successfully delete sublicate sku')
