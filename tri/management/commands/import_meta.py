# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction


from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice
from django.contrib.sites.models import Site

from tri.views import location

PARSE_URL = 'http://www.3tn.ru'


class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'

    option_list = BaseCommand.option_list + (
        make_option('--update',
                    action='store_true',
                    help='Update file from server'),
    )

    # @transaction.atomic
    def handle(self, *args, **options):
        kwargs = {}


        try:
            to_site = args[0]
        except:
            print 'EXIT'
            return False

        try:
            entity = args[1]
            kwargs['entity__slug'] = entity
        except:
            pass


        from_site  = '3tn.ru'
        kwargs['sites__name'] = from_site

        product_site = Site.objects.get(name=from_site)
        site = Site.objects.get(name=to_site)


        # if import_site == 'this':
        # import_site = get_current_site(request).domain
        #     current_site = get_current_site(request)
        #
        #
        #
        json_download_link = 'http://www.3tn.ru/dataexport/prices?site=%s&nocache=1' % to_site
        json_file_name = 'media/docs/%s.json' % to_site

        json_meta_file_name = 'media/docs/%s.meta.json' % to_site


        production_products = Product.objects.filter(**kwargs)

        '''
        Take info from jsons
        '''

        count = 0

        try:


            json_data = open(location(json_meta_file_name))
            json_load = json.load(json_data)
            print json_load

            for item in json_load:
                json_product = json_load[item]


                try:
                    print json_product["name"].encode('utf-8')
                    print json_product["id"].encode('utf-8')
                    product = Product.objects.get(nid=json_product["id"], sites=site)
                    print '--'
                    # product.teaser = json_product["s_description"]
                    product.body = json_product["description"]

                    product.meta_keywords = json_product["meta_keywords"]
                    product.meta_desc = json_product["meta_description"]
                    product.page_title = json_product["title"]

                    product.dot_slug = json_product["url"]
                    product.slug = json_product["url"]
                    product.title = json_product["name"]
                    product.h1 = json_product["name"]

                    '''
                    PHOTOS
                    '''

                    try:
                        img_url = json_product["thumb_image"]
                        name = "%s-%s-thumb.%s" % (product.dot_slug, to_site, os.path.splitext(img_url)[1])
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        product.photo.save(name, content, save=True)
                    except Exception, e:
                        print 'thumb_image_err %s %s %s' % (img_url, Exception, e)

                    try:
                        photo = AdditionalPhoto(node=product, position=0)
                        img_url = json_product["main_image"]
                        product.additionalphoto_set.all().delete()
                        # name = urlparse(img_url).path.split('/')[-1]
                        name = "%s-%s-main.%s" % (product.dot_slug, to_site, os.path.splitext(img_url)[1])
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        photo.photo.save(name, content, save=True)
                    except Exception, e:
                        print 'main_image_err', Exception, e

                    try:
                        index = 1
                        for image in json_product["images"]:
                            photo = AdditionalPhoto(node=product, position=index)
                            img_url = image
                            # name = urlparse(img_url).path.split('/')[-1]
                            name = "%s-%s-photo-%s.%s" % (product.dot_slug, to_site, str(index), os.path.splitext(img_url)[1])
                            content = ContentFile(urllib2.urlopen(img_url).read())
                            photo.photo.save(name, content, save=True)
                            index += 1
                    except Exception, e:
                        print 'image_err', Exception, e

                    product.save()
                    count += 1
                except Exception, e:
                    print "json_product %s %s" % (Exception, e)
                    pass
        except Exception, e:
            print 'common_err', Exception, e



        #     try:
        #         print 'try'
        #         product = Product.on_site.get(tn_alias=json_product["alias"])
        #         print product
        #         #product_details, created_product_details = ProductDetails.objects.get_or_create(product=product, sites=current_site)
        #         product.json = json_product
        #         product.json_save()
        #     #product.save(current_site=current_site, product_details=product_details)
        #     except Product.DoesNotExist:
        #         print 'does_not_exist'
        #         product = Product(title=json_product["name"],
        #                           tn_alias=json_product["alias"], sites=import_site)
        #         print product, import_site
        #         #product_details, created_product_details = ProductDetails.objects.get_or_create(product=product, sites=current_site)
        #         product.save()
        #         product.json = json_product
        #         product.json_save()


        print('Successfully %d import/update products' % count)


from django.db.models import AutoField


def copy_model_instance(obj):
    """Create a copy of a model instance.

    M2M relationships are currently not handled, i.e. they are not
    copied.

    See also Django #4027.
    """
    initial = dict([(f.name, getattr(obj, f.name))
                    for f in obj._meta.fields
                    if not isinstance(f, AutoField) and \
                    not f in obj._meta.parents.values()])
    return obj.__class__(**initial)