# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location

PARSE_URL = 'http://www.3tn.ru'


class Command(BaseCommand):
    args = ''
    help = 'Assign prices to materials'

    # option_list = BaseCommand.option_list + (
    #     make_option('--update',
    #                 action='store_true',
    #                 help='Update file from server'),
    # )

    def handle(self, *args, **options):


        try:
            import_site = args[0]
        except:
            import_site = '3tn.ru'

        try:
            sku_row = int(args[1])-1
        except:
            sku_row = 1

        try:
            price_row = int(args[2])-1
        except:
            price_row = 2

        # product_site = Site.objects.get(id=1)
        site = Site.objects.get(name=import_site)
        site_info = SiteInfo.objects.get(site=site)

        import_id = int(site_info.import_id) + 1
        site_info.import_id = import_id
        site_info.save()

        import_date = datetime.datetime.now()



        # if import_site == 'this':
        # import_site = get_current_site(request).domain
        #     current_site = get_current_site(request)
        #
        #
        #
        # json_download_link = 'http://www.3tn.ru/dataexport/prices?site=%s&nocache=1' % import_site



        with open(csv_file_name, 'rb') as csvfile:
            csv_data = csv.reader(csvfile, delimiter=';')
            for row in csv_data:
                if row[sku_row]:

                    try:
                        price = int(str(row[price_row]).replace(",",".").replace(" ","").replace("руб.",""))
                        product_skus = SKU.objects.filter(sku=row[sku_row], product__sites = site).update(price=row[price_row])
                    except Exception, e:
                        print Exception, e, row[sku_row], row[price_row]
                        pass



        print('Successfully recalc prices')


from django.db.models import AutoField


def copy_model_instance(obj):
    """Create a copy of a model instance.

    M2M relationships are currently not handled, i.e. they are not
    copied.

    See also Django #4027.
    """
    initial = dict([(f.name, getattr(obj, f.name))
                    for f in obj._meta.fields
                    if not isinstance(f, AutoField) and \
                    not f in obj._meta.parents.values()])
    return obj.__class__(**initial)