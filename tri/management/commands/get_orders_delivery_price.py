# -*- coding: utf-8 -*-

__author__ = 'hramik'

from dpd.models import *

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node, SEONode
from tri.models import Page
from datetime import *


class Command(BaseCommand):
    args = ''
    help = 'Sync orders statuses'

    def handle(self, *args, **options):

        debug = False
        try:
            debug = args[0]
        except:
            pass

        # Get DPD order id's data for each order for last 3 month
        print "Get DPD order id's data for each order for last 3 month"
        # orders = Order.objects.exclude(carrier_number='')
        orders = Order.objects.filter(carrier=22446, pub_date__range=(datetime.now() - timedelta(days=14), datetime.now()))
        for order in orders:
            local_order_id = 'IM-%s' % order.id
            if order.version:
                local_order_id = '%s/%s' % (local_order_id, order.version)
            print local_order_id, order.carrier.id
            # continue

            a = DpdService()

            # data = a.check_order_status_by_dpd_id(order.carrier_number)
            data = a.check_order_status_by_local_id(local_order_id)

            if not data:
                continue

            try:
                DeliveryOperations.objects.get(carrier_number=order.carrier_number)
            except:
                op = DeliveryOperations()
                op.order = order
                op.carrier_number = order.carrier_number
                op.save()
                print 'Added', order.carrier_number

            try:
                carrier_number = data.states[0].dpdOrderNr
                try:
                    DeliveryOperations.objects.get(carrier_number=carrier_number)
                except:
                    op = DeliveryOperations()
                    op.order = order
                    op.carrier_number = carrier_number
                    op.save()
                    print 'Added', carrier_number
            except:
                carrier_number = ''


            for state in data.states:
                try:
                    if state.dpdOrderReNr != carrier_number:
                        try:
                            DeliveryOperations.objects.get(carrier_number=state.dpdOrderReNr)
                        except:
                            op = DeliveryOperations()
                            op.order = order
                            op.carrier_number = state.dpdOrderReNr
                            op.save()
                            print 'Added', state.dpdOrderReNr
                except:
                    pass

        # return
        # Get orders cost for last 3 month and update database
        print "Get orders cost for last 3 month and update database"
        prev_month = (date.today() - timedelta(days=14)).isoformat()
        now = date.today().isoformat()

        if not debug:
            a = DpdService()
            data = a.get_orders_amount(prev_month, now)
            self._log_data(data)

        for item in data:
            print item
            try:
                op = DeliveryOperations.objects.get(carrier_number=item['ordernum'])
                op.delivery_price = item.amount
                op.save()
                print 'Updated ', item['ordernum'], ', price ', item.amount
            except:
                print 'Missing DPD order id: ', item['ordernum']

        print('Successfully updated costs for delivery')

    def _log_data(self, data):
        now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        # file_name = location("/dpd_logs/%s" % now) settings.MEDIA_ROOT + '\\dpd_logs\\' + now + '-' + method_name
        file_name = os.path.join(settings.MEDIA_ROOT, 'dpd_delivery_amounts',  '%s' % now)
        handle = open(file_name, 'w+')
        handle.write(str(data))
        handle.close()
        return
