# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.utils.html import strip_tags


from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
from questions.models import Question


class Command(BaseCommand):
    args = ''
    help = 'Auto title for questions'

    def handle(self, *args, **options):
        questions = Question.objects.all()

        for question in questions:
            try:
                body = strip_tags(question.body).replace('&nbsp;', ' ')
                question.title = body[:300]
                question.h1 = question.title
                question.page_title = question.title
                question.dot_slug = 'question-%s' % question.id
                question.save()
                print "DONE: %s %s" % (question.pk, question.title.encode('utf-8'))
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully append questions titles' )
