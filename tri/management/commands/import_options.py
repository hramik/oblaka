# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option


from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Option, OptionsGroup

from tri.views import location

PARSE_URL = 'http://www.3tn.ru'

class Command(BaseCommand):
    args = ''
    help = 'Import options'


    option_list = BaseCommand.option_list + (
        make_option('--update',
            action='store_true',
            help='Update file from server'),
    )

    def handle(self, *args, **options):
        json_download_link = '%s/dataexport/options' % PARSE_URL
        json_file_name = 'media/docs/options.json'

        if options.get('update'):
            # Create json file for this site
            data = urllib.urlopen(json_download_link)
            with open(location(json_file_name), 'w') as f:
                myfile = File(f)
                myfile.write(data.read())

        json_data = open(location(json_file_name))
        json_load = json.load(json_data)
        none_comment_sum = 0
        for_index = 0

        for item in json_load:
            option = json_load[item]
            for_index += 1
            try:
                if option["group"]:
                    print "option_group"
                    og, created = OptionsGroup.objects.get_or_create(name=option["group"], slug=option["group_alias"])
                    o, created = Option.objects.get_or_create(name=option["name"], slug=option["alias"], shortname=option["shortname"], group=og)
                else:
                    print "option_one"
                    og, created = OptionsGroup.objects.get_or_create(name="None", slug="none")
                    o, created = Option.objects.get_or_create(name=option["name"], slug=option["alias"], shortname=option["shortname"], group=og)

                # op, created = OptionPrice.objects.get_or_create(product=self, option=o, price="0")
                # op.position = for_index
                # op.save()

                # o.exclude = []
                if option["exclude"]:
                    for exclude in option["exclude_alias"]:
                        # "hydromassazh", "aeromassazh"
                        print 'exclude: %s' % exclude
                        exclude_option = Option.objects.get(slug=exclude)
                        o.exclude.add(exclude_option)

                # o.dependent = []
                if option["dependent"]:
                    for dependent in option["dependent_alias"]:
                        # "hydromassazh", "aeromassazh"
                        dependent_option = Option.objects.get(slug=dependent)
                        o.dependent.add(dependent_option)


                #exact_option = OptionPrice.get(option__slug=option["alias"], product = self)
            except Exception, e:
                print Exception, e, "none"
                print "option_error"


        print('Successfully import/update options')