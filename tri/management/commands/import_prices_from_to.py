# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location

PARSE_URL = 'http://www.3tn.ru'


class Command(BaseCommand):
    args = ''
    help = 'Assign prices to materials'

    # option_list = BaseCommand.option_list + (
    #     make_option('--update',
    #                 action='store_true',
    #                 help='Update file from server'),
    # )

    def handle(self, *args, **options):


        try:
            import_site = args[0]
        except:
            import_site = '3tn.ru'

        export_site = args[1]


        # product_site = Site.objects.get(id=1)
        from_site = Site.objects.get(name=import_site)
        to_site = Site.objects.get(name=export_site)

        site_info = SiteInfo.objects.get(site=to_site)

        import_id = int(site_info.import_id) + 1
        site_info.import_id = import_id
        site_info.save()

        import_date = datetime.datetime.now()

        from_producs = Product.objects.filter(sites__domain=from_site)

        for product in from_producs:
            to_products = Product.objects.filter(nid=product.nid, sites__domain=export_site)

            for to_product in to_products:
                print to_product.title.encode('utf-8')

                to_product.price = product.price
                to_product.save()

                for option_price in product.optionprices.all():
                    region_option_price, create = OptionPrice.objects.get_or_create(product=to_product, option=option_price.option)

                    region_option_price.position = option_price.position
                    region_option_price.sku = option_price.sku
                    region_option_price.price = option_price.price

                    region_option_price.save()
                #     region_option_prices = OptionPrice.objects.filter(product__nid=option_price.product.nid, option__slug=option_price.option.slug).update(sku=option_price.sku)
                    # region_option_price.sku = optionprice.sku


                print('Product update %s' % product.title.encode('utf-8'))




        '''
        Create dublicates of all prices
        '''


        print('Successfully import/update prices')

