# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option


from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo


from tri.views import location
import csv

PARSE_URL = 'http://www.3tn.ru'

class Command(BaseCommand):
    args = ''
    help = 'Update region sku from main site'

    def handle(self, *args, **options):
        count = 0

        main_site = Site.objects.get(id=1)
        products = Product.objects.filter(sites=main_site)
        option_prices = OptionPrice.objects.filter(product__in=products, price='').delete()



        print('Successfully delete extra options')