# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
import os.path
from django.apps import apps, AppConfig

from django.db import connections

from products.models import *
from tri.models import *
from menu.models import *
from oblaka.settings import location
from custom_sites.models import *


class Command(BaseCommand):

    args = ''
    help = 'Migrate datas from mysql to postgree DB'

    def handle(self, *args, **options):

        app = args[0] #'menu'
        model = args[1] #'Menu'


        Model = apps.get_model(app, model)

        items = Model.objects.using('mysql').all().order_by('id')
        triton_site, create = Site.objects.get_or_create(domain='www.triton3tn.ru', name='www.triton3tn.ru')

        for item in items:
            print item
            try:
                if hasattr(item,'sites') and triton_site in item.sites.all():
                    item.save(using='default', force_insert=True)
                    item.sites.add(triton_site)
                else:
                    item.save(using='default', force_insert=True)
            except Exception as e:
                print(Exception, e)

        print('Successfully port materials')
