# -*- coding: utf-8 -*-

__author__ = 'm13v246'

import datetime
from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node, SEONode
from tri.models import Page
from dpd.models import *

class Command(BaseCommand):
    help = 'Get all orders changed ids'

    def handle(self, *args, **options):

        orders = Order.objects.exclude(carrier_number='')
        for order in orders:
            print '\nOrder number', order.carrier_number

            a = DpdService()

            data = a.check_order_status_by_dpd_id(order.carrier_number)
            if not data:
                continue

            try:
                ops = DeliveryOperations(order)
            except Exception, e:
                print Exception, e
                ops = None
                pass

            try:
                print data.docId
            except Exception, e:
                print Exception, e

            try:
                DeliveryOperations.objects.get(carrier_number=order.carrier_number)
            except:
                op = DeliveryOperations()
                op.order = order
                op.carrier_number = order.carrier_number
                op.save()
                print 'Added', order.carrier_number

            for state in data.states:
                try:
                    if state.dpdOrderReNr != order.carrier_number:
                        try:
                            DeliveryOperations.objects.get(carrier_number=state.dpdOrderReNr)
                        except:
                            op = DeliveryOperations()
                            op.order = order
                            op.carrier_number = state.dpdOrderReNr
                            op.save()
                            print 'Added', state.dpdOrderReNr
                except:
                    pass


