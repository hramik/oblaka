# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re

from optparse import OptionParser
from optparse import make_option

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import Node
from tri.models import *

from tri.views import location


IMPORT_ID = 9


class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'

    option_list = BaseCommand.option_list + (
        make_option('--update',
            action='store_true',
            help='Update file from server'),
    )

    def handle(self, *args, **options):
        json_download_link = 'http://www.3tn.ru/dataexport/comments'
        json_file_name = 'media/docs/comments.json'

        comment_user = False

        # Create json file for this site
        if options.get('update'):
            data = urllib.urlopen(json_download_link)
            with open(location(json_file_name), 'w') as f:
                myfile = File(f)
                myfile.write(data.read())

        json_data = open(location(json_file_name))
        json_load = json.load(json_data)
        none_comment_sum = 0
        for_index = 0

        for item in json_load:
            for_index += 1

            json_c = json_load[item]

            # print '--------------------------------------------------'
            # print json_c['nid']

            # Take comment node — exist node or homeless_comments
            try:
                comment_node = Node.objects.get(nid=json_c['nid'])
            except Exception, e:
                comment_node = Node.objects.get(slug='homeless_comments')
                # print 'hommeless comment', Exception, e


            # Edit comment_body
            try:
                comment_body = json_c['body'].encode('utf-8').replace('/sites/all/libraries/tinymce/jscripts/tiny_mce/plugins/emotions/img/', '/static/images/smiles/')

                images_re = re.compile(r'<img\s+src="(\/sites\/default\/files\/[\w\d\/_]+.[\w]+)"[\s\d\w="\/А-я]+>')
                script_re = re.compile(r'<script src=\"[htps]+:\/\/[\w.\/]+\.js\">.*<\/script>')
                comment_images = images_re.findall(comment_body)
                comment_body = images_re.sub('', comment_body)
                comment_body = script_re.sub('', comment_body)
            except Exception, e:
                print 'body_error', Exception, e
                comment_body = ''

            try:
                try:
                    '''
                    If item is question
                    '''
                    comment_user = UserProfile.objects.get(uid=json_c['uid']).user
                    comment_question = Question.objects.get(pub_date=datetime.datetime.fromtimestamp(int(json_c['date'])))
                    if comment_question.user != comment_user:
                        comment_question.user = comment_user
                    comment_question.last_comment_date = comment_question.pub_date
                    comment_question.body = comment_body

                    for image in comment_images:
                        if not comment_question.additionalphoto_set.all():
                            try:
                                photo = AdditionalPhoto(node=comment_question)
                                img_url = "http://3tn.ru" + image
                                name = urlparse(img_url).path.split('/')[-1]
                                content = ContentFile(urllib2.urlopen(img_url).read())
                                photo.photo.save(name, content, save=True)
                            except Exception, e:
                                print 'photo_error', Exception, e

                    comment_question.save()
                    print "comment_question SAVE"
                except Exception, e:
                    '''
                    Item is just comment
                    '''
                    try:
                        comment_user = UserProfile.objects.get(uid=json_c['uid']).user
                        comment = Comment.objects.get(cid=json_c['cid'])
                        comment_node = comment.node
                    except Exception, e:
                        comment = Comment(cid=json_c['cid'], user=comment_user, node=comment_node)
                        comment.import_id = IMPORT_ID
                        comment.save()
                        # print 'new_comment'

                    # none_comment_sum += 1
                    # print 'none_comment_sum',none_comment_sum


                    # PHOTO
                    for image in comment_images:
                        if not comment.commentadditionalphoto_set.all():
                            try:
                                photo = CommentAdditionalPhoto(comment=comment, position=0)
                                img_url = "http://3tn.ru" + image
                                print img_url
                                name = urlparse(img_url).path.split('/')[-1]
                                content = ContentFile(urllib2.urlopen(img_url).read())
                                photo.photo.save(name, content, save=True)
                            except Exception, e:
                                print 'photo_error', Exception, e

                    comment.body = comment_body
                    comment.pub_date = datetime.datetime.fromtimestamp(int(json_c['date']))
                    comment.user = comment_user

                    comment.save()
                    comment_node.last_comment_date = comment.pub_date
                    comment_node.save()
                    # print "coment and comment_node SAVE"
            except Exception, e:
                print 'big_error', Exception, e
                pass


        print('Successfully import/update comments')
