# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import *
from tri.models import UserProfile

from transliterate import translit, get_available_language_codes


class Command(BaseCommand):
    args = ''
    help = 'Set site field fo 3tn.ru'

    def handle(self, *args, **options):
        # nodes = Product.objects.all()
        users = UserProfile.objects.all()

        for user in users:
            try:
                if user.color == 'red':
                    user.color = '#2aa4e9'
                    user.save()
                    print "DONE: %s" % user.user.username.encode('utf=8')
            except Exception, e:
                print '%s %s %s' % (Exception, e, user.user.username)
                pass

        print('Successfully set colors to users' )

