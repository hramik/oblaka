__author__ = 'hramik'
# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo
from django.contrib.contenttypes.models import ContentType

from tri.views import location
import csv

PARSE_URL = 'http://www.3tn.ru'


class Command(BaseCommand):
    args = ''
    help = 'Update region sku from main site'

    def handle(self, *args, **options):
        count = 0

        try:
            in_site = args[0]
        except:
            in_site = None

        try:
            from_site = Site.objects.get(name=args[1])
        except:
            from_site = Site.objects.get(id=1)

        try:
            if args[1] == 'delete':
                delete = True
                # SKU.objects.filter(sites=in_site).delete()
        except:
            delete = None

        product_site = Site.objects.get(id=1)


        main_site = Site.objects.get(id=1)
        products = Product.objects.filter(sites=main_site)

        if in_site:
            sites = Site.objects.filter(domain=in_site)
        else:
            sites = Site.objects.filter(id__gt=1)


        skus = SKU.objects.filter(sites=from_site)

        for sku in skus:

            object = sku.content_object

            for site in sites:
                try:
                    if sku.content_type.name == 'product' and object.type.slug == 'sinks':
                        count += 1
                        product = Product.objects.get(nid=object.nid, sites=site)
                        try:
                            product_sku = SKU.objects.get(object_id=product.id, content_type=sku.content_type, sku=sku.sku, sites=site)
                        except:
                            product_sku = SKU(content_object=product, sku=sku.sku, price='0')
                            product_sku.save()
                            for product_site in product.sites.all():
                                product_sku.sites.add(product_site)
                            product_sku.save()

                        print object.title.encode('utf-8')


                except Exception, e:
                    print Exception, e

        print('Successfully updates %s sku' % count)