# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from price_parsing.views import parse_analogs


class Command(BaseCommand):
    args = ''
    help = 'Parse analogs'

    def handle(self, *args, **options):
        print 'start'
        parse_analogs(request='')
        print 'stop'
        pass

