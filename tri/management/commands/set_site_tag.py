# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import *
from tri.models import *

from transliterate import translit, get_available_language_codes


class Command(BaseCommand):
    args = ''
    help = 'Set site tags'

    def handle(self, *args, **options):
        # nodes = Product.objects.all()
        nodes = Node.objects.all()

        for node in nodes:
            try:

                node.save()

                print "DONE: %s" % node.dot_slug
            except Exception, e:
                print '%s %s %s' % (Exception, e, node.slug)
                pass

        print('Successfully set sitetags')

