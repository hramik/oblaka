# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option


from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product

from tri.views import location

PARSE_URL = 'http://www.3tn.ru'

class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'


    option_list = BaseCommand.option_list + (
        make_option('--update',
            action='store_true',
            help='Update file from server'),
    )

    def handle(self, *args, **options):
        json_download_link = '%s/dataexport/getsaves' % PARSE_URL
        json_file_name = 'media/docs/saves.json'

        if options.get('update'):
            # Create json file for this site
            data = urllib.urlopen(json_download_link)
            with open(location(json_file_name), 'w') as f:
                myfile = File(f)
                myfile.write(data.read())

        json_data = open(location(json_file_name))
        json_load = json.load(json_data)
        none_comment_sum = 0
        for_index = 0

        for item in json_load:
            json_item = json_load[item]


            save_3d, created = Save3d.objects.get_or_create(nid=item)


            try:
                save_3d.node_type = NodeType.objects.get(slug='save_3d')
            except Exception, e:
                print "node_type"

            try:
                user = UserProfile.objects.get(uid=json_item['uid']).user
                save_3d.user = user
            except Exception, e:
                print "user"

            try:
                save_3d.save_name = json_item["title"] | ""
                save_3d.title = json_item["title"] | ""
            except Exception, e:
                print "title"

            try:
                dat_url = PARSE_URL+json_item["data_file"].replace('/var/www/3tn.ru/httpdocs', '')
                name = urlparse(dat_url).path.split('/')[-1]
                content = ContentFile(urllib2.urlopen(dat_url).read())
                save_3d.save_data_filename.save(name, content, save=True)
            except Exception, e:
                print "data_file %s %s" % (Exception, e)

            try:
                save_3d.pub_date = datetime.datetime.fromtimestamp(int(json_item['changed']))
            except Exception, e:
                print "pub_date"

            # PHOTO
            try:
                if not save_3d.photo:
                    img_url = json_item["preview_file"]
                    name = urlparse(img_url).path.split('/')[-1]
                    content = ContentFile(urllib2.urlopen(img_url).read())
                    save_3d.photo.save(name, content, save=True)
            except:
                a_p_error = 'main_photo'

            #     PRODUCTS
            try:
                for element in json_item["elements"]:
                    save_3d.products.add(Product.on_site.get(nid=element))
                    print "3D_PRODUCTS: %s" % element

                save_3d.save()
            except:
                pass

            print '%s done' % item

        print('Successfully import/update saves')