# -*- coding: utf-8 -*-

__author__ = 'hramik'
# Add triton3tn.ru site to all object with 3tn.ru site

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import *
from tri.models import *
from seo.models import *
from menu.models import *

from transliterate import translit, get_available_language_codes


class Command(BaseCommand):
    args = ''
    help = 'Set site field fo 3tn.ru'

    def handle(self, *args, **options):
        tn_site = Site.objects.get(domain='www.3tn.ru')
        triton_site = Site.objects.get(domain='www.triton3tn.ru')
        objects = Node.objects.filter(sites=tn_site)

        for object in objects:
            try:

                object.sites.add(triton_site)
                object.save()

            except Exception, e:
                print '%s %s %s' % (Exception, e, object.id)
                pass

        print('Successfully set site to objects')

