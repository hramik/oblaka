# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location


class Command(BaseCommand):
    args = ''
    help = 'Set basecomplect according style'

    @transaction.atomic
    def handle(self, *args, **options):
        products = Product.objects.filter(entity=1724)
        for product in products:
            product = Product.objects.get(id=product.id)
            if 'standart' in product.categories_slugs():
                product.base_complectation_id = 1
            else:
                product.base_complectation_id = 2

            product.save()

        print('Base complections set done')


