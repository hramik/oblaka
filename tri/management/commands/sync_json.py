# -*- coding: utf-8 -*-

__author__ = 'hramik'


from django.db.models import Q, F

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node, SEONode
from  tri.models import Page


class Command(BaseCommand):
    args = ''
    help = 'Sync jsons'


    def handle(self, *args, **options):
        nodes = Product.objects.filter(sites__name = '3tn.ru')
        count = 0

        for node in nodes:
            try:

                products = Product.objects.filter(~Q(sites__name = '3tn.ru'), nid=node.nid).update(json=node.json)
                print count
                count += 1


            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print 'Successfully sync %d nodes json' % count
