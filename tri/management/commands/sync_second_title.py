# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
from node.models import NodeType, Node
from  tri.models import Page


class Command(BaseCommand):
    args = ''
    help = 'Sync second title between products and clear nodes'

    def handle(self, *args, **options):
        nodes = Node.objects.all()

        for node in nodes:
            try:
                print node.pk
                product = Product.objects.get(pk=node.pk)
                node.second_title = product.second_title
                node.save()
                print "%s DONE: %s %s" % (node.pk, node.second_title.encode('utf-8'), product.second_title.encode('utf-8'))
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully sync nodes second title' )
