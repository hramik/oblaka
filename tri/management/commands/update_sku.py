# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option
from django.db import transaction


from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo
from django.contrib.contenttypes.models import ContentType



from tri.views import location
import csv

PARSE_URL = 'http://www.3tn.ru'

class Command(BaseCommand):
    args = ''
    help = 'Update region sku from main site'

    @transaction.atomic
    def handle(self, *args, **options):
        count = 0

        try:
            import_site = args[0]
        except:
            import_site = None

        try:
            if args[1] == 'delete':
                delete = True
        except:
            delete = None

        product_site = Site.objects.get(id=1)
        # site = Site.objects.get(name=import_site)


        main_site = Site.objects.get(id=1)
        products = Product.objects.filter(sites=main_site)
        # option_prices = OptionPrice.objects.filter(product__sites__id__gt=1).delete()
        # option_prices = OptionPrice.objects.filter(product__sites__domain='piter.3tn.ru').delete()

        for product in products:
            if import_site:
                region_products = Product.objects.filter(nid=product.nid, sites__domain=import_site)
                # for region_product in region_products:
                sites = Site.objects.filter(domain=import_site)
                if delete:
                    for region_product in region_products:
                       region_product.optionprices.all().delete()
            else:
                region_products = Product.objects.filter(nid=product.nid, sites__id__gt=1)
                sites = Site.objects.filter(id__gt=1)

            region_products.update(sku=product.sku)

            for region_product in region_products:
                content_type = ContentType.objects.get(model='product')

                for sku in product.skus.all():
                    try:
                        sku = SKU.objects.get(object_id=region_product.id, content_type=content_type, sku=sku, sites=region_product.sites)
                    except:
                        sku = SKU(content_object=region_product, sku=sku)
                        sku.save()
                        for site in region_product.sites.all():
                            sku.sites.add(site)

                for option_price in product.optionprices.all():
                    for sku in option_price.skus.all():
                        try:
                            sku = SKU.objects.get(content_object=region_product, sku=sku, sites=region_product.sites)
                        except:
                            sku = SKU(content_object=region_product, sku=sku)
                            sku.save()
                            for site in region_product.sites.all():
                                sku.sites.add(site)


            for region_product in region_products:
                print region_product.title.encode('utf-8')
                for option_price in product.optionprices.all():
                    region_option_price, create = OptionPrice.objects.get_or_create(product=region_product, option=option_price.option)

                    region_option_price.position = option_price.position
                    region_option_price.sku = option_price.sku

                    region_option_price.save()
                #     region_option_prices = OptionPrice.objects.filter(product__nid=option_price.product.nid, option__slug=option_price.option.slug).update(sku=option_price.sku)
                    # region_option_price.sku = optionprice.sku

                count += 1

                print('Product update %s' % product.title.encode('utf-8'))




        print('Successfully updates %s sku' % count)