# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node, SEONode
from tri.models import Region

from transliterate import translit, get_available_language_codes


class Command(BaseCommand):
    args = ''
    help = 'Set picasa gallery to product'

    def handle(self, *args, **options):
        nodes = Region.objects.all()

        for node in nodes:
            try:

                node.slug = slugify(translit(node.name, 'ru', reversed=True))
                node.save()

                print "DONE: %s" % node.slug
            except Exception, e:
                print '%s %s %s' % (Exception, e, node.slug)
                pass

        print('Successfully set picasa gallery' )

