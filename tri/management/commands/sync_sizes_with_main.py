# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location
from django.conf.urls.static import static
from django.conf import settings
from price_parsing.views import get_page

class Command(BaseCommand):
    args = ''
    help = ''

    @transaction.atomic
    def handle(self, *args, **options):

        main_site = Site.objects.get(id=1)

        sites = Site.objects.exclude(id=1)

        products = Product.objects.filter(sites=main_site, entity__slug='bath')
        for product in products:
            product = Product.objects.get(id=product.id)
            # if product.nid != '23563':
            #     continue
            #                                  depth=product.depth, height=product.height, value=product.value)
            # print product.title.encode('utf-8'), product.tn_alias.encode('utf-8'), product.nid
            # print product.length.encode('utf-8'), product.width.encode('utf-8'), product.depth.encode('utf-8'), product.height.encode('utf-8'), product.value.encode('utf-8')
            # Product.objects.filter(nid=product.nid).update(length=product.length, width=product.width,
            #
            try:
                products_to_change = Product.objects.filter(nid=product.nid)
                for product_to_change in products_to_change:
                    # print 'before', product_to_change.length.encode('utf-8'), product_to_change.width.encode('utf-8'), product_to_change.depth.encode('utf-8'), product_to_change.height.encode('utf-8'), product_to_change.value.encode('utf-8')
                    try:
                        # product_to_change = Product.objects.get(id=)
                        product_to_change.length = product.length
                        product_to_change.width = product.width
                        product_to_change.depth = product.depth
                        product_to_change.height = product.height
                        product_to_change.value = product.value
                        product_to_change.save()
                        print product_to_change.length.encode('utf-8'), product_to_change.width.encode('utf-8'), product_to_change.depth.encode('utf-8'), product_to_change.height.encode('utf-8'), product_to_change.value.encode('utf-8')
                    except Exception, e:
                        print Exception, e
            except Exception, e:
                print Exception, e



            # for site in sites:
            #     try:
            #         product_to_change = Product.objects.get(sites=site, nid=product.nid)
            #         print product_to_change
            #         # print product_to_change.title.encode('utf-8'), product_to_change.tn_alias.encode('utf-8'),product_to_change.nid,
            #         print product_to_change.length.encode('utf-8'), product_to_change.width.encode('utf-8'), product_to_change.depth.encode('utf-8'), product_to_change.height.encode('utf-8'), product_to_change.value.encode('utf-8')
            #         try:
            #             product_to_change.length = product.length
            #             product_to_change.width = product.width
            #             product_to_change.depth = product.depth
            #             product_to_change.height = product.height
            #             product_to_change.value = product.value
            #             product_to_change.save()
            #         except Exception, e:
            #             print Exception, e
            #     except Exception, e:
            #         print Exception, e
            #         pass

        print('Check done')

