# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction


from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice
from django.contrib.sites.models import Site

from tri.views import location


class Command(BaseCommand):
    args = ''
    help = 'Delete base complectations'

    def handle(self, *args, **options):
        items = Product.objects.all()

        for item in items:
            try:
                item.base_complectation = None
                item.save()
                print "DONE: %s %s" % (item.pk, item.title.encode('utf-8'))
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully delete base complectations')
