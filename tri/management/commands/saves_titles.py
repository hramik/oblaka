# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
from node.models import NodeType, Node
from  tri.models import Page, Save3d


class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'

    def handle(self, *args, **options):
        nodes = Save3d.objects.all()

        for node in nodes:
            try:
                node_title = u"Моя планировка"
                products = node.products.all()
                # print products
                products_titles = ''
                for product in products:
                    products_titles += product.title+", "

                products_title = products_titles.encode('utf-8')[:-2] or node.id
                node.title = "%s: %s" % (node_title.encode('utf-8'), products_title)
                node.h1 = node.title
                print node.title
                node.save()
            except:
                pass

        print('Successfully assign titles to "%s"' % node.title)
