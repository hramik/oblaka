# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo
from django.contrib.contenttypes.models import ContentType

from tri.views import location
import csv

class Command(BaseCommand):
    args = ''
    help = 'Update region sku from main site'

    def handle(self, *args, **options):
        count = 0


        try:
            old_sku = args[0]
            new_sku = args[1]
        except:
            site_name = None
            old_sku = ''
            new_sku = ''

        SKU.objects.filter(sku=old_sku).update(sku=new_sku)


        print('Successfully change skus %s to %s' % (old_sku, new_sku))