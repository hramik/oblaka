# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction


from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import *
from node.models import *
from tri.models import *

from django.contrib.sites.models import Site

from tri.views import location

PARSE_URL = 'http://www.3tn.ru'

kod_field = u"Код товара:"
country_field = u"Страна:"
manufacture_field = u"Производство:"
seria_field = u"Серия:"

class Command(BaseCommand):
    args = ''
    help = 'Import products from santehnika online'


    @transaction.atomic
    def handle(self, *args, **options):
        kwargs = {}

        site = Site.objects.get(name='www.triton3tn.ru')


        json_file = 'media/docs/santechnika_online/van_akril/_product_akrilovaya_vanna_1marka_aelita_180_sm_.json'

        #  Import products from JSON

        try:
            # options = args[2]

            json_data = open(location(json_file))
            json_product = json.load(json_data)



            print json_product

            try:
                product = Product.objects.get(nid=json_product["chars_main"][kod_field], sites=site)
            except:
                product = Product()

                product.meta_keywords = json_product["meta_keywords"]
                product.meta_desc = json_product["meta_description"]
                product.page_title = json_product["name"]

                product.nid = json_product["chars_main"][kod_field]
                product.title = json_product["name"]
                product.h1 = json_product["name"]

                product.country, create = Country.objects.get_or_create(name = json_product["chars_main"][country_field])

                product.dot_slug = slugify(translit(json_product["name"], 'ru', reversed=True)).lower()
                product.slug = product.dot_slug

                # '''
                # PHOTOS
                # '''
                #
                # try:
                #     img_url = json_product["thumb_image"]
                #     name = "%s-%s-thumb.%s" % (product.dot_slug, to_site, os.path.splitext(img_url)[1])
                #     content = ContentFile(urllib2.urlopen(img_url).read())
                #     product.photo.save(name, content, save=True)
                # except Exception, e:
                #     print 'thumb_image_err %s %s %s' % (img_url, Exception, e)
                #
                # try:
                #     photo = AdditionalPhoto(node=product, position=0)
                #     img_url = json_product["main_image"]
                #     product.additionalphoto_set.all().delete()
                #     # name = urlparse(img_url).path.split('/')[-1]
                #     name = "%s-%s-main.%s" % (product.dot_slug, to_site, os.path.splitext(img_url)[1])
                #     content = ContentFile(urllib2.urlopen(img_url).read())
                #     photo.photo.save(name, content, save=True)
                # except Exception, e:
                #     print 'main_image_err', Exception, e
                #
                # try:
                #     index = 1
                #     for image in json_product["images"]:
                #         photo = AdditionalPhoto(node=product, position=index)
                #         img_url = image
                #         # name = urlparse(img_url).path.split('/')[-1]
                #         name = "%s-%s-photo-%s.%s" % (product.dot_slug, to_site, str(index), os.path.splitext(img_url)[1])
                #         content = ContentFile(urllib2.urlopen(img_url).read())
                #         photo.photo.save(name, content, save=True)
                #         index += 1

                product.save()

                product.sites.add(site)

                # ADD SKU
                content_type = ContentType.objects.get(name="product")

                product_sku = SKU()
                product_sku.content_type = content_type
                product_sku.object_id = product.id
                product_sku.content_object = product
                product_sku.sku = "SO%s" % product.nid
                product_sku.price = json_product["sol_price"]
                product_sku.provider_price = json_product["sol_price"]
                # product_sku.weight = sku.weight

                product_sku.save()



        except Exception, e:
            print Exception, e


        print('Successfully import product %s' % json_product["name"])


