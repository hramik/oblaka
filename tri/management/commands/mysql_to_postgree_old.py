# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
import os.path

from django.db import connections

from products.models import *
from tri.models import *
from menu.models import *
from oblaka.settings import location
from custom_sites.models import *


class Command(BaseCommand):
    args = ''
    help = 'Copy optionprices to products'

    def handle(self, *args, **options):

        def sql_values(x):
            if type(x) is unicode or type(x) is str:
                return "'" + str(x) + "'"
            else:
                return str(x)

        mysql_cursor = connections['mysql'].cursor()
        postgree_cursor = connections['default'].cursor()

        # mysql_cursor.execute("SELECT table_name FROM information_schema.tables WHERE NOT table_schema='information_schema'")
        postgree_cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='public'")


        table_names = postgree_cursor.fetchall()

        for table in table_names:
            try:
                postgree_cursor.execute("DELETE FROM %s" % table[0])
                # items = mysql_cursor.fetchall()
                # for item in items:
                #     try:
                #         postgree_cursor.execute("INSERT INTO %s VALUES %s" % (table[0], "(" + ', '.join(map(sql_values, item)) + ")"))
                #     except Exception as e:
                #         print(Exception, e)

            except Exception as e:
                print(e)


        # items = Menu.objects.using('mysql').filter(sites__domain='www.triton3tn.ru').order_by('id')
        #
        # for item in items:
        #     print(item.sites.all())
        #     try:
        #         item.save(using='default', force_insert=True)
        #     except Exception as e:
        #         print(Exception, e)

        print('Successfully port materials')
