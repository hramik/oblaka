# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node
from  tri.models import Page


class Command(BaseCommand):
    args = ''
    help = 'Sync second title between products and clear nodes'

    def handle(self, *args, **options):
        components = Component.objects.all()

        for component in components:
            try:
                print component.dot_slug
                dot_slug = 'accessories/%s' % component.dot_slug.encode('utf-8')
                node = SEONode.objects.get(dot_slug=dot_slug)
                print node.title.encode('utf-8')
                node.photo = component.photo
                node.teaser = component.teaser
                node.body = component.body
                node.slug = component.dot_slug.encode('utf-8')
                node.node_type = NodeType.objects.get(slug='component')
                node.save()
                print "%s DONE: %s" % (node.pk, node.title.encode('utf-8'))
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully sync nodes second title' )
