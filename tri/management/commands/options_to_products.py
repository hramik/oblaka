# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
import os.path

from products.models import *
from  tri.models import Page
from oblaka.settings import location


class Command(BaseCommand):
    args = ''
    help = 'Copy optionprices to products'

    def handle(self, *args, **options):
        options = OptionPrice.objects.filter(id=1541)

        for option in options:

            product = Product()

            product_entity, create = ProductEntity.objects.get_or_create(title=u"Комплектующие", slug="accessorie", dot_slug="accessorie")

            product_type, create = ProductType.objects.get_or_create(title=u"Комплектующие для %s" % option.product.type.title, slug="accessory_for_%s" % option.product.type.slug,
                                                                     dot_slug="accessory_for_%s" % option.product.type.slug
                                                                     )

            product.title = u"%s для %s" % (option.option.name, option.product.title)
            product.entity = product_entity
            product.type = product_type
            product.desc = option.option.desc
            product.slug = "%s_for_%s" % (option.option.slug, option.product.slug)
            product.dot_slug = "%s_for_%s" % (option.option.slug, option.product.dot_slug)

            product.related_product = option.product
            product.option = option.option
            product.price = option.price
            product.overide_sku_price = option.overide_sku_price
            product.position = 10000
            product.action = option.action
            product.discount = option.discount
            product.not_change = option.not_change
            product.sku = option.sku
            product.import_id = option.import_id
            product.import_date = option.import_date
            product.publish = False

            # / media / optionvideos / hydromassazh.jpg)

            if os.path.isfile(location("media/optionvideos/%s.jpg" % option.option.slug)):
                product.photo = "optionvideos/%s.jpg" % option.option.slug

            product.save()

            for site in option.product.sites.all():
                product.sites.add(site)

            try:
                if not product.additionalphoto_set.all():
                    photo = AdditionalPhoto(node=product, position=0)
                    photo.photo.name = product.photo.name
                    photo.save()
            except:
                pass

            # Create SKUS
            for sku in option.skus.all():
                content_type = ContentType.objects.get(name="product")
                try:
                    product_sku = SKU.objects.get(object_id=product.id, content_type=content_type, sku=sku.sku)
                except:
                    product_sku = SKU()
                    product_sku.content_type = content_type
                    product_sku.object_id = product.id
                    product_sku.content_object = product
                    product_sku.sku = sku.sku
                    product_sku.price = sku.price
                    product_sku.provider_price = sku.provider_price
                    product_sku.weight = sku.weight

                    product_sku.save()

                for site in option.product.sites.all():
                    product_sku.sites.add(site)

            print('Successfully convert option "%s" to product "%s" - %s' % (option, product, product.pk))
