# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location
from django.conf.urls.static import static
from django.conf import settings
from price_parsing.views import get_page

class Command(BaseCommand):
    args = ''
    help = 'Set basecomplect according style'

    @transaction.atomic
    def handle(self, *args, **options):


        url = 'http://old.3tn.ru/dataexport/prices?site=3tn.ru'
        data = get_page(url)
        data = json.loads(data)
        for item in data:
            model = ''
            alias = ''
            # print '\n'
            try:
                if data[item]['type'] == 'mixer' or data[item]['type'] == 'nishto' or data[item]['type'] == 'sinks':
                    continue
            except:
                continue

            try:
                if data[item]['model_file']:
                    file_path = settings.MEDIA_ROOT + '/3dpr/Models/src/a3d/' + data[item]['model_file']
                    if os.path.isfile(file_path):
                       model = data[item]['model_file']
                    else:
                        print file_path + ' not exists (model_file)'
            except:
                pass
            try:
                if data[item]['alias']:
                    file_path = settings.MEDIA_ROOT + '/3dpr/Models/src/a3d/' + data[item]['alias'] + '.a3d'
                    if os.path.isfile(file_path):
                        alias = data[item]['alias']
                    else:
                        print file_path + ' not exists (alias)'
            except:
                pass

            if not alias and not model:
                print 'NO MODELS ', data[item]['name'].encode('utf-8')

            continue

            if alias and not model:
                print data[item]['alias'], 'Empty model, but alias is ok'
                products = Product.objects.filter(nid=item)
                count = 0
                for product in products:
                    # print product.title.encode('utf-8')
                    product.model_file = model
                    if product.tn_alias:
                        if product.tn_alias and product.tn_alias != alias:
                            print product.tn_alias, ' exists and not match ', ' id: ', alias, product.id
                    else:
                        product.tn_alias = alias
                    count = count+1
                    product.save()
                print 'Updated: ', count
        return


        m_files = {'8388': 'zerkalo_d-80.html.a3d',
                   '8393': 'zerkalo_l-80.html.a3d',
                   '21065': 'zerkalo_mirta-100.html.a3d',
                   '21066': 'zerkalo_mirta-30.html.a3d',
                   '21105': 'zerkalo_n_90.a3d',
                   '15584': 'zerkalo_n_90.a3d',
                   '21107': 'zerkalo_n_90.a3d',
                   '15586': 'zerkalo_n_90.a3d',
                   '21106': 'zerkalo_n_90.a3d',
                   '15588': 'zerkalo_n_90.a3d',
                   '21078': 'orion_1.a3d',
                   '21079': 'orion_2.a3d',
                   '8076': 'orion_3.a3d',
                   '21059': 'penal_mirta-30.html.a3d',
                   '22335': 'penal_30_2y_2d_korzina.a3d',
                   '22336': 'penal_30_2y_2d_korzina.a3d',
                   '22338': 'penal_30_2y_2d_korzina.a3d',
                   '22337': 'penal_30_2y_2d_korzina.a3d',
                   '8046': 'poddon_pryamougolnyy_150-70.html.a3d',
                   '8003': 'tumba_d-80d.html.a3d',
                   '14084': 'tumba_d-80d.html.a3d',
                   '8004': 'tumba_d-80ya.html.a3d',
                   '14085': 'tumba_d-80ya.html.a3d',
                   '8005': 'tumba_k-80d.html.a3d',
                   '14086': 'tumba_k-80d.html.a3d',
                   '8011': 'tumba_k-80ya.html.a3d',
                   '14087': 'tumba_k-80ya.html.a3d',
                   '8012': 'tumba_l-80d.html.a3d',
                   '14088': 'tumba_l-80d.html.a3d',
                   '8013': 'tumba_l-80ya.html.a3d',
                   '14089': 'tumba_l-80ya.html.a3d',
                   '21114': 'tumba_100_4y_1d.a3d',
                   '21108': 'tumba_90_2y.a3d',
                   '15596': 'tumba_90_2y.a3d',
                   '21110': 'tumba_90_2y.a3d',
                   '15600': 'tumba_90_2y.a3d',
                   '21109': 'tumba_90_2y.a3d',
                   '15598': 'tumba_90_2y.a3d',
                   '21060': 'tumba_mirta-100.html.a3d',
                   '15230': 'tumba_mirta-100.html.a3d',
                   '21068': 'tumba_mirta-30.html.a3d',
                   '15229': 'tumba_mirta-30.html.a3d',
                   '21111': 'tumba_90_1y.a3d',
                   '15590': 'tumba_90_1y.a3d',
                   '21113': 'tumba_90_1y.a3d',
                   '15592': 'tumba_90_1y.a3d',
                   '21112': 'tumba_90_1y.a3d',
                   '15594': 'tumba_90_1y.a3d',
                   }

        for nid in m_files:
            products = Product.objects.filter(nid=nid)
            for product in products:
                product.model_file = m_files[nid]
                product.save()

        return

        ids = []
        titles = []
        products = Product.objects.exclude(entity__slug__in=['mixer', 'nishto', 'soput'])
        for product in products:
            model = alias = ''
            if not product.tn_alias and not product.model_file:

                if product.title not in titles:
                    titles.append(product.title)
                if product.nid not in ids:
                    ids.append(product.nid)
                    # print product.nid
                    print product.title.encode('utf-8'), product.nid, product.id

        # print '\n'.join(titles).encode('utf-8')

            # file_path = settings.MEDIA_ROOT + '/3dpr/Models/src/a3d/' + product.tn_alias + '.a3d'
            # if product.tn_alias and not os.path.isfile(file_path):
            #     print file_path + ' not exists (alias)'
            #
            # file_path = settings.MEDIA_ROOT + '/3dpr/Models/src/a3d/' + product.model_file
            # if product.model_file and not os.path.isfile(file_path):
            #     print file_path + ' not exists (alias)'

        return

        main_site = Site.objects.get(id=1)
        products = Product.objects.filter(sites=main_site)

        for product in products:
            # product = Product.objects.get(id=product.id)
            # print product.title, product.tn_alias

            if product.entity.slug == 'mixer':
                continue
            if product.entity.slug == 'mebel' and product.model_file:
                file_path = settings.MEDIA_ROOT + '/3dpr/Models/src/a3d/' + product.model_file
            elif product.tn_alias:
                file_path = settings.MEDIA_ROOT + '/3dpr/Models/src/a3d/' + product.tn_alias + '.a3d'
            else:
                print (product.title + ' have no tn_alias and model_file').encode('utf-8')
            if os.path.isfile(file_path):
                pass
            else:
                print product.tn_alias, product.model_file, product.entity.slug
                print (product.title + ' have no file in Media (' + file_path + ')').encode('utf-8')

        print('Check done')


