# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option
from django.db.models import Q

from slugify import Slugify, UniqueSlugify, slugify, slugify_unicode
from slugify import slugify_url, slugify_filename
from slugify import slugify_ru

from django.core.files import File
from django.core.files.base import ContentFile


from django.core.management.base import BaseCommand, CommandError
from node.models import Node


class Command(BaseCommand):
    args = ''
    help = 'Set slug and dot_slug for all nodes what have empty slugs'

    def handle(self, *args, **options):
        count = 0

        nodes = Node.objects.filter(Q(dot_slug='') | Q(slug=''))

        for node in nodes:
            print node.title.encode('utf-8')

            if node.dot_slug == '':
                node.dot_slug = slugify_ru(node.title, to_lower=True)

            if node.slug == '':
                node.slug = node.dot_slug

            print node.dot_slug

            count += 1

        print('Successfully set %s slug' % count)

