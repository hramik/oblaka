# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option


from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product

from tri.views import location
import csv

PARSE_URL = 'http://www.3tn.ru'

class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'


    option_list = BaseCommand.option_list + (
        make_option('--update',
            action='store_true',
            help='Update file from server'),
    )

    def handle(self, *args, **options):
        csv_file_name = 'media/docs/eburg3tn.ru.csv'
        csv_export_file_name = 'media/docs/skus_new.csv'
        count = 0

        with open(csv_export_file_name, 'wb') as csvfile_new:
            csv_new = csv.writer(csvfile_new)

            with open(csv_file_name, 'rb') as csvfile:
                csv_data = csv.reader(csvfile, delimiter=';')
                for row in csv_data:
                    row.append(u'huy')

                    try:
                        # title = str(row[1]).encode('utf-8').replace(u'Ванна ', '').replace(u'ЭКСТРА ', '')
                        # title = str(row[1]).encode('utf-8')
                        # print title
                        products = Product.objects.filter(title=row[1])
                        if products:
                            print row[1], "PRODUCT", products
                            count += 1
                    except Exception, e:
                        print Exception, e
                        pass

                    # csv_new.writerow(row)


                    # print row[1], row[7]

        print('Successfully find %s' % count)