# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location
from django.conf.urls.static import static
from django.conf import settings
from price_parsing.views import get_page
from bs4 import BeautifulSoup

class Command(BaseCommand):
    args = ''
    help = 'Set basecomplect according style'

    @transaction.atomic
    def handle(self, *args, **options):

        items = {}
        url = 'http://old.3tn.ru/dataexport/prices?site=www.moscow3tn.ru'
        data = get_page(url)
        data = json.loads(data)
        for item in data:
            if data[item]['entity'] not in ('bathcab', 'bath', 'furniture'):
                continue
            current_item = dict()
            current_item['images'] = []
            current_item['id'] = item
            current_item['name'] = data[item]['name']

            try:
                current_item['description'] = data[item]['description']
            except:
                current_item['description'] = ''
            try:
                current_item['s_description'] = data[item]['market_description']
            except:
                current_item['s_description'] = ''
            if data[item]['entity'] in ['bath', 'bathcab', 'furniture']:
                try:
                    current_item['url'] = data[item]['old_path'].encode('utf-8')
                except:
                    try:
                        current_item['url'] = '/%s/%s' % (data[item]['category_alias'],data[item]['alias'])
                    except:
                        print 'Error ', data[item]['name']
                        break

            try:
                page_data = get_page('http://www.moscow3tn.ru%s' % current_item['url'])
                soup = BeautifulSoup(page_data, 'html.parser')
            except:
                print data[item]['name'].encode('utf-8')
                print 'Get page failed', current_item['url']
                break

            try:
                current_item['title'] = soup.head.title.string
                current_item['meta_description'] = soup.find(attrs={'name': 'description'}).string
                if not current_item['meta_description']:
                    current_item['meta_description'] = ''
                current_item['meta_keywords'] = soup.find(attrs={'name': 'keywords'}).string
                if not current_item['meta_keywords']:
                    current_item['meta_keywords'] = ''
                # try:
                # except Exception, e:
                #     print Exception, e
            except Exception, e:
                print Exception, e

            for key, link in data[item]['pictures'].iteritems():
                if key == 'main':
                    current_item['main_image'] = link
                    current_item['thumb_image'] = link
                    current_item['images'].append(link)
                else:
                    current_item['images'].append(link)
            items[data[item]['siteitemid']] = current_item

            # break
        # return

        json_data = json.dumps(items)
        parse_file_name = os.path.join(settings.MEDIA_ROOT, 'docs',  'moscow.3tn.ru.meta.json')
        handle = open(parse_file_name, 'w')
        file_data = handle.write(json_data)
        handle.close()

