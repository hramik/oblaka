# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random


from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
from node.models import NodeType, Node
from  tri.models import Page

from tri.views import location


class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'

    def handle(self, *args, **options):
        json_download_link = 'http://www.3tn.ru/dataexport/users'
        json_file_name = 'media/docs/users.json'


        # Create json file for this site
        data = urllib.urlopen(json_download_link)
        with open(location(json_file_name), 'w') as f:
            myfile = File(f)
            myfile.write(data.read())

        json_data = open(location(json_file_name))
        json_load = json.load(json_data)

        for item in json_load:
            json_user = json_load[item]
            # dr_password = DrupalPasswordHasher.encode('amd008', '', iter_code=None)
            # print dr_password
            try:
                # print 'try'
                user = User.objects.get(email=json_user["mail"], username=json_user["name"])
                user.date_joined = datetime.datetime.fromtimestamp(int(json_user['created']))
                user.save()
            except User.DoesNotExist:
                print 'does_not_exist'
                # if json_user['name'].__len__() > 30:
                #     print json_user['name']

                try:
                    pass_hash = 'drupal' + json_user['pass']
                    print pass_hash
                    user = User.objects.create_user(username=json_user['name'], email=json_user['mail'], password=pass_hash)
                    user.password = pass_hash
                    user.date_joined = datetime.datetime.fromtimestamp(int(json_user['created']))
                    user.save()
                except Exception, e:
                    print e, 'name'

            user_profile = user.get_profile()

            try:
                user_profile.uid = json_user["uid"]
            except Exception, e:
                print "phone_uid"

            try:
                user_profile.phone = json_user["phone"]
            except Exception, e:
                print "phone_err"

            try:
                user_profile.city = json_user["city"]
            except Exception, e:
                print "city_err"

            #     Points
            try:
                user_profile.points = json_user["points"]
            except Exception, e:
                print "points_err"

            # AVATAR PHOTO
            try:
                if not user_profile.avatar:
                    img_url = json_user["picture"]
                    name = urlparse(img_url).path.split('/')[-1]
                    content = ContentFile(urllib2.urlopen(img_url).read())
                    user_profile.avatar.save(name, content, save=True)
            except:
                a_p_error = 'main_photo'

            user_profile.save()

        print('Successfully import/update users')
