# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node, SEONode
from  tri.models import Page


class Command(BaseCommand):
    args = ''
    help = 'Sync second title between products and clear nodes'

    def handle(self, *args, **options):
        nodes = SEONode.objects.all()

        for node in nodes:
            try:
                node.h1 = node.title
                node.h2 = node.second_title
                node.save()
                print "%s DONE: %s" % (node.pk, node.title.encode('utf-8'))
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully sync nodes seo' )
