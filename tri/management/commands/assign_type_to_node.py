# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
from node.models import NodeType, Node
from  tri.models import Page


class Command(BaseCommand):
    args = ''
    help = 'Assign some node type to materials'

    def handle(self, *args, **options):
        node_type = 'advices'
        nodes = Page.objects.filter(tags__slug__in=['advices'])

        try:
            node_type, create = NodeType.objects.get_or_create(slug=node_type)
        except:
            print 'page_type'
            pass

        for node in nodes:
            node.node_type = node_type
            node.save()

        print('Successfully assign type "%s"' % node_type)
