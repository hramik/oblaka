# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.utils.html import strip_tags


from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import Product
from questions.models import Question
from tri.models import Save3d


class Command(BaseCommand):
    args = ''
    help = 'Auto title for questions'

    def handle(self, *args, **options):
        items = Save3d.objects.all()

        for item in items:
            try:
                body = strip_tags(item.body).replace('&nbsp;', ' ')
                item.title = body[:300]
                item.page_title = item.title
                item.dot_slug = 'question-%s' % item.id
                item.save()
                print "DONE: %s %s" % (item.pk, item.title.encode('utf-8'))
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully append questions titles' )
