# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from price_parsing.views import parse_cron


class Command(BaseCommand):
    args = ''
    help = 'Get prices'

    def handle(self, *args, **options):
        print 'start'
        parse_cron()
        print 'stop'
        pass

