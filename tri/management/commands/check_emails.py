# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
from optparse import OptionParser
from optparse import make_option

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto
from tri.models import Save3d, UserProfile
from products.models import Product, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo
from django.contrib.contenttypes.models import ContentType

from tri.views import location
import csv

from django.contrib.auth.models import User
from validate_email import validate_email
from da_mailer.models import Subscription

class Command(BaseCommand):
    args = ''
    help = 'Verify if an email addresses are valid and really exists.'

    def handle(self, *args, **options):
        count = 0

        try:
            site_name = args[0]
            old_sku = args[1]
            new_sku = args[2]
        except:
            site_name = None
            old_sku = ''
            new_sku = ''

        users = User.objects.all()

        for user in users:
            # user.save()
            # user_profile = user.get_profile()
            count += 1
            # if count == 10:
            #     break

            exist = validate_email(user.email,verify=True)

            if exist:
                try:
                    user.userprofile.subscribtions.add(Subscription.objects.get(slug='common'))
                    # print "%s %s is %s and subscribed\n" % (count,user.email, exist)
                except Exception:
                    print "%s %s" % (count,user.email)
                    # user.save()
                    # user_profile = user.get_profile()
                    # user_profile.subscribtions.add(Subscription.objects.get(slug='common'))


                # user_profile.subscribtions.add(Subscription.objects.get(slug='common'))
                # print "%s %s is %s and subscribed\n" % (count,user.email, exist)
            else:
                user.is_active = False
                # print "%s %s is %s and deactivated\n" % (count,user.email, exist)
            user.save()

        print('End of list')