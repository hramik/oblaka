# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node, SEONode
from  tri.models import Page


class Command(BaseCommand):
    args = ''
    help = 'Sync second title between products and clear nodes'

    def handle(self, *args, **options):
        nodes = Product.objects.all()

        for node in nodes:
            try:
                for option in node.optionprices.all():
                    if option.option.slug == 'hydromassazh':
                        print "HYDRO"
                        node.hydromassage = True
                    # else:
                    #     node.hydromassage = False


                node.save()
                print "%s DONE: %s %s" % (node.pk, node.title.encode('utf-8'), node.hydromassage)
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully sync nodes hydro' )
