# -*- coding: utf-8 -*-

__author__ = 'hramik'

import json
import urllib
import urllib2
from urlparse import urlparse
import datetime
import random
import re
import os
import csv
from optparse import OptionParser
from optparse import make_option
from copy import deepcopy, copy
from django.db.models import AutoField
from django.db import transaction

from django.forms import model_to_dict

from django.core.files import File
from django.core.files.base import ContentFile

import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from node.models import NodeType, Node, Comment, CommentAdditionalPhoto, AdditionalPhoto, AdditionalVideos
from tri.models import Save3d, UserProfile
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU
from django.contrib.sites.models import Site
from custom_sites.models import SiteInfo

from tri.views import location
from django.conf.urls.static import static
from django.conf import settings
from price_parsing.views import get_page

class Command(BaseCommand):
    args = ''
    help = 'Get 3d models for products from old.3tn.ru'

    @transaction.atomic
    def handle(self, *args, **options):
        url = 'http://old.3tn.ru/dataexport/prices?site=3tn.ru'
        data = get_page(url)
        data = json.loads(data)
        for item in data:
            try:
                print item, data[item]['model_file']
                if data[item]['model_file']:
                    products = Product.objects.filter(nid=item)
                    for product in products:
                        product.model_file = data[item]['model_file']
                        product.save()
            except:
                pass
        return

        main_site = Site.objects.get(id=1)
        products = Product.objects.filter(sites=main_site)

        for product in products:
            # product = Product.objects.get(id=product.id)
            # print product.title, product.tn_alias
            if product.tn_alias:
                file_path = settings.MEDIA_ROOT +  '/3dpr/Models/src/a3d/' + product.tn_alias + '.a3d'
                if os.path.isfile(file_path):
                    # print (u' ' + product.title + ' ' + file_path).encode('utf-8')
                    pass
                else:
                    print (product.title + ' have no file in Media (' + file_path + ')').encode('utf-8')
            else:
                print (u"\t\t" + product.title + ' have no model ').encode('utf-8')
            # print file_path
            pass

        print('Check done')


