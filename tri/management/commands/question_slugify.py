# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *
from node.models import NodeType, Node
from questions.models import Question


class Command(BaseCommand):
    args = ''
    help = 'Auto assign dot_slug and slug for questions'

    def handle(self, *args, **options):
        questions = Question.objects.all()

        for question in questions:
            try:
                question.dot_slug = 'question-%s' % question.id
                question.slug = 'question-%s' % question.id
                question.save()
                print "%s DONE: %s" % (question.pk, question.dot_slug)
            except Exception, e:
                print '%s %s' % (Exception, e)
                pass

        print('Successfully assign questions slugs' )
