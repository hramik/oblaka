# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.core.management.base import BaseCommand, CommandError
from questions.models import Question
from products.models import *


class Command(BaseCommand):
    args = ''
    help = 'Delete all "diana" visor objects'

    def handle(self, *args, **options):
        try:
            Visor.objects.filter(name='diana').delete()
            print('Successfully deleted' )
        except Exception, e:
            print Exception, e