# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url

SITE_ID = 30

urlpatterns = patterns('',

                       url(r'^sitemap$', 'tri.views.site_structure', {'site_id': SITE_ID}, name='site_structure'),

                       url(r'^vanny-yaroslavl$', 'tri.views.products', {'entity': 'bath', 'site_id': SITE_ID}, name='baths'),
                       url(r'^pryamougolnyie-vannyi$', 'tri.views.products', {'entity': 'bath', 'cats': ('pryamougolnie',), 'site_id': SITE_ID}, name='pryamougolnyie_baths'),
                       url(r'^asimmetrichnyie-vannyi$', 'tri.views.products', {'entity': 'bath', 'cats': ('asimmetrichnie',), 'site_id': SITE_ID}, name='asimetric_baths'),
                       url(r'^uglovyie-vannyi$', 'tri.views.products', {'entity': 'bath', 'cats': ('uglovie',), 'site_id': SITE_ID}, name='uglovie_baths'),
                       # url(r'^$', 'tri.views.products', {'entity': 'bath', 'cats': ('standart',), 'site_id': SITE_ID}, name='standart_baths'),

                       url(r'^mebel-dlya-vannyih-komnat$', 'tri.views.products', {'entity': 'mebel', 'site_id': SITE_ID}, name='mebel'),
                       url(r'^zerkala$', 'tri.views.products', {'entity': 'mebel', 'cats': ('mirrors',), 'site_id': SITE_ID},name='mebel_mirrors'),
                       url(r'^shkafyi$', 'tri.views.products', {'entity': 'mebel', 'cats': ('selfs',), 'site_id': SITE_ID},name='mebel_selfs'),
                       url(r'^penalyi$', 'tri.views.products', {'entity': 'mebel', 'cats': ('penals',), 'site_id': SITE_ID},name='mebel_penals'),
                       url(r'^komodyi$', 'tri.views.products', {'entity': 'mebel', 'cats': ('commodes',), 'site_id': SITE_ID},name='mebel_commodes'),
                       url(r'^tumbyi$', 'tri.views.products', {'entity': 'mebel', 'cats': ('sinks',), 'site_id': SITE_ID},name='mebel_sinks'),
                       url(r'^tumby-s-rakovinoy-podvesnye-yaroslavl$', 'tri.views.products', {'entity': 'mebel', 'cats': ('hang_sinks',), 'site_id': SITE_ID},name='mebel_hang_sinks'),
                       url(r'^tumbyi-napolnyie$', 'tri.views.products', {'entity': 'mebel', 'cats': ('tumb',), 'site_id': SITE_ID},name='mebel_tumb'),

                       url(r'^smesiteli$', 'tri.views.products', {'entity': 'mixer', 'site_id': SITE_ID}, name='mixers'),

                       url(r'^dushevyie-kabinyi$', 'tri.views.products', {'entity': 'bathcab', 'site_id': SITE_ID}, name='bathcabs'),

                       url(r'^news$', 'tri.views.list', {'slug_in': ('news', 'advices'), 'site_id': SITE_ID}, name='news'),
                       url(r'^questions-yaroslavl$', 'tri.views.questions', name='questions'),
)



