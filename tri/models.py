# -*- coding: utf-8 -*-


import datetime, sys, os
from django.utils.encoding import force_unicode

from django.db import models
# from taggit.managers import TaggableManager
from django.forms import ModelForm
from django.db import models
from django import forms
from django.contrib.auth.models import User
# from django.db.models import get_model
from django.apps import apps
from django.utils.translation import gettext_lazy as _
from node.models import *
from django.contrib.sites.models import Site
from django.contrib.postgres.fields import JSONField

from django.contrib.sites.managers import CurrentSiteManager

from django.core.cache import cache

import urllib2
from django.core.files.base import ContentFile

from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete

from questions.models import *

ARTICLES_STATUS_CHOICES = (
    ('N', 'normal'),
    ('G', 'important'),
)

WEEK_DAYS_CHOICES = (
    ('monday', u'понедельник'),
    ('tuesday', u'вторник'),
    ('wednesday', u'среда'),
    ('thursday', u'четверг'),
    ('friday', u'пятница'),
    ('saturday', u'суббота'),
    ('sunday', u'воскресенье'),
)


class Country(models.Model):
    '''
    Country
    '''
    name = models.CharField(
        max_length=100
    )

    slug = models.SlugField(
        max_length=50
    )

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = 'counties'
        # app_label = 'directories'


class Region(models.Model):
    '''
    Region
    '''
    name = models.CharField(
        max_length=100
    )

    code = models.CharField(
        max_length=10,
        default=1,
    )

    slug = models.SlugField(
        max_length=50,
        default="",
        blank=True,
        null=True
    )

    country = models.ForeignKey(
        Country,
        default=1
    )

    map_coords = models.CharField(
        max_length=100,
        blank=True
    )

    map_zoom = models.CharField(
        max_length=2,
        blank=True
    )

    phone = models.CharField(
        max_length=20,
        blank=True
    )

    site = models.CharField(
        max_length=255,
        blank=True
    )

    sites = models.ForeignKey(Site, default=1, blank=True)

    def __unicode__(self):
        return self.name

    def site_info(self):
        SiteInfo = apps.get_model('custom_sites', 'SiteInfo')
        return SiteInfo.objects.get(site=self.sites)

    def phone(self):
        SiteInfo = apps.get_model('custom_sites', 'SiteInfo')
        site_info = SiteInfo.objects.get(site=self.sites)
        if site_info.phone:
            return site_info.phone
        else:
            return None

    class Meta:
        ordering = ["name"]


class City(models.Model):
    '''
    City
    '''
    name = models.CharField(
        max_length=100
    )
    slug = models.SlugField(
        max_length=50,
        blank=True,
        default="",
        null=True
    )

    region = models.ForeignKey(
        Region,
        blank=True
    )

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ["name"]
        verbose_name_plural = 'cities'
        # app_label = 'directories'


class SalePlace(SEONode):
    '''
    Baths sell place or parntner or official dealer
    '''
    city = models.ForeignKey(City)
    # region = models.ForeignKey(Region)

    address = models.TextField(
        blank=True,
        default=""
    )

    map_coords = models.CharField(
        max_length=100,
        blank=True
    )

    map_zoom = models.CharField(
        max_length=2,
        blank=True
    )

    phone = models.CharField(
        max_length=255,
        blank=True,
        default=""
    )

    site_url = models.URLField(
        blank=True
    )

    work_time = models.CharField(
        max_length=100,
        blank=True
    )

    sites = models.ManyToManyField(
        Site,
        blank=True
    )

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])

    def get_region(self):
        return self.city.region

    class Meta:
        ordering = ["position"]


class DealerPointPhone(models.Model):
    '''
    Phone number for Dealer Point in Dealer Profile
    '''
    saleplace = models.ForeignKey(
        SalePlace
    )

    dealer_point_phone = models.CharField(
        max_length=50,
        default="",
        blank=True
    )


class Phone(models.Model):
    phone = models.CharField(
        max_length=20,
        default="",
        blank=True
    )


class WorkTime(models.Model):
    '''
    Work Time Interval (primary use in SellPlaces)
    '''
    saleplace = models.ForeignKey(
        SalePlace
    )

    from_day = models.CharField(
        choices=WEEK_DAYS_CHOICES,
        max_length=10,
        default='monday',
    )

    to_day = models.CharField(
        choices=WEEK_DAYS_CHOICES,
        max_length=10,
        default='friday',
    )

    from_time = models.TimeField(
    )

    to_time = models.TimeField(
    )


class Dealer(SEONode):
    '''
    Baths official dealer
    '''
    city = models.ForeignKey(City)

    address = models.TextField(
        blank=True,
        default=""
    )

    phone = models.CharField(
        max_length=255,
        blank=True,
        default=""
    )


    site_url = models.URLField(
        blank=True
    )

    sites = models.ManyToManyField(
        Site
    )

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])

    def get_region(self):
        return self.city.region

    class Meta:
        ordering = ["city"]


class CompanyType(models.Model):
    name = models.CharField(
        max_length=254,
        verbose_name=u'Название',
    )

    slug = models.CharField(
        max_length=254,
    )

    def __unicode__(self):
        return self.name


class Legal(Node):
    '''
    Company
    '''

    inn = models.CharField(
        max_length=255,
        blank=True,
        default=""
    )

    address = models.TextField(
        blank=True,
        default=""
    )

    def __unicode__(self):
        return self.title


class Company(Node):
    '''
    Company
    '''

    company_type = models.ForeignKey(
        CompanyType
    )

    # TODO: back
    company_legal = models.ForeignKey(
        Legal,
        related_name='company'
    )

    address = models.TextField(
        blank=True,
        default=""
    )

    country = models.CharField(
        max_length=255,
        default=u"Россия"
    )

    index = models.CharField(
        max_length=255,
        default=u"",
        blank=True,
        verbose_name='Индекс',
    )

    region = models.CharField(
        max_length=255,
        blank=True,
        default=u"",
        verbose_name='Регион',
    )

    city = models.CharField(
        max_length=255,
        default=""
    )

    street = models.CharField(
        max_length=255,
        default=""
    )

    street_abbr = models.CharField(
        max_length=10,
        default="",
    )

    house = models.CharField(
        max_length=20,
        default=""
    )

    house_korpus = models.CharField(
        max_length=20,
        default="",
        verbose_name=u'Корпус',
        blank=True,
    )

    str = models.CharField(
        max_length=20,
        default="",
        verbose_name=u'Строение',
        blank=True,
    )

    office = models.CharField(
        max_length=20,
        default="",
        verbose_name=u'Офис',
        blank=True,
    )

    phone = models.CharField(
        max_length=255,
        blank=True,
        default=""
    )

    regions = models.ManyToManyField(
        Region,
        blank=True
    )

    contact = models.CharField(
        max_length=255,
        blank=True,
        default="",
        verbose_name='Контактное лицо'
    )

    contacts = models.ManyToManyField(
        User,
        blank=True
    )

    def __unicode__(self):
        return self.title

        # def save(self, *args, **kwargs):
        #     self.street_abbr = self.street_abbr.replace('.', '')
        #     super(Company, self).save(*args, **kwargs)


class Page(SEONode):
    '''
    Page
    '''
    sidebar = models.TextField(
        verbose_name=_('Sidebar'),
        blank=True
    )

    color = models.CharField(
        default='black',
        max_length=55,
    )

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])


class PagePart(models.Model):
    '''
    Logic part of page
    '''
    page = models.ForeignKey(Page)
    # city = models.ForeignKey(City, default=1)

    title = models.CharField(max_length=200)

    position = models.PositiveSmallIntegerField(
        "Position",
        default=0
    )

    body = models.TextField(
        verbose_name=_("Body"),
        blank=True
    )

    sites = models.ManyToManyField(Site)

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])

    class Meta:
        ordering = ["position"]


class Article(SEONode):
    '''
    Article
    '''
    # city = models.ForeignKey(City, default=1)
    status = models.CharField(max_length=1, choices=ARTICLES_STATUS_CHOICES, default="N")

    sites = models.ManyToManyField(Site)
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ["-pub_date"]


class Gallery(SEONode):
    def __unicode__(self):
        return self.title


class GalleryPhoto(Photo):
    gallery = models.ForeignKey(Gallery)

    def get_upload_to(self, field_attname):
        return 'galleries/' + self.gallery.title


STATUS_CHOICES = (
    ('N', 'normal'),
    ('G', 'важно'),
    ('A', 'акция'),
)

HOUSE_TYPES_CHOICES = (
    ('stalin', '«Сталинка»'),
    ('hrush', '«Хрущевка»'),
    ('brezhnev', '«Брежневка»'),
    ('narod', '«Народная стройка»'),
    ('derevolution', 'Дореволюционный жилой фонд'),
    ('new', '«Новостройка»'),
    ('personal', '«Частный дом»'),
)

YES_NO_DONTKNOW_CHOICES = (
    ('yes', 'да'),
    ('no', 'нет'),
    ('dontknow', 'не знаю'),
)

EQUIPMENT_CHOICE = (
    ('pan', 'Унитаз'),
    ('bidet', 'Биде'),
    ('washing_mashine', 'Стиральная машина'),
    ('heat_towel', 'Полотенцесушитель'),
    ('bathcab', 'Душевая кабина'),
)

STORAGE_PLACE_CHOICE = (
    ('more_as_possible', 'Как можно больше'),
    ('just_everyday_items', 'Только для вещей повседневного пользования'),
    ('as_can', 'Как получится'),
)


class SolutionOrder(SEONode):
    location = models.CharField(
        max_length=255,
        default="",
    )

    house_type = models.CharField(
        max_length=50,
        choices=HOUSE_TYPES_CHOICES,
        default='stalin',

    )

    series = models.CharField(
        max_length=255,
        default="",
        blank=True,
    )

    height = models.CharField(
        default=0,
        blank=True,
        max_length=10,
    )

    transfer = models.BooleanField(
        default=False,
        blank=True,
    )

    counters = models.BooleanField(
        default=False,
        blank=True,
    )

    rude_filters = models.BooleanField(
        default=False,
        blank=True,
    )

    thin_filters = models.BooleanField(
        default=False,
        blank=True,
    )

    living_people = models.CharField(
        max_length=10,
        default=1,
        blank=True,
    )

    old_man_interests = models.BooleanField(
        default=False,
        blank=True,
    )

    children_interests = models.BooleanField(
        default=False,
        blank=True,
    )

    bath_need = models.BooleanField(
        default=False,
        blank=True,
    )

    hydromassage_need = models.CharField(
        max_length=50,
        choices=YES_NO_DONTKNOW_CHOICES,
        default='dontknow',
        blank=True,
    )

    extra_equipment = models.CharField(
        max_length=50,
        choices=EQUIPMENT_CHOICE,
        default='dontknow',
        blank=True,
    )

    second_washbasin = models.BooleanField(
        default=False,
        blank=True,
    )

    storage_places = models.CharField(
        max_length=50,
        choices=STORAGE_PLACE_CHOICE,
        default='dontknow',
        blank=True,
    )

    color_preferences = models.CharField(
        max_length=255,
        default='',
        blank=True,
    )

    wishes = models.TextField(
        default='',
        blank=True,

    )

    def save(self, *args, **kwargs):
        self.node_type = NodeType.objects.get(slug="solution_order")
        super(SolutionOrder, self).save(*args, **kwargs)


#
# class Solution(Page):
# order = models.ForeignKey(
#         SolutionOrder,
#     )


# TODO: That is this class do? I dont remember. Delete??
class Test(SEONode):
    sites = models.ManyToManyField(Site)
    on_site = CurrentSiteManager()


# TODO: delete this class ?? because it is not use, I think
class News(SEONode):
    # city = models.ForeignKey(City, default=1)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default="N")

    sites = models.ManyToManyField(Site)
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])

    class Meta:
        ordering = ["-pub_date"]
        verbose_name_plural = 'news'


class MainLink(models.Model):
    title = models.CharField(max_length=200, blank=True)
    desc = models.TextField(blank=True)
    img = models.ImageField(upload_to='main_links', verbose_name=_('Image'), blank=True,
                            help_text="For main page image size: 90x70px")
    position = models.PositiveSmallIntegerField("Position", default=0)
    url = models.CharField(max_length=200, blank=True)

    sites = models.ManyToManyField(Site)
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ["position"]

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])





class LoginForm(ModelForm):
    '''
    From for user login
    '''
    username = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'login.username', 'required': 'True'}),
        label='Логин'
    )

    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'ng-model': 'login.password', 'required': 'True', 'type': 'password'}),
        label='Пароль'
    )

    class Meta:
        model = User
        fields = ('username', 'password')


class RegisterForm(ModelForm):
    '''
    Form for user registration
    '''
    username = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.username', 'required': 'True'}),
        label='Логин'
    )

    # first_name = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'register.first_name', 'ng-initial': 'True', 'required': 'False'}),
    #     label='Имя'
    # )
    #
    # last_name = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'register.last_name ', 'ng-initial': 'True', 'required': 'False'}),
    #     label='Фамилия'
    # )

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={'ng-model': 'register.email', 'ng-initial': 'True', 'required': 'True',
                   'type': 'email'}),
        label='Email'
    )

    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'ng-model': 'register.password', 'required': 'True', 'type': 'password'}),
        label='Пароль'
    )

    confirm_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'ng-model': 'register.confirm_password', 'required': 'True',
                   'type': 'password'}),
        label='Подвердите пароль')

    # city = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'register.city', 'ng-initial': 'True', 'required': 'True'}),
    #     label='Город')
    #
    # phone = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'register.phone', 'ng-initial': 'True'}),
    #     label='Телефон')

    subscribe = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'ng-model': 'register.subscribe '}),
        label=u'Подписаться на рассылку журнала «Тритон»: советы, дизайн, новости.',
        required=False,
    )

    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'confirm_password', 'subscribe')


class EditProfileForm(ModelForm):
    '''
    Form for user registration
    '''
    username = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.username', 'ng-initial': 'True'}),
        label='Логин',
        required=False,
    )

    # first_name = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'register.first_name', 'ng-initial': 'True', 'required': 'False'}),
    #     label='Имя'
    # )
    #
    # last_name = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'register.last_name ', 'ng-initial': 'True', 'required': 'False'}),
    #     label='Фамилия'
    # )

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={'ng-model': 'register.email', 'ng-initial': 'True', 'type': 'email'}),
        label='Email',
        required=False,
    )

    password = forms.CharField(
        widget=forms.PasswordInput(attrs={'ng-model': 'register.password', 'type': 'password'}),
        label='Пароль',
        required=False,
    )

    confirm_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'ng-model': 'register.confirm_password', 'type': 'password'}),
        label='Подвердите пароль',
        required=False,
    )

    # city = forms.CharField(
    #     widget=forms.TextInput(attrs={'ng-model': 'register.city', 'ng-initial': 'True', 'required': 'True'}),
    #     label='Город')
    #
    phone = forms.CharField(
        widget=forms.TextInput(attrs={'ng-model': 'register.phone', 'ng-initial': 'true'}),
        label='телефон',
        required=False,
    )

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'confirm_password']


class RecoveryForm(ModelForm):
    '''
    Form for password recovery
    '''

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={'ng-model': 'confirm.email', 'ng-initial': 'True', 'required': 'True',
                   'type': 'email'}),
        label='Email'
    )

    class Meta:
        model = User
        fields = ['email']


class UserProfile(models.Model):
    '''
    User profile
    '''
    user = models.OneToOneField(User)

    second_name = models.CharField(
        max_length=50,
        default="",
        blank=True
    )

    uid = models.CharField(
        max_length=50,
        default="",
        blank=True
    )

    avatar = models.ImageField(
        upload_to='avatars',
        blank=True
    )

    AVATAR_CHOICES = (
        ('/static/images/avatar/common.png', 'общая'),
        ('/static/images/avatar/glasses.png', 'очки'),
        ('/static/images/avatar/beard.png', 'борода'),
    )

    phone = models.CharField(
        max_length=50,
        default="",
        blank=True
    )

    city = models.CharField(max_length=100, default="", blank=True)

    points = models.IntegerField(
        default=0,
        blank=True
    )

    template_avatar = models.CharField(
        max_length=100,
        blank=True,
        default='/static/images/avatar/common.png',
        choices=AVATAR_CHOICES,

    )

    color = models.CharField(
        max_length=30,
        default='#2aa4e9',
        blank=True
    )

    phone_confirmed = models.BooleanField(
        default=False,
    )

    personal_data_confirmed = models.BooleanField(
        default=False,
    )

    yandex_money_account = models.CharField(
        max_length=15,
        default="",
        blank=True
    )

    subscribtions = models.ManyToManyField(
        Subscription,
        blank=True
    )

    sites = models.ManyToManyField(
        Site,
        default=None,
        blank=True,
    )

    def __str__(self):
        return "%s's profile" % self.user

    def publish_last_question(self, request):
        try:
            last_question = Question.objects.get(id=request.session["last_question"])
            last_question.publish = True
            last_question.user = self.user
            last_question.save()
            del request.session["last_question"]
        except Exception, e:
            pass

    def date_joined(self):
        return self.user.date_joined


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = UserProfile.objects.get_or_create(user=instance)


post_save.connect(create_user_profile, sender=User)


@receiver(post_save, sender=Page)
@receiver(post_delete, sender=Page)
def clear_cache(instance, **kwargs):
    '''
    clear cache
    '''
    cache.clear()


@receiver(post_save, sender=SolutionOrder)
def add_photo(instance, **kwargs):
    if not instance.photo and instance.additionalphoto_set.all():
        first_photo = instance.additionalphoto_set.all()[:1].get().photo
        instance.photo = first_photo
        instance.save()
