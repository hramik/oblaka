# -*- coding: utf-8 -*-


__author__ = 'hramik'
__author__ = 'hramik'

import datetime, sys, os
from django.utils.encoding import force_unicode

from django.db import models
# from markitup.fields import MarkupField
# from taggit.managers import TaggableManager
from django.forms import ModelForm
from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from custom_sites.models import SiteInfo

from django.utils.translation import gettext_lazy as _

# from forms_builder.forms import models as fb_models
# from node import models as NodeModels

TOWATCH_STATUS_CHOICES = (
    ('W', 'ожидает'),
    ('R', 'перезвонить'),
    ('D', 'совершен'),
)


class Callback(models.Model):
    name = models.CharField(
            max_length=150,
            verbose_name='Your Name'
    )

    phone = models.CharField(
            max_length=20
    )

    call_time = models.CharField(
            max_length=150,
            blank=True
    )

    call_time = models.CharField(
            max_length=150,
            blank=True
    )

    email = models.EmailField(
            blank=True
    )

    status = models.CharField(
            max_length=1,
            choices=TOWATCH_STATUS_CHOICES,
            default="W"
    )

    comment = models.TextField(
            default="",
            blank=True
    )

    manager_comment = models.TextField(
            default="",
            blank=True
    )

    pub_date = models.DateTimeField(
            'date published',
            auto_now_add=True,
    )
    # pub_date.editable = True

    call_date = models.DateTimeField(
            'date call',
            blank=True,
            null=True,
            default=None,
    )

    site = models.ForeignKey(
            Site,
            blank=True,
            null=True,
            default=1,
            verbose_name=u'Сайт',
    )

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.name


class CallbackForm(ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'ng-model': 'callback.name', 'required': 'True'}), label="Ваше имя")

    # site_id = forms.ModelChoiceField(
    #         # widget=forms.Select(attrs={'ng-model': 'callback.site'}),
    #         label="Сайт",
    #         to_field_name="id",
    #         queryset=Site.objects.all(),
    #         initial=SiteInfo.on_site.select_related().get().site.id,
    #         required=False)

    phone = forms.CharField(widget=forms.TextInput(attrs={'ng-model': 'callback.phone', 'required': 'True'}), label="Ваш телефон")

    call_time = forms.CharField(widget=forms.TextInput(attrs={'ng-model': 'callback.call_time'}), initial="", required=False, label="Время звонка")

    email = forms.EmailField(widget=forms.TextInput(attrs={'ng-model': 'callback.email', 'type': 'email'}), label="Email", required=False)

    class Meta:
        model = Callback
        fields = ('name', 'phone', 'call_time', 'email')
