# -*- coding: utf-8 -*-

__author__ = 'hramik'

import datetime, sys, os
from django.utils.encoding import force_unicode

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
#from taggit.managers import TaggableManager
from django.forms import ModelForm
from django.db import models
from django import forms
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from node.models import Node, NodeType, SEONode


class Question(SEONode):

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def save(self, *args, **kwargs):
        self.node_type = NodeType.objects.get(slug='question')
        super(Question, self).save(*args, **kwargs)


    def __unicode__(self):
        return self.title





class QuestionForm(ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'class': 'validate[required]', 'ng-model': 'question.body', 'required': 'True', 'msd-elastic': 'true'}), label="Вопрос")
    # upload_images = forms.CharField(widget=forms.TextInput(attrs={'ng-model': 'upload_images'}))

    class Meta:
        model = Question
        fields = ('body',)

class AnonimousQuestionForm(ModelForm):
    author = forms.CharField(widget=forms.TextInput(attrs={'class': 'validate[required]', 'ng-model': 'question.author', 'required': 'True'}), label='Имя')
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'question.email', 'required': 'True', 'type': 'email'}))
    body = forms.CharField(widget=forms.Textarea(attrs={'class': 'validate[required]', 'ng-model': 'question.body', 'required': 'True', 'msd-elastic': 'true'}), label="Вопрос")
    class Meta:
        model = Question
        fields = ('author', 'email', 'body')

