# -*- coding: utf-8 -*-


import datetime, sys, os
from django.utils.encoding import force_unicode

from django.db import models
from django.contrib.postgres.fields import JSONField


class Log(models.Model):
    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True
    )

    json = JSONField(
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ['-pub_date']