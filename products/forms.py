# -*- coding: utf-8 -*-

import datetime, sys, os, re
from django.utils.encoding import force_unicode

from django.db import models
from django.forms import ModelForm
from django.db import models
from django import forms
from django.utils.translation import gettext_lazy as _
from products.models import *
from tri.models import *

from seo.models import *

'''
Форма комментария
'''


class BathFilterForm(ModelForm):
    hydromassage = forms.CharField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'global.filter.hydromassage',
            }
        ),
        label=u"Гидромассаж"
    )

    min_length = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'global.filter.min_length',
            }
        ),
        label=u"Минимальная длина",
        required=False
    )

    max_length = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'global.filter.max_length',
            }
        ),
        label=u"Максимальная длина",
        required=False
    )

    class Meta:
        model = SEOFields
        fields = ('hydromassage',)


class CreditOrderForm(ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'validate[required]'}), label="Имя")
    phone = forms.CharField(widget=forms.TextInput(attrs={'class': 'validate[required]'}), label=" Телефон")
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': ''}), label="Email")
    term = forms.ChoiceField(widget=forms.Select(attrs={'class': 'validate[required]'}),
                             choices=CREDIT_TERM_CHOICES, label="Срок страхования")
    first_pay_percent = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate[required]'}), label="Первый платеж (в процентах)")
    credit_type = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': 'validate[required]'}),
                                    choices=CREDIT_TYPE_CHOICES, label="Тип кредитования")
    agree = forms.ChoiceField(widget=forms.CheckboxInput(attrs={'class': 'validate[required]'}),
                              label=_("С условиями согласен"))

    class Meta:
        model = Credit
        fields = ('name', 'phone', 'email', 'term', 'first_pay_percent', 'credit_type')


class OrderForm(ModelForm):
    # poduct = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.product_id'}), label="",
    #                          required=False)
    name = forms.CharField(
        widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.name', 'required': 'True', 'ng-initial': 'True'}),
        label="Ваше имя")

    address = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.address', 'ng-initial': 'True'}),
                              label="Адрес",
                              required=False)

    country = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.country', 'ng-initial': 'True'}),
                              label="Страна", required=False)

    index = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.index', 'ng-initial': 'True'}),
                           label="Индекс",
                           required=False)

    region = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.region', 'ng-initial': 'True'}),
                           label="Регион",
                           required=False)

    city = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.city', 'ng-initial': 'True'}),
                           label="Город",
                           required=False)

    street_abbr = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.street_abbr', 'ng-initial': 'True'}), required=False)

    street = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.street', 'ng-initial': 'True'}),
                           label="Улица",
                           required=False)

    house = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.house', 'ng-initial': 'True'}),
                           label="Дом",
                           required=False)

    destination = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.destination', 'ng-initial': 'True'}),
                           label="Расстояние",
                           required=False)

    phone = forms.CharField(
        widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.phone', 'required': 'True', 'ng-initial': 'True'}),
        label="Телефон")


    email = forms.EmailField(widget=forms.TextInput(
        attrs={'class': '', 'ng-model': 'quick_order.email', 'required': 'True', 'type': 'email', 'ng-initial': 'True'}), label="Email")

    comment = forms.CharField(widget=forms.Textarea(attrs={'class': '', 'ng-model': 'quick_order.comment', 'ng-initial': 'True'}),
                              label="Комментарий",
                              required=False)

    # call_value = forms.CharField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'quick_order.call_value', 'required': 'True', 'ng-initial': 'True'}), label="")

    class Meta:
        model = Order
        fields = ('name', 'address', 'country', 'index', 'region', 'city',  'street_abbr', 'street', 'house', 'phone', 'email', 'comment')


from django.forms.extras.widgets import SelectDateWidget


class PayForm(ModelForm):
    delivery = forms.ModelChoiceField(queryset=Delivery.on_site.filter(publish=True),
                                      to_field_name="slug",
                                      # initial=Delivery.on_site.filter(publish=True)[0],
                                      widget=forms.Select(attrs={'class': '', 'icon-select': 'true', 'ng-model': 'pay_form.delivery', 'ng-initial': 'True',
                                                                 'ng-change': 'save();'}),
                                      label="Выберите способ доставки",
                                      required=False)

    carrier = forms.ModelChoiceField(queryset=Company.objects.filter(company_type__slug__in=('cargocarrier', 'distributor')),
                                     to_field_name="slug",
                                     # initial=Delivery.on_site.filter(publish=True)[0],
                                     widget=forms.Select(attrs={'class': '', 'ui-dropdown-o':
                                         'true', 'ng-model': 'pay_form.carrier', 'ng-initial': 'True', 'ng-change': 'save();'}),
                                     label="Перевозчик",
                                     required=False)

    carrier_number = forms.CharField(
        widget=forms.TextInput(attrs={'class': '', 'ng-model': 'pay_form.carrier_number', 'ng-initial': 'True'}),
        label="Номер заявки перевозчика",
        required=False)

    need_cargo_service = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'class': '', 'ng-model': 'pay_form.need_cargo_service', 'ng-initial': 'True'}),
        label="Нужны услуги грузчиков(в т.ч. подъем на этаж)",
        required=False)

    version = forms.CharField(
        widget=forms.TextInput(attrs={'class': '', 'ng-model': 'pay_form.version', 'ng-initial': 'True'}),
        label="Версия",
        required=False)

    delivery_date = forms.DateField(
        widget=forms.DateInput(attrs={'class': 'form_datetime', 'ng-model': 'pay_form.delivery_date', 'ng-initial': '', 'da-line-calendar': ""}),
        label="Дата отгрузки",
        required=False
    )

    PICKUP_TIME_PERIOD_CHOICES = (
        ('9_18', u'9-18'),
        ('9_13', u'9-13'),
        ('13_18', u'13-18'),
    )

    delivery_pickup_time_period = forms.ChoiceField(
        choices=PICKUP_TIME_PERIOD_CHOICES,
        widget=forms.Select(attrs={'class': 'ui  dropdown', 'ui-dropdown': 'true', 'ng-model': 'pay_form.delivery_pickup_time_period', 'ng-initial': 'True', 'ng-change': 'save();'}),
        label="Время отгрузки",
        required=False
    )

    custom_delivery_sum = forms.CharField(
        widget=forms.TextInput(attrs={'class': '', 'ng-model': 'pay_form.custom_delivery_sum', 'ng-initial': 'True'}),
        label="Стоимость индивидуальной доставки",
        required=False)

    distributor = forms.ModelChoiceField(queryset=Company.objects.filter(company_type__slug='distributor'),
                                         to_field_name="slug",
                                         # initial=Delivery.on_site.filter(publish=True)[0],
                                         widget=forms.Select(
                                             attrs={'class': '', 'ui-dropdown-o': 'true', 'ng-model': 'pay_form.distributor', 'ng-initial': 'True', 'ng-change': 'save();'}),
                                         label="Дистрибьютор",
                                         required=False)

    # delivery_date = forms.ModelChoiceField(queryset=Company.objects.filter(company_type__slug='cargocarrier'),
    #                                   to_field_name="slug",
    #                                   # initial=Delivery.on_site.filter(publish=True)[0],
    #                                   widget=forms.Select(attrs={'class': '', 'ng-model': 'pay_form.delivery_date', 'ng-initial': 'True'}),
    #                                   label="Желаемая дата доставки",
    #                                   required=False)

    prepay = forms.ModelChoiceField(queryset=Prepay.objects.filter(publish=True),
                                    # initial=Prepay.objects.filter(publish=True)[0],
                                    to_field_name="slug",
                                    widget=forms.Select(attrs={'class': '', 'icon-select': 'true', 'ng-model': 'pay_form.prepay', 'ng-initial': 'True', 'ng-change': 'save();'}),
                                    label="Выберите размер предоплаты",
                                    required=False)

    pay_type = forms.ModelChoiceField(queryset=Payment.objects.filter(publish=True),
                                      # initial=Payment.objects.filter(publish=True)[0],
                                      to_field_name="slug",
                                      widget=forms.Select(attrs={'class': '', 'icon-select': 'true', 'ng-model': 'pay_form.pay_type', 'ng-initial': 'True',
                                                                 'ng-change': 'save();', 'ng-change': 'save();'}),
                                      label="Выберите способ оплаты",
                                      required=False)

    class Meta:
        model = Order
        fields = ('version', 'distributor', 'delivery_date', 'delivery_pickup_time_period', 'carrier', 'carrier_number', 'need_cargo_service', 'custom_delivery_sum', 'delivery',  'prepay', 'pay_type')
