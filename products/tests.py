# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.contrib.auth.models import User
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.test import Client
from products.models import *

class BonusTestCase(TestCase):
    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        order = Order.objects.get(id=242)
        self.assertEqual(order.weight(), '72.7')