# -*- coding: utf-8 -*-

__author__ = 'hramik'

import datetime, sys, os
from django.utils.encoding import force_unicode

from django.conf import settings
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
# from taggit.managers import TaggableManager
# from taggit.managers import TaggableManager
from django.forms import ModelForm
from django.db import models
from django import forms
from django.core.cache import cache
import logging
from django.db.models import Sum
import logging
from dpd.models import *

from django.contrib.contenttypes.models import ContentType
# from django.contrib.contenttypes import generic
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation


from django.utils.text import slugify
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from mptt.models import MPTTModel
from django.utils.translation import gettext_lazy as _
from node.models import *
from django.contrib.postgres.fields import JSONField
import django_filters
import simplejson as json
import StringIO
import urllib2
from urlparse import urlparse
from django.core.files import File
from django.core.files.base import ContentFile
from PIL import Image
from transliterate import translit, get_available_language_codes

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from tri.helper import *
from node.managers import *
# from custom_sites.models import SiteInfo
from da_mailer.helper import *
from sms_api.models import *

# from custom_sites.models import SiteInfo

PROJECT_DIR = os.path.dirname(__file__)
location = lambda x: os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', x)

logger = logging.getLogger(__name__)


class CategoryType(Node):
    pass

    def __unicode__(self):
        return "%s %s" % (self.title, self.second_title)


class BaseComplectation(models.Model):
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)

    desc = models.TextField(
        blank=True
    )

    def __unicode__(self):
        return self.name


class BaseComplectationFields(models.Model):
    '''
    Abstarct class for base complectation
    '''
    base_complectation = models.ForeignKey(
        BaseComplectation,
        blank=True,
        null=True,
        default=1,
    )

    class Meta():
        abstract = True


class Category(SEONode):
    category_type = models.ForeignKey(CategoryType, default=18)
    video_url = models.CharField(max_length=500, verbose_name=_("Video URL"), blank=True)
    video_poster = models.ImageField(upload_to='catvideo_posters', blank=True)

    class Meta:
        ordering = ['position']

    # def __unicode__(self):
    # return self.category_type.title+" / "+self.title

    def __unicode__(self):
        return "%s / %s %s" % (self.title, self.category_type.title, self.category_type.second_title)
        # return self.title

    def category_type_second_title(self):
        return self.category_type.second_title

    def all_title(self):
        return "%s / %s %s" % (self.title, self.category_type.title, self.category_type.second_title)


# class ProductStyleCat(Category):
# pass

# class ProductFormCat(Category):
#     pass
#
# class ProductMaterial(Category):
#     pass

# class ProductColor(Category):
#     pass

class ProductType(Category):
    pass


class ProductEntity(Category):
    pass


class ProductCategory(Category):
    pass


class SKU(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    sku = models.CharField(
        default="",
        blank=True,
        verbose_name=_('SKU'),
        max_length=50
    )

    price = models.CharField(
        default="",
        blank=True,
        verbose_name=_('price'),
        max_length=10
    )

    provider_price = models.CharField(
        default="",
        blank=True,
        verbose_name=_('provider price'),
        max_length=10
    )

    weight = models.CharField(
        default="0",
        blank=True,
        verbose_name=_('weight'),
        max_length=10
    )

    sites = models.ManyToManyField(
        Site,
        blank=True,
    )

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def discount_price(self, discount):
        discount_value = float(self.price) / 100 * float(discount)
        discount_cart_sum = float(self.price) - discount_value
        return discount_cart_sum

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])

    # def save(self, *args, **kwargs):
    #     for product_site in self.content_object.sites.all():
    #         self.sites.add(product_site)
    #
    #     print self.sites.all()
    #     super(SKU, self).save(*args, **kwargs)

    def __unicode__(self):
        # return "%s / %s" % (self.option.group, self.option)
        return "%s" % (self.content_object,)


# @track_data('price')
class Product(SEONode, BaseComplectationFields):
    '''
    Product
    '''
    # second_title = models.CharField(max_length=200, blank=True)

    entity = models.ForeignKey(
        ProductEntity,
        related_name='entities',
        verbose_name=u'Сущность',
        blank=True, default=1679
    )

    type = models.ForeignKey(
        ProductType,
        related_name='types',
        verbose_name=u'Тип',
        blank=True,
        default=1680
    )

    category = models.ManyToManyField(ProductCategory, related_name='categories', verbose_name=u'Категория', blank=True)

    related_product = models.ForeignKey(
        'self',
        related_name='relation_products',
        blank=True,
        null=True,
        default=None
    )

    option = models.ForeignKey(
        "products.Option",
        blank=True,
        null=True,
        default=None
    )

    price = models.CharField(
        default="",
        blank=True,
        max_length=10,
        verbose_name=u'Цена'
    )

    overide_sku_price = models.BooleanField(
        default=False,
        verbose_name=u'Кастомная цена'
    )

    soon = models.BooleanField(
        default=False,
        verbose_name=u'Скоро в продаже',
    )

    sku = models.CharField(
        default="",
        blank=True,
        verbose_name=u'SKU',
        max_length=50)

    skus = GenericRelation(SKU)



    provider = models.ForeignKey(
        'tri.Company',
        blank=True,
        default=30766,
        related_name='provider',
        # default=30755,

    )

    country = models.ForeignKey(
        'tri.Country',
        related_name='country',
        blank=True,
        default=1,
    )

    manufacture = models.ForeignKey(
        'tri.Company',
        related_name='manufacture',
        blank=True,
        default=30766,
        # default=30755,
    )

    seria = models.CharField(
        max_length=255,
        blank=True,
        default=""
    )

    popular = models.BooleanField(verbose_name=_('popular'), blank=True, default=False)
    action = models.BooleanField(verbose_name=_('action'), blank=True, default=False)
    discount = models.IntegerField(default=0, verbose_name=_('discount'))

    not_change = models.BooleanField(verbose_name=_('not change'), blank=True, default=False)

    json = JSONField(blank=True)

    tn_id = models.CharField(default="", blank=True, verbose_name=_('Id on 3tn.ru'), max_length=10)
    tn_alias = models.CharField(default="", blank=True, verbose_name=_('alias'), max_length=500)

    char_photo = models.ImageField(upload_to='charphotos', blank=True)

    chars = JSONField(blank=True)

    length = models.CharField(default="", blank=True, verbose_name=_('length'), max_length=10)
    width = models.CharField(default="", blank=True, verbose_name=_('width'), max_length=10)
    height = models.CharField(default="", blank=True, verbose_name=_('height'), max_length=10)
    depth = models.CharField(default="", blank=True, verbose_name=_('depth'), max_length=10)
    value = models.CharField(default="", blank=True, verbose_name=_('value'), max_length=10)
    hydromassage = models.BooleanField(default=False, verbose_name=_('hydromassage'))

    weight = models.CharField(default="0", blank=True, verbose_name=_('weight'), max_length=10)

    model_file = models.CharField(
        max_length=255,
        blank=True
    )

    additional_products = models.ManyToManyField(
        'self',
        symmetrical=False,
        related_name='core_product',
        blank=True
    )

    # cats
    # style = models.ManyToManyField(ProductStyleCat, related_name='products', verbose_name=_('style'), blank=True, null=True)
    # form = models.ForeignKey(ProductFormCat, related_name='products', verbose_name=_('form'), blank=True, null=True)
    # material = models.ManyToManyField(ProductMaterial, related_name='products', verbose_name=_('material'), blank=True, null=True)
    #
    #
    # color = models.ForeignKey(ProductColor, related_name='products', blank=True, verbose_name=_('color'), null=True)

    # Sites
    # sites = models.ForeignKey(Site)
    objects = models.Manager()
    on_site = CurrentSiteManager()

    class Meta:
        permissions = (
            ("view_parse", "Can view parse"),
            ("view_parse_m", "Can view secret parse"),
        )
        ordering = ['position', 'title']

    def __unicode__(self):
        return self.title
        # return "%s %s" % (self.title, self.get_form())

    def model(self):
        model = False
        for category in self.category.all():
            if category.category_type.slug == 'modelline':
                model = {
                    'name': category.title,
                    'slug': category.slug,
                }
        return model

    def deliveries(self):
        return Delivery.objects.filter(sites=self.first_site(), publish=True)

    def available_delivery_prices(self):
        prices = []
        for delivery in self.deliveries():
            try:
                if self.entity.slug == 'bath' and self.hydromassage:
                    price = delivery.json['bath_hydro']
                else:
                    price = delivery.json[self.entity.slug]

                if "km" in delivery.json:
                    km = delivery.json['km']
                else:
                    km = None

                if "from" in delivery.json:
                    from_km = delivery.json['from']
                else:
                    from_km = None

                prices.append({
                    'title': delivery.title,
                    'slug': delivery.slug,
                    'price': price,
                    'from_km': from_km,
                    'km': km,

                })
            except Exception, e:
                print Exception, e
        return prices

    def save(self, *args, **kwargs):
        if not self.overide_sku_price and self.skus.all():
            price = 0
            for sku in self.skus.all():
                add_price = 0 if sku.price == '' else int(sku.price.encode('utf-8').replace(",", ".").replace(" ", "").replace("руб.", ""))
                price += add_price
            self.price = price

        # for sku in self.skus.all():
        #     for product_site in self.sites.all():
        #         sku.sites.add(product_site)
        #         sku.save()
        #         print sku.sites.all()

        self.node_type = NodeType.objects.get(slug='product')
        super(Product, self).save(*args, **kwargs)

    def style_names(self):
        return ', '.join([a.title for a in self.style.all()])

    style_names.short_description = "Styles"

    def copy(self):
        try:
            url = "<a href='/clone-%s' target='_blank'>Клонировать</a>" % (str(self.id),)
        except Exception, e:
            url = '%s %s' % (Exception, e)
        return url

    copy.short_description = u'Клонирование'
    copy.allow_tags = True

    def get_form(self):
        form = ''
        for category in self.category.all():
            if category.category_type.slug == 'form':
                form = category

        return form

    def high_model(self):
        # high_model = self.additionalfile_set.filter(name='model')[:1].get()
        high_model = False
        if os.path.isfile(location("media/hight_models/model_%s.max" % self.nid)):
            high_model = "/media/hight_models/model_%s.max" % self.nid
        return high_model

    def get_type(self):
        form = ''
        for category in self.category.all():
            if category.category_type.slug == 'type':
                form = category

        return form

    # def form_names(self):
    #    return ', '.join([a.title for a in self.form.all()])

    def get_weight(self):
        weight = self.skus.aggregate(Sum('weight'))['weight__sum']
        print weight
        if weight == 0.0 or not weight:
            if 'pryamougolnie' in self.category.all().values_list('slug', flat=True):
                weight = 35
            if 'standart' in self.category.all().values_list('slug', flat=True):
                weight = 18
            if 'asimmetrichnie' in self.category.all().values_list('slug', flat=True):
                weight = 25
            if 'uglovie' in self.category.all().values_list('slug', flat=True):
                weight = 30
            if 'sinks' in self.category.all().values_list('slug', flat=True):
                weight = 22
            if 'hang_sinks' in self.category.all().values_list('slug', flat=True):
                weight = 19
            if 'tumb' in self.category.all().values_list('slug', flat=True):
                weight = 16
            if 'mirrors' in self.category.all().values_list('slug', flat=True):
                weight = 13
            if 'selfs' in self.category.all().values_list('slug', flat=True):
                weight = 18
            if 'commodes' in self.category.all().values_list('slug', flat=True):
                weight = 27
            if 'penals' in self.category.all().values_list('slug', flat=True):
                weight = 33
            if self.type.slug == 'poddon':
                weight = 17
            if self.type.slug == 'bathcabs':
                weight = 50
            if self.type.slug == 'mixer':
                weight = 2

                # width = "приблизительно %d" % width
        return weight

    def skus_names(self):
        return ', '.join([a.sku for a in self.skus.all()])

    def skus_with_price(self):
        return self.skus.filter(price__gt=0)

    def material_names(self):
        return ', '.join([a.title for a in self.material.all()])

    def categories_slugs(self):
        return ', '.join([a.slug for a in self.category.all()])

    # def save(self, force_insert=False, force_update=False):
    #     if not self.overide_sku_price and self.skus:
    #         price = 0
    #         for sku in self.skus.all():
    #             add_price = 0 if sku.price == '' else int(sku.price)
    #             price += add_price
    #         self.price = price
    #
    #     super(Product, self).save(force_insert, force_update)

    def json_save(self, current_site='', product_details='', *args, **kwargs):
        print 'save'
        # print current_site
        # print product_details

        # product_details = ProductDetails.objects.get(product=self, sites=current_site)
        # product_details = ProductDetails.objects.get(product=self, sites=current_site)

        if self.json:
            # self_json = json.loads(self.json)

            # ID
            self.tn_id = self.json["siteitemid"]
            self.nid = self.json["siteitemid"]

            # TITLE
            self.title = self.json["name"]

            # SLUG and ALIAS
            self.slug = self.json["alias"].replace('.html', '').replace('-', '_')
            self.dot_slug = self.json["alias"]
            self.tn_alias = self.json["alias"]

            # DESC
            try:
                self.body = self.json["description"]
            except Exception, e:
                self.body = ""

            # DESC
            try:
                self.teaser = self.json["summary"]
            except Exception, e:
                self.teaser = ""

            # PRICE
            try:
                self.price = self.json["price"]
            except Exception, e:
                self.price = ""
                # price, created = ProductDetails.objects.get_or_create(product=self, price=option["price"], body = self.json["description"])

                # product = models.ForeignKey(Product)
                # price = models.CharField(default="", blank=True, verbose_name=_('price'), max_length=10)
                #
                # sites = models.ManyToManyField(Site)
                # on_site = CurrentSiteManager()

            # META
            try:
                self.page_title = self.json["meta_title"]
            except Exception, e:
                self.page_title = ''

            try:
                self.meta_desc = self.json["meta_desc"]
            except Exception, e:
                self.meta_desc = ""

            try:
                self.meta_keywords = self.json["meta_keywords"]
            except Exception, e:
                self.meta_keywords = ""

            # SIZES
            try:
                self.length = self.json["size"]["x"]
            except Exception, e:
                pass

            try:
                self.width = self.json["size"]["z"]
            except Exception, e:
                pass

            try:
                self.height = self.json["size"]["y"]
            except Exception, e:
                pass

            try:
                self.depth = self.json["size"]["d"]
            except Exception, e:
                pass

            try:
                self.value = self.json["size"]["v"]
            except Exception, e:
                pass

            # TAGS
            try:
                tag, created = Tag.objects.get_or_create(name="продукция", slug="products")
                self.tags.add(tag)
                tag, created = Tag.objects.get_or_create(name=self.json["entity"], slug=slugify(
                    translit(self.json["entity"], 'ru', reversed=True)))
                self.tags.add(tag)
                tag, created = Tag.objects.get_or_create(name=self.json["name"],
                                                         slug=slugify(translit(self.json["name"], 'ru', reversed=True)))
                self.tags.add(tag)
            except Exception, e:
                print "entity_error"
                pass

            # ENTITY
            try:
                entity, created = ProductEntity.objects.get_or_create(title=self.json["entity"],
                                                                      slug=self.json["entity"])
                self.entity = entity
            except Exception, e:
                print "entity_error"
                pass

            # TYPEyes
            try:
                type, created = ProductType.objects.get_or_create(title=self.json["type"], slug=self.json["type"])
                self.type = type
            except Exception, e:
                print "type_error"
                pass

            # CATEGORY
            # try:
            #     category, created = ProductCategory.objects.get_or_create(title=self.json["category"])
            #     self.category.add(category)
            #     # print category.slug
            #     #form = ProductFormCat.objects.get(title=self.json["category"])
            #     #self.form = form
            # except Exception, e:
            #     print "cat_error"
            #     pass

            # BY_FORM
            try:
                category_type, created = CategoryType.objects.get_or_create(title="По форме", slug='form')
                category, created = ProductCategory.objects.get_or_create(title=self.json["form"],
                                                                          slug=self.json["form_alias"],
                                                                          category_type=category_type)
                self.category.add(category)
            except Exception, e:
                print "form_error"
                pass

            # BY_STYLE
            try:
                category_type, created = CategoryType.objects.get_or_create(title="По стилю", slug='style')
                category, created = ProductCategory.objects.get_or_create(title=self.json["style"],
                                                                          slug=self.json["style_alias"],
                                                                          category_type=category_type)
                self.category.add(category)
            except Exception, e:
                print "style_error"
                pass

            # BY_MATERIAL
            try:
                category_type, created = CategoryType.objects.get_or_create(title="По материалу", slug='material')
                category, created = ProductCategory.objects.get_or_create(title=self.json["material"],
                                                                          slug=self.json["material_alias"],
                                                                          category_type=category_type)
                self.category.add(category)
            except Exception, e:
                print "material_error"
                pass

            # BY_COLOR
            try:
                category_type, created = CategoryType.objects.get_or_create(title="По цвету", slug='color')
                category, created = ProductCategory.objects.get_or_create(title=self.json["color"],
                                                                          slug=self.json["color_alias"],
                                                                          category_type=category_type)
                self.category.add(category)
            except Exception, e:
                print "color_error"
                pass

            # BY_TYPE
            try:
                category_type, created = CategoryType.objects.get_or_create(title="По типу", slug='type')
                category, created = ProductCategory.objects.get_or_create(title=self.json["furType"],
                                                                          slug=self.json["furType_alias"],
                                                                          category_type=category_type)
                self.category.add(category)
            except Exception, e:
                print "type_error"
                pass

            # BY_MODEL
            try:
                category_type, created = CategoryType.objects.get_or_create(title="По модели", slug='modelline')
                category, created = ProductCategory.objects.get_or_create(title=self.json["modelline"],
                                                                          slug=self.json["modelline_alias"],
                                                                          category_type=category_type)
                self.category.add(category)
            except Exception, e:
                print "modelline_error"
                pass

            # Color
            # try:
            #     color, created = ProductColor.objects.get_or_create(title=self.json["color"], slug=self.json["color"])
            #     self.color = color
            #     #color = ProductColor.objects.get(title=self.json["color"])
            #     #self.form = form
            # except Exception, e:
            #     pass

            # OPTIONS
            try:
                index = 0
                for option in self.json["options"]:
                    index += 1
                    try:
                        if option["group"]:
                            print "option_group"
                            og, created = OptionsGroup.objects.get_or_create(name=option["group"],
                                                                             slug=option["group_alias"])
                            o, created = Option.objects.get_or_create(name=option["name"], slug=option["alias"],
                                                                      shortname=option["shortname"], group=og)
                        else:
                            print "option_one"
                            og, created = OptionsGroup.objects.get_or_create(name="None", slug="none")
                            o, created = Option.objects.get_or_create(name=option["name"], slug=option["alias"],
                                                                      shortname=option["shortname"], group=og)

                        op, created = OptionPrice.objects.get_or_create(product=self, option=o, price="0")
                        op.position = index
                        op.save()

                        # o.exclude = []
                        if option["exclude"]:
                            for exclude in option["exclude_alias"]:
                                # "hydromassazh", "aeromassazh"
                                print 'exclude: %s' % exclude
                                exclude_option = Option.objects.get(slug=exclude)
                                o.exclude.add(exclude_option)

                        # o.dependent = []
                        if option["dependent"]:
                            for dependent in option["dependent_alias"]:
                                # "hydromassazh", "aeromassazh"
                                dependent_option = Option.objects.get(slug=dependent)
                                o.dependent.add(dependent_option)


                                # exact_option = OptionPrice.get(option__slug=option["alias"], product = self)
                    except Exception, e:
                        print Exception, e, "none"
                        print "option_error"

            except Exception, e:
                pass

            # VIDEOS
            try:
                # if not self.additionalvideos_set.all():
                video, created = AdditionalVideos.objects.get_or_create(node=self, position=0,
                                                                        video_url=self.json["youtube"])
                video.video_type = 'youtube'

                img_url = self.json["youtube_cover"]
                name = urlparse(img_url).path.split('/')[-1]
                content = ContentFile(urllib2.urlopen(img_url).read())
                video.video_poster.save(name, content, save=True)

                video.save()

                index = 1
                for video_url in self.json["videos"]:
                    name = urlparse(video_url).path.split('/')[-1]
                    video, created = AdditionalVideos.objects.get_or_create(node=self, position=index, video_url=name)
                    video.video_type = urlparse(video_url).path.split(".")[-1]

                    video_new_url = "media/nodevideos/%s" % name
                    # content = ContentFile(urllib2.urlopen(video_url).read())
                    # with open(location(video_new_url), 'w') as f:
                    #     myfile = File(f)
                    #     myfile.write(urllib2.urlopen(video_url).read())

                    video.video_url = name

                    index += 1
                    video.save()



            except Exception, e:
                print "video_error: %s %s" % (Exception, e)

            # MAIN PHOTO
            try:
                if not self.photo:
                    img_url = self.json["front_img"]
                    name = urlparse(img_url).path.split('/')[-1]
                    content = ContentFile(urllib2.urlopen(img_url).read())
                    self.photo.save(name, content, save=True)
            except:
                a_p_error = 'main_photo'

            # CHAR PHOTO
            try:
                if not self.char_photo:
                    img_url = self.json["char_img"]
                    name = urlparse(img_url).path.split('/')[-1]
                    content = ContentFile(urllib2.urlopen(img_url).read())
                    self.char_photo.save(name, content, save=True)
            except:
                a_p_error = 'main_photo'

            # PHOTOS
            try:
                if not self.additionalphoto_set.all():
                    # print self.json["pictures"]["pic1"]

                    # photo = AdditionalPhoto(node=self, position=0)
                    # if self.json["entity"] == 'bath':
                    #     img_url = self.json["pictures"]["pic1"]
                    # else:
                    #     img_url = self.json["pictures"]["main"]
                    #
                    # name = urlparse(img_url).path.split('/')[-1]
                    # content = ContentFile(urllib2.urlopen(img_url).read())
                    # photo.photo.save(name, content, save=True)
                    #
                    # photo = AdditionalPhoto(node=self, position=1)
                    # photo.photo.save(name, content, save=True)
                    #
                    photo_pos = 2

                    try:
                        photo = AdditionalPhoto(node=self, position=0)
                        img_url = self.json["pictures"]["main"]
                        name = urlparse(img_url).path.split('/')[-1]
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        photo.photo.save(name, content, save=True)
                    except:
                        a_p_error = 'main'

                    try:
                        photo = AdditionalPhoto(node=self, position=1)
                        img_url = self.json["pictures"]["pic1"]
                        name = urlparse(img_url).path.split('/')[-1]
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        photo.photo.save(name, content, save=True)
                    except:
                        a_p_error = 'pic1'

                    try:
                        photo = AdditionalPhoto(node=self, position=2)
                        img_url = self.json["pictures"]["pic2"]
                        name = urlparse(img_url).path.split('/')[-1]
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        photo.photo.save(name, content, save=True)
                    except:
                        a_p_error = 'pic2'

                    try:
                        photo = AdditionalPhoto(node=self, position=3)
                        img_url = self.json["pictures"]["pic3"]
                        name = urlparse(img_url).path.split('/')[-1]
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        photo.photo.save(name, content, save=True)
                    except:
                        a_p_error = 'pic3'
                    try:
                        photo = AdditionalPhoto(node=self, position=4)
                        img_url = self.json["pictures"]["pic4"]
                        name = urlparse(img_url).path.split('/')[-1]
                        content = ContentFile(urllib2.urlopen(img_url).read())
                        photo.photo.save(name, content, save=True)
                    except:
                        a_p_error = 'pic4'

                        # super(AdditionalPhoto, self).save()
            except Exception, e:
                pass

        # if self.pk is None:
        super(Product, self).save(*args, **kwargs)

        # def full_clean(self, *args, **kwargs):
        #     return self.clean(*args, **kwargs)


# class ProductDetails(models.Model):
#     product = models.ForeignKey(Product)
#     json = JSONField(blank=True)
#
#     price = models.CharField(default="", blank=True, verbose_name=_('price'), max_length=10)
#
#     teaser = models.TextField(blank=True)
#     body = models.TextField(verbose_name=_("Body"), blank=True)
#
#     page_title = models.CharField(max_length=200, blank=True, help_text=_("for windows title"))
#     meta_desc = models.TextField(blank=True, help_text=_("for meta description"), verbose_name=_("Meta Description"))
#     meta_keywords = models.CharField(max_length=255, blank=True)
#
#     sites = models.ManyToManyField(Site)
#     on_site = CurrentSiteManager()
#
#     def __unicode__(self):
#         return self.product.title


class SKUProduct(Product):
    class Meta:
        proxy = True


class ImportProduct(Product):
    class Meta:
        proxy = True


class Visor(models.Model):
    product = models.ForeignKey(Product)

    name = models.CharField(
        max_length=255,
        default="",
        blank=True,
        null=True
    )

    width = models.CharField(
        max_length=4,
        default='800',
        blank=True
    )

    height = models.CharField(
        max_length=4,
        default='600',
        blank=True
    )

    frames = models.CharField(
        max_length=3,
        default='32',
        blank=True
    )

    ROTATE_DIRECTIONS = (
        ('right', u'прямой'),
        ('reverse', u'обратный')
    )

    rotate = models.CharField(
        max_length=20,
        default='right',
        blank=True,
        choices=ROTATE_DIRECTIONS
    )

    #
    # directional = models.BooleanField(
    #     default=False,
    # )

    def __unicode__(self):
        return self.name


class Component(SEONode):
    pass


class OptionsGroup(models.Model):
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    desc = models.TextField(verbose_name=_("Description"), blank=True)

    def __unicode__(self):
        return self.name


class Option(models.Model):
    name = models.CharField(max_length=200)

    shortname = models.CharField(max_length=200)

    for_dealer = models.BooleanField(
        default=False,
    )

    requeries = models.BooleanField(
        default=False,
    )

    override = models.BooleanField(
        default=False,
    )

    none_price = models.BooleanField(
        default=False,
    )

    group = models.ForeignKey(
        OptionsGroup,
        blank=True,
        null=True,
        default=1,
    )

    exclude = models.ManyToManyField(
        'self',
        symmetrical=False,
        related_name='exclude_options',
        blank=True
    )

    dependent = models.ManyToManyField(
        'self',
        symmetrical=False,
        related_name='dependent_options',
        blank=True
    )

    exclude_group = models.ManyToManyField(
        OptionsGroup,
        related_name='dependent_options_group',
        blank=True,
    )

    dependent_group = models.ManyToManyField(
        OptionsGroup,
        related_name='exclude_options_group',
        blank=True,
    )

    # weight = models.CharField(default="", blank=True, verbose_name=_('weight'), max_length=10)


    position = models.PositiveSmallIntegerField("Position", default="0")

    desc = models.TextField(verbose_name=_("Description"), blank=True)
    slug = models.CharField(max_length=200)

    def __unicode__(self):
        # return "%s / %s" % (self.group, self.name)
        return "%s" % self.name

    def sku_names(self):
        return ', '.join([a.name for a in self.skus.all()])

    class Meta:
        ordering = ['position']


class Complectation(models.Model):
    name = models.CharField(max_length=200)

    options = models.ManyToManyField(
        Option,
        related_name='complectation'
    )

    icon = models.CharField(
        max_length=100,
        blank=True,
    )

    slug = models.SlugField(unique=True)

    def __unicode__(self):
        return "%s" % self.name


class OptionPrice(models.Model):
    product = models.ForeignKey(
        Product,
        related_name='optionprices')

    option = models.ForeignKey(
        Option
    )

    price = models.CharField(
        default="",
        blank=True,
        verbose_name=_('price'),
        max_length=10
    )

    overide_sku_price = models.BooleanField(
        default=False,
        verbose_name=u'Кастомная цена'
    )

    position = models.PositiveSmallIntegerField(
        "Position",
        default="0"
    )

    action = models.BooleanField(
        verbose_name=_('action'),
        blank=True,
        default=False
    )

    discount = models.IntegerField(default=0, verbose_name=_('discount'))

    not_change = models.BooleanField(verbose_name=_('not change'), blank=True, default=False)

    sku = models.CharField(default="", blank=True, verbose_name=_('SKU'), max_length=50)
    skus = GenericRelation(SKU)

    import_id = models.IntegerField(
        default="0"
    )

    import_date = models.DateField(
        auto_now_add=True,
        null=True,
    )

    # sites = models.ForeignKey(Site)
    # on_site = CurrentSiteManager()

    class Meta:
        ordering = ['position']

    def selflink(self):
        if self.id:
            return "<a href='/admin_tri_ua/products/optionprice/%s/' target='_blank'>Edit</a>" % str(self.id)
        else:
            return "Not present"

    selflink.allow_tags = True

    def discount_price(self, discount):
        if self.price:
            self_price = self.price
        else:
            self_price = 0

        discount_value = float(self_price) / 100 * float(discount)
        discount_cart_sum = float(self_price) - discount_value
        return discount_cart_sum

    def get_weight(self):
        return self.skus.aggregate(Sum('weight'))['weight__sum']

    def save(self, *args, **kwargs):

        pat = re.compile(r'([a-zA-Zа-яА-Я0-9]+)')
        option_skus = re.findall(pat, self.sku.encode('utf-8'))

        # print self.sku.encode('utf-8')
        #
        # for option_sku in option_skus:
        #     try:
        #         sku = SKU.objects.get(object_id=self.id, sites=self.product.sites)
        #     except:
        #         sku = SKU(content_object=self, sku=option_sku)
        #         sku.save()
        #         for site in self.product.sites.all():
        #             sku.sites.add(site)
        #             sku.save()

        if not self.overide_sku_price and self.skus.all():
            price = 0
            for sku in self.skus.all():
                add_price = 0 if sku.price == '' else int(sku.price.encode('utf-8').replace(",", ".").replace(" ", "").replace("руб.", ""))
                price += add_price
            self.price = price

        super(OptionPrice, self).save(*args, **kwargs)

    def __unicode__(self):
        # return "%s / %s" % (self.option.group, self.option)
        return "%s" % self.option.name

    def sites(self):
        return self.product.sites.all()

    def sites_names(self):
        return ', '.join([a.name for a in self.product.sites.all()])

    def sku_names(self):
        return ', '.join([a.sku for a in self.skus.all()])

    def first_sku(self):
        return self.skus.all()[:1].get().sku

    def group(self):
        return self.option.group


class ImportOptionPrice(OptionPrice):
    class Meta:
        proxy = True


class ProductComment(Comment):
    rate = models.CharField(max_length=1, choices=RATE_CHOICES, blank=True)
    plus_comment = models.TextField(verbose_name=_("Pluses"), blank=True, default="")
    minus_comment = models.TextField(verbose_name=_("Minuses"), blank=True, default="")


class ProductCommentForm(ModelForm):
    node = forms.IntegerField(widget=forms.HiddenInput(attrs={'class': '', 'ng-model': 'comment.node'}), label="")
    rate = forms.ChoiceField(choices=RATE_CHOICES,
                             widget=forms.RadioSelect(attrs={'ng-model': 'comment.rate', 'required': 'True'}),
                             label="Оценка", )
    title = forms.CharField(
        widget=forms.TextInput(attrs={'class': '', 'ng-model': 'comment.title', 'required': 'True'}),
        label='Заголовок комментария')
    # author = forms.CharField(widget=forms.TextInput(attrs={'class': 'validate[required]', 'ng-model': 'comment.author', 'required': 'True'}), label='Имя')
    # email = forms.EmailField(widget=forms.TextInput(attrs={'class': '', 'ng-model': 'comment.email', 'required': 'True', 'type': 'email'}))
    # body = forms.Textarea(attrs={'ng-model': 'comment.body'})
    body = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'validate[required]', 'ng-model': 'comment.body', 'required': 'True'}),
        label="Комментарий")
    plus_comment = forms.CharField(widget=forms.Textarea(attrs={'class': '', 'ng-model': 'comment.plus_comment'}),
                                   label='Плюсы', required=False)
    minus_comment = forms.CharField(widget=forms.Textarea(attrs={'class': '', 'ng-model': 'comment.minus_comment'}),
                                    label='Минусы', required=False)

    class Meta:
        model = ProductComment
        fields = ('rate', 'title', 'body', 'plus_comment', 'minus_comment')


        # class BathFilter(django_filters.FilterSet):
        # price = django_filters.RangeFilter()
        # hydromassage = django_filters.BooleanFilter()
        # category = django_filters.ModelMultipleChoiceFilter()
        # price_from = django_filters.NumberFilter(lookup_type='lte')
        # class Meta:
        # model = Product
        # fields = ['style', 'form', 'material', 'hydromassage', 'length', 'price']
        # fields = ['type', 'category']
        #    type = models.ForeignKey(ProductType, related_name='products', verbose_name=_('type'), blank=True, null=True)
        # category = models.ForeignKey(ProductCategory, related_name='products', verbose_name=_('form'), blank=True, null=True)
        # def __init__(self, *args, **kwargs):
        #    super(BathFilter, self).__init__(*args, **kwargs)
        #    self.filters['style'].extra.update(
        # {'all': 'All'})


ORDER_STATUS_CHOICES = (
    ('NO', u'новый'),
    ('CA', u'отменен'),
    ('SA', u'оформлен'),
    # ('FO', _('formation')),
    # ('SE', _('send')),
    ('PR', u'отправлен на предоплату'),
    ('PA', u'предоплачен'),
    ('EX', u'отправлен на исполнение'),
    ('SH', u'отгружен'),
    ('DE', u'доставлен'),
    ('CO', u'полностью оплачен'),
)


class Delivery(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField(verbose_name=_("Body"), blank=True)
    slug = models.SlugField(unique=True)
    publish = models.BooleanField(verbose_name="Publish", default=True)
    price = models.CharField(max_length=50, blank=True)
    icon = models.CharField(max_length=50, blank=True)

    json = JSONField(blank=True)

    position = models.PositiveSmallIntegerField("Position", default="0")

    sites = models.ManyToManyField(Site)
    on_site = CurrentSiteManager()
    objects = models.Manager()

    class Meta:
        ordering = ['position']

    def __unicode__(self):
        return self.title

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])


class Payment(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField(verbose_name=_("Body"), blank=True)
    slug = models.SlugField(unique=True)
    test = models.BooleanField(verbose_name="Test", default=True)
    publish = models.BooleanField(verbose_name="Publish", default=True)

    icon = models.CharField(max_length=50, blank=True)

    def __unicode__(self):
        return self.title


class Prepay(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField(verbose_name=_("Body"), blank=True)
    slug = models.SlugField(unique=True)
    test = models.BooleanField(verbose_name="Test", default=True)
    publish = models.BooleanField(verbose_name="Publish", default=True)
    factor = models.FloatField(default=1)

    icon = models.CharField(max_length=50, blank=True)

    def __unicode__(self):
        return self.title


class Discount(InitialFields):
    '''
    Discount for Order
    '''
    value = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return str(self.value) + "%"


class Order(models.Model):
    # orderID = models.CharField(max_length=200)

    hash = models.CharField(
        max_length=200,
        blank=True,
        default="",
        verbose_name=u'Хэш код',
    )

    version = models.CharField(
        max_length=20,
        default="",
        blank=True
    )

    status = models.CharField(
        max_length=2,
        choices=ORDER_STATUS_CHOICES,
        default="NO",
        verbose_name=u'Статус',
    )
    # user = models.CharField(max_length=50)

    test = models.BooleanField(
        default=False,
        verbose_name=u'Тест',
    )

    # BUYER INFO

    name = models.CharField(
        max_length=200,
        blank=True,
        default="",
        verbose_name=u'Имя',
    )

    ip = models.CharField(
        max_length=20,
        blank=True,
        default="",
        verbose_name=u'IP',
    )

    address = models.CharField(
        max_length=1000,
        default="",
        blank=True,
        verbose_name=u'Адрес',
    )

    country = models.CharField(
        max_length=255,
        default=u"Россия",
        blank=True,
    )

    city = models.CharField(
        max_length=255,
        default="",
        blank=True,
    )

    index = models.CharField(
        max_length=255,
        default=u"",
        blank=True,
        verbose_name='Индекс',
    )

    region = models.CharField(
        max_length=255,
        default=u"",
        blank=True,
        verbose_name='Регион',
    )

    street = models.CharField(
        max_length=255,
        default="",
        blank=True,
    )

    street_abbr = models.CharField(
        max_length=10,
        default="",
        blank=True,
    )

    house = models.CharField(
        max_length=20,
        blank=True,
        default="",
    )

    phone = models.CharField(
        max_length=20,
        default="",
        blank=True,
        verbose_name=u'Телефон',
    )

    email = models.EmailField(
        max_length=250,
        default="",
        blank=True,
        verbose_name=u'Имейл',
    )

    delivery = models.ForeignKey(
        Delivery,
        blank=True,
        null=True,
        default=None,
        verbose_name=u'Доставка',
    )

    carrier = models.ForeignKey(
        'tri.Company',
        blank=True,
        null=True,
        verbose_name=u'Перевозчик',
    )

    carrier_number = models.CharField(
        max_length=100,
        default="",
        blank=True,
        verbose_name=u'Номер заявки перевозчика',
    )

    need_cargo_service = models.BooleanField(
        default=False,
        verbose_name=u'Нужны услуги грузчиков(в т.ч. подъем на этаж)',
    )

    distributor = models.ForeignKey(
        'tri.Company',
        blank=True,
        null=True,
        verbose_name=u'Дистрибутор',
        related_name='distributor',
    )

    pay_agent = models.ForeignKey(
        'tri.Company',
        blank=True,
        null=True,
        verbose_name=u'Платежный агент',
        related_name='pay_agent',
    )

    pay_date = models.DateField(
        'payment date',
        blank=True,
        null=True,
    )

    additional_pay_date = models.DateField(
        'additional payment date',
        blank=True,
        null=True,
    )

    delivery_date = models.DateField(
        'delivery date',
        blank=True,
        null=True,
    )

    delivery_sum = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Стоимость доставки',
    )

    custom_delivery_sum = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Индивидуальная стоимость доставки',
    )

    delivery_did = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'ID номера доставки',
    )

    PICKUP_TIME_PERIOD_CHOICES = (
        ('9_18', u'9-18'),
        ('9_13', u'9-13'),
        ('13_18', u'13-18'),
    )

    delivery_pickup_time_period = models.CharField(
        max_length=20,
        default="9_18",
        blank=True,
        verbose_name=u'Время отгрузки',
        choices=PICKUP_TIME_PERIOD_CHOICES,
    )

    custom_delivery = models.BooleanField(
        default=False
    )

    prepay = models.ForeignKey(
        Prepay,
        blank=True,
        null=True,
        default=1,
        verbose_name=u'Предоплата',
    )

    pay_type = models.ForeignKey(
        Payment,
        blank=True,
        null=True,
        default=None,
        verbose_name=u'Тип оплаты',
    )

    comment = models.TextField(
        default="",
        blank=True,
        verbose_name=u'Комментарий',
    )

    pub_date = models.DateTimeField('date published', auto_now_add=True)

    price = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Цена',
    )

    bonus = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Бонус',
    )

    bonus_user = models.ForeignKey(
        User,
        blank=True,
        null=True,
        verbose_name=u'Бонусодержатель',
        related_name='bonus_user'
    )

    cart_sum_end = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Цена без скидки',
    )

    discount_value = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Цена скидки',
    )

    prepay_sum = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Сумма предоплаты',
    )

    order_sum_amount = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Оплачено',
    )

    additional_order_sum_amount = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Доплачено',
    )

    invoice_id = models.CharField(
        max_length=30,
        blank=True,
        default="",
        verbose_name=u'Номер инвойса',
    )

    additional_invoice_id = models.CharField(
        max_length=30,
        blank=True,
        default="",
        verbose_name=u'Номер дополнительного инвойса',
    )

    user = models.ForeignKey(
        User,
        blank=True,
        null=True,
        verbose_name=u'Пользователь',
    )

    site = models.ForeignKey(
        Site,
        blank=True,
        default=1,
        verbose_name=u'Сайт',
    )

    discount = models.CharField(
        default=0,
        blank=True,
        verbose_name=u'Скидка',
        max_length=20,
    )

    products = models.TextField(
        blank=True
    )

    delivery_price = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    destination = models.CharField(
        max_length=20,
        default="",
        blank=True,
        verbose_name=u'Расстояние в км',
    )

    objects = GetOrNoneManager()

    class Meta:
        permissions = (("can_view_bonus", "Can view bonus"),)

    def __unicode__(self):
        return str(self.id)

    # SET DELIVERY PRICE ----------------------------------------------------------------------------
    def set_delivery_price(self):
        a = DpdService()
        minus_day = self.delivery_date - datetime.timedelta(days=1)
        plus_day = self.delivery_date + datetime.timedelta(days=1)

        result = a.get_orders_amount(minus_day.isoformat(), plus_day.isoformat())

        for order in result:
            if order['ordernum'] == self.carrier_number:
                self.delivery_price = order['amount']
                self.save()

    # GET DELIVERY STATUS -----------------------------------------------------------------------------
    def get_delivery_status(self):
        a = DpdService()
        print self.carrier_number
        print self
        result = a.check_order_status_by_dpd_id(self.carrier_number)
        status = ''
        on_road = ['OnTerminalPickup', 'OnRoad', 'OnTerminal', 'OnTerminalDelivery', 'Delivering']
        try:
            status = result[-1][-1]['newState']
            order_log_json = {
                'sender': 'DPD',
                'action': 'change_status',
                'value': status
            }
            orderlog = OrderLog(order=self, json=order_log_json)
            orderlog.save()
            print 'status: ', status
            if status in on_road:
                print 'set on_road'
                self.status = 'SH'
                self.save()
            elif status == 'Delivered':
                print 'set delivered'
                self.status = 'DE'
                self.save()
                # a.confirm_order_statuses(result['docId'])

        except Exception, e:
            print Exception, e

            # for status in result:
            #     print status
            #     if order['ordernum'] == self.delivery_id:
            #         self.delivery_price = order['amount']
            #         self.save()

    # GET FULL PRICE ----------------------------------------------------------------------------
    def full_price(self):
        try:
            return int(self.price) + int(self.delivery_sum)
        except Exception, e:
            return "-"

    # GET WEIGHT ----------------------------------------------------------------------------
    def weight(self):
        try:
            order_weight = 0
            for orderitem in self.order_items.all():
                order_weight += orderitem.product.get_weight()
                for option in orderitem.options.all():
                    order_weight += option.get_weight()
        except Exception, e:
            print Exception, e
        return order_weight

    # GET BONUS ----------------------------------------------------------------------------
    def get_bonus(self, save=True):
        try:
            order_bonus = 0
            for orderitem in self.order_items.all():
                if orderitem.product.entity.slug == 'mebel' or 'standart' in orderitem.product.category.all().values_list('slug', flat=True) or orderitem.product.type.slug == 'poddon' or \
                                orderitem.product.type.slug == \
                                'mixer':
                    order_bonus += 200
                elif orderitem.product.entity.slug == 'bathcab':
                    order_bonus += 300
                elif 'gelkoyt' in orderitem.product.category.all().values_list('slug', flat=True):
                    order_bonus += 500
                elif orderitem.product.entity.slug == 'bath':
                    order_bonus += 300

                try:
                    for option in orderitem.options.all():
                        if int(float(option.price)) > 8000:
                            if 'gelkoyt' in orderitem.product.category.all().values_list('slug', flat=True):
                                order_bonus += 500
                            else:
                                order_bonus += 300
                        elif int(float(option.price)) > 800:
                            order_bonus += 100

                except Exception, e:
                    print Exception, e

        except Exception, e:
            print Exception, e

        if save:
            self.bonus = order_bonus
            self.save()
        return order_bonus

    # GET PREPAY SUM ----------------------------------------------------------------------------
    def prepay_sum(self):
        try:
            # return int(round(float(self.price) * float(self.prepay.factor) + float(self.delivery_sum)))
            # if ($scope.global.cart_prepay == 1) {
            #     $scope.global.cart_prepay_sum = Math.round(parseFloat($scope.global.pay_price) * $scope.global.cart_prepay + parseFloat($scope.global.cart_delivery_sum));
            # } else {
            #     $scope.global.cart_prepay_sum = Math.round(parseFloat($scope.global.pay_price) * $scope.global.cart_prepay);
            # }


            if self.prepay.factor == 1:
                if self.order_sum_amount:
                    return int(round(float(self.order_sum_amount)))
                else:
                    return int(round(float(self.price) * float(self.prepay.factor) + float(self.delivery_sum)))
            else:
                if self.additional_order_sum_amount and self.order_sum_amount:
                    return int(round(float(self.order_sum_amount)) + round(float(self.additional_order_sum_amount)))
                elif self.order_sum_amount:
                    return int(round(float(self.order_sum_amount)))
                else:
                    return int(round(float(self.price) * float(self.prepay.factor)))
        except Exception, e:
            return 0

    # GET ADDITIONAL SUM ----------------------------------------------------------------------------
    def additional_sum(self):
        try:
            return int(round(float(self.price) + int(float(self.delivery_sum)) - self.prepay_sum()))

            # if self.prepay.factor == 1:
            #     return 0
            # else:
            #     if self.order_sum_amount:
            #         return int(round(float(self.price) - int(round(float(self.order_sum_amount)))) + int(self.delivery_sum))
            #     else:
            #         return int(round(float(self.price) - (float(self.price) * float(self.prepay.factor))) + int(self.delivery_sum))

        except Exception, e:
            return "0"

    # GET ADDRESS ----------------------------------------------------------------------------
    def get_address(self):
        if self.city:
            return "%s, %s, %s %s, %s" % (self.country, self.city, self.street_abbr, self.street, self.house)
        else:
            return self.address

    # CACHE ----------------------------------------------------------------------------
    def cache(self):
        try:
            return float(float(self.price) - float(self.price) * float(self.prepay.factor))
        except Exception, e:
            return "-"

    def deliveries(self):
        return Delivery.objects.filter(sites=self.site)

    # GET DELIVERY PRICE ----------------------------------------------------------------------------
    def get_delivery_price(self, delivery=None):
        delivery_sum = 0
        if delivery.slug == 'custom':
            delivery_sum = self.custom_delivery_sum
        else:
            for orderitem in self.order_items.all():
                delivery_sum += float(orderitem.get_delivery_price(delivery))
        return delivery_sum

    def available_delivery_prices(self):
        prices = []
        for delivery in self.deliveries():
            try:
                price = self.get_delivery_price(delivery)
                prices.append({
                    'title': delivery.title,
                    'slug': delivery.slug,
                    'price': price
                })
            except Exception, e:
                print Exception, e
        return prices

    # SAV OVERRIDE ----------------------------------------------------------------------------
    def get_invoice_items(self):
        data = []
        price_sum = 0

        oreder_len = len(self.order_items.all()) - 1
        last = False

        for i, orderitem in enumerate(self.order_items.all()):
            if i == 0:
                nach = 1
            else:
                nach = 0

            if i == oreder_len:
                last = True

            orderitem_len = len(orderitem.product.skus_with_price()) - 1
            for j, orderitemsku in enumerate(orderitem.product.skus_with_price()):

                price_sum += float(orderitemsku.discount_price(self.discount))

                options_list = ''
                price = orderitemsku.discount_price(self.discount)
                provider_price = float(0 if orderitemsku.provider_price == "" else float(orderitemsku.provider_price))

                if j == 0:
                    options_price, options_price_sum, options_provider_price = 0, 0, 0
                    for option in orderitem.non_dealer_options():
                        options_price += float(option.discount_price(self.discount))
                        option_sku = option.skus.all()[:1].get()
                        options_provider_price += 0 if option_sku.provider_price == "" else float(option_sku.provider_price)

                    for option in orderitem.options.all():
                        options_price_sum += float(option.discount_price(self.discount))

                    price_sum += float(options_price_sum)
                    price += float(options_price)
                    provider_price += float(options_provider_price)
                    options_list = orderitem.non_dealer_options_list()
                else:
                    nach = 0

                if j == orderitem_len and last:
                    price_diff = float(self.price) - float(price_sum)
                    price += float(price_diff)

                try:
                    delivery_date = date(self.delivery_date.year,
                                         self.delivery_date.month,
                                         self.delivery_date.day)
                except:
                    delivery_date = ''

                try:
                    pay_date = date(self.pay_date.year, self.pay_date.month, self.pay_date.day)
                except:
                    pay_date = ''

                try:
                    pay_agent_title = self.pay_agent.company_legal.title
                    pay_agent_inn = self.pay_agent.company_legal.inn
                except:
                    pay_agent_title = ''
                    pay_agent_inn = ''

                try:
                    pub_date = date(self.pub_date.year, self.pub_date.month, self.pub_date.day)
                except:
                    pub_date = ''

                # if 'REPLACE' in orderitemsku.sku:
                #     orderitemsku_sku = orderitemsku.sku.replace('REPLACE', '')

                orderitemsku_sku = orderitemsku.sku

                try:
                    for j, option in enumerate(orderitem.override_options()):
                        if option.option.override:
                            orderitemsku_sku = option.skus.all()[:1].get().sku
                except Exception, e:
                    print Exception, e
             

                data.append({
                    'sku': orderitemsku_sku,
                    'title': orderitem.product.title,
                    'ed': u'шт',
                    'count': orderitem.count,
                    'price': price,
                    'sum': price
                })

            try:
                for j, option in enumerate(orderitem.dealer_options()):

                    if not option.option.group.slug == 'none':
                        option_name = "%s %s" % (option.option.group.name, option.option.name)
                    else:
                        option_name = "%s" % (option.option.name,)

                    option_sku = option.skus.all()[:1].get()

                    data.append({
                        'sku': option.skus.all()[:1].get().sku,
                        'title': option_name,
                        'ed': u'шт',
                        'count': 1,
                        'price': float(option.discount_price(self.discount)),
                        'sum': float(option.discount_price(self.discount))
                    })


            except Exception, e:
                print Exception, e

        return data

    # SAV OVERRIDE ----------------------------------------------------------------------------
    def save(self, *args, **kwargs):

        # Create products field
        try:
            self.products = get_template("products/parts/products_field.html").render(
                Context({
                    'orderitems': self.order_items.all(),
                })
            )
            # print self.products
        except Exception, e:
            print Exception, e

        # Count Delivery sum
        try:
            if self.delivery.slug == 'custom':
                delivery_sum = self.custom_delivery_sum
            else:
                delivery_sum = 0
                for orderitem in self.order_items.all():
                    delivery_sum += float(orderitem.get_delivery_price())
            self.delivery_sum = delivery_sum
        except Exception, e:
            print Exception, e

        if not self.pk is None:
            pre_order = Order.objects.get(pk=self.pk)
            # if self.status == "PR":
            #     DaMailer.send_mail_to_address(template='products/email/customer_pay.email.html', subject=u'Доступ к вашему заказу на 3tn', data=self, email=self.email)
            # if self.status != pre_order.status:
            #     elif self.status == "PA":
            #         try:
            #             admins_emails = self.site.get_admins_emails()

            # DaMailer.send_mail_to_addresses(template='products/email/payment_success.email.html', subject=u'Заказ #%s %s оплачен' % (self.id, SiteInfo.on_site.get().site.domain), data=self, emails=admins_emails)
            # DaMailer.send_mail_to_address(template='products/email/customer_payment_success.email.html', subject=u'Ваш %sзаказ на %s' % (adjective, order.site.name), data=order, email=order.email)

            # Sms_Api.send_message_to_phone_number(phone_list=admins_phones, message="Order #%s on %s pay. Sum: %s" % (self.id, self.site.domain, self.order_sum_amount))
            # except:
            #     pass
            # else:
            #     pass
            # DaMailer.send_mail_to_address(template='products/email/customer_change_order_status.email.html', subject=u'Изменение вашего заказ на 3tn', data=self,
            #                               email=self.email)

        self.bonus = self.get_bonus(save=False)
        super(Order, self).save(*args, **kwargs)

    def get_discount(self):
        common_discount = 0
        for discount in self.discount.all():
            common_discount += discount
        return discount

    def get_price(self):
        return self.price - ((self.price / 100) * self.get_discount())

    def change_status(self, status):
        self.status = status
        self.save()

    def change_orderid(self, orderid):
        self.orderID = orderid
        self.save()

    def change_bayer_info(self, name, address, phone, email, delivery, pay_type, comment):
        self.name = name
        self.address = address
        self.phone = phone
        self.email = email
        self.delivery = delivery
        self.pay_type = pay_type
        self.comment = comment
        self.save()

    def set_responce(self, responce):
        self.peyment_responce = responce
        self.save()

    def get_order_items(self):
        return ', '.join([orderitem.product.title for orderitem in self.order_items.all()])

    def get_order_items_id(self):
        return ', '.join([orderitem.product.id for orderitem in self.order_items.all()])

    def im_id(self):
        if self.version:
            return 'IM-%s/%s' % (self.id, self.version)
        else:
            return 'IM-%s' % self.id

    def im_id_wo_version(self):
        return 'IM-%s' % self.id

    def siteinfo(self):
        return self.site.siteinfo_set.all()[:1].get()

    def on_site(self):
        try:
            url = "<a href='/order%s' target='_blank'>смотреть на сайте</a>" % (str(self.hash),)
        except Exception, e:
            url = '%s %s' % (Exception, e)
        return url

    def delivery_operations(self):
        return DeliveryOperations.objects.filter(order=self)

    def can_edit(self):
        if self.status in ('NO', 'CA', 'SA', 'PR', 'PA'):
            return True
        else:
            return False

    on_site.short_description = u'Смотреть на сайте'
    on_site.allow_tags = True


class DeliveryOperations(models.Model):
    order = models.ForeignKey(
        Order,
        related_name='order',
    )

    carrier = models.ForeignKey(
        'tri.Company',
        blank=True,
        null=True,
        verbose_name=u'Перевозчик',
    )

    carrier_number = models.CharField(
        max_length=100,
        default="",
        blank=True,
        verbose_name=u'Номер заявки перевозчика',
    )

    delivery_price = models.CharField(
        max_length=255,
        default="",
        blank=True,
        verbose_name=u'Стоимость доставки установленная перевозчиком',
    )

    date = models.DateTimeField(
        'date published',
        auto_now_add=True
    )


class OrderLog(models.Model):
    order = models.ForeignKey(
        Order,
        related_name='order_logs'
    )

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True
    )

    json = JSONField(blank=True)

    class Meta:
        ordering = ['-pub_date']


import math


class OrderItemOption(models.Model):
    option = models.ForeignKey(
        Option,
    )

    price = models.CharField(
        max_length=255,
        default="",
        blank=True
    )


class OrderItem(models.Model):
    order = models.ForeignKey(
        Order,
        related_name='order_items'
    )

    product = models.ForeignKey(
        Product,
    )

    # Options with prices on buy moment
    order_item_options = models.ManyToManyField(
        OrderItemOption,
        related_name='order_item_options',
    )

    # options with actual prices
    options = models.ManyToManyField(
        OptionPrice,
        related_name='options',
    )

    # Price with discounts
    price = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    # Price without discounts
    full_price = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    delivery_price = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    count = models.PositiveSmallIntegerField(
        default=1
    )

    def options_list(self):
        return ', '.join([optionprice.option.name for optionprice in self.options.all()])

    def options_ids(self):
        return [int(optionprice.id) for optionprice in self.options.all()]

    def non_dealer_options_list(self):
        return ', '.join([optionprice.option.name for optionprice in self.options.filter(option__for_dealer=False)])

    def non_dealer_options(self):
        return self.options.filter(option__for_dealer=False)

    def dealer_options(self):
        return self.options.filter(option__for_dealer=True)

    def none_price_options(self):
        return self.options.filter(option__none_price=True)

    def override_options(self):
        return self.options.filter(option__override=True)

    def __unicode__(self):
        return str(self.id)

    def order_id(obj):
        return obj.order.id

    def duscount_price(self):
        discount_value = float(self.price) / 100 * float(self.order.discount)
        discount_cart_sum = float(self.price) - discount_value
        return discount_cart_sum

    def deliveries(self):
        return Delivery.objects.filter(sites=self.order.site_id)

    def deliveries_names(self):
        return ', '.join([delivery.title for delivery in self.deliveries()])

    def available_delivery_prices(self):
        print "AVAILABLE DEIVERY PRICES ----------------------------------------------"
        prices = []
        print prices
        for delivery in self.deliveries():
            try:
                if self.product.entity.slug == 'bath' and self.product.hydromassage:
                    price = delivery.json['bath_hydro']
                else:
                    price = delivery.json[self.product.entity.slug]

                if "km" in delivery.json:
                    km = delivery.json['km']
                else:
                    km = None

                if "from" in delivery.json:
                    from_km = delivery.json['from']
                else:
                    from_km = None

                prices.append({
                    'title': delivery.title,
                    'slug': delivery.slug,
                    'price': price,
                    'from_km': from_km,
                    'km': km,

                })
            except Exception, e:
                print Exception, e
        print prices
        return prices

    def get_delivery_price(self, delivery=None):
        self.delivery_price = 0
        if self.price == '0':
            return self.delivery_price
        if delivery:
            try:
                if self.product.entity.slug == 'bath' and self.product.hydromassage:
                    self.delivery_price = delivery.json['bath_hydro']
                else:
                    self.delivery_price = delivery.json[self.product.entity.slug]
            except:
                pass
        else:
            try:
                if self.product.entity.slug == 'bath' and self.product.hydromassage:
                    self.delivery_price = self.order.delivery.json['bath_hydro']
                else:
                    self.delivery_price = self.order.delivery.json[self.product.entity.slug]
            except:
                pass
        return self.delivery_price


CREDIT_TERM_CHOICES = (
    ('6', _('6')),
    ('10', _('10')),
    ('12', _('12')),
    ('24', _('24')),
    ('30', _('30')),
    ('36', _('36')),
)

CREDIT_TYPE_CHOICES = (
    ('CR', _('кредит')),
    ('RA', _('рассрочка')),
)


class Credit(models.Model):
    name = models.CharField(max_length=200, blank=True, default="")
    phone = models.CharField(max_length=20, default="", blank=True)
    email = models.EmailField(max_length=250, default="", blank=True)
    term = models.CharField(max_length=2, default='6', choices=CREDIT_TERM_CHOICES)
    first_pay_percent = models.CharField(max_length=2, default='10')
    credit_type = models.CharField(max_length=2, default='CR', choices=CREDIT_TYPE_CHOICES)



class Save3d(SEONode):
    '''
    Users 3D plans saves
    '''
    save_name = models.CharField(
        max_length=255,
        default="",
        blank=True
    )

    save_data_filename = models.FileField(
        upload_to='saves3d',
        blank=True
    )

    products = models.ManyToManyField(
        Product,
        related_name='products',
        blank=True
    )

    def save(self, *args, **kwargs):
        self.save_name = self.title
        super(Save3d, self).save(*args, **kwargs)

# SIGNALS

@receiver(post_save, sender=Product)
def set_product_nid(instance, **kwargs):
    '''
    Set product nid from id automaticaly
    '''
    product = instance

    if not product.nid:
        product.nid = product.id
        product.save()


# @receiver(post_save, sender=Order)
# def change_status(instance, **kwargs):
#     '''
#     Send emails about order status changes
#     '''
#     print kwargs


@receiver(post_save, sender=SKU)
def calc_product_price(instance, **kwargs):
    '''
    Set product prtice from id automaticaly
    '''
    # print "calc_product_price"
    content_object = instance.content_object
    content_object.save()

    # if not instance.sites.all():
    #     for site in instance.content_object.sites.all():
    #         instance.sites.add(site)
    #         instance.save()


@receiver(post_save, sender=Product)
@receiver(post_delete, sender=Product)
def clear_cache(instance, **kwargs):
    '''
    clear cache
    '''
    cache.clear()
