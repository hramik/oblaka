# -*- coding: utf-8 -*-

__author__ = 'hramik'

from django.db.models.signals import post_save
from django.dispatch import receiver
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)



from products.models import *

@receiver(post_save, sender = Product)
def set_nid(instance, **kwargs):
    product = instance
    print "SIGNAL"



    if not product.nid:
        product.nid = product.id
        product.save()


# post_save.connect(set_nid, sender = Product)