# -*- coding: utf-8 -*-
__author__ = 'm13v246'

import os
import re
import urllib2
import cookielib, Cookie
import ssl
import urllib
import json

from datetime import datetime
from django.http import HttpResponse
from django.contrib.auth.decorators import permission_required
from django.conf import settings
from django.db.models.loading import get_model
from django.template.loader import render_to_string
from django.template import RequestContext
# from products.models import Product
from price_parsing.models import Prices, Analogs
from bs4 import BeautifulSoup
from price_parsing.forms import PeriodForm, regions_rus
from transliterate import translit, get_available_language_codes
from transliterate import detect_language
from django.db.models import Max

# Data to iterate while filling html table
sites = {
    'triton': ['triton', 'triton-san.ru', 'triton-plus.ru', 'svton.ru', 'sanexpert24.ru',],
    'novosib': ['novosib', 'www.triton-sfo.ru', 'www.djakudza.ru', 'www.sksanta.ru',
                'www.akvasib.ru', 'vachsosed.ru', 'www.kwadratura.ru', 'pomoshcnik.ru', 'triton54.ru',
                'skladremonta.ru', 'piramida-nsk.com', 'vanny.dvaproraba.ru', 'xn--54-vlcueknc.xn--p1ai',
                'nsk-santeh.ru', 'vodolei54.ru', 'kvadrat-nsk.ru', 'xn--80aaaag4cofadj5cxk.su', 'sardana24.ru',
                'algoritmremonta.ru', 'meduzza.ru', 'www.sibvanna.ru', 'www.santsib.ru', '4komnaty-nsk.ru'],
    'piter': ['piter', 'vengoshop.ru', 'www.vannavdom.com', 'ukroy.spb.ru', 'ovanna.ru', 'inhouse-spb.ru', '812vann.ru',
              'santehland.ru', 'www.bt-deluxe.ru', 'aquaink.ru', 'stroytorg812.ru', 'atst-spb.ru',
              'santex-spb.ru', 'samtexnik.ru', 'webdush.ru', 'stroytorg97.ru', 'mks-market.ru', 'san-sanych.ru',
              'santehmartspb.ru', 'h2oprofi.ru', 'vodopad.spb.ru', 'aquanta-spb.ru', 'www.akvaformat.ru',
              'santeh-allegro.ru', 'sanspb.ru', 'aquacera.ru', 'xn----8sbsnkbhebctyeu.xn--p1ai',
              'santehvann.ru', 'hitkabina.ru', 'masterovoy-spb.ru', 'mebelvan.ru', 'rushop01.ru', 'santehmaster.ru',
              'viavanna.ru', 'nychshop.ru', 'www.pitermart.ru', 'santehsalonspb.ru', 'gidrobum.ru',
              'www.aquadomspb.ru', 'www.santechnika-spb.ru', 'webdush.ru'],

    'krasnodar': ['krasnodar',
                  'www.santideal.ru',
                  # 'krasnodar.pulscen.ru',
                  'www.san-elit.ru',
                  '23vanni.ru',
                  # 'evropa.san.ru',
                  # 'bizorg.su',
                  # 'krasnodar-kray.tiu.ru',
                  # 'krasnodar.stro18.ru',
                  'santehmega.com'],
    'perm': ['perm', 'www.sansit.ru', 'www.ar59.ru', 'perm.e96.ru', 'vanna59.ru', 'santehopt-perm.ru'],
    'chelyabinsk': ['chelyabinsk', 'www.vanna74.ru', 'aquapulse.su', 'bathroom74.ru', 'kabinka74.ru',
                    'line74.ru', 'mirvann74.ru', 'oazis74.com', 'santehavrora.ru', 'santop74.ru',
                    'santorg74.ru', 'vanna21veka.ru', 'vanni74.ru', 'www.chelyabinsk.h2oprofi.ru', 'www.odis74.ru',
                    'www.sansay74.ru', 'www.vannaiya.ru', 'yavanna.ru'],
    'eburg': ['eburg', 'greenloft.satom.ru', 'aqua-tetis.ru', 'brigadirshop.ru', 'domwc.ru', 'ekb.dushevoi.ru',
              'e-gran.ru', 'esmag.ru', 'greenloft.ru', 'la-vanna.ru', 'shop.piastrella.info', 'plitca.ru',
              'pol-doma.com', 'qpall.ru', 'remont96.com', 'santechbomba.ru', 'santeh-magazin.ru', 'santehnd.ru',
              'santehprivoz.ru', 'santehural.ru', 'smesitel96.ru', 'www.stkbegemot.ru', 'www.stm66.ru', 'stroymag66.ru',
              'www.trendkeramika.ru', 'triton-ekb.ru',
              'uralsanteh.ru', 'vanna-anna.ru', 'www.vannam.ru', 'www.vanna-ural.ru', 'vanny-triton.ru', 'vernax.ru',
              'wcshop.ru'],
    'samara': ['samara', 'akvametriya.ru', 'santehnika163.ru', 'vannyplus.ru', 'vanna63.ru', 'www.dushkabina63.ru',
               'stroytandem63.ru', 'svsan.ru', 'www.samara.h2oprofi.ru', 'samara.pomoshcnik.ru'],
    'nnovgorod': ['nnovgorod', 'mebelwelcome.ru', 'xn----gtbcqvbabdfx.xn--p1ai', 'vanna52.ru', 'www.novosel-52.ru',
                  'plitka-nnov.ru', 'saniterm.blizko.ru', 'www.keramika52.ru', 'nn.h2oprofi.ru', 'www.nn.ru',
                  'santerra-shop.ru'],
    'rostov': ['rostov', '61vanna.ru', 'santehmega.com', 'www.santeh-online.ru', 'www.santehdon.ru', 'triton-store.ru'],
    'moscow': ['moscow', 'www.triton-vanna.ru', '3tn24.ru', 'deltasan.ru', 'global-san.ru', 'www.remont-vann.ru',
               'spavdome.ru', 'sanok.ru', 'www.gidromarket.ru', 'triton-store.ru', 'www.mirsanteh.ru',
               'axor.su', 'sogo-shop.ru', 'aquavil.ru', 'santut.ru', 'www.aquasanremo.ru', 'santeh-snab.ru',
               'kiosk-santehniki.ru', 'www.spa.com.ru', 'miratop.ru', 'mebelvan.ru', 'www.santehgorod.ru',
               'globalsantehniks.ru'],
}

regions = ['novosib', 'piter', 'perm', 'krasnodar', 'chelyabinsk', 'eburg', 'moscow', 'nnovgorod', 'rostov', 'samara']
regions_s = ['triton']

# regions_rus = {'chelyabinsk': 'Челябинск', 'eburg': 'Екатеринбург', 'samara': 'Самара', 'rostov': 'Ростов',
# 'moscow': 'Москва', 'nnovgorod': 'Нижний Новгород'}

regions_rus = dict(regions_rus)

site_methods = {
    # 'triton': 'parse_local',
    'novosib': 'parse_local',
    'triton': 'parse_local',
    'www.xn--54-6kcaj1bll6ck6i.xn--p1ai': 'parse_xn__54_6kcaj1bll6ck6i',
    'www.triton-sfo.ru': 'parse_triton_sfo',
    'www.djakudza.ru': 'parse_djakudza',
    'www.sksanta.ru': 'parse_sksanta',
    'www.akvasib.ru': 'parse_akvasib',
    'vachsosed.ru': 'parse_vachsosed',
    'www.kwadratura.ru': 'parse_kwadratura',
    'pomoshcnik.ru': 'parse_pomoshcnik',
    'triton54.ru': 'parse_triton54',
    'skladremonta.ru': 'parse_skladremonta',
    'piramida-nsk.com': 'parse_piramida_nsk',
    'vanny.dvaproraba.ru': 'parse_dvaproraba',
    'xn--54-vlcueknc.xn--p1ai': 'parse_54_vlcueknc',
    'nsk-santeh.ru': 'parse_nsk_santeh',
    'vodolei54.ru': 'parse_vodolei54',
    'kvadrat-nsk.ru': 'parse_kvadrat_nsk',
    'xn--80aaaag4cofadj5cxk.su': 'parse_xn__80aaaag4cofadj5cxk',
    'sardana24.ru': 'parse_sardana24',
    'algoritmremonta.ru': 'parse_algoritmremonta',
    'meduzza.ru': 'parse_meduzza',
    'www.sibvanna.ru': 'parse_sibvanna',
    'www.santsib.ru': 'parse_santsib',
    '4komnaty-nsk.ru': 'parse_4komnaty_nsk',
    'piter': 'parse_local',
    'inhouse-spb.ru': 'parse_inhouse_spb',
    '812vann.ru': 'parse_812vann',
    'ovanna.ru': 'parse_ovanna',
    'vengoshop.ru': 'parse_vengoshop',
    'ukroy.spb.ru': 'parse_ukroy_spb',
    'www.vannavdom.com': 'parse_vannavdom',
    'webdush.ru': 'parse_webdush',
    'www.santechnika-spb.ru': 'parse_santechnika_spb',
    'www.aquadomspb.ru': 'parse_aquadomspb',
    'gidrobum.ru': 'parse_gidrobum',
    'santehsalonspb.ru': 'parse_santehsalonspb',
    'www.pitermart.ru': 'parse_pitermart',
    'nychshop.ru': 'parse_nychshop',
    'viavanna.ru': 'parse_viavanna',
    'santehmaster.ru': 'parse_santehmaster',
    'rushop01.ru': 'parse_rushop01',
    'masterovoy-spb.ru': 'parse_masterovoy_spb',
    'hitkabina.ru': 'parse_hitkabina',
    'xn----8sbsnkbhebctyeu.xn--p1ai': 'parse_comfort_montage',
    'santehvann.ru': 'parse_santehvann',
    'aquacera.ru': 'parse_aquacera',
    'sanspb.ru': 'parse_sanspb',
    'santeh-allegro.ru': 'parse_santeh_allegro',
    'www.akvaformat.ru': 'parse_akvaformat',
    'aquanta-spb.ru': 'parse_aquanta_spb',
    'vodopad.spb.ru': 'parse_vodopad_spb',
    'h2oprofi.ru': 'parse_h2oprofi',
    'santehmartspb.ru': 'parse_santehmartspb',
    'san-sanych.ru': 'parse_san_sanych',
    'mks-market.ru': 'parse_mks_market',
    'stroytorg97.ru': 'parse_stroytorg97',
    # 'stroytorg812.ru': 'parse_stroytorg812',
    'webdush.ru': 'parse_webdush',
    'samtexnik.ru': 'parse_samtexnik',
    'santex-spb.ru': 'parse_santex_spb',
    'atst-spb.ru': 'parse_atst_spb',
    'santehland.ru': 'parse_santehland',
    'www.bt-deluxe.ru': 'parse_bt_deluxe',
    'aquaink.ru': 'parse_aquaink',
    'stroytorg812.ru': 'parse_stroytorg812',
    'krasnodar': 'parse_local',
    'www.santideal.ru': 'parse_santideal',
    'krasnodar.pulscen.ru': 'parse_krasnodar_pulscen',
    'www.san-elit.ru': 'parse_san_elit',
    '23vanni.ru': 'parse_23vanni',
    'evropa.san.ru': 'parse_evropa_san',
    'bizorg.su': 'parse_bizorg',
    'krasnodar-kray.tiu.ru': 'parse_krasnodar_kray.tiu',
    'krasnodar.stro18.ru': 'parse_krasnodar_stro18',
    # 'santehmega.com': 'parse_santehmega',
    'perm': 'parse_local',
    'www.sansit.ru': 'parse_sansit',
    'www.ar59.ru': 'parse_ar59',
    'perm.e96.ru': 'parse_perm_e96',
    'vanna59.ru': 'parse_vanna59',
    'santehopt-perm.ru': 'parse_santehopt_perm',
    '59zimaleto.ru': 'parse_59zimaleto',
    'chelyabinsk': 'parse_local',
    'www.vanna74.ru': 'parse_vanna74',
    '7770003.ru': 'parse_7770003',
    'aquapulse.su': 'parse_stroymag66',
    'bathroom74.ru': 'parse_bathroom74',
    'kabinka74.ru': 'parse_bathroom74',
    'line74.ru': 'parse_line74',
    'mirvann74.ru': 'parse_mirvann74',
    'nautilus74.ru': 'parse_nautilus74',
    'oazis74.com': 'parse_oazis74',
    'santehavrora.ru': 'parse_stroymag66',
    'santop74.ru': 'parse_stroymag66',
    'santorg74.ru': 'parse_santorg74',
    'vanna21veka.ru': 'parse_vanna21veka',
    'vanni74.ru': 'parse_vanni74',
    'www.chelyabinsk.h2oprofi.ru': 'parse_h2oprofi',
    'www.odis74.ru': 'parse_odis74',
    'www.sansay74.ru': 'parse_sansay74',
    'www.vannaiya.ru': 'parse_vannaiya',
    'yavanna.ru': 'parse_yavanna',
    'eburg': 'parse_local',
    'greenloft.satom.ru': 'parse_greenloft_satom',
    'aqua-tetis.ru': 'parse_aqua_tetis',
    'brigadirshop.ru': 'parse_brigadirshop',
    'domwc.ru': 'parse_domwc',
    'ekb.dushevoi.ru': 'parse_dushevoi',
    'e-gran.ru': 'parse_e_gran',
    'esmag.ru': 'parse_esmag',
    'greenloft.ru': 'parse_greenloft',
    'la-vanna.ru': 'parse_la_vanna',
    'shop.piastrella.info': 'parse_piastrella',
    'plitca.ru': 'parse_plitca',
    'pol-doma.com': 'parse_pol_doma',
    'qpall.ru': 'parse_qpall',
    'remont96.com': 'parse_remont96',
    'santechbomba.ru': 'parse_santechbomba',
    'santeh-magazin.ru': 'parse_santeh_magazin',
    'santehnd.ru': 'parse_santehnd',
    'santehprivoz.ru': 'parse_santehprivoz',
    'santehural.ru': 'parse_santehural',
    'smesitel96.ru': 'parse_smesitel96',
    'www.stkbegemot.ru': 'parse_stkbegemot',
    'www.stm66.ru': 'parse_stm66',
    'stroymag66.ru': 'parse_stroymag66',
    'www.trendkeramika.ru': 'parse_trendkeramika',
    'triton-ekb.ru': 'parse_triton_ekb',
    'unitazoff.net': 'parse_unitazoff',
    'uralsanteh.ru': 'parse_uralsanteh',
    'vanna-anna.ru': 'parse_vanna_anna',
    'www.vannam.ru': 'parse_vannam',
    'www.vanna-ural.ru': 'parse_vanna_ural',
    'vanny-triton.ru': 'parse_vanny_triton',
    'vernax.ru': 'parse_vernax',
    'wcshop.ru': 'parse_wcshop',
    'rostov': 'parse_local',
    '61vanna.ru': 'parse_61vanna',
    'santehmega.com': 'parse_santehmega',
    'www.santeh-online.ru': 'parse_santehonline',
    'www.santehdon.ru': 'parse_santehdon',
    'triton-store.ru': 'parse_trit_store',
    'sanone.ru': 'parse_sunone',
    'moscow': 'parse_local',
    'www.triton-vanna.ru': 'parse_triton_vanna',
    '3tn24.ru': 'parse_3tn24',
    'deltasan.ru': 'parse_deltasan',
    'global-san.ru': 'parse_global_san',
    'www.remont-vann.ru': 'parse_remont_vann',
    'spavdome.ru': 'parse_spavdome',
    'sanok.ru': 'parse_sanok',
    'aquavegas.ru': 'parse_aquavegas',
    'www.mirsanteh.ru': 'parse_mirsanteh',
    'axor.su': 'parse_axor',
    'www.gidromarket.ru': 'parse_gidromarket',
    'nnovgorod': 'parse_local',
    'mebelwelcome.ru': 'parse_mebelwelcome',
    'xn----gtbcqvbabdfx.xn--p1ai': 'parse_xn',
    'vanna52.ru': 'parse_vanna52',
    'www.novosel-52.ru': 'parse_novosel_52',
    'plitka-nnov.ru': 'parse_plitka_nnov',
    'saniterm.blizko.ru': 'parse_saniterm',
    'www.keramika52.ru': 'parse_keramika52',
    'nn.h2oprofi.ru': 'parse_h2oprofi',
    'www.nn.ru': 'parse_nn',
    'santerra-shop.ru': 'parse_santerra_shop',
    'samara': 'parse_local',
    'akvametriya.ru': 'parse_akvametriya',
    'santehnika163.ru': 'parse_santehnika163',
    'vannyplus.ru': 'parse_vannyplus',
    'vanna63.ru': 'parse_vanna63',
    'www.dushkabina63.ru': 'parse_dushkabina63',
    'stroytandem63.ru': 'parse_stroytandem63',
    'svsan.ru': 'parse_svsan',
    'www.samara.h2oprofi.ru': 'parse_h2oprofi',
    'samara.pomoshcnik.ru': 'parse_pomoshcnik',
    'sogo-shop.ru': 'parse_sogo_shop',  # msk
    'aquavil.ru': 'parse_aquavil',
    'santut.ru': 'parse_santut',
    'www.aquasanremo.ru': 'parse_aquasanremo',
    'santeh-snab.ru': 'parse_santeh_snab',
    'kiosk-santehniki.ru': 'parse_kiosk_santehniki',
    'www.spa.com.ru': 'parse_spa_com',
    'miratop.ru': 'parse_miratop',
    'mebelvan.ru': 'parse_mebelvan',
    'www.santehgorod.ru': 'parse_santehgorod',
    'globalsantehniks.ru': 'parse_globalsantehniks',
}

debug_data = []
# @staff_member_required


def parse_local(id=0):
    if id:
        product = get_model('products', 'Product')
        p = product.objects.get(id=id)
        return p.price
    else:
        return '-'


def parse_inhouse_spb(data=''):
    try:
        result = re.search('<title>.*?\s+(\d+\s\d+)\s+.*?</title>', data)
        # result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_sdvk_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        data = soup.title.get_text()
        result = soup.find('div', {'id': "item"}).find('div', {'class': "price"}).span.get_text()
        result = re.sub('[^0-9]', '', result)
        print result
        return result

        # soup = BeautifulSoup(data, 'html.parser')
        # result = soup.find('span', {'itemprop': "price"}).get_text()
        # result = re.sub('\..*', '', result)
        # result = re.sub('[^0-9]', '', result)
        # print result
        # return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_santehnika_online_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "newprice"}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_www_vivon_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'itemprop': "price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_www_remont_vann_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'itemprop': "price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_www_dlyavann_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'itemprop': "price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_santehnika_24_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'id': "item-top-price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_santehnika_tut_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "productPrice"}).span.get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_santehnika_msk_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "price"}).find('span', {'id': "priceItem"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_www_santehnica_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('meta', {'itemprop': "price"}).get('content')
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_www_aquanet_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': "price-val"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_bas21vek_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': "shk-price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        print result
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_vengoshop(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "right"}).find('span', {'class': "price-new"}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vannavdom(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "product-prices"}).find('span', {'class': "prices-current"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_ukroy_spb(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "item_price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_ovanna(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': "catalog-price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_812vann(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "price-current"}).strong.get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_4komnaty_nsk(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "item_current_price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santsib(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "info-box"}).find('div', {'class': "price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_sibvanna(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "item_other_price"}).find('span', {'class': "price_price"}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_meduzza(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': "product-price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_algoritmremonta(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('strong', {'class': "handler-price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_sardana24(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': "block_price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_xn__80aaaag4cofadj5cxk(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'itemprop': "price"}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_kvadrat_nsk(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "description"}).get_text()
        result = re.search('.*?(\d+\s\d+).*?', result)
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vodolei54(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': "block_price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_nsk_santeh(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': "price-default"}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_54_vlcueknc(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'id': "price_element_tover"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_dvaproraba(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'itemprop': "price"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_piramida_nsk(data=''):
    try:
        result = re.search('price: (\d+),', data)
        # result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_skladremonta(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'price_block'}).div.span.get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_triton54(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': 'pricee'}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_kwadratura(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': 'product__priceRegular'}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vachsosed(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'start-price'}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_akvasib(data=''):
    try:
        result = re.search('class="gr2">.*?<h4>(\d+\s\d+).*?</h4>', data)
        # result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_sksanta(data=''):
    try:
        result = re.search('<title>.*?(\d+\s\d+).*?</title>', data)
        # result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_djakudza(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': 'price_m'}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_triton_sfo(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('p', {'class': 'big-price'}).span.get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_xn__54_6kcaj1bll6ck6i(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('i', {'class': 'bp-price'}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_webdush(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'box_item_info'}).find('p', {'class': 'price'}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santechnika_spb(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'product-price'}).find('span', {'class': 'price-actual'}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_aquadomspb(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'text-holder'}).find('div', {'class': 'price-holder'}).strong.get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_gidrobum(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('p', {'class': 'price'}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehsalonspb(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'product-details'}).find('p', {'class': 'price'}).strong.get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_pitermart(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': 'productPrice'}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_viavanna(data=''):
    try:
        result = re.search('<span id="formated_price" price=".*?">(.*?)</span>', data)
        # result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_nychshop(data=''):
    try:
        result = re.search('<span class="base-price">(.*?)</span>', data)
        result = re.sub('\..*', '', result.group(1))
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehmaster(data=''):
    try:
        result = re.search('<p class="good-card-buy-price">(.*?)<span', data)
        # result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_rushop01(data=''):
    try:
        result = re.search('padding: 10px;" >(.*?)</p>', data)
        # result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_masterovoy_spb(data=''):
    try:
        result = re.search('<span class="master_price"> <span style="float:left;">(.*?)</span>', data)
        result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_hitkabina(data=''):
    try:
        result = re.search('<span class="bk_price">(.*?)</span>', data)
        result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehvann(data=''):
    try:
        # result = re.search("<div class='price'>(.*?)</div>", data)
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('meta', {'itemprop': 'price'}).get('content')
        # result = re.sub(',.*', '', result.group(1))
        # result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_comfort_montage(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'price_cntr'}).find('span', {'class': 'catalog-price'}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_aquacera(data=''):
    try:
        result = re.search('([\d\s]+).*?</span></p>', data)
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_sanspb(data=''):
    try:
        result = re.search("</s><br /><b style='color: #DE0006'>(\d+),\d+ руб</b></td>", data)
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santeh_allegro(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'card-product_gr-box_summ-new'}).strong.get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_akvaformat(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': 'green_price'}).get_text()
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_aquanta_spb(data=''):
    try:
        result = re.search('<meta itemprop="price" content="(.*?)" />', data)
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vodopad_spb(data=''):
    try:
        result = re.search('<span itemprop="price">(\d+)</span>', data)
        result = re.sub('[^0-9]', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_san_sanych(data=''):
    try:
        result = re.search("'DISCOUNT_VALUE':'(\d+).*?'", data)
        return result.group(1)
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehmartspb(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': 'formated_price'}).get('price')
        result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_mks_market(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'id': 'element_info_p'}).span.get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_stroytorg97(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        # result = soup.find('td', {'class': 'centerline'}).find('td', {'class': 'tableline'}).strong.get_text()
        result = soup.find('span', {'style': "background-color: rgb(255,255,0); color: rgb(255,0,0)"}).strong.get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_webdush(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('p', {'class': 'price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_samtexnik(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'red-price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santex_spb(data=''):
    try:
        result = re.search("'DISCOUNT_VALUE':'(\d+).*?'", data)
        # result = re.sub('\..*', '', result.group(1))
        return result.group(1)
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_atst_spb(data=''):
    try:
        result = re.search('<span data-price="(\d+)\.\d+" class="price nowrap">', data)
        result = re.sub('\..*', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_aquaink(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('li', {'class': 'goodsDataMainModificationPriceNow'}). \
            find('span', {'itemprop': "price"}).get('content')
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_stroytorg812(data=''):
    try:
        result = re.search('<div class="b">(.*?)</div>', data)
        result = re.sub(',.*', '', result.group(1))
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_bt_deluxe(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': 'currentProductPrice'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehland(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        # result = soup.find('span', {'class': 'main_price'}).find('span', {'class': 'figure'}).get_text()
        result = soup.find('span', {'class': 'superbig'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santideal(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'item_current_price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_san_elit(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'price'}).span.get_text()
        result = re.sub('\..*', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_23vanni(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', id=re.compile(r"main_price_[0-9]*")).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehopt_perm(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('section', {'class': 'item-main'}).find('div', {'class': 'price'}).strong.get_text()
        result = re.sub('\.\d\d', '', result)
        result = re.sub('[^0-9]', '', result)
        print result
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vanna59(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', id=re.compile(r"main_price_[0-9]*")).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_perm_e96(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "basketinfo"}).find('meta', {'itemprop': "price"}).get('content')
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_sansit(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': "main_price"}).get_text()
        # result = re.sub(',\d\d', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_ar59(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "price-holder"}).find('b', {'class': "basic"}).get_text()
        # result = re.sub(',\d\d', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vannyplus(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': "b-sticky-panel__price"}).get_text()
        result = re.sub(',\d\d', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehnika163(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "b-one-click-order_loc_product"}).get('data-price-in-default-currency')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_akvametriya(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "prod_price"}).span.get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_greenloft_satom(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('meta', itemprop="price").get('content')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_yavanna(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', id=re.compile(r"main_price_[0-9]*")).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vannaiya(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'cpt_maincontent'}).form.find('input', {'class': 'product_price'}).get(
            'value')
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_sansay74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('form', {'class': 'shop-form'}).find('table', {'class': 'shop-form-param'}).tr.find('span', {
            'class': 'shop_price_value'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_odis74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('td', {'id': 'optionPrice'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vanni74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'product-essential'}).find('div', {'class': 'price-box'}).find('span', {
            'class': 'price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vanna21veka(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'id': 'total_price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santorg74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'product_total_price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_oazis74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': 'price_span'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_nautilus74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': 'cost-by-impact'}).find('span', {'class': 'product-price-data'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_mirvann74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'start-price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_line74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'left-right-column-single'}).div.div.find('span').get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_bathroom74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': 'PricesalesPrice'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vanna74(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('p', {'class': 'price'}).span.get_text()
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_7770003(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('table', {'class': 'buy'}).find('div', {'class': 'club'}).get_text()
        result = re.sub(',.*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_remont96(data=''):
    try:
        result = re.search('<div class=price.*?><b>(\d+).*?</b></div>', data)
        return result.group(1)
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_globalsantehniks(data=''):
    try:
        result = re.search('<input id="product_price" type="hidden" value="(.*?) .*?.">', data)
        return result.group(1)
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehgorod(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', {'id': 'base_price'}).get('value')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_mebelvan(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('p', {'class': 'b-product__price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_miratop(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('meta', {'itemprop': 'price'}).get('content')
        result = re.sub('\..*', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_spa_com(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', {'name': 'price'}).get('value')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santeh_snab(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('meta', {'itemprop': 'price'}).get('content')
        result = re.sub(',.*', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_kiosk_santehniki(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'b-one-click-order_loc_product'}).get('data-price-in-default-currency')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_aquasanremo(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', {'id': 'sprice'}).get('value')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santut(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': 'formated_price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_aquavil(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'id': 'alloptsum'}).get_text()
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_sogo_shop(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'product-price'}).find('span', {'class', 'PricesalesPrice'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_qpall(data=''):
    try:
        result = re.search('<span class="price">(.*?)</span>.*?</span>', data)
        result = re.sub(',00', '', result.group(1))
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_stkbegemot(data='', url=''):
    try:
        to_find = re.sub('http://www\.stkbegemot\.ru', '.', url)
        # print to_find
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('a', {'href': to_find}).parent.find('td', {'class', 'price'}).strong.get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_piastrella(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'active_mod'}).find('div', {'class': 'modprice'}).div.get_text()
        # result = re.sub('\.0', '', result)
        # result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_triton_ekb(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('td', {'id': 'optionPrice'}).get_text()
        result = re.sub('\.0', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vanna_ural(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': 'shk-price'}).get_text()
        price = re.sub('[^0-9]', '', result)
        price = int(price)
        if price > 60000:
            discount = 7
        elif price > 40000:
            discount = 5
        elif price > 20000:
            discount = 3
        elif price > 10000:
            discount = 1
        elif price > 0:
            discount = 0
        price = int(price) - int(price) * int(discount) / 100
        return price
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_wcshop(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'price'}).find('div', {'class': 'total'}).strong.get_text()
        # result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vernax(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'itemPrice'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vanny_triton(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', {'id': 'baseprice'}).get('value')
        # result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vannam(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('p', {'class': 'price_card'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vanna_anna(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_uralsanteh(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'price'}).span.get_text()
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_stroymag66(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'product-price'}).find('div', {'class': 'price-current'}).strong.get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_trendkeramika(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('table', {'class': 'cat_ass'}).find('div', {'class': 'plus'}).span.strong.get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_stm66(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'homeprod'}).table.find('span', {
            'style': 'color: #0b5073; font-size: 23px;'}).get_text()
        result = re.sub('\.00', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_smesitel96(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'itemBuyTop'}).find('span', {'class': 'price'}).span.get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehural(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', {'class': 'product_price'}).get('value')
        # result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehprivoz(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', {'class': 'product_price'}).get('value')
        # result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santehnd(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'uss_shop_price'}).find('em', {'class': 'price_class'}).get(
            'data-clear-price')
        # result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santeh_magazin(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'price'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_santechbomba(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', {'name': 'price_now'}).get('value')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_pol_doma(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', id="pricePcs").get('value')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_plitca(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', class_="price").get_text()
        result = re.sub('\.00', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_la_vanna(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', class_="box-product").find('div', class_="price").get_text()
        result = re.sub('[^0-9]', '', result)
        # print result.group()
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_greenloft(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', class_="prodzena").get_text()
        result = re.search('[0-9]*', result)
        return result.group()
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_esmag(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('meta', itemprop="price").get('content')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_e_gran(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('input', id="cost").get('value')
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_dushevoi(data=''):
    try:
        # result = re.search('price = (.*?) ', data)
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.title.get_text()
        result = re.search('[\d]{3,}', result)
        # result = re.sub('[^\d]+.*?', '', result.group())
        return result.group()

    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_domwc(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', class_="card-block_total").find('span', class_="js-cartTempCost").get_text()
        result = re.search('[0-9\s]*', result)
        result = re.sub('\s', '', result.group())
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_brigadirshop(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', class_="pricediv1").font.get_text()
        result = re.search('[0-9]*', result)
        return result.group()
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_aqua_tetis(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find(id="full-price").get_text()
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_mebelwelcome(data=''):
    # 14 479 <span class="rubl">Р</span>.    </span>
    result = re.search("<span class=\"price\" id=\"ys_top_price\">.*?([\d\s]+)", data, re.M)
    # result = re.search("</strong> <span class=\"PriceB\">(.*?)</span><br />", data)
    try:
        result = re.sub('[\s+\n]', '', result.group(1), re.MULTILINE)
        return result
    except:
        try:
            soup = BeautifulSoup(data, 'html.parser')
            result = soup.find(class_="catalog-list-light").find(class_='price')
            result = re.sub('\n', '', result.get_text())
            result = re.sub('\s', '', result)
            result = re.search('[0-9]*', result)
            return result.group()
        except Exception, e:
            print Exception, e, 'fuck'
            pass
        return '-'


def parse_h2oprofi(data=''):
    # 14 479 <span class="rubl">Р</span>.    </span>
    try:
        soup = BeautifulSoup(data, 'html.parser')
        # result = soup.find(class_="b-product").find(class_='b-product__content').find(class_='b-product__price')
        result = soup.find('div', {'class': 'b-product_full'}).find('div', {'class': 'b-product__content'}).find('p', {'class': 'b-product__price'}).get_text()
        result = re.sub(',.*', '', result)
        # result = re.sub('\n', '', result.get_text())
        # result = re.sub('\s', '', result)
        # result = re.sub('[^\d,]+', '', result)
        # result = re.search('[0-9]*', result)
        result = re.sub('[^0-9]', '', result)
        print result
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'


def parse_vanna52(data=''):
    result = re.search('<p class="price"><span id="main_price_\d+">([0-9]+)</span>', data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_svsan(data=''):
    soup = BeautifulSoup(data, 'html.parser')
    try:
        # result = soup.find(class_="info-action").find(class_='price').get_text()
        result = soup.title.get_text()
        result = re.search('\d+[^-\d]\d+', result)
        result = re.sub('[^\d,]+', '', result.group())
        return result
    except Exception, e:
        print e
        return '-'


def parse_pomoshcnik(data=''):
    soup = BeautifulSoup(data, 'html.parser')
    try:
        # debug_data.append('---<br/>')
        # debug_data.append(data)
        # result = soup.find(class_="product-options-bottom-gggggg").find(itemprop='price').get_text()
        result = soup.find(class_="product-options-bottom-gggggg").find('span', class_='price').span.get_text()
        # debug_data.append(result)
        # result = re.sub('&nbsp;', '', result)
        result = re.sub('[^\d,]+', '', result)
        result = re.search('[0-9]*', result)
        return result.group()
    except:
        return '-'


def parse_vanna63(data=''):
    result = re.search('<p class="price">([0-9]+) руб &nbsp;', data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_dushkabina63(data=''):
    result = re.search('<div class="item_current_price" id=".*?">([\d\s]+) руб.</div>', data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_plitka_nnov(data=''):
    result = re.search('<a href="#" id="tab1"><span>([\d\s]+)р.</span>', data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_saniterm(data=''):
    try:
        # soup = BeautifulSoup(data, 'html.parser')
        # result = soup.find('div', {'class': 'pp-short'}).find('div', {'class': 'bp-price-cover'}).get_text()
        result = re.search('itemprop="price">(.*?)</i>', data)
        result = re.sub('[^0-9]', '', result.group(1))
        # return result
        # result = re.sub(' ', '', result.group(1))
        return result
    except Exception, e:
        print Exception, e
        return '-'


def parse_santerra_shop(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find(class_="general_info").find(class_="price").get_text()
        result = re.sub('[^\d]+', '', result)
        result = re.search('[0-9]*', result)
        return result.group()
    except:
        return '-'


def parse_nn(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find(class_="price-avg").get_text()
        result = re.search('[0-9]*', result)
        return result.group()
    except:
        return '-'


def parse_stroytandem63(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('span', {'class': 'jbcurrency-value'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
    return '-'


def parse_keramika52(data=''):
    result = re.search('<span class=\'black24bold\'>([\d\s]+)</span> ', data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_xn(data=''):
    result = re.search('<input type="hidden" value="([0-9]+).00" id="startsumm">', data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_novosel_52(data=''):
    result = re.search('<div class="pr" itemprop="price" content="([0-9]+)">', data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_sanok(data=''):
    # print data
    result = re.search("<span class=\"PriceB\">(\d+)\s.*?</span></div>", data)
    # result = re.search("</strong> <span class=\"PriceB\">(.*?)</span><br />", data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_axor(data=''):
    result = re.search("<div class=\"cost\">([\d\s]+) \.-</div>", data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_mirsanteh(data=''):
    # result = re.search("<div><span style=\"font-size:18px;color:#80c908;\">([\d\s]+) руб.</span>", data)
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': 'price'}).find('span', {'class': 'number'}).get_text()
        result = re.sub('[^0-9]', '', result)
        return result
        # result = re.sub(' ', '', result.group(1))
        # return result
    except Exception, e:
        print Exception, e
        return '-'


def parse_aquavegas(data=''):
    result = re.search("<input class=\"product_price\" value=\"(\d+)\" type=\"hidden\" >", data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_gidromarket(data=''):
    result = re.search("to-basket\" data-price=\"(.*?)\"", data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_spavdome(data=''):
    result = re.search("class=\"price nowrap\">([\d\s]+) <span class=\"ruble\">", data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_deltasan(data=''):
    result = re.search("<p class=\"big_price\">([\d\s]+) <sup>руб</sup></p>", data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_global_san(data=''):
    result = re.search("<div class=\"item_current_price\".*?>([\d\s]+) руб.</div>", data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_3tn24(data=''):
    result = re.search("<strong class=\"h3\">(\d+)</strong>", data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_remont_vann(data=''):
    soup = BeautifulSoup(data, 'html.parser')
    result = soup.find('span', itemprop='price').get_text()
    # result = re.sub('\n', '', result.get_text())
    # result = re.search("<strong itemprop=\"price\">([\d\s]+)</strong>", data)
    try:
        result = re.sub(' ', '', result)
        return result
    except:
        return '-'


def parse_triton_vanna(data=''):
    result = re.search("class=\"currentProductPrice\">(\d+)</span>", data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_61vanna(data=''):
    result = re.search('<span id="main_price_\d+">(.*?)</span>', data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_santehmega(data=''):
    # result = re.search('<span class="num">(.*?)</span>', data)
    try:
        # result = re.sub(' ', '', result.group(1))
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('meta', itemprop="price").get('content')
        result = round(float(result), 0)
        result = int(result)
        return result
    except Exception, e:
        print Exception, e
        return '-'


def parse_santehonline(data=''):
    result = re.search('<span class="price">(.*?)\s.*?</span>', data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_santehdon(data=''):
    result = re.search('<span style=\'color: #B2080A\'>(\d+).*?</span>', data)
    try:
        return result.group(1)
    except:
        return '-'


def parse_trit_store(data=''):
    result = re.search("<span class=\"productPrice\">\n.*?\s+([\d\s]+).*?</span>", data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'


def parse_sunone(data=''):
    result = re.search("<span class=\"price\" style=\"color:#007CC3;\">\n\s+([\d\s]+).*?</span>", data)
    try:
        result = re.sub(' ', '', result.group(1))
        return result
    except:
        return '-'

def parse_sanexpert24_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "dproductprice"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_svton_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "dproductprice"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_triton_plus_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "dproductprice"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

def parse_triton_san_ru(data=''):
    try:
        soup = BeautifulSoup(data, 'html.parser')
        result = soup.find('div', {'class': "dproductprice"}).get_text()
        # result = re.sub('\..*', '', result)
        result = re.sub('[^0-9]', '', result)
        return result
    except Exception, e:
        print Exception, e, 'fuck'
        pass
    return '-'

@permission_required('products.view_parse')
def parse(request):
    # Get working region

    region = request.REQUEST.get('region', None)
    site = request.REQUEST.get('site', None)
    not_save = request.REQUEST.get('not_save', None)
    debug = request.REQUEST.get('debug', None)
    date = request.REQUEST.get('date', None)
    action = request.REQUEST.get('action', None)
    getdata = request.REQUEST.get('getdata', None)
    actuate_sites = request.POST.getlist('sites[]', None)

    selected_sites = request.POST.get('selected_sites', None)
    from_date = request.REQUEST.get('from_date', None)
    to_date = request.REQUEST.get('to_date', None)

    styles = '<style type="text/css">table {border-collapse: collapse} td, th {border: 1px solid grey; padding: ' \
             '5px; margin: 0px; text-align: center;}  a {color: black}</style>'

    data = ''

    if not region:
        data = u'<h2>Выбор региона:</h2>'
        for region in regions:
            data = u'%s%s<br/><a href="/parse?region=%s"><img align="middle" width="25" src="/media/images/kalendar.jpg" ' \
                   u'title="по датам"/></a><a href="/parse?region=%s&action=actual"><img align="middle" width="25" ' \
                   u'src="/media/images/sekundomer.jpg" title="на текущий момент, с выбором сайтов"/></a> ' \
                   u'<a href="/parse?region=%s&action=period"> Период </a><br/><br/>' \
                   % (data, regions_rus[region], region, region, region)

        return HttpResponse(data)

    # Check if it in list
    if region not in regions:
        return HttpResponse(u'Неверно указан регион')

    if region and not date and not getdata and not action:
        # Get all dates from DB
        dates = Prices.objects.filter(region=region).order_by('-date').values('date').distinct()
        data = '<h2>Регион: %s</h2>' % regions_rus[region]
        if dates:
            for date in dates:
                data = '%s<a href="/parse?region=%s&date=%s">%s<br/>' % (
                    data, region, date['date'].strftime("%Y-%m-%d"), date['date'].strftime("%Y-%m-%d"))
                # data = '%s<a href="/parse?region=%s&getdata=True">Parse now<br/>' % (data, region)
        else:
            # data = 'No dates for this region.<br/><a href="/parse?region=%s&getdata=true">Parse now</a>' % (region)
            data = u'Нет данных для региона %s' % region
        return HttpResponse(data)

    # Go if actual request got
    if action and region:
        if action == 'actual':
            not_save = True
            if not actuate_sites:
                # if actuate_sites:
                # for site in actuate_sites:
                #         print site

                # Make a list of sites for region
                data = '<h2>Регион: %s</h2><form method="post" name="actual-sites">' % (regions_rus[region])
                for site in sites[region]:
                    if site == region:
                        data = '%s<input type="checkbox" name="sites[]" value="%s" checked=checked/>%s3tn.ru<br/>' % (
                            data, site, site)
                    else:
                        data = '%s<input type="checkbox" name="sites[]" value="%s"/>%s<br/>' % (data, site, site)
                data = '%s<input type="hidden" name="region" value="%s"/>' % (data, region)
                data = '%s<input type="hidden" name="action" value="%s"/>' % (data, action)
                data = u'%s<br/><br/><input type="submit" name="submit" value="Запустить анализ"/>' % (data)
                data = '%s</form>' % data
                return HttpResponse(data)
        if action == 'period':
            list_of_sites = ()

            if region and (not selected_sites or not from_date or not to_date):
                list_of_sites = Prices.objects.filter(region=region).order_by().values_list('site', 'site').distinct()
                form = PeriodForm(initial={'region': region, 'selected_sites': selected_sites,
                                           'from_date': from_date, 'to_date': to_date}, list_of_sites=list_of_sites,
                                  request=request)
                return_data = render_to_string('triton_ua/price_parse/period_form.html', {
                    'form': form,
                    'from_date': from_date,
                    'to_date': to_date,
                    'regions_rus': regions_rus[region],
                    'list_of_sites': list_of_sites,
                }, context_instance=RequestContext(request))
                return HttpResponse(return_data, content_type='text/html; charset=utf-8')

            if selected_sites and from_date and to_date:
                data_array = Prices.objects.filter(date__range=(from_date, to_date),
                                                   region=region, site__in=[region, selected_sites]). \
                    values_list('nid', 'date', 'site', 'price').distinct()
                return_data = render_to_string('triton_ua/price_parse/period.html', {
                    'sites': sites,
                    'region': region,
                    'regions': regions,
                    'regions_rus': regions_rus[region],
                    'data_array': data_array,
                    # 'actuate_sites': actuate_sites,
                    'selected_sites': selected_sites,
                    'from_date': from_date,
                    'to_date': to_date,
                    'action': action,
                }, context_instance=RequestContext(request))
                return HttpResponse(return_data, content_type='text/html; charset=utf-8')

    # If date selected try get data from DB
    if date:
        data = Prices.objects.filter(date=date, region=region)
        if not data:
            data = u'Нет данных на дату %s для региона %s' % (date, regions[region])
        else:
            ids = Prices.objects.filter(date=date, region=region).order_by().values_list('nid', flat='True').distinct()
            item_names = {}
            data_array = {}

            product = get_model('products', 'Product')
            for product_id in ids:
                item_names[product_id] = product.objects.filter(id=product_id).values_list('title', flat='True')[0]
                data_array[item_names[product_id]] = {}
            for record in data:
                name = item_names[record.nid]
                try:
                    data_array[name][region]
                except:
                    data_array[name].update({region: {}})

                data_array[name][region].update({record.site: {'price': record.price, 'url': record.url}})

            return_data = render_to_string('triton_ua/price_parse/table.html', {
                'sites': sites,
                'region': region,
                'regions': regions,
                'regions_rus': regions_rus,
                'data_array': data_array,
                'actuate_sites': actuate_sites,
                'date': date,
                'action': action,
            }, context_instance=RequestContext(request))

            return HttpResponse(return_data, content_type='text/html; charset=utf-8')

    # Go through regular straight parsing without save
    parse_file_name = os.path.join(settings.MEDIA_ROOT, 'parse_data', '%s.csv' % (region))
    handle = open(parse_file_name, 'r')
    file_data = handle.read()
    handle.close()
    lines = file_data.split("\n")

    data_array = {}
    for line in lines:
        cells = line.split(';')
        item = cells[0].strip(' \n\t')
        for i in range(len(cells)):
            if not i:
                if item:
                    data_array[item] = ''
                continue
            else:
                # Check if it local price
                if re.match('^\d+$', cells[i]):
                    cells[i] = re.sub("\r", '', cells[i])
                    data_array[item] = {region: {region: {'price': '-', 'url': cells[i].strip('"')}}}
                    continue
                # Get site url and save it to data_array
                result = re.search('http[s]?://(.*?)/', cells[i])
                if not result:
                    continue
                else:
                    # print 'no result'
                    # print result
                    site = result.group(1)
                    cells[i] = re.sub("\r", '', cells[i])
                    data_array[item][region][site] = {'price': '-', 'url': cells[i].strip('"')}

    new_items = data_array

    # Iterate prods to get url for parse

    for item in new_items:
        for key, value in new_items[item][region].iteritems():
            try:
                if key not in actuate_sites:
                    continue
                func = eval(site_methods[key])
                # Check if it own site
                if key == region:
                    price = func(value['url'])
                else:
                    # if not key == 'stroytandem63.ru':
                    # continue
                    if key == 'www.stkbegemot.ru':
                        url = re.search('(.*)/.*?', value['url'])
                        data = get_page(url.group())
                        price = func(data, value['url'])
                    else:
                        data = get_page(value['url'])
                        price = func(data)
                # Save price to common data array
                new_items[item][region][key]['price'] = price
            except Exception, e:
                print Exception, e
                value['price'] = '-'

    # Save prices to database
    if not not_save:
        save_prices(new_items, region)

    return_data = render_to_string('triton_ua/price_parse/table.html', {
        'sites': sites,
        'region': region,
        'regions': regions,
        'regions_rus': regions_rus,
        'data_array': data_array,
        'actuate_sites': actuate_sites,
        'date': date,
        'action': action,

    }, context_instance=RequestContext(request))

    return HttpResponse(return_data, content_type='text/html; charset=utf-8')


def var_dump(var, prefix=''):
    """
    You know you're a php developer when the first thing you ask for
    when learning a new language is 'Where's var_dump?????'
    """
    data = ''
    sep = ''
    my_type = '[' + var.__class__.__name__ + '(' + str(len(var)) + ')]:'
    data = '%s%s%s\n' % (prefix, my_type, sep)
    prefix += '    '
    for i in var:
        if type(i) in (list, tuple, dict, set):
            var_dump(i, prefix)
        else:
            if isinstance(var, dict):
                data = '%s%s%s%s%s%s%s%s\n' % (data, prefix, i, ': (', var[i].__class__.__name__, ') ', var[i], sep)
            else:
                data = '%s%s%s%s%s%s%s\n' % (data, prefix, '(', i.__class__.__name__, ') ', i, sep)
    return data


def parse_cron():
    regions.extend(regions_s)
    # regions = regions_s
    for region in regions:
        print 'Parse %s' % region
        parse_file_name = os.path.join(settings.MEDIA_ROOT, 'parse_data', '%s.csv' % (region))
        handle = open(parse_file_name, 'r')
        file_data = handle.read()
        handle.close()
        lines = file_data.split("\n")

        data_array = {}
        for line in lines:
            cells = line.split(';')
            item = cells[0].strip(' \n\t')
            for i in range(len(cells)):
                if not i:
                    if item:
                        data_array[item] = ''
                    continue
                else:
                    # Check if it local price
                    if re.match('^\d+$', cells[i]):
                        data_array[item] = {region: {region: {'price': '-', 'url': cells[i].strip('"')}}}
                        continue
                    # Get site url and save it to data_array
                    result = re.search('http[s]?://(.*?)/', cells[i])
                    if not result:
                        continue
                    else:
                        site = result.group(1)
                        # data_array[item][region][site] = {'price': '-', 'url': cells[i].strip('"')}
                        data_array[item][region][site] = {'price': '-', 'url': re.sub('"', '', cells[i])}

        new_items = data_array
        # Iterate prods to get url for parse

        for item in new_items:
            for key, value in new_items[item][region].iteritems():
                try:
                    if key not in site_methods:
                        value['url'] = re.sub('"', '', value['url'])
                        if value['url'][:4] == 'http':
                            price = parse_page(value['url'])
                        elif re.match('\d+', value['url']) is not None:
                            price = parse_local(value['url'])
                        print "url, price", value['url'], price
                    else:
                        func = eval(site_methods[key])

                    # Check if it own site
                    # print value['url']
                        if key == region:
                            price = func(value['url'])
                        else:
                            # if not key == 'aqua-tetis.ru':
                            # continue
                            if key == 'www.stkbegemot.ru':
                                url = re.search('(.*)/.*?', value['url'])
                                data = get_page(url.group())
                                price = func(data, value['url'])
                            else:
                                data = get_page(value['url'])
                                price = func(data)
                                # if key == 'greenloft.satom.ru':
                                #     _log_data(data, new_items[item][region][region]['url'])

                    # Save price to common data array
                    new_items[item][region][key]['price'] = price
                except:
                    value['price'] = '-'

        # Save prices to database
        save_prices(new_items, region)

    print 'Parsing done'


def update_prices(new_items, region):
    for item in new_items:
        for site in sites[region]:
            try:
                price = new_items[item][region][site]['price']
                if not int(price):
                    continue
                url = new_items[item][region][site]['url']
                nid = new_items[item][region][region]['url']
                date = datetime.now()
                record = Prices.objects.filter(site=site, region=region, url=url, date=date)
                if record:
                    record.update(price=price)
                else:
                    record = Prices(url=url, price=price, site=site, nid=nid, date=date, region=region)
                    record.save()
            except:
                continue
    return

def save_prices(new_items, region):
    for item in new_items:
        for site in sites[region]:
            try:
                price = new_items[item][region][site]['price']
                url = new_items[item][region][site]['url']
                nid = new_items[item][region][region]['url']
                date = datetime.now()
                record = Prices(url=url, price=price, site=site, nid=nid, date=date, region=region)
                record.save()
            except:
                continue
    return


def save_analogs(data_array):
    for j, item in enumerate(data_array):
        for i, url_item in enumerate(data_array[j]['urls']):
            try:
                group = data_array[j]['group']
                name = data_array[j]['name']
                price = data_array[j]['urls'][i]['price']
                url = data_array[j]['urls'][i]['url']
                date = datetime.now()
                order = i
                if not url:
                    continue
                try:
                    host = re.search('http[s]?:\/\/(.*?)\/', url).group(1)
                except Exception, e:
                    host = ''

                record = Analogs.objects.filter(url=url, date=date)
                if record:
                    record.update(price=price)
                else:
                    record = Analogs(url=url, price=price, group=group, host=host, date=date, name=name, order=order)
                    record.save()
            except Exception, e:
                print Exception, e

    return


def get_page(url=''):
    url = re.sub("\r", '', url)
    if url:
        try:
            headers = {'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'}
            print 'Get page %s' % url
            try:
                cookies = cookielib.CookieJar()
                opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies))
                req = urllib2.Request(url, headers=headers)
                # response = urllib2.urlopen(req)
                response = opener.open(req)
                html = response.read()

                if re.search('globalsantehniks', url):
                    req.add_header("Cookie", "beget=begetok" + ";")

                response = opener.open(req)
                html = response.read()
            # except urllib2.HTTPError, error:
            except Exception, e:
                print Exception, e
                # contents = error.read()
                # debug_data.append(contents)
            # _log_data(html, 'html')
            return html
        except Exception, e:
            print 'Get page error', Exception, e
            pass
    return ''


def _log_data(data, item):
    now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    file_name = os.path.join(settings.MEDIA_ROOT, 'parse_logs', '%s-%s' % (now, item))
    handle = open(file_name, 'w')
    handle.write(str(data))
    handle.close()
    return


def _save_json(file_name, data):
    data = json.dumps(data, indent=4, separators=(',', ': '))
    file_name = os.path.join(settings.MEDIA_ROOT, 'parse_son', '%s' % file_name)
    handle = open(file_name, 'w')
    handle.write(str(data))
    handle.close()
    return


def _json_exist(file_name):
    file_name = os.path.join(settings.MEDIA_ROOT, 'parse_son', '%s' % file_name)
    if os.path.exists(file_name):
        return True
    return False


def parse_son(url, host):
    # urls = {'http://santehnika-online.ru/product/akrilovaya_vanna_aquanet_nord_140_sm/',
    # 'http://santehnika-online.ru/product/unitaz_kompakt_ifo_frisk_21030000/',
    #         'http://santehnika-online.ru/product/smesitel_zenta_celeste_z0306_r_dlya_rakoviny/'}
    parsed_items = {}
    urls = {url}
    for url in urls:
        json_file_name = "%s.json" % re.sub('[\W]+', '_', url)
        if _json_exist(json_file_name):
            print 'File exist: ', json_file_name
            continue
        parsed_item = {}
        data = get_page("%s%s" % (host, url))
        soup = BeautifulSoup(data.decode('windows-1251', 'ignore'), 'html.parser')
        img_folder_path = os.path.join(settings.MEDIA_ROOT, 'parse_son', 'img')
        # groops
        navi = soup.find('div', {'id': 'breadcrumb-navigation'}).ul.find_all('li')
        dirs = []
        dirs_rus = []
        for nav in navi:
            try:
                nav_name = nav.span.a.span.get_text()
                dirs_rus.append(nav_name)
                # print nav_name
                try:
                    lang = detect_language(nav_name)
                    pass
                except Exception, e:
                    print Exception, e
                try:
                    nav_name_translit = translit(nav_name, reversed=True)
                    # nav_name_translit = translit(unicode(nav_name, "utf-8"), reversed=True)
                    # nav_name_translit = translit(nav_name.encode("utf-8"), reversed=True)
                    nav_name_translit = re.sub('[\W]+', '_', nav_name_translit)
                except Exception, e:
                    print Exception, e
                    try:
                        nav_name_translit = translit(nav_name.decode('utf8'), reversed=True)
                        nav_name_translit = re.sub('[\W]+', '_', nav_name_translit)
                    except Exception, e:
                        print Exception, e

                # print nav_name_translit
                dirs.append(nav_name_translit)
                # print dirs
                directory = os.path.join(img_folder_path, *dirs)
                print directory
                if not os.path.exists(directory):
                    os.makedirs(directory)
            except Exception, e:
                print Exception, e
                continue
        if dirs_rus:
            parsed_item['groups'] = dirs_rus

        # h1
        h1 = soup.h1.get_text()
        if h1:
            parsed_item['name'] = h1

        # price
        price = soup.find('meta', {'itemprop': 'price'}).get('content')
        price = re.sub('\..*', '', price)
        price = re.sub('[^0-9]', '', price)
        if price:
            parsed_item['sol_price'] = price

        # meta
        meta_keywords = soup.find('meta', {'name': 'keywords'}).get('content')
        if meta_keywords:
            parsed_item['meta_keywords'] = meta_keywords
        meta_description = soup.find('meta', {'name': 'description'}).get('content')
        if meta_description:
            parsed_item['meta_description'] = meta_description

        # check complect
        try:
            complect = soup.find('div', {'class', 'wrap_complects_box'})

            # find static items
            try:
                fixed_complect_items = complect.find('div', {'class', 'complgreen'}).find_all('a',
                                                                                              {'class': 'cmplopislink'})
                # print 'fixed_complect_items:'
                fixed_complect_items_artikuls = []
                for fixed_complect_item in fixed_complect_items:
                    try:
                        fixed_complect_item_url = "%s" % (fixed_complect_item.get('href'))
                        # print fixed_complect_item_url
                        fixed_complect_item_data = parse_son(fixed_complect_item_url, host)
                        fixed_complect_item_data_url, fixed_complect_item_data_value = fixed_complect_item_data.items()[
                            0]
                        fixed_complect_items_artikuls.append(fixed_complect_item_data_value['Artikul'])
                    except Exception, e:
                        print Exception, e
                parsed_item['fixed_complect_items_artikuls'] = fixed_complect_items_artikuls

            except Exception, e:
                print Exception, e
                pass

            # find optional items
            try:
                optional_complect_items = complect.find('div', {'class', 'complgreenchek'}).find_all('div', {
                    'class': 'minisize'})
                # print 'optional_complect_items:'
                optional_complect_items_list = {}
                for optional_complect_item in optional_complect_items:
                    optional_complect_items_list_artikuls = []
                    optional_complect_item_name = optional_complect_item.find('span', {'class': 'name'}).get_text()
                    # print optional_complect_item_name
                    optional_complect_item_selects = optional_complect_item.find_all('a', {'class': 'cmplopislink'})
                    for optional_complect_item_select in optional_complect_item_selects:
                        try:
                            optional_complect_item_select_url = "%s" % (optional_complect_item_select.get('href'))
                            # print optional_complect_item_select_url
                            optional_complect_item_data = parse_son(optional_complect_item_select_url, host)
                            optional_complect_item_data_url, optional_complect_item_data_value = \
                                optional_complect_item_data.items()[0]
                            optional_complect_items_list_artikuls.append(optional_complect_item_data_value['Artikul'])
                        except Exception, e:
                            print Exception, e
                    # optional_complect_items_list[translit(optional_complect_item_name, reversed=True)] = optional_complect_items_list_artikuls
                    optional_complect_items_list[optional_complect_item_name] = optional_complect_items_list_artikuls
                parsed_item['optional_complect_items_list'] = optional_complect_items_list
            except Exception, e:
                print Exception, e
                pass

        except Exception, e:
            print Exception, e
        # complectation
        compl_list = []
        try:
            compl = soup.find('div', {'class': 'complpost_main'}).ul.find_all('li')
            for comp in compl:
                compl_list.append(comp.get_text())
            parsed_item['complect'] = compl_list
        except Exception, e:
            print Exception, e
            pass

        # get images
        images = []
        imgs = soup.find('div', {'class': 'slider-for'}).find_all("img")
        for idx, img in enumerate(imgs):
            img_url = img.get('src')
            # print img_url
            # file_name = re.search(".*\/(.*?)\?.*", img_url)
            file_name = re.search(".*\/(.*)$", img_url)

            img_url = re.sub("\/\/", 'http://', img_url)
            # img_url = "%s%s" % (host, img_url)

            # print img_url
            # save images to local folder
            if file_name.group(1):
                try:
                    img_extention = re.search('(\..*)$', file_name.group(1))
                    img_extention = re.sub('\?+.*$', '', img_extention.group(1))
                except Exception, e:
                    print Exception, e
                try:
                    new_img_name = translit(h1, reversed=True)
                    new_img_name = re.sub('[\W]+', '_', new_img_name)
                    new_img_name = "%s_%s%s" % (new_img_name, idx, img_extention)
                    img_file_name = os.path.join(img_folder_path, *dirs)
                    # img_file_name = os.path.join(img_file_name, '%s' % (file_name.group(1)))
                    img_file_name = os.path.join(img_file_name, '%s' % (new_img_name))
                    if img_url[:4] != 'http':
                        img_url = "%s%s" % (host, img_url)
                    urllib.urlretrieve(img_url, img_file_name)
                    image_path = "%s/%s" % ('/'.join(dirs), new_img_name)
                    images.append(image_path)
                except Exception, e:
                    print Exception, e
                    pass
        parsed_item['images'] = images

        # main chars

        chars_mains = soup.find('div', {'class': "leftsubcol"}).find_all("li")
        char_main_dict = {}
        for chars_main in chars_mains:
            name = chars_main.find('span', {'class': "property_name"}).get_text().strip()
            value = chars_main.find('span', {'class': "property_value"}).get_text().strip()
            # print name, value
            char_main_dict[name] = value
        parsed_item['chars_main'] = char_main_dict

        # properies
        chars_divs = soup.find('div', {'class': "chars"}).find_all('div', {'class': "props_group"})
        char_props = {}
        for chars_div in chars_divs:
            result = chars_div.find_all("li")
            for item in result:
                name = item.find('div').get_text().strip()
                value = item.find('span', {'class': "value"}).get_text().strip()
                if name == u'Артикул':
                    parsed_item['Artikul'] = value
                # print name, value
                char_props[name] = value
        parsed_item['chars_props'] = char_props

        try:
            dops = soup.find('div', {'class': 'complbluechek'}).find_all('div', {'class', 'minisize'})
            dop_items = {}
            for dop in dops:
                dop_hrefs = []
                try:
                    dop_name = dop.find('div', {'class': 'cmplzagl'}).span.get_text()
                except:
                    continue
                # print dop_name

                dops_items = dop.find_all('a', {'class': 'cmplopislink'})
                for dops_item in dops_items:
                    # print dops_item.get('href')
                    dop_hrefs.append(dops_item.get('href'))
                dop_translit = translit(dop_name, reversed=True)
                # dop_items[dop_translit] = dop_hrefs
                dop_items[dop_name] = dop_hrefs
            parsed_item['dop_items'] = dop_items
        except Exception, e:
            pass
        # print parsed_item
        parsed_items[url] = parsed_item

        _save_json(json_file_name, parsed_item)
        pass
    return parsed_items
    # return HttpResponse('Done', content_type='text/html; charset=windows-1251')


def get_smes():
    host = 'http://santehnika-online.ru'
    # url = 'http://santehnika-online.ru/smesiteli/'
    # url = 'http://santehnika-online.ru/unitazy/brand-roca/'
    # url = 'http://santehnika-online.ru/rakoviny/brand-roca/'
    # url = 'http://santehnika-online.ru/bide/brand-roca/'
    # url = 'http://santehnika-online.ru/smesiteli/'
    # url = 'http://santehnika-online.ru/rakoviny/'
    # url = 'http://santehnika-online.ru/unitazy/'
    # url = 'http://santehnika-online.ru/dushevye_kabiny/'
    # url = 'http://santehnika-online.ru/vanny/chugunnye/'
    # url = 'http://santehnika-online.ru/vanny/stalnye/'
    url = 'http://santehnika-online.ru/vanny/akrilovye/'
    start_from = 0
    pages = 73
    page_query = '?PAGEN_1='
    all_data = {}
    for page_num in range(start_from, pages):
        page_url = "%s%s%s" % (url, page_query, page_num)
        print page_url
        data = get_page(page_url)
        soup = BeautifulSoup(data, 'html.parser')
        items = soup.find('div', {'class': 'vidacha'}).find_all('a', {'itemprop': 'url'})
        for item in items:
            href = item.get('href')
            # full_url = "%s%s" % (host, href)
            current_data = parse_son(href, host)
            all_data.update(current_data)
    file_name = re.sub('^.*\/\/.*?/', '', url)
    file_name = re.sub('\/', '_', file_name)
    file_name = "%s.json" % file_name
    _save_json(file_name, all_data)


@permission_required('products.view_parse')
def parse_analogs(request):

    """

    :param request:
    :return:
    """

    action = request.REQUEST.get('action', None)
    date = request.REQUEST.get('date', None)

    if not action:
        data = u'<h3>Цены на аналогичную продукцию</h3><a href="/parse_analogs?action=bydate"><img align="middle" width="25" src="/media/images/kalendar.jpg" ' \
            u'title="по датам"/></a><a href="/parse_analogs?action=actual"><img align="middle" width="25" ' \
            u'src="/media/images/sekundomer.jpg" title="на данный момент"/></a> '
        return HttpResponse(data)

    if action == 'bydate':
        if not date:
            dates = Analogs.objects.order_by('-date').values('date').distinct()
            if dates:
                data = '<h3>Цены на аналогичную продукцию</h3><p>Выберите дату:</p>'
                for date in dates:
                    data = '%s<a href="/parse_analogs?action=bydate&date=%s">%s<br/>' % (
                        data, date['date'].strftime("%Y-%m-%d"), date['date'].strftime("%Y-%m-%d"))
            else:
                data = u'Нет сохраненных данных %s'
            return HttpResponse(data)
        else:
            names = Analogs.objects.filter(date=date).order_by().values_list('name', flat=True).distinct()
            if not names:
                data = u'Нет данных на дату %s' % date
            else:
                data_array = []
                for name in names:
                    max_order = Analogs.objects.filter(date=date).values('order').order_by('-order')
                    max_order = max_order[0]['order']
                    item_object = {'name': name, 'group': '', 'urls': [{'url': '', 'price': ''} for x in range(max_order+1)]}
                    records = Analogs.objects.filter(date=date, name=name).order_by('order')
                    for record in records:
                        item_object['group'] = record.group
                        item_object['urls'][record.order] = {'url': record.url, 'price': record.price}
                    data_array.append(item_object)

        return_data = render_to_string('triton_ua/price_parse/parse_analogs_table.html', {
            'data_array': data_array,
        }, context_instance=RequestContext(request))
        return HttpResponse(return_data, content_type='text/html; charset=utf-8')

    parse_file_name = os.path.join(settings.MEDIA_ROOT, 'parse_data', 'analogs.csv')
    handle = open(parse_file_name, 'r')
    file_data = handle.read()
    handle.close()
    lines = file_data.split("\n")

    data_array = []
    for j, line in enumerate(lines):
        # print j, line
        cells = line.split(';')
        if not line:
            continue
        item = cells[0].strip(' \n\t')
        item_object = {}
        cells[1] = re.sub('"', '', cells[1])
        item_object = {'name': cells[1], 'group': cells[0], 'urls': []}
        for i in range(2, len(cells)):
            object_num = i-2
            price = ''
            cells[i] = re.sub('"', '', cells[i])
            if cells[i][:4] == 'http':
                price = parse_page(cells[i])
            elif re.match('\d+', cells[i]) is not None:
                price = parse_local(cells[i])
            item_object['urls'].append({'url': cells[i], 'price': price})

        data_array.append(item_object)

    save_analogs(data_array)
    return_data = render_to_string('triton_ua/price_parse/parse_analogs_table.html', {
        'data_array': data_array,
    }, context_instance=RequestContext(request))
    return HttpResponse(return_data, content_type='text/html; charset=utf-8')


def parse_page(url):
    domain = re.search('http[s]?:\/\/(.*?)\/', url).group(1)
    parse_func = "parse_%s" % re.sub('[\W]+', '_', domain)
    try:
        func = eval(parse_func)
    except Exception, e:
        return False
    data = get_page(url)
    price = func(data)
    return price

@permission_required('products.view_parse_m')
def parse_m(request):
    # Get working region

    # region = 'm'
    regions = regions_s
    not_save = request.REQUEST.get('not_save', None)
    update = request.REQUEST.get('update', 0)
    date = request.REQUEST.get('date', None)
    region = request.REQUEST.get('region', None)
    action = request.REQUEST.get('action', None)
    getdata = request.REQUEST.get('getdata', None)
    actuate_sites = request.POST.getlist('sites[]', None)

    selected_sites = request.POST.get('selected_sites', None)
    from_date = request.REQUEST.get('from_date', None)
    to_date = request.REQUEST.get('to_date', None)

    styles = '<style type="text/css">table {border-collapse: collapse} td, th {border: 1px solid grey; padding: ' \
             '5px; margin: 0px; text-align: center;}  a {color: black}</style>'

    data = ''
    if not region:
        data = u'<h2>Выбор региона:</h2>'
        for region in regions:
            data = u'%s%s<br/><a href="/parse_m?region=%s"><img align="middle" width="25" src="/media/images/kalendar.jpg" ' \
                   u'title="по датам"/></a><a href="/parse_m?region=%s&action=actual"><img align="middle" width="25" ' \
                   u'src="/media/images/sekundomer.jpg" title="на текущий момент, с выбором сайтов"/></a> ' \
                   u'<a href="/parse_m?region=%s&action=period"> Период </a><br/><br/>' \
                   % (data, regions_rus[region], region, region, region)

        return HttpResponse(data)

    if region and not date and not getdata and not action:
        # Get all dates from DB
        dates = Prices.objects.filter(region=region).order_by('-date').values('date').distinct()
        data = '<h2>Регион: %s</h2>' % regions_rus[region]
        if dates:
            for date in dates:
                data = '%s<a href="/parse_m?region=%s&date=%s">%s<br/>' % (
                    data, region, date['date'].strftime("%Y-%m-%d"), date['date'].strftime("%Y-%m-%d"))
                # data = '%s<a href="/parse?region=%s&getdata=True">Parse now<br/>' % (data, region)
        else:
            # data = 'No dates for this region.<br/><a href="/parse?region=%s&getdata=true">Parse now</a>' % (region)
            data = u'Нет данных для региона %s' % regions_rus[region]
        return HttpResponse(data)

    # Go if actual request got
    if action and region:
        if action == 'actual':
            not_save = True
            if not actuate_sites:
                # if actuate_sites:
                # for site in actuate_sites:
                #         print site

                # Make a list of sites for region
                data = '<h2>Регион: %s</h2><form method="post" name="actual-sites">' % (regions_rus[region])
                for site in sites[region]:
                    if site == region:
                        data = '%s<input type="checkbox" name="sites[]" value="%s" checked=checked/>%s3tn.ru<br/>' % (
                            data, site, site)
                    else:
                        data = '%s<input type="checkbox" name="sites[]" value="%s"/>%s<br/>' % (data, site, site)
                data = '%s<input type="hidden" name="region" value="%s"/>' % (data, region)
                data = '%s<input type="hidden" name="action" value="%s"/>' % (data, action)
                data = '%s<input type="hidden" name="update" value="%s"/>' % (data, update)
                data = u'%s<br/><br/><input type="submit" name="submit" value="Запустить анализ"/>' % (data)
                data = '%s</form>' % data
                return HttpResponse(data)
        if action == 'period':
            list_of_sites = ()

            if region and (not selected_sites or not from_date or not to_date):
                list_of_sites = Prices.objects.filter(region=region).order_by().values_list('site', 'site').distinct()
                form = PeriodForm(initial={'region': region, 'selected_sites': selected_sites,
                                           'from_date': from_date, 'to_date': to_date}, list_of_sites=list_of_sites,
                                  request=request)
                return_data = render_to_string('triton_ua/price_parse/period_form.html', {
                    'form': form,
                    'from_date': from_date,
                    'to_date': to_date,
                    'regions_rus': regions_rus[region],
                    'list_of_sites': list_of_sites,
                }, context_instance=RequestContext(request))
                return HttpResponse(return_data, content_type='text/html; charset=utf-8')

            if selected_sites and from_date and to_date:
                data_array = Prices.objects.filter(date__range=(from_date, to_date),
                                                   region=region, site__in=[region, selected_sites]). \
                    values_list('nid', 'date', 'site', 'price').distinct()
                return_data = render_to_string('triton_ua/price_parse/period.html', {
                    'sites': sites,
                    'region': region,
                    'regions': regions,
                    'regions_rus': regions_rus[region],
                    'data_array': data_array,
                    # 'actuate_sites': actuate_sites,
                    'selected_sites': selected_sites,
                    'from_date': from_date,
                    'to_date': to_date,
                    'action': action,
                }, context_instance=RequestContext(request))
                return HttpResponse(return_data, content_type='text/html; charset=utf-8')

    # If date selected try get data from DB
    if date:
        data = Prices.objects.filter(date=date, region=region)
        if not data:
            data = u'Нет данных на дату %s для региона %s' % (date, regions[region])
        else:
            ids = Prices.objects.filter(date=date, region=region).order_by().values_list('nid', flat='True').distinct()
            item_names = {}
            data_array = {}

            product = get_model('products', 'Product')
            for product_id in ids:
                item_names[product_id] = product.objects.filter(id=product_id).values_list('title', flat='True')[0]
                data_array[item_names[product_id]] = {}
            for record in data:
                name = item_names[record.nid]
                try:
                    data_array[name][region]
                except:
                    data_array[name].update({region: {}})

                data_array[name][region].update({record.site: {'price': record.price, 'url': record.url}})

            return_data = render_to_string('triton_ua/price_parse/table.html', {
                'sites': sites,
                'region': region,
                'regions': regions,
                'regions_rus': regions_rus,
                'data_array': data_array,
                'actuate_sites': actuate_sites,
                'date': date,
                'action': action,
            }, context_instance=RequestContext(request))

            return HttpResponse(return_data, content_type='text/html; charset=utf-8')

    # Go through regular straight parsing without save
    parse_file_name = os.path.join(settings.MEDIA_ROOT, 'parse_data', '%s.csv' % (region))
    handle = open(parse_file_name, 'r')
    file_data = handle.read()
    handle.close()
    lines = file_data.split("\n")

    data_array = {}
    for line in lines:
        cells = line.split(';')
        item = cells[0].strip(' \n\t')
        for i in range(len(cells)):
            if not i:
                if item:
                    data_array[item] = ''
                continue
            else:
                # Check if it local price
                if re.match('^\d+$', cells[i]):
                    cells[i] = re.sub("\r", '', cells[i])
                    data_array[item] = {region: {region: {'price': '-', 'url': cells[i].strip('"')}}}
                    continue
                # Get site url and save it to data_array
                result = re.search('http[s]?://(.*?)/', cells[i])
                if not result:
                    continue
                else:
                    # print 'no result'
                    # print result
                    site = result.group(1)
                    cells[i] = re.sub("\r", '', cells[i])
                    data_array[item][region][site] = {'price': '-', 'url': cells[i].strip('"')}

    new_items = data_array

    # Iterate prods to get url for parse

    for item in new_items:
        for key, value in new_items[item][region].iteritems():
            try:
                if key not in actuate_sites:
                    continue
                # func = eval(site_methods[key])
                # Check if it own site
                # data = get_page(value['url'])
                # price = func(data)
                price = ''
                value['url'] = re.sub('"', '', value['url'])
                if value['url'][:4] == 'http':
                    price = parse_page(value['url'])
                elif re.match('\d+', value['url']) is not None:
                    price = parse_local(value['url'])
                # Save price to common data array
                new_items[item][region][key]['price'] = price
            except Exception, e:
                print Exception, e
                value['price'] = '-'

    # Save prices to database
    if not not_save:
        save_prices(new_items, region)
    if int(update) == 1:
        update_prices(new_items, region)

    return_data = render_to_string('triton_ua/price_parse/table.html', {
        'sites': sites,
        'region': region,
        'regions': regions,
        'regions_rus': regions_rus,
        'data_array': new_items,
        'actuate_sites': actuate_sites,
        'date': date,
        'action': action,

    }, context_instance=RequestContext(request))

    return HttpResponse(return_data, content_type='text/html; charset=utf-8')

