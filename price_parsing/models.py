# -*- coding: utf-8 -*-
__author__ = 'hramik'

from django.db import models


class Prices(models.Model):
    '''
    Prices from sites
    '''
    site = models.CharField(
        max_length=255,
        default=''
    )

    region = models.CharField(
        max_length=128,
        default=''
    )

    url = models.CharField(
        max_length=512,
        default=''
    )

    date = models.DateField(
        auto_now_add=True
    )

    price = models.CharField(
        max_length=15,
        default=''
    )

    nid = models.CharField(
        max_length=50
    )

    def __unicode__(self):
        return "%d" % self.pk

class Analogs(models.Model):
    '''
    Prices from sites
    '''
    host = models.CharField(
        max_length=255,
        default=''
    )

    url = models.CharField(
        max_length=512,
        default=''
    )

    date = models.DateField(
        auto_now_add=True
    )

    price = models.CharField(
        max_length=15,
        default=''
    )

    group = models.CharField(
        max_length=3
    )

    name = models.CharField(
        max_length=255
    )
    order = models.IntegerField(
    )

    def __unicode__(self):
        return "%d" % self.pk
