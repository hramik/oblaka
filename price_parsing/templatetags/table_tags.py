# -*- coding: utf-8 -*-
__author__ = 'm13v246'

import os
import os.path
import re
from django import template
from django.template import Context, Template
from django.db.models.loading import get_model

register = template.Library()


@register.simple_tag
def get_price(data_array, sites, regions, regions_rus, region, actuate_sites, date, action):
    colors = '<table>' \
             '<tr><td style="background-color: #2AA4E9; height: 15px; width: 40px"></td><td>цена регулятор</td></tr>' \
             '<tr><td style="background-color: #FFFF94; height: 15px; width: 40px"></td><td>0 - 5 %</td></tr>' \
             '<tr><td style="background-color: #FFB836; height: 15px; width: 40px"></td><td>5 - 10 % </td></tr>' \
             '<tr><td style="background-color: #FF5959; height: 15px; width: 40px"></td><td>свыше 10%</td></tr>' \
             '</table>'
    if date:
        tables = u'<table class="top-table">' \
                 u'<tr><td><h2>Регион: %s</h2> Дата: %s<br/><br/></td><td>%s</td></tr></table><table>' \
                 % (regions_rus[region], date, colors)
    elif action == 'actual':
        tables = u'<table class="top-table">' \
                 u'<tr><td><h2>Регион: %s</h2> на текущий момент<br/><br/></td><td>%s</td></tr></table><table>' \
                 % (regions_rus[region], colors)
    else:
        tables = u'<h2>Регион: %s</h2><table>' % regions_rus[region]


    th = '<tr><th style="width: 215px; display: inline-block;"></th>'
    sorted_sites_without_www = {}
    for site in sites[region]:
        site_without_www = re.sub('^www\.', '', site)
        sorted_sites_without_www[site_without_www] = site

    # sorted_sites = sorted(sites[region])
    sorted_sites = sorted(sorted_sites_without_www.keys())
    sorted_sites.remove(region)
    sorted_sites.insert(0, region)

    for site_shorted_name in sorted_sites:
        site = sorted_sites_without_www[site_shorted_name]
        if actuate_sites and site not in actuate_sites:
                continue
        if site == region:
            th = '%s<th>%s3tn.ru</th>' % (th, site)
        else:
            th = '%s<th>%s</th>' % (th, site_shorted_name)
    th = '%s</tr>' % th

    tables = '%s %s' % (tables, th)

    print data_array

    for item in data_array:
        try:
            one = int(data_array[item][region][region]['price'])/100
        except Exception, e:
            print 'one', Exception, e
            continue
        td = '<tr><td>%s</td>' % item
        # print sorted_sites
        for site_shorted_name in sorted_sites:
            site = sorted_sites_without_www[site_shorted_name]
            if actuate_sites and site not in actuate_sites:
                continue
            try:
                if not site == region:
                    diff = int(data_array[item][region][site]['price'])/one - 100
                    if diff < 0:
                        color = '#FFFF94'
                        if diff < -5:
                            color = '#FFB836'
                            if diff < -10:
                                color = '#FF5959'
                    else:
                        color = 'white'
                    price = '<a href="%s" target="_blank">%s</a>' % (data_array[item][region][site]['url'], data_array[item][region][site]['price'])
                else:
                    color = '#2AA4E9'
                    price = '%s' % (data_array[item][region][site]['price'])
                # print price
                td = '%s<td style="background-color:%s">%s</td>' % (td, color, price)
            except:
                try:
                    td = '%s<td>%s</td>' % (td, data_array[item][region][site]['price'])
                except:
                    td = '%s<td>%s</td>' % (td, '-')
        td = '%s</tr>' % td
        tables = '%s%s' % (tables, td)

    # tables = '%s%s' % (tables, '</table>')
    return tables


@register.simple_tag
def get_price_by_period(data_array, selected_sites, sites, regions, regions_rus, region, from_date, to_date, action):
    colors = '<table>' \
             '<tr><td style="background-color: #2AA4E9; height: 15px; width: 40px"></td><td>цена регулятор</td></tr>' \
             '<tr><td style="background-color: #FFFF94; height: 15px; width: 40px"></td><td>0 - 5 %</td></tr>' \
             '<tr><td style="background-color: #FFB836; height: 15px; width: 40px"></td><td>5 - 10 % </td></tr>' \
             '<tr><td style="background-color: #FF5959; height: 15px; width: 40px"></td><td>свыше 10%</td></tr>' \
             '</table>'
    tables = u'<table class="top-table"><tr><td><h2>Регион: %s</h2> архив цен с %s по %s<br/>по сайту %s<br/></td>' \
             u'<td>%s</td></tr></table><table>' % (regions_rus, from_date, to_date, selected_sites, colors)

    dates = []
    for (nid, date, site, price) in data_array:
        if date not in dates:
            dates.append(date)

    nids = []
    for (nid, date, site, price) in data_array:
        if nid not in nids:
            nids.append(nid)

    item_names = {}
    product = get_model('products', 'Product')
    for nid in nids:
        item_names[nid] = product.objects.filter(id=nid).values_list('title', flat='True')[0]

    th = '<tr><th style="width: 215px; display: inline-block;"></th>'
    for date in dates:
            th = '%s<th>%s</th>' % (th, date)
    th = '%s</tr>' % th
    tables = '%s %s' % (tables, th)



    for nid in nids:

        td = '<tr><td>%s</td>' % item_names[nid]
        for date in dates:
            # print nid, date, selected_sites
            # print [price1 for (nid1, date1, site, price1) in data_array if nid1 == nid and date1 == date and site == selected_sites]

            price = [price1 for (nid1, date1, site, price1) in data_array if nid1 == nid and date1 == date and site == selected_sites]
            base_price = [price1 for (nid1, date1, site, price1) in data_array if nid1 == nid and date1 == date and site == region]
            color = 'white'
            try:
                if price[0] and base_price[0]:
                    one = int(base_price[0])/100
                    diff = int(price[0])/one - 100
                    if diff < 0:
                        color = '#FFFF94'
                        if diff < -5:
                            color = '#FFB836'
                            if diff < -10:
                                color = '#FF5959'
            except:
                color = 'white'

            try:
                td = '%s<td style="background-color:%s">%s</td>' % (td, color, price[0])
            except:
                td = '%s<td style="background-color:%s">%s</td>' % (td, color, '-')
        td = '%s</tr>' % td
        tables = '%s%s' % (tables, td)


    # for date in dates:
    #     td = '<tr><td>%s</td>' % item

    return tables

@register.simple_tag
def get_analogs_table(data_array):
    print 'data_array', data_array
    tables = ''
    th = []
    for j, item in enumerate(data_array):
        even = (int(data_array[j]['group']) + 2) % 2
        style = ''
        if even:
            style = 'style="background-color: #DAE2E6"'
        tables = '%s<tr><td %s>%s</td>' % (tables, style, data_array[j]['name'])
        for i, url_item in enumerate(data_array[j]['urls']):
            price = data_array[j]['urls'][i]['price']
            url = data_array[j]['urls'][i]['url']
            try:
                host = re.search('http[s]?:\/\/(.*?)\/', url).group(1)
            except Exception, e:
                host = ''
            try:
                th[i]
                if not th[i]:
                    th[i] = host
            except:
                th.insert(i, host)

            tables = '%s<td %s><a href="%s" target="_new">%s</a></td>' % (tables, style, url, price)
        tables = '%s</tr>' % tables
    th.pop(0)
    tables = '<table><tr><th></th><th>www.triton3tn.ru</th><th>%s</th></tr>%s<tr><th></th><th>www.triton3tn.ru</th><th>%s</th></tr></table>' % \
                        ('</th><th>'.join(th), tables, '</th><th>'.join(th))
    # tables = '%s</table><br/>' % tables


    return tables

    # colors = '<table>' \
    #          '<tr><td style="background-color: #2AA4E9; height: 15px; width: 40px"></td><td>цена регулятор</td></tr>' \
    #          '<tr><td style="background-color: #FFFF94; height: 15px; width: 40px"></td><td>0 - 5 %</td></tr>' \
    #          '<tr><td style="background-color: #FFB836; height: 15px; width: 40px"></td><td>5 - 10 % </td></tr>' \
    #          '<tr><td style="background-color: #FF5959; height: 15px; width: 40px"></td><td>свыше 10%</td></tr>' \
    #          '</table>'
    # tables = u'<table class="top-table"><tr><td><h2>Регион: %s</h2> архив цен с %s по %s<br/>по сайту %s<br/></td>' \
    #          u'<td>%s</td></tr></table><table>' % (regions_rus, from_date, to_date, selected_sites, colors)

    # tables = ''
    # dates = []
    # for (nid, date, site, price) in data_array:
    #     if date not in dates:
    #         dates.append(date)
    #
    # nids = []
    # for (nid, date, site, price) in data_array:
    #     if nid not in nids:
    #         nids.append(nid)
    #
    # item_names = {}
    # product = get_model('products', 'Product')
    # for nid in nids:
    #     item_names[nid] = product.objects.filter(id=nid).values_list('title', flat='True')[0]
    #
    # th = '<tr><th style="width: 215px; display: inline-block;"></th>'
    # for date in dates:
    #         th = '%s<th>%s</th>' % (th, date)
    # th = '%s</tr>' % th
    # tables = '%s %s' % (tables, th)
    #
    #
    #
    # for nid in nids:
    #
    #     td = '<tr><td>%s</td>' % item_names[nid]
    #     for date in dates:
    #         print nid, date, selected_sites
            # print [price1 for (nid1, date1, site, price1) in data_array if nid1 == nid and date1 == date and site == selected_sites]

            # price = [price1 for (nid1, date1, site, price1) in data_array if nid1 == nid and date1 == date and site == selected_sites]
            # base_price = [price1 for (nid1, date1, site, price1) in data_array if nid1 == nid and date1 == date and site == region]
            # color = 'white'
            # try:
            #     if price[0] and base_price[0]:
            #         one = int(base_price[0])/100
            #         diff = int(price[0])/one - 100
            #         if diff < 0:
            #             color = '#FFFF94'
            #             if diff < -5:
            #                 color = '#FFB836'
            #                 if diff < -10:
            #                     color = '#FF5959'
            # except:
            #     color = 'white'
            #
            # try:
            #     td = '%s<td style="background-color:%s">%s</td>' % (td, color, price[0])
            # except:
            #     td = '%s<td style="background-color:%s">%s</td>' % (td, color, '-')
        # td = '%s</tr>' % td
        # tables = '%s%s' % (tables, td)


    # for date in dates:
    #     td = '<tr><td>%s</td>' % item

    # return tables
