# -*- coding: utf-8 -*-
__author__ = 'm13v246'

from django import forms
from price_parsing.models import Prices
from django.forms.extras.widgets import SelectDateWidget

# from price_parsing.views import regions_rus

# REGIONS = (('Екатеринбург', 'eburg'), ('Москва', 'moscow'))
regions_rus = (('chelyabinsk', 'Челябинск'),
                ('novosib', 'Новосибирск'),
                ('piter', 'Санкт-Петербург'),
                ('perm', 'Пермь'),
                ('eburg', 'Екатеринбург'),
                ('samara', 'Самара'),
                ('rostov', 'Ростов'),
                ('moscow', 'Москва'),
                ('krasnodar', 'Краснодар'),
                ('nnovgorod', 'Нижний Новгород'),
                ('triton', 'Москва(с)'))


class PeriodForm(forms.ModelForm):

    # region = forms.ChoiceField(widget=forms.RadioSelect, choices=regions_rus, label=u'Регион')
    selected_sites = forms.ChoiceField(
        widget=forms.RadioSelect,
        label=u'Сайты'
    )
    from_date = forms.CharField(widget=forms.TextInput(attrs={'class': 'datePicker'}), label=u'Начало периода')
    to_date = forms.CharField(widget=forms.TextInput(attrs={'class': 'datePicker'}), label=u'Конец периода')
    action = forms.CharField(widget=forms.HiddenInput(attrs={'value': 'period'}), required=False)
    region = forms.CharField(widget=forms.HiddenInput(attrs={}), required=False)


    class Meta:
        model = Prices
        fields = ('region', 'from_date', 'to_date', 'action')


    def __init__(self, *args, **kwargs):
        try:
            self.request = kwargs.pop('request', None)
            self.list_of_sites = kwargs.pop('list_of_sites', None)
            super(PeriodForm, self).__init__(*args, **kwargs)
            self.fields['selected_sites'].choices = self.list_of_sites
        except Exception, e:
            self.fields['selected_sites'].choices = ()
