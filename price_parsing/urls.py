# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse
#from oscar.app import application
# Uncomment the next two lines to enable the admin:

from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from tastypie.api import Api
from tri.api.resources import QuestionResource

# from django.conf.urls import handler404, handler500

from django.contrib import admin
from tri.views import questionViewSet, additionalPhotoViewSet
from products.models import ProductEntity
from tri.djangular_views import *

from django.views.generic import TemplateView
from tri import views


admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^parse$', 'price_parsing.views.parse', name='parse'),
                       url(r'^parse_son$', 'price_parsing.views.parse_son', name='parse_son'),
                       url(r'^parse_analogs$', 'price_parsing.views.parse_analogs', name='parse_analogs'),
                       url(r'^parse_m$', 'price_parsing.views.parse_m', name='parse_m'),
)

