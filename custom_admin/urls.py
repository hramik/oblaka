# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse
#from oscar.app import application
# Uncomment the next two lines to enable the admin:

from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from tastypie.api import Api
from tri.api.resources import QuestionResource

# from django.conf.urls import handler404, handler500

from django.contrib import admin
from tri.views import questionViewSet, additionalPhotoViewSet
from products.models import ProductEntity
from tri.djangular_views import *

from django.views.generic import TemplateView

from custom_admin import views


admin.autodiscover()


urlpatterns = patterns('',

                       url(r'^admin_tri_ua/test$', 'custom_admin.views.price_table', name='test'),

                       # Price Table
                       url(r'^admin_tri_ua/price_table$', 'custom_admin.views.price_table', name='price_table'),

                       # Upload prices to regional sites
                       url(r'^admin_tri_ua/upload_prices$', 'custom_admin.views.upload_prices', name='upload_prices'),
)

