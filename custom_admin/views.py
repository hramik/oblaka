# -*- coding: utf-8 -*-

import os
import re

from django.db import models
from django.db.models import Q, F
import operator

import datetime
import csv

from django.utils.html import strip_tags

from django.core.cache import cache
from django.views.decorators.cache import cache_page

from django.db import transaction

from xml.dom import minidom
from bs4 import BeautifulSoup

from django.shortcuts import get_object_or_404

import json
from datetime import datetime
import random
from django.contrib.auth.decorators import login_required

from itertools import chain
from django.shortcuts import render_to_response, redirect
from django.template.loader import render_to_string

from django.contrib.sites.shortcuts import get_current_site
from django.core.files import File
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from ipgeo.models import Range
from django.db.models import Max
from django.db.models import F

from djangular.views.crud import NgCRUDView
from pprint import pprint
import logging
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User

from django.http import HttpResponse
from django.template import RequestContext
from django.views.generic import DetailView, TemplateView, ListView
from django.utils.translation import gettext_lazy as _
from django.db.models import Sum

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers

# from django.utils import simplejson
import json
import urllib
import urllib2
from urlparse import urlparse
from django.core.files import File
from django.core.files.base import ContentFile

from array import *
from django.conf import settings

from django.contrib.sites.models import Site

from tri.models import *
from slides.models import Slide
from node.models import *
from products.models import Category, Product, Product, ProductEntity, ProductType, ProductCategory, Component
from products.models import ProductComment, ProductCommentForm, Option, OptionPrice, \
    Order, OrderItem, CategoryType, Visor
from products.forms import *
from custom_sites.models import SiteInfo
from callback.models import Callback, CallbackForm
from menu.models import Menu, MenuGroup
from questions.models import Question, QuestionForm, AnonimousQuestionForm
from tri import helper
from tri.forms import *
from custom_admin.forms import *
from products.models import Product, Visor, OptionsGroup, Option, OptionPrice, SKU

from da_mailer.helper import *
from operator import attrgetter

from custom_admin.constatnts import *


def price_table(request):

    product_kwargs = {}
    options_kwargs = {}
    product_kwargs['sites__id__in'] = ['1',]

    filter_sites = {}

    sites = Site.objects.all()

    entities = ProductEntity.objects.all()

    price_table_filter_form = PriceTableFilterForm(initial={
        'sites': ['1'],
        'entities': ['1724'],
        'options': ['60']
    })

    if request.POST:
        price_table_filter_form = PriceTableFilterForm(request.POST)

        print request.POST

        if price_table_filter_form.is_valid():
            if price_table_filter_form.cleaned_data['entities']:
                product_kwargs['entity__id__in'] = price_table_filter_form.cleaned_data['entities']

            if price_table_filter_form.cleaned_data['sites']:
                # logging.debug(price_table_filter_form.cleaned_data['sites'])
                product_kwargs['sites__id__in'] = price_table_filter_form.cleaned_data['sites'][:1]
                filter_sites = Site.objects.filter(id__in=price_table_filter_form.cleaned_data['sites'])

            if price_table_filter_form.cleaned_data['options']:
                # product_kwargs['sites__id__in'] = price_table_filter_form.cleaned_data['sites']
                options_kwargs['id__in'] = price_table_filter_form.cleaned_data['options']
        else:
            logging.error(price_table_filter_form.errors)

    else:
        product_kwargs['sites__id__in'] = [1,]
        filter_sites = Site.objects.filter(domain='www.3tn.ru')
        product_kwargs['entity__in'] = ProductEntity.objects.filter(slug='bath')
        options_kwargs['id__in'] = Option.objects.filter(slug='litcevoy_ekran')


    # start, end = helper.get_start_end_pages(2, request)

    products = Product.objects.filter(**product_kwargs).order_by('sku')
    # logging.debug("продукты: %s %s" % (product_kwargs, products))

    # for site in filter_sites:
    #     product_kwargs['sites__id'] = site.id
    #     site_products = Product.objects.filter(**product_kwargs).order_by('sku')
    #     site.products = site_products
    #     logging.debug(site_products)


    return render_to_response('custom_admin/pages/price_table.html', {
        'title': u'Цены: %s' % ', '.join([a.name for a in filter_sites]),
        # 'sites': sites,
        'filter_sites': filter_sites,
        'filter_sites_plain': '"%s"' % (','.join([a.name for a in filter_sites])),
        'entities': entities,
        'products': products,
        'filter_form': price_table_filter_form,
        'options': Option.objects.filter(**options_kwargs)
    },
                              context_instance=RequestContext(request))




def handle_uploaded_file(f):
    with open(CSV_FILE_DEST, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def upload_prices(request):

    product_kwargs = {}
    options_kwargs = {}
    product_kwargs['sites__id__in'] = ['1',]

    filter_sites = {}

    sites = Site.objects.all()

    entities = ProductEntity.objects.all()

    upload_filter_filter_form = UploadPriceFilterForm()

    if request.POST:
        upload_filter_filter_form = UploadPriceFilterForm(request.POST, request.FILES)

        print request.FILES

        if upload_filter_filter_form.is_valid():
            print upload_filter_filter_form.cleaned_data['csv']

            import_sites = upload_filter_filter_form.cleaned_data['sites']

            try:
                sku_row = int(upload_filter_filter_form.cleaned_data['sku_row'])-1
            except:
                sku_row = 1

            try:
                price_row = int(upload_filter_filter_form.cleaned_data['price_row'])-1
            except:
                price_row = 2

            # product_site = Site.objects.get(id=1)
            sites = Site.objects.filter(id__in=upload_filter_filter_form.cleaned_data['sites'])
            site_info = SiteInfo.objects.get(site=sites[:1].get())

            import_id = int(site_info.import_id) + 1
            print import_id
            site_info.import_id = import_id
            site_info.save()

            SiteInfo.objects.filter(site__in=sites).update(import_id = import_id)

            import_date = datetime.datetime.now()

            handle_uploaded_file(request.FILES['csv'])

            with open(CSV_FILE_DEST, 'rb') as csvfile:
                csv_data = csv.reader(csvfile, delimiter=';')
                for row in csv_data:
                    if row[sku_row]:
                        print row[sku_row]


                        try:
                            price = int(str(row[price_row]).replace(",",".").replace(" ","").replace("руб.",""))
                            skus = SKU.objects.filter(sku=row[sku_row], sites=sites).update(price = price)
                        except Exception, e:
                            print Exception, e, row[sku_row], row[price_row]
                            pass

                        # Import Options prices
                        # try:
                        #     price = int(str(row[price_row]).replace(",",".").replace(" ","").replace("руб.",""))
                        #     options = OptionPrice.objects.filter(sku__contains=row[sku_row], product__sites = sites)
                        #     for option in options:
                        #         if option.import_id != import_id:
                        #             option.price = 0
                        #
                        #         if '/' in option.sku:
                        #             option.price = price
                        #         else:
                        #             sku = option.sku.encode('utf-8')
                        #             sku_count = sku.count(row[sku_row])
                        #             option.price = float(option.price) + (price * sku_count)
                        #
                        #         option.import_id = import_id
                        #         option.import_date = import_date
                        #
                        #         option.save()
                        # except Exception, e:
                        #     pass

                products = Product.objects.filter(sites = sites)
                for product in products:
                    product.save()

                options = OptionPrice.objects.filter(product__sites = sites)
                for option in options:
                    option.save()

            filter_sites = Site.objects.filter(id__in=upload_filter_filter_form.cleaned_data['sites'])

        else:
            logging.error(upload_filter_filter_form.errors)

    else:
        pass


    return render_to_response('custom_admin/pages/upload_prices.html', {
        'title': u'Загрузка цен: %s' % ', '.join([a.name for a in filter_sites]),
        'filter_sites': filter_sites,
        'filter_sites_plain': '"%s"' % (','.join([a.name for a in filter_sites])),
        'filter_form': upload_filter_filter_form,
    },
                              context_instance=RequestContext(request))

