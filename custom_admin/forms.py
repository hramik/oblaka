# -*- coding: utf-8 -*-

import datetime, sys, os, re
from django.utils.encoding import force_unicode

from django.db import models
from django.forms import ModelForm
from django.db import models
from django import forms

from django.contrib.sites.models import Site
from products.models import Product, ProductEntity, Option


class PriceTableFilterForm(forms.Form):


    sites = forms.MultipleChoiceField(
        widget=forms.SelectMultiple(
            attrs={
                'class': '',
                'ng-model': 'price.sites',
            },
        ),
        # choices=list(Site.objects.all().values_list('id', 'domain')),
        label=u"Сайты",
        required=False
    )

    entities = forms.MultipleChoiceField(
        widget=forms.SelectMultiple(
            attrs={
                'class': '',
                'ng-model': 'price.entities',
            },
        ),
        # choices=list(ProductEntity.objects.all().values_list('id', 'title')),
        label=u"Типы",
        required=False
    )

    options = forms.MultipleChoiceField(
        widget=forms.SelectMultiple(
            attrs={
                'class': '',
                'ng-model': 'price.options',
            },
        ),
        # choices=list(Option.objects.all().values_list('id', 'name')),
        label=u"Опции",
        required=False
    )



class UploadPriceFilterForm(forms.Form):
    sites = forms.MultipleChoiceField(
        widget=forms.SelectMultiple(
            attrs={
                'class': '',
                'ng-model': 'price.sites',
            },
        ),
        # choices=list(Site.objects.all().values_list('id', 'domain')),
        label=u"Сайты",
        required=True,
    )

    sku_row = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'price.sku_row',
            },
        ),
        label=u'Столбец SKU',
        required=True,
    )

    price_row = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'price.price_row',
            },
        ),
        label=u'Столбец цены',
        required=True,
    )

    csv = forms.FileField(
        widget=forms.FileInput(
            attrs={
                'class': '',
                'ng-model': 'price.csv',
            },
        ),
        label=u"CSV",
        required=True,
    )



class SitesForm(forms.Form):
    sites = forms.MultipleChoiceField(
        widget=forms.SelectMultiple(
            attrs={
                'class': '',
                'ng-model': 'price.sites',
            },
        ),
        # choices=list(Site.objects.all().values_list('id', 'domain')),
        label=u"Сайты",
        required=False
    )



class EntitiesForm(ModelForm):


    title = forms.ChoiceField(
        widget=forms.SelectMultiple(
            attrs={
                'class': '',
                'ng-model': 'price.title',
            },
        ),
        # choices=list(ProductEntity.objects.all().values_list('id', 'title')),
        label=u"Типы",
        required=True
    )

    class Meta:
        model = ProductEntity
        fields = ('title', )



class OptionsForm(ModelForm):


    name = forms.ChoiceField(
        widget=forms.SelectMultiple(
            attrs={
                'class': '',
                'ng-model': 'price.name',
            },
        ),
        # choices=list(Option.objects.all().values_list('id', 'name')),
        label=u"Опции",
        required=True
    )

    class Meta:
        model = Option
        fields = ('name', )





