# -*- coding: utf-8 -*-

__author__ = 'hramik'


import datetime, sys, os
from django.utils.encoding import force_unicode

from django.db import models
from django.forms import ModelForm
from django.db import models
from django.utils.translation import gettext_lazy as _
from sorl.thumbnail import ImageField
from PIL import Image
from autoslug import AutoSlugField
from node.models import *
from django.contrib.auth.models import User



STATUS_CHOICES = (
    ('new', _('новый')),
    ('accepted', _('принят')),
    ('test', _('тестирование')),
    ('invalid', _('инвалид')),
    ('fixed', _('готово')),
)

class Ticka(Node):
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default="new")
    assign_to = models.ManyToManyField(User, related_name='assign_to_user')
    creator = models.ForeignKey(User, related_name='creator_user')

    def __unicode__(self):
        return self.title




