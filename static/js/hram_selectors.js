jQuery.extend(jQuery.expr[':'], {
  position_top_bigger_zero: function (el) {
    var $e = $(el),
    top = $e.position().top;
    return (top > 0);
  }
});
