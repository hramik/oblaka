var product_video, node_video, option_video

$(document).on('scroll', function () {
    if ($(document).scrollTop() > 200) {
        $('body').addClass('bottom_scroll');
    } else {
        $('body').removeClass('bottom_scroll');
    }

});

$.expr[':'].external = function (obj) {
    return !obj.href.match(/^mailto\:/) && (obj.hostname != location.hostname);
};

$(document).ajaxComplete(function () {
    console.log('ajax complete!');
    $("a:not([target]):not('.no_target')").attr('target', '_self');

    jQuery('img.lazzy')
        .visibility({
            type: 'image',
            transition: 'fade in',
            duration: 1000
        });
});

$(document).ready(function () {

    $("#id_phone").mask("+7(999)999-99-99");
    $(".digits").mask("99");
    $("#report_form #id_price").mask("9?99999");

    //Lazzy load image
    $('img.lazzy')
        .visibility({
            type: 'image',
            transition: 'fade in',
            duration: 1000
            //once: false,
            //continuous: true
        })
    ;

        // Centered image
    $('.product_slide.index_0 img').each(function(){
        $(this).css('left', '50%').css('position', 'absolute').css('margin-left', "-" + this.width/2 + "px");
    });

    //$('[da-modal]').each(function () {
    //
    //    $(this).bind('click', function () {
    //        $($(this).attr('da-modal'))
    //            .modal({
    //                onDeny: function () {
    //                    return true;
    //                },
    //                onApprove: function () {
    //                    console.log($scope, 'true');
    //                    return true;
    //                }
    //            })
    //            .modal('setting', 'duration', 250)
    //            .modal('show')
    //        ;
    //    });
    //};





    $(".ui.baths.modal").scroll(function () {
        console.log('scroll');
    });

    $("[class^=report_image]").each(function (index) {
        $(this).colorbox({rel: $(this).attr('rel')})
//        console.log($(this).attr('rel'))
    });

    $('i')
        .popup({
            inline: true
        });

    $('.dropdown')
        .dropdown({
            // you can use any ui transition
            transition: 'drop',
            fullTextSearch: true
        })
    ;

//$(".field_wrapper.field_delivery_date").datetimepicker({format: 'dd.mm.yyyy'});

//$('#form_datetime').DatePicker({
//    flat: true,
//    date: '2008-07-31',
//    current: '2008-07-31',
//    calendars: 1,
//    starts: 1
//});

// SEO sidebar
    $('.left.seo.sidebar').first()
        .sidebar('setting', 'transition', 'overlay')
        .sidebar('attach events', '.seo.launch.button')
    ;

    $("a:not([target]):not('.no_target')").attr('target', '_self');

    $("a.cbox").colorbox();


    $("label").each(function () {
        $(this).attr('ng-show', $(this).parents('.field_wrapper').find('input').attr('ng-show'));
    });

    if ($('#main_slideshow').length != 0) {
        var main_slideshow = $('#main_slideshow').slideme({
            autoslide: true,
            arrows: true,
            pagination: 'numbers',
            interval: 5000,
            transition: 'zoom',
            loop: true,
            speed: 200,
            css3: true
        });
    }

    if (window.PIE) {
        $('.pie').each(function () {
            PIE.attach(this);
        });
    }


    if ($('.slide_video').length != 0) {

        var slide_id = '';

        //var slide_video = videojs('slide_video');
        //slide_video.volume(0);
        $('.video_slide video').each(function () {
            //console.log($(this).attr('id'));
            //var vid = document.getElementById($(this).attr('id'));
            //vid.onended = function () {
            //    alert("The video has ended");
            //};
            $(this).get(0).onended = function (e) {
                console.log('ended');
                $('#content').removeClass('vjs-playing');
                $('#main_slideshow').slideme('play');

            };
        });

        setInterval(function () {
            //console.log($('.video_slide'));

            if ($('.slide').hasClass('current') == true && slide_id != $('.slide.current').attr('id')) {

                slide_id = $('.slide.current').attr('id');


                console.log(slide_id, $('.slide.current').attr('id'));

                $('.video_slide.current video').get(0).play();
                $('#content').addClass('vjs-playing');
                $('#main_slideshow').slideme('stop');

            }

            if ($('.video_slide').hasClass('current') == false) {
                //console.log('if_2');
                //slide_video.pause();
                //$('.video_slide.current').trigger("pause");
                $('.video_slide:not(.current) video').each(function () {
                    //var id = $('.video_slide.current video').attr('id');
                    //$(this).get(0).currentTime = 0;
                    //$(this).get(0).start(0);
                    //$(this).get(0).load();
                    //$(this).get(0).pause();
                });

                //$('#main_slideshow').slideme('play');
                //$('#content').removeClass('vjs-playing');
            }
        }, 200);


        //$('.sound_on_off').click(function (e) {
        //    if ($(this).hasClass('off')) {
        //        $(this).removeClass('off').addClass('on');
        //        slide_video.volume(0.5);
        //        $(this).text('выключить звук');
        //    } else {
        //        $(this).removeClass('on').addClass('off');
        //        slide_video.volume(0);
        //        $(this).text('включить звук');
        //    }
        //});
        //
        //
        //slide_video.on("play", function () {
        //    console.log('play');
        //    $('#content').addClass('vjs-playing');
        //    $('#main_slideshow').slideme('stop');
        //});
        //
        //slide_video.on("ended", function () {
        //    console.log('ended');
        //    $('#main_slideshow').slideme('play');
        //    //$('#content').removeClass('vjs-playing');
        //
        //});
        //
        //$('.slidesjs-play').click();
        //
        //
        //$('.product_menu ul li').click(function () {
        //
        //});

    }

    var product_video_pause = false;

    if ($('#product_video').length != 0) {

        product_video = videojs('product_video');

        product_video.on("play", function () {
            $('#content').addClass('vjs-playing');
            if (!product_video_pause) {
                reachYandexGoal('VIDEO');
                reachGoogleGoal('Video', 'Open', 'Open Video Part');
            }
        });

        product_video.on("pause", function () {
            $('#content').removeClass('vjs-playing');
            product_video_pause = true;
        });


        $('.product_menu ul li').click(function () {

        });

    }


    if ($('#node_video').length != 0) {

        node_video = videojs('node_video');

        node_video.on("play", function () {
            $('#content').addClass('vjs-playing');
        });

        node_video.on("pause", function () {
            $('#content').removeClass('vjs-playing');
        });
    }

//    кастом скроллбокс
    if ($('.scroll').length != 0) {
        $('.scroll').mCustomScrollbar({
            scrollButtons: {
                enable: true
            },
            theme: "dark",
            advanced: {
                updateOnContentResize: true,
                updateOnBrowserResize: true
            }
        });

        $('.openhere, .main_link_a').click(function (e) {
            console.log('UPDATE');

            setTimeout(function () {
                $('.scroll').mCustomScrollbar('update');
            }, 10);

        })
    }


//    кастом horizontal скроллбокс
    if ($('.h-scroll').length != 0) {
        $('.h-scroll').mCustomScrollbar({
            scrollButtons: {
                enable: true
            },
//            axis: "x",
            theme: "dark",
            horizontalScroll: true,
            mouseWheel: {
                enable: true,
                scrollAmount: 400
            },
            advanced: {
//                updateOnImageLoad: true,
                updateOnBrowserResize: true,
//                updateOnContentResize: true,
                updateOnSelectorChange: '.main_view_wrapper',
                autoExpandHorizontalScroll: true
            }
        });

        $('.openhere, .main_link_a').click(function (e) {
            console.log('UPDATE');

            setTimeout(function () {
                $('.h-scroll').mCustomScrollbar('update');
            }, 10);

        })
    }


//ДОПАОЛНИТЕЛЬНОЕ МЕНЮ
    $('.second_menu .menu_item.more').click(function () {
        $('ul.second_menu_more').stop(true, true).slideToggle('fast');
    });

    $('ul.second_menu_more, .second_menu .menu_item.more').mouseleave(function () {
        $('ul.second_menu_more').delay(200).slideUp();
    });

    $('ul.second_menu_more, .second_menu .menu_item.more').mouseenter(function () {
        $('ul.second_menu_more').stop(true, true).slideDown();
    });

//ВЫБОР ГОРОДА
    $('.cities .city').click(function () {
        $.ajax({
            url: '/set_city/' + $(this).attr('rel') + '/'
        }).done(function () {
            location.reload();
        });
    });

})
;

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function setCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function deleteCookie(name) {
    setCookie(name, "", -1);
}


function send_form() {
    Dajaxice.mi.flat_filter(Dajax.process, {'form': $('#flat_filter_form').serialize(true)});
}

