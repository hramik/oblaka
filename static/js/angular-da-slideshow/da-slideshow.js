/*
 * angular-elastic 0.0.1
 * (c) 2013 Da http://da.com
 * License: MIT
 */

var scripts = document.getElementsByTagName("script")
var currentScriptPath = scripts[scripts.length - 1].src;
//console.log(currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1) + 'slideshow.html');


angular.module('da.slideshow', [])
    .directive('daSlideshow', function () {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                previmg: '@',
                nextimg: '@',
                interval: '@'
            },
            controller: function ($scope, $element, $interval, $timeout, $attrs) {

                var slides = $scope.slides = [];
                $scope.slides_left = 0;
                $scope.window_width = window.innerWidth;
                $scope.is_prev = 1;
                $scope.is_next = 1;
                $scope.muted = 1;

                $scope.current_interval = $scope.interval;

                if ('autoplay' in $attrs) {
                    $timeout.cancel(nextTimer);
                    var nextTimer = $timeout(function () {
                        $scope.next();
                    }, parseInt($scope.current_interval));
                }


                //if ('autoplay' in $attrs) {
                //    $interval(function () {
                //        $scope.next();
                //    }, parseInt($scope.current_interval));
                //}


                $scope.select = function (slide, index) {
                    try {

                        angular.forEach(slides, function (slide) {
                            slide.active = false;
                        });
                        slide.active = true;
                        $scope.current_slide = slide;
                        $scope.slides_left = $scope.window_width * index * -1;

                        //console.log("SLIDE", $scope.current_slide.videoSlide, $scope.current_slide);

                        if ($scope.current_slide.videoSlide == '') {
                            var video = document.getElementById($('#' + $scope.current_slide.id).find('video').attr('id'));
                            video.currentTime = 0;
                            video.play();
                            $scope.mute();

                            $scope.current_interval = parseInt(video.duration) * 1000;
                        } else {
                            $scope.current_interval = $scope.interval;
                        }

                        if ('autoplay' in $attrs) {
                            $timeout.cancel(nextTimer);
                            nextTimer = $timeout(function () {
                                $scope.next();
                            }, parseInt($scope.current_interval));
                        }
                    } catch (err) {
                        console.log(err);
                    }

                };

                $scope.mute = function () {
                    $('#' + $scope.current_slide.id).find('video').prop('muted', true);
                    $scope.muted = 1;
                };

                $scope.sound = function () {
                    $('#' + $scope.current_slide.id).find('video').prop('muted', false);
                    $scope.muted = 0;
                };

                $scope.next = function () {
                    angular.forEach(slides, function (slide) {
                        slide.active = false;
                        if (slide.videoSlide == '') {
                            $('#' + slide.id).find('video').get(0).pause();
                            //$('#' + slide.id).find('video').get(0).currentTime = 0;
                            //$('#' + slide.id).find('video').get(0).start(0);

                        }
                    });
                    var next_slide_index = slides.indexOf($scope.current_slide) + 1;
                    if (next_slide_index == slides.length) {
                        var next_slide = slides[0];
                    } else {
                        var next_slide = slides[next_slide_index];
                    }


                    next_slide.active = true;
                    $scope.current_slide = next_slide;

                    if ($scope.current_slide.videoSlide == '') {
                        var video = document.getElementById($('#' + $scope.current_slide.id).find('video').attr('id'));
                        video.currentTime = 0;
                        video.play();
                        $scope.mute();

                        $scope.current_interval = parseInt(video.duration) * 1000;
                    } else {
                        $scope.current_interval = $scope.interval;
                    }

                    if ('autoplay' in $attrs) {
                        $timeout.cancel(nextTimer);
                        nextTimer = $timeout(function () {
                            $scope.next();
                        }, parseInt($scope.current_interval));
                    }

//                    if ($scope.slides_left <= $scope.window_width * slides.length * -1 + $scope.window_width) {
////                        $scope.slides_left = 0
//                    } else {
//                        $scope.slides_left = $scope.slides_left - $scope.window_width;
//                    }
//
//                    if ($scope.slides_left < $scope.window_width * slides.length * -1 + ($scope.window_width * 2)) {
//                        $scope.is_next = 0;
//                    } else {
//                        $scope.is_next = 1;
//                    }
//                    if (($scope.slides_left + $scope.window_width) > 0) {
//                        $scope.is_prev = 0;
//                    } else {
//                        $scope.is_prev = 1;
//                    }
                }

                $scope.prev = function () {
                    angular.forEach(slides, function (slide) {
                        slide.active = false;
                    });
                    var prev_slide_index = slides.indexOf($scope.current_slide) - 1;
                    if (prev_slide_index < 0) {
                        var prev_slide = slides[(slides.length - 1)];
                    } else {
                        var prev_slide = slides[prev_slide_index];
                    }
                    prev_slide.active = true;
                    $scope.current_slide = prev_slide;

                    if ($scope.current_slide.videoSlide == '') {
                        var video = document.getElementById($('#' + $scope.current_slide.id).find('video').attr('id'));
                        video.currentTime = 0;
                        video.play();
                        $scope.mute();

                        $scope.current_interval = parseInt(video.duration) * 1000;
                    } else {
                        $scope.current_interval = $scope.interval;
                    }

                    if ('autoplay' in $attrs) {
                        $timeout.cancel(nextTimer);
                        nextTimer = $timeout(function () {
                            $scope.next();
                        }, parseInt($scope.current_interval));
                    }

//                    if ($scope.slides_left >= 0) {
////                      $scope.slides_left = $scope.window_width * slides.length * -1 + $scope.window_width
//                    } else {
//                        $scope.slides_left = $scope.slides_left + $scope.window_width;
//                    }
//
//                    if ($scope.slides_left < $scope.window_width * slides.length * -1 + ($scope.window_width * 2)) {
//                        $scope.is_next = 0;
//                    } else {
//                        $scope.is_next = 1;
//                    }
//                    if (($scope.slides_left + $scope.window_width ) > 0) {
//                        $scope.is_prev = 0;
//                    } else {
//                        $scope.is_prev = 1;
//                    }
                }

                this.addSlide = function (slide) {
                    //console.log(slide);
                    if (slides.length == 0) $scope.select(slide, 0);
                    slides.push(slide);
                }
            },
            templateUrl: currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1) + 'slideshow.html',
            replace: true
        };
    })
    .directive('daSlide', function () {
        return {
            require: '^daSlideshow',
            restrict: 'E',
            transclude: true,
            scope: {
                title: '@',
                slideId: '@',
                id: '@',
                videoSlide: '@'
            },
            link: function (scope, element, attrs, da_slideshowCtrl) {
                da_slideshowCtrl.addSlide(scope);
                scope.window_width = window.innerWidth;

            },
            templateUrl: currentScriptPath.substring(0, currentScriptPath.lastIndexOf('/') + 1) + 'slide.html',
            replace: true
        };
    });