/**
 * Created by hramik on 29/10/14.
 */

'use strict';


angular.module('tritonProductDirectives', [])
    //.directive('product', function ($compile) {
    //    return {
    //        restrict: 'EA',
    //        controller: function ($scope, $element, $location, $http, Global) {
    //            $scope.global = Global;
    //
    //
    //
    //
    //        }
    //    };
    //})
    .directive('productinput', function ($compile) {
        return {
            restrict: 'A',
            controller: function ($scope, $element, $location, $http, Global) {
                $scope.global = Global;

                $scope.changePrice = function (nid, price, sites) {
                    console.log($element);
                    if (price.replace(/ /g, '') != '' && angular.element($element).find('input').hasClass('ng-dirty')) {
                        $http.post('http://' + $location.host() + ":" + $location.port() + '/change_price/' + nid, {price: price, sites: sites}).success(function (result) {
                            if (result == 'True') {
                                console.log("changeOptionPrice OK");
                                angular.element($element).addClass('changed');
                                console.log($element);
                            }
                        });
                    }
                }

                $scope.changeOptionPrice = function (id, price, sites) {
                    if (price.replace(/ /g, '') != '' && angular.element($element).find('input').hasClass('ng-dirty')) {
                        $http.post('http://' + $location.host() + ":" + $location.port() + '/change_option_price/' + id, {price: price, sites: sites}).success(function (result) {
                            if (result == 'True') {
                                console.log("changeOptionPrice OK");
                                angular.element($element).addClass('changed');
                                console.log($element);
                            }
                        });
                    }
                }

                $scope.changeSKU = function (nid, sku, sites) {
                    if (sku.replace(/ /g, '') != '' && angular.element($element).find('input').hasClass('ng-dirty')) {
                        $http.post('http://' + $location.host() + ":" + $location.port() + '/change_sku/' + nid, {sku: sku, sites: sites}).success(function (result) {
                            if (result == 'True') {
                                console.log("changeSKUPrice OK");
                                angular.element($element).addClass('changed');
                                console.log($element);
                            }
                        });
                    }
                }


            }
        };
    });