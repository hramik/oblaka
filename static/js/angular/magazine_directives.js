/**
 * Created by hramik on 29/10/14.
 */

'use strict';


angular.module('tritonMagazineDirectives', [])
    .directive('insert', function () {
        return {
            restrict: 'A',
            transclude: true,
            scope: {
                href: '@',
                rel: '@'
            },
            link: function ($scope, $element, $attrs) {
            },
//            template: '<li class="menu_item"  ng-class="{active:selected}"><a ng-transclude></a></li>',
            template: '<img src="oipip" />',
            replace: true
        };
    });