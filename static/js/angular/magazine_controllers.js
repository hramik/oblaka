'use strict';


angular.module('magazineControllers', ['ngResource'])

    .controller('magazineCtrl', function ($scope, Global, $http, $location, $compile, cfg) {
        $scope.global = Global;
        $scope.gracie = 1;


        //$scope.$watch('global.current_article_id', function (newValue, oldValue) {
        //
        //    console.log(newValue, oldValue);
        //
        //    if (newValue != oldValue) {
        //
        //        jQuery('.article_details .wrap').html("");
        //
        //        $http.get('http://' + $location.host() + ":" + $location.port() + '/magazine/get_article/' + $scope.global.current_article_id).success(function (data) {
        //            var element = $compile(data)($scope);
        //            jQuery('.article_details .wrap').prepend(element);
        //
        //            //$scope.global.current_article = element;
        //        });
        //    }
        //})


    })
    .controller('articleCtrl', function ($scope, Global, $http, $location, $document, cfg) {
        $scope.global = Global;
        $scope.gracie = 1;

        $scope.get_article = function () {
            $http.get('http://' + $location.host() + ":" + $location.port() + 'magazine/get_article/' + $scope.id).success(function (data) {
                $scope.current_article = data;
            });
        };

        $scope.open = function () {
            $scope.global.current_article_id = $scope.id;
            $scope.global.article_details_show = 1;
            //document.body.style.overflow = 'hidden';
            //$scope.global.path = $location.path();

            $location.path($scope.slug);
        };

        $scope.close = function () {
            $scope.global.article_details_show = 0;
            document.body.style.overflow = 'auto';
            $location.path($scope.global.path);
        };

    })
    .controller('solutionCtr', function ($scope, Global, $http, $location, $document, cfg) {
        $scope.global = Global;
        $scope.gracie = 1;

        $scope.send_form = function () {
            try {
                $scope.solution.additionalphoto = $scope.$$childHead.upload_images;
            }
            catch (err) {
                $scope.solution.additionalphoto = []
            }

            $http({
                method: 'POST',
                url: 'http://' + $location.host() + ":" + $location.port() + '/magazine/send_solution_order',
                data: $scope.solution,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                if (data == 'True') {
                    $scope.gracie = 1;
                    $scope.loader = 0;
                    $scope.$$childHead.upload_images = []
                }
            });
        }
    });
