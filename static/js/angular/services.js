//ID: ad741814828345d2a45c4e4701abe100
//Пароль: 7b37c5017cd648729efb43450f2492ac

angular.module('daServices', ['ngResource'])
    .factory('Question', function ($resource, $location, cfg) {
        return $resource($location.protocol() + '://' + $location.host() + cfg.PORT + '/api/questions/?format=json', {
            id: '@id',
            start: '@start',
            num: '@num'
        }, {
            all: {method: 'GET', params: {id: ''}}
        });
    })
    //.factory('Goals', function ($resource, $location, cfg) {
    //    return $resource('https://api-metrika.yandex.ru/:method/:id/goals', {
    //        id: '22100566'
    //    }, {
    //        create: {method: 'GET', params: {method: 'counter', }}
    //    });
    //})
    .factory('Orders', function ($resource, $location, cfg) {
        return $resource(
            $location.protocol() + '://' + $location.host() + ':' + $location.port() + '/api/orders/:id',
            {
                id: '@id',
                title: '@title',
                body: '@body'
            },
            {
                all: {method: 'GET', isArray: true},
                add: {method: 'POST'},
                delete: {method: 'DELETE'},
                save: {method: 'PUT'}
            });
    })
    .factory('Tags', function ($resource, $location) {
        console.log($location.protocol() + '://' + $location.host() + ':' + $location.port() + '/crud/tags');
        return $resource('/crud/tags/', {}, {
            all: {method: 'GET', isArray: true}
        });
    })
//    .factory('Comments', function ($resource, cfg) {
//        return $resource(cfg.API_URL + 'comments/:id/:start/:num/:method', {
//            id: '@id',
//            start: '@start',
//            num: '@num',
//            method: ''
//        }, {
//            get: {method: 'GET', cache : false},
//            edit: {method: 'PUT'},
//            delete_comment: {method: 'POST', try_method: 'delete'},
//            save: {method: 'POST', params: {object_id: '@object_id', text: '@text'}}
////            post: {method: 'POST', params: {uid: '1', text: '@text'}}
//        });
//    })
    .factory('Global', function ($http) {
        return {
            callback_form_show: '0',
            quick_order_form_show: '0',
            credit_order_form_show: '0',
            comment_form_show: '0',
            quick_product: "",
            cart: ""
        };
    });
;


//http://estatistics.ru/mcu/callback/index.php?r=friends/delete/uid/777
//http://estatistics.ru/mcu/callback/index.php?r=friends/get/fnum/16/start/0/