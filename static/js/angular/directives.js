'use strict';


angular.module('daDirectives', [])
    .directive("scroll", function () {
        return function (scope, element, attrs) {
            angular.element(element).bind("scroll", function () {
                scope[attrs.scroll] = true;
                scope.$apply();
            });
        };
    })
    .directive('colorbox', function () {

        return {
            restrict: 'AC',
            link: function (scope, element, attrs) {
                $(element).colorbox(attrs.colorbox);
            }
        };
    })
    .directive('uiDropdown', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                console.log('dropdown');
                element.dropdown();
            }
        };
    })
    .directive('daModal', function () {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attrs) {

                $element.bind('click', function () {
                    angular.element($attrs.daModal)
                        .modal({
                            onDeny: function () {
                                return true;
                            },
                            onApprove: function () {
                                console.log($scope, 'true');
                                return true;
                            }
                        })
                        .modal('setting', 'duration', 250)
                        .modal('show')

                    ;
                });

            }
        };
    })
    .directive('daModalClick', function () {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attrs) {
                $element.bind('click', function () {
                    angular.element($attrs.daModal)
                        .modal({
                            onDeny: function () {
                                return true;
                            },
                            onApprove: function () {
                                console.log($scope, 'true');
                                return true;
                            },
                            onVisible: function () {
                                $('img.lazzy').visiblity('refresh');
                            }
                        })
                        .modal('setting', 'duration', 250)
                        .modal('show')
                    ;
                });

            }
        };
    })
    .directive('iconSelect', function ($compile) {
        return {
            restrict: 'AC',
            //transclude: true,
            scope: {
                name: "@"
            },
            link: function ($scope, $element, $attrs, Global) {
                $scope.global = Global;

                $scope.pay_form = $scope.$parent.pay_form;
                $scope.delivery = $scope.$parent.delivery;

                var icons = [];
                $element.find('option').each(function () {

                    if ($(this).val() != "") {
                        var val = $(this).val();
                        var icon = {
                            "val": val,
                            "text": $(this).text(),
                            "icon": $("#icon-data .icon[slug=" + val + "]").attr('icon'),
                            "body": $("#icon-data .icon[slug=" + val + "]").text(),
                            "price": $("#icon-data .icon[slug=" + val + "]").attr('price')
                        };
                        icons.push(icon);
                    }
                });

                $scope.items = angular.fromJson(icons);
                console.log($scope.items);

                $scope.set_val = function (val, price) {
                    if ($scope.name == 'pay_type') {
                        $scope.pay_form.pay_type = val;
                    }
                    if ($scope.name == 'delivery') {
                        $scope.pay_form.delivery = val;
                        //$scope.pay_form_delivery_price = price;
                    }
                    if ($scope.name == 'prepay') {
                        $scope.pay_form.prepay = val;
                    }
                };

                $scope.setPopup = function () {
                    $('.is_popup').popup();
                }

                var html = '' +
                    '<div class="icons paymants ui stackable five column center aligned grid" ng-click="save();">' +
                    '<div class="column is_popup"  ng-init="setPopup();" ng-repeat="item in items" ng-click="set_val(item.val, item.price);" ng-class="{active: pay_form.pay_type == item.val || pay_form.delivery == item.val || pay_form.prepay == item.val}" data-title="{? item.text ?}" data-content="{? item.body ?}">' +
                    '<div class="icon-{? item.icon | lowercase ?} alarm huge " style="margin-bottom: 1rem; font-size: 4rem;"></div> <div class="text">{? item.text ?}</div> <div class="price" ng-if="item.price">{? item.price ?}</div>' +

                    '</div>' +
                    '</div>' +
                    '<div class="ui divider" style="margin-top:3rem;"></div>';
                var e = $compile(html)($scope);
                $element.after(e);
            }
            //template: '<div>бубубу</div>',
            //replace: true
        };
    })
    .directive('daLineCalendar', function ($compile) {
        return {
            restrict: 'AC',
            scope: {},
            link: function ($scope, $element, $attrs, Global) {
                $scope.global = Global;
                var now = moment();

                if ($scope.$parent.pay_form.delivery_date != '') {
                    $scope.curent_date = $scope.$parent.pay_form.delivery_date;
                } else {
                    $scope.$parent.pay_form.delivery_date = moment().format('DD.MM.YYYY');
                    $scope.curent_date = $scope.$parent.pay_form.delivery_date;
                }

                console.log($attrs, $scope);

                $scope.days = [];

                for (var i = 0; i <= 14; i++) {
                    var nextday = moment().add(i, 'days');
                    var day = {
                        "day": nextday.date(),
                        "weekday": nextday.locale('ru-ru').format('ddd'),
                        "en_weekday": nextday.locale('en').format('ddd').toLowerCase(),
                        "plus": i,
                        "date": nextday.format('DD.MM.YYYY')
                    };
                    $scope.days.push(day);
                }


                //console.log($scope.days);

                $scope.set_date = function (date) {
                    $scope.$parent.pay_form.delivery_date = date;
                    $scope.curent_date = date;
                };

                var html = '<div class="da-calendar">' +
                    '<div class="day {? day.en_weekday ?}" ng-click="set_date(day.date);" ng-repeat="day in days" ng-class="{current: curent_date==day.date}"><div' +
                    ' class="weekday">{?day.weekday?}</div><div' +
                    ' class="daynum">{?day.day?}</div></div>' +
                    '</div>' +
                    '<div class="ui divider"></div>';
                var e = $compile(html)($scope);
                $element.after(e);
            }
        };
    })
    .directive('daesc', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind("keyup", function (event) {
                    if (event.which === 27) {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngEsc);
                        });

                        event.preventDefault();
                    }
                });
            }
        }
    })
    .directive('iframecolorbox', function () {
        return {
            restrict: 'AC',
            link: function (scope, element, attrs) {
                $(element).colorbox({iframe: true, innerWidth: 800, innerHeight: 600});
            }
        };
    })
    //.directive('inlines', function () {
    //    return {
    //        restrict: 'E',
    //        scope: {},
    //        controller: function ($scope, $element, $location, $http, $compile, Global) {
    //            $scope.global = Global;
    //            var inlines = $scope.inlines = [];
    //
    //
    //            this.addInline = function (inline) {
    //                inlines.push(inline);
    //            }
    //        }
    //    };
    //})
    //.directive('inline', function () {
    //    return {
    //        restrict: 'E',
    //        //require: '^inlines',
    //        //link: function ($scope, $element, $attrs, da_inlinesCtrl) {
    //        //    da_inlinesCtrl.addInline($scope);
    //        //},
    //        controller: function ($scope, $rootScope, $element, $location, $http, $compile, Global) {
    //            $scope.global = Global;
    //
    //            $scope.clone = function () {
    //                var new_inline = $($element).clone();
    //                new_inline.find('input').val("");
    //                var element = $compile(new_inline)($scope);
    //                $('.inlines_wrapper').append(new_inline);
    //            }
    //
    //            $scope.remove = function () {
    //                $element.remove();
    //            }
    //        }
    //    };
    //})
    .directive('hrScrollLoad', function () {
        return {
            restrict: 'A',
            scope: {
                class: '@'
            },
            transclude: true,
            replace: true,
            template: '<div class="{? class ?}" infinite-scroll="load_next_page()" infinite-scroll-distance="2" ng-transclude></div>',
            controller: function ($scope, Global, $http, $location, cfg, $compile, $element) {
                $scope.global = Global;
                $scope.global.next_page = 1;
                $scope.global.end_list = false;


                $scope.load_next_page = function (insert_to) {
                    if ($scope.global.busy || $scope.global.end_list) return;

                    $scope.global.busy = true;


                    $http({
                        method: 'POST',
                        url: $location.absUrl(),
                        params: {
                            'page': $scope.global.next_page,
                            'tag': $scope.global.current_tag_slug
                        },
                        headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                            if (data == 'end') {
                                $scope.global.end_list = true;
                                $scope.global.busy = false;
                            } else {
                                $scope.global.next_page = $scope.global.next_page + 1;
                                $scope.global.busy = false;

                                var compile_data = $compile(data)($scope);

                                angular.element($element).append(compile_data);
                                $("a:not([target]):not('.no_target')").attr('target', '_self');

                            }

                        }, function error(response) {
                            $scope.global.end_list = true;
                        }
                    );
                }
            }
        };
    })

    .directive('hrTableScrollLoad', function () {
        return {
            restrict: 'A',
            scope: {
                class: '@',
                path: '@'
            },
            transclude: true,
            replace: true,
            template: '<tbody class="{? class ?}" infinite-scroll="load_next_page()" infinite-scroll-distance="2" ng-transclude></tbody>',
            controller: function ($scope, Global, $http, $location, cfg, $compile, $element) {
                $scope.global = Global;
                $scope.next_page = 2;
                $scope.end_list = false;


                $scope.load_next_page = function (insert_to) {
                    if ($scope.busy || $scope.end_list) return;

                    $scope.busy = true;

                    var url = $location.absUrl();

                    if (typeof $scope.path !== 'undefined') {
                        var url = $location.protocol() + "://" + $location.host() + ":" + $location.port() + $scope.path;
                        if($.param( $location.search(), true ).length > 0){
                            url = url + "?" + $.param( $location.search(), true );
                        }
                    }


                    $http({
                        method: 'POST',
                        url: url,
                        params: {
                            'page': $scope.next_page,
                            'tag': $scope.current_tag_slug
                        },
                        headers: {'Content-Type': 'application/json'}
                    }).success(function (data) {
                            if (data == 'end') {
                                $scope.end_list = true;
                                $scope.busy = false;
                            } else {
                                $scope.next_page = $scope.next_page + 1;
                                $scope.busy = false;

                                var compile_data = $compile(data)($scope);

                                angular.element($element).append(compile_data);
                                $("a:not([target]):not('.no_target')").attr('target', '_self');

                            }

                        }, function error(response) {
                            $scope.end_list = true;
                        }
                    );
                }
            }
        };
    })
    .directive('ngInitial', function ($parse) {
        return {
            restrict: "A",
            link: function ($scope, $element, $attrs, Global) {
                var initialValue = $attrs.value || $element.val();
                if ($element.attr('type') == 'checkbox' && $element.attr('checked') == 'checked') {
                    var initialValue = true;
                } else if ($element.attr('type') == 'checkbox') {
                    var initialValue = false;
                } else if ($element.is('select')) {
                    var initialValue = $element.find("option[selected=selected]").val();
                }
                $parse($attrs.ngModel).assign($scope, initialValue);
            }
        }
    })
    .directive('hrNotCacheInclude', function ($compile, $templateCache) {
        return {
            restrict: 'A',
            scope: {
                href: "=" //filterby is a separate field used for data binding
            },
            compile: function ($element, $attrs, $scope) {
                var templateName = $scope.href;
                //console.log($scope.href);
                //console.log(templateName);
                var template = $templateCache.get(templateName);
                //console.log(template);
                //$element.html(template);
            }
        };
    });