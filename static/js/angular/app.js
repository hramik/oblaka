'use strict';

var da_app = angular.module('daApp', [
    //'ngAnimate',
    //'ngRoute',
    'ngSanitize',
    'ngTouch',
    'daServices',
    'daDirectives',
    'daControllers',
    'magazineControllers',
    'tritonProductDirectives',
    //'tritonMagazineDirectives',
    'daFilters',
    'daCfg',
    'daTabor',
    'ui.bootstrap',
    'ng-iscroll',
    'monospaced.elastic',
    'angularFileUpload',
    'infinite-scroll',
    'ui.slider',
    'ui.bootstrap',
    'da.slideshow',
    'angularSpectrumColorpicker'
    //'yaMap',
    //'datePicker',
    //'ng-bootstrap-datepicker'
    //'ng-bootstrap-datepicker'
    //'com.2fdevs.videogular',
    //"com.2fdevs.videogular.plugins.controls",
    //"com.2fdevs.videogular.plugins.overlayplay",
    //"com.2fdevs.videogular.plugins.poster"
], function ($interpolateProvider, $locationProvider, $httpProvider, $compileProvider) {
    //$compileProvider.debugInfoEnabled(false);

    $interpolateProvider.startSymbol('{?');
    $interpolateProvider.endSymbol('?}');


    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: true
    });

    $httpProvider.defaults.useXDomain = true;

    delete $httpProvider.defaults.headers.common['X-Requested-With'];
});

angular.module('daCfg', [])
    .constant('cfg', {
        'API_URL': 'http://mcu.sweetdrinks.ru/callback/index.php/api2/',
        'PORT': ':8000'
    })

