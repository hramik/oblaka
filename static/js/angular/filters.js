angular.module('daFilters', [])
    .filter('capitalize', function () {
        return function (input, $scope) {
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
    })
    .filter('price', function () {
        return function (input, $scope) {
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
    })
    .filter('newlines', function () {
        return function (text) {
            return text.replace(/\n/g, "<br />");
        }
    })
    .filter('deletespace', function () {
        return function (input, $scope) {
            try {
                return input.replace(" ", "");
            } catch (e) {
                return ""
            }
        }
    });
