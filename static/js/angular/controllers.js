'use strict';

//var API_URL = 'http://ayvazovsky.webfactional.com/';
var API_URL = 'http://127.0.0.1:8000/';
//var API_URL = 'http://triton_ua.in.ua/';
//var API_URL = 'http://triton_ua.webfactional.com/';
var PORT = ':8000';
//var PORT = '';
var all_products = "";


angular.module('daControllers', ['ngResource'])

    .controller('menuCtr', function ($scope, Global, $http, $location, cfg) {
        $scope.global = Global;
    })
    .controller('imgCtr', function ($scope, Global, $http, $location, cfg) {
        $scope.global = Global;
    })
    .controller('appCtrl', function ($scope, Global, $http, $location, cfg) {
        $scope.global = Global;

        //http://www.3tn.ru/check_login


        $scope.datepickerOptions = {
            format: 'dd.mm.yyyy',
            container: "#container",
            language: 'ru',
            inline: true,
            sideBySide: true,
            todayBtn: true
        };

        //$http({
        //    method: 'GET',
        //    url: 'http://www.3tn.ru/check_login'
        //}).success(function (data) {
        //    console.log("MULTILOGIN", data);
        //});

        $scope.logKeys = function ($rootScope, $event) {

            if ($event.keyCode == 27) {
                $scope.global.article_details_show = 0;
                $scope.global.main_view_1 = 0;
                $scope.global.solution_order_form_modal_show = 0;
                $scope.global.admin_panel_open = 0;
                $scope.global.discount_desc_modal_show = 0;
                $scope.global.quick_order_form_modal_show = 0;
                $scope.global.callback_form_modal_show = 0;
                $scope.global.map_show = 0;

                $scope.global.blockdesc.show = 0;
                $scope.global.blockdesc.url = '';

                document.body.style.overflow = 'auto';
                $location.path($scope.global.path);
            }
        };

        $scope.setPopup = function () {
            console.log('popup');
            $('.is_popup').popup();
        };

        $scope.SelectAll = function (id) {
            document.getElementById(id).focus();
            document.getElementById(id).select();
        };

        $scope.LazzyRefresh = function () {
            console.log('refresh')
            jQuery('img.lazzy')
                .visibility({
                    type: 'image',
                    transition: 'fade in',
                    duration: 1000,
                    once: false
                });
        };

        $scope.reachYandexGoal = function (name) {
            reachYandexGoal(name);
        };


        $scope.reachGoogleGoal = function (category, event, label) {
            reachGoogleGoal(category, event, label);
        };

        $scope.refreshModal = function () {
            console.log('refresh');
            $('.ui.modal').modal('refresh');
        };

        $scope.hideModals = function () {
            $('.ui.modal')
                .modal('hide all')
            ;
        };

        $scope.changeURL = function (url) {
            $location.path(url, false);
        }


//    WATCHERS

        //$scope.$watch('global.quick_order_form_modal_show', function (newValue, oldValue) {
        //    if (newValue == 1 || newValue == true) {
        //        reachYandexGoal('OPEN_ONE_CLICK_BUY_FORM');
        //        ga('Oneclick', 'Open', 'Open Product In One Click');
        //    }
        //});
        //
        //
        //$scope.$watch('global.callback_form_modal_show', function (newValue, oldValue) {
        //    if (newValue == 1 || newValue == true) {
        //        reachYandexGoal('OPEN_ADVICE_FORM');
        //        ga('Advice', 'Open', 'Open Advice Form');
        //    }
        //});

    })
    .controller('tagsCtrl', function ($scope, Global, $http, $location, cfg, Tags, $compile) {
        $scope.global = Global;
        $scope.tags = Tags.all();
    })
    .controller('tagCtrl', function ($scope, Global, $http, $location, cfg, Tags, $compile) {
        $scope.global = Global;

        $scope.load_tag = function () {
            $scope.global.next_page = 1;

            var tag = this;

            $scope.global.current_tag = tag.name;
            $scope.global.current_tag_slug = tag.slug;

            $http({
                method: 'POST',
                url: $location.absUrl(),
                params: {
                    'page': $scope.global.next_page,
                    'tag': tag.slug
                },
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                    console.log(data);
                    if (data == 'end') {
                        console.log('end');
                        $scope.global.end_list = true;
                        $scope.global.busy = false;
                    }
                    else if (data.replace(/\s+/g, '') == '') {
                        angular.element('.questions_view').html("");
                        $scope.global.end_list = true;
                        $scope.global.busy = false;
                    }
                    else {
                        $scope.global.next_page = $scope.global.next_page + 1;
                        $scope.global.busy = false;

                        var compile_data = $compile(data)($scope);

                        angular.element('.questions_view').html(compile_data);
                        $scope.global.end_list = false;
                    }


                }, function error(response) {
                    console.log('error');
                    $scope.global.end_list = true;
                }
            );
        }
    })


    // Списки
    .controller('listCtrl', function ($scope, Global, $http, $location, cfg, Tags, $compile) {
        $scope.global = Global;
        $scope.global.next_page = 2;
        $scope.global.end_list = false;

        $scope.next_page = function (insert_to) {

            if (typeof insert_to === 'undefined') insert_to = ".item_view";

            if ($scope.global.busy || $scope.global.end_list) return;
            $scope.global.busy = true;
            $http({
                method: 'POST',
                url: $location.absUrl() + '?page=' + $scope.global.next_page,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                $scope.global.next_page = $scope.global.next_page + 1;
                $scope.global.busy = false;
                var element = $compile(data)($scope);
                jQuery(insert_to).append(element);
            }, function error(response) {
                $scope.global.end_list = true;
            });
        }
    })
    .controller('questionCtrl', function ($scope, Global, $http, $location, $filter, $compile, cfg, Question, $dialog) {
        $scope.global = Global;
        $scope.gracie = 1;
        $scope.upload_images = [];
        $scope.global.next_page = 2;
        $scope.global.end_question = false;

//    $scope.questions = Question.all();

        $scope.reload = function () {
            location.reload();
        }


        $scope.action = function (question_id) {
            console.log($scope.global.selected_comment_id, $scope.global.action);
            if ($scope.global.action == 'comment_transfer') {

                var title = 'Перенос комментария в вопрос';
                var msg = 'Вы действительно хотите перенести комментарий комментарий ' + $scope.global.selected_comment_id + ' в вопрос ' + question_id + '?';
                var btns = [
                    {result: 'no', label: 'Нет'},
                    {result: 'yes', label: 'Да', cssClass: 'btn-primary'}
                ];

                $dialog.messageBox(title, msg, btns)
                    .open()
                    .then(function (result) {
                        if (result == 'yes') {
                            $scope.comment = {}
                            $scope.comment.comment_id = $scope.global.selected_comment_id
                            $scope.comment.question_id = question_id

                            $http({
                                method: 'POST',
                                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/comment_transfer',
                                data: $scope.comment,
                                headers: {'Content-Type': 'application/json'}
                            }).success(function (data) {
                                if (data) {
                                    jQuery(".comment_" + $scope.global.selected_comment_id).hide();
                                    var comment_count = parseInt(jQuery(".simple_question_" + question_id).find('.comment_count').text()) + 1;
                                    jQuery(".simple_question_" + question_id).find('.comment_count').text(comment_count);
                                }
                            });
                        }
                    });

            }
        }


//    $scope.change_image = function ($files) {
//        var $file = $files[0];
////        for (var $file in $files) {
//        console.log($file);
//        $http.uploadFile({
//            url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + "/image_upload", //upload.php script, node.js route, or servlet upload url
//            // headers: {'optional', 'value'}
////            data: {myObj: $scope.myModelObj},
//            file: $file
//        }).progress(function (evt) {
////            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
//        }).then(function (data, status, headers, config) {
//            // file is uploaded successfully
//            $scope.upload_image_show = 1;
//            $scope.upload_images.push(data);
//            $scope.$apply();
//        });
//    }
////    }
//
//    $scope.delete_image = function (element, $index) {
//        $scope.upload_images.splice($index, 1);
//    }

        $scope.send_form = function () {
            console.log($scope);

            try {
                $scope.question.additionalphoto = $scope.$$childHead.upload_images;
            }
            catch (err) {
                $scope.question.additionalphoto = []
            }


            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/send_question',
                data: $scope.question,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                if (data.user != 'anonym') {
                    $scope.global.last_question_body = $scope.question.body;
                    $scope.global.last_question_date = $filter('date')(new Date(), 'd.M.yyyy hh:mm');
                    $scope.global.last_question_show = 1;

                    $scope.question.body = ''
                    $scope.question.author = ''
                    $scope.question.email = ''
                    $scope.$$childHead.upload_images = []

                    $scope.loader = 0;
//                $scope.gracie = 1;
                    var element = $compile(data)($scope);
                    jQuery('.questions_view').prepend(element);

                } else {
                    $scope.global.login_modal_desc = "Ваш вопрос появится на сайте после проверки его модератором. Чтоб размещать вопросы без модерации войдите под своим" +
                        " логином или зарегистрируйтесь."
                    $scope.global.login_modal_show = 1;
                    $scope.loader = 0;
                }


            });
        }
    })
    .controller('mainCtrl', function ($scope, Global, $http, $location) {
        $scope.global = Global;

        $scope.open_main_view = function (view_id) {
            $('.h-scroll').mCustomScrollbar('update');
        }
    })
    .controller('modalCtrl', function ($scope, Global, $http, $location) {
        $scope.global = Global;
    })
    .controller('profileCtrl', function ($scope, Global, $http, $location) {
        $scope.global = Global;


        $scope.change_avatar = function ($files) {
            var $file = $files[0];
            $http.uploadFile({
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + "/profile_change_avatar", //upload.php script, node.js route, or servlet upload url
                // headers: {'optional', 'value'}
//            data: {myObj: $scope.myModelObj},
                file: $file
            }).progress(function (evt) {
//            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).then(function (data, status, headers, config) {
                // file is uploaded successfully
                $scope.upload_image_show = 1;
                $scope.global.avatar = data;
                $scope.$apply();

            });
        }


    })
    .controller('uploadimagesCtrl', function ($scope, Global, $http, $location, $filter, cfg, $compile, $dialog) {
        $scope.global = Global;
        $scope.upload_images = [];

        $scope.change_image = function ($files) {
            var $file = $files[0];
            console.log("UPLOAD");
//        for (var $file in $files) {
            $http.uploadFile({
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + "/image_upload", //upload.php script, node.js route, or servlet upload url
                // headers: {'optional', 'value'}
//            data: {myObj: $scope.myModelObj},
                file: $file
            }).progress(function (evt) {
//            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).then(function (data, status, headers, config) {
                // file is uploaded successfully
                $scope.upload_image_show = 1;
                $scope.upload_images.push(data);
                $scope.$apply();

            });
        }
//    }

        $scope.delete_image = function (element, $index) {
            $scope.upload_images.splice($index, 1);
        }

    })
    .controller('commentCtrl', function ($scope, Global, $http, $location, $filter, cfg, $compile, $dialog) {
        $scope.global = Global;
        $scope.upload_images = [];


        $scope.reload = function () {
            location.reload();
        };

        $scope.like = function (comment_id) {
            $scope.comment_like = !$scope.comment_like;


            $scope.comment_like_post = {}
            $scope.comment_like_post.comment_id = comment_id


            if ($scope.comment_like == 1) {
                $http({
                    method: 'POST',
                    url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_comment_like',
                    data: $scope.comment_like_post,
                    headers: {'Content-Type': 'application/json'}
                }).success(function (data) {
//                    $scope.comment_like = !$scope.comment_like;
                    if (data) {
                        $scope.comment_like_count += 1;
                    }
                });
            } else {
                $http({
                    method: 'POST',
                    url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/remove_comment_like',
                    data: $scope.comment_like_post,
                    headers: {'Content-Type': 'application/json'}
                }).success(function (data) {
//                    $scope.comment_like = !$scope.comment_like;
                    $scope.comment_like_count -= 1;
                });
            }
        };


        $scope.to_question = function (comment_id) {


            var title = 'Превращение комментария в вопрос';
            var msg = 'Вы действительно хотите превратить комментарий ' + comment_id + ' в вопрос?';
            var btns = [
                {result: 'no', label: 'Нет'},
                {result: 'yes', label: 'Да', cssClass: 'btn-primary'}
            ];

            $dialog.messageBox(title, msg, btns)
                .open()
                .then(function (result) {
                    if (result == 'yes') {
                        $scope.comment = {}
                        $scope.comment.comment_id = comment_id

                        $http({
                            method: 'POST',
                            url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/to_question',
                            data: $scope.comment,
                            headers: {'Content-Type': 'application/json'}
                        }).success(function (data) {
                            if (data) {
                                jQuery(".comment_" + comment_id).hide();
                                var element = $compile(data)($scope);
                                jQuery('.questions_right_view').prepend(element);
                            }
                        });
                    }
                });
        };

        $scope.send_form = function () {
//        $scope.comment.node.node_id = 8;

            try {
                $scope.comment.additionalphoto = $scope.$$childHead.upload_images;
            }
            catch (err) {
                $scope.comment.additionalphoto = []
            }

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/send_comment',
                data: $scope.comment,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                try {
                    $scope.global.last_comment_body = $scope.comment.body;
                    $scope.global.last_comment_date = $filter('date')(new Date(), 'd.M.yyyy hh:mm');
                    $scope.global.last_comment_show = 1;
                    $scope.global.last_comment_q_id = $scope.comment.node;

                    $scope.comment.$setPristine();
                    //$scope.comment.body = '';
                    //$scope.comment.author = '';
                    //$scope.comment.email = '';
                    $scope.$$childHead.upload_images = [];
                } catch (err) {
                    console.log(err);
                }


//                $scope.gracie = 1;
                $scope.loader = 0;

                var element = $compile(data)($scope);
                jQuery('.comments_for_node_' + $scope.comment.node).append(element);
                console.log('.view_row_' + $scope.comment.node);
                jQuery('.view_row_' + $scope.comment.node).removeClass('publish_False');
            });
        }
    })
    .controller('productCommentCtrl', function ($scope, Global, $http, $location, $filter, cfg, $compile) {
        $scope.global = Global;

        $scope.reload = function () {
            location.reload();
        };

        $scope.next_comment = function () {
        };

        $scope.send_form = function () {
//        $scope.comment.node.node_id = 8;
            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/send_product_comment',
                data: $scope.comment,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                $scope.global.last_comment_body = $scope.comment.body;
                $scope.global.last_comment_date = $filter('date')(new Date(), 'd.M.yyyy hh:mm');
                $scope.global.last_comment_show = 1;
                $scope.global.last_comment_q_id = $scope.comment.node;

                $scope.comment.body = ''
                $scope.comment.author = ''
                $scope.comment.email = ''

//                $scope.gracie = 1;
                $scope.loader = 0;
                $scope.global.comment_form_show = 0;

                var element = $compile(data)($scope);
                angular.element('.comments_view').append(element);
                angular.element(".scroll").mCustomScrollbar("scrollTo", "bottom");
            });
        }
    })
    .controller('toTop', function ($scope, Global, $http, $location, cfg, $anchorScroll) {
        $scope.global = Global;

        $scope.to_top = function () {
            $location.hash('container');
            $anchorScroll();
        }
    })
    .controller('callbackCtr', function ($scope, Global, $http, $location, cfg) {
        $scope.global = Global;

        $scope.set_current = function () {
            $scope.global.callback = this;
        };

        $scope.done = function (status) {

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/change_callback_status',
                data: {
                    'callback_id': $scope.global.callback.id,
                    'manager_comment': $scope.global.callback.manager_comment,
                    'status': status
                },
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {

                console.log(data);
                if (data == 'W') {
                    $scope.global.callback.status = 'W';
                } else {
                    $scope.global.callback.status = 'D';
                }
            });
        };

        $scope.send_form = function () {
//        $scope.comment.node.node_id = 8;
            try {
                $scope.callback.product_id = $scope.global.callback.product_id;
                reachY
                andexGoal("ADVICE");
                reachGoogleGoal('Advice', 'Send', 'Send Advice Form');
            } catch (err) {
                reachYandexGoal("CALLBACK");
                reachGoogleGoal('Callback', 'Send', 'Send Callback Form');
            }

            try {
                $scope.callback.call_value = call_value
            } catch (err) {
                $scope.callback.call_value = ''
            }


            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/send_callback',
                data: {
                    'callback': $scope.callback,
                    'call_value': $scope.callback.call_value
                },
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                console.log(data);
                if (data == 'True') {
                    $scope.gracie = 1;
                    $scope.loader = 0;
                }
            });
        }
    })
    .controller('addProductCtrl', function ($scope, Global, $http, $location, cfg, $window, $interval, Orders) {
        $scope.global = Global;

    })
    .controller('ordersCtrl', function ($scope, Global, $http, $location, cfg, $window, $interval, Orders) {
        $scope.global = Global;
        //
        //
        //
        //    $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/order_list/' + order_id).success(function (data) {
        //        var element = $compile(data)($scope);
        //        angular.element('#order_list').html(element);
        //        $scope.global.complectation_loading = false;
        //    });
        //
    })
    .controller('orderCtr', function ($scope, Global, $http, $location, cfg, $window, $interval, $filter, $anchorScroll, $compile) {
        $scope.global = Global;


        $scope.$watch('pay_form.delivery', function (newValue, oldValue) {
            try {
                $scope.global.cart_delivery_slug = $scope.pay_form.delivery;
                $scope.save();
            } catch (err) {

            }
        });

        $scope.global.loadProducts = function (order_id, entity) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_products/' + order_id + '/' + entity).success(function (data) {
                var element = $compile(data)($scope);
                angular.element('#add_products').html(element);
            });
        };

        $scope.global.get_order = function (order_id) {
            if (typeof order_id !== 'undefined') {
                $scope.global.complectation_loading = true;

                $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_order/' + order_id).success(function (data) {
                    $scope.global.cart = data;
                    console.log($scope.global.cart);
                });

                $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/order_list/' + order_id).success(function (data) {
                    var element = $compile(data)($scope);
                    angular.element('#order_list').html(element);
                    $scope.global.complectation_loading = false;
                });
            }
        };

        //
        //
        //$scope.$watch('pay_form.prepay', function (newValue, oldValue) {
        //    $scope.save();
        //});
        //
        //$scope.$watch('pay_form.pay_type', function (newValue, oldValue) {
        //    $scope.save();
        //});
        //$scope.$watch('pay_form.delivery_date', function (newValue, oldValue) {
        //    $scope.save();
        //});
        //$scope.$watch('pay_form.carrier_number', function (newValue, oldValue) {
        //    $scope.save();
        //});

        $scope.$watchCollection('pay_form', function (newValue, oldValue) {
            $scope.save();
        });

        $scope.$watchCollection('quick_order', function (newValue, oldValue) {
            $scope.save();
        });


        $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
            $scope.global.cart = data;
            //$scope.global.cart_delivery_sum = 0;


            $interval(function () {
                try {

                    var current_delivery = $('#id_delivery').val();

                    $scope.global.cart_delivery_sum = $("#icon-data .icon[slug=" + current_delivery + "]").attr('price');

                    if ($scope.pay_form.prepay == 'prepay_30') {
                        $scope.global.cart_prepay = 0.3;
                    } else if ($scope.pay_form.prepay == 'prepay_full') {
                        $scope.global.cart_prepay = 1;

                    } else if ($scope.pay_form.prepay == 'prepay_zero') {
                        $scope.global.cart_prepay = 1;

                    }

                    if (parseFloat($scope.global.cart_delivery_sum)) {
                        $scope.global.all_sum = Math.round(parseFloat($scope.global.pay_price) + parseFloat($scope.global.cart_delivery_sum));
                    } else {
                        $scope.global.all_sum = Math.round(parseFloat($scope.global.pay_price));
                    }

                    if ($scope.global.cart_prepay == 1) {
                        $scope.global.cart_prepay_sum = Math.round(parseFloat($scope.global.pay_price) * $scope.global.cart_prepay + parseFloat($scope.global.cart_delivery_sum));
                    } else {
                        $scope.global.cart_prepay_sum = Math.round(parseFloat($scope.global.pay_price) * $scope.global.cart_prepay);
                    }
                    $scope.global.cart_pay_sum = Math.round(parseFloat($scope.global.pay_price) * $scope.global.cart_prepay + parseFloat($scope.global.cart_delivery_sum));
                } catch (err) {
                }

                //console.log(parseFloat($scope.global.pay_price), $scope.global.cart_prepay, parseFloat($scope.global.cart_delivery_sum), $scope.global.cart_prepay_sum);
            }, 10);

        });


        $scope.set_agree_make_review = function () {
            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/set_agree_make_review/' + $scope.agree_make_review,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                    $scope.global.cart = data;
                });
                $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_current_price/' + $scope.global.current_product_id).success(function (data) {
                    $scope.$parent.$parent.$parent.current_price = data;
                    $scope.global.current_price = data;
                });
            });
        };

        $scope.change_status = function (status) {

            console.log($scope.order_id, status);

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/change_status',
                data: {
                    'status': status,
                    'order_id': $scope.order_id
                },
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                console.log('success');
            });
        };

        //SEND ORDER FORM
        $scope.save = function () {
            //$scope.pay_form.delivery_date = $scope.pay_form.delivery_date_pick;
            //$scope.pay_form.delivery_date = $filter('date')($scope.pay_form.delivery_date_pick, 'dd.MM.yyyy');
            //console.log($scope.pay_form.delivery_date_pick, $scope.pay_form.delivery_date);

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/save_order',
                data: {
                    'pay_form': $scope.pay_form,
                    'order_form': $scope.quick_order,
                    'order_id': $scope.order_id
                },
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                //console.log(data);
                if (data.result != 'False') {
                    //$scope.gracie = 1;
                    $scope.loader = 0;

                    //$location.hash('container');
                    //$anchorScroll();
                    $scope.global.message = 'Заказ сохранен';
                    $scope.global.get_order($scope.order_id);
                    //$('message').fadeIn(1000).delay(3000).fadeOut(1000);
                }
            });
        };

        //SEND TO DEALER
        $scope.to_dealer = function () {

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/to_diler/' + $scope.id,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                console.log(data);
                if (data != 'False') {
                    //$scope.gracie = 1;
                    $scope.loader = 0;

                    $location.hash('container');
                    $anchorScroll();
                    $scope.global.message = 'Заказ отправлен дилеру';
                    $('message').fadeIn(1000).delay(3000).fadeOut(1000);
                }
            });
        };

        //SEND TO DPD
        $scope.to_dpd = function () {

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/to_dpd/' + $scope.id,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                console.log(data);
                if (data != 'False') {
                    //$scope.gracie = 1;
                    $scope.loader = 0;

                    $location.hash('container');
                    $anchorScroll();
                    $scope.global.message = data;
                    $('message').fadeIn(1000);
                }
            });
        };

        //SEND TO PREPAY
        $scope.to_prepay = function () {

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/to_prepay/' + $scope.id,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                console.log(data);
                if (data.result != 'False') {
                    //$scope.gracie = 1;
                    $scope.loader = 0;

                    $location.hash('container');
                    $anchorScroll();
                    $scope.global.message = 'Письмо с сылкой на оплату заказа отправлено покупателю';
                    $('message').fadeIn(1000).delay(3000).fadeOut(1000);
                }
            });
        };

        //SEND ORDER FORM
        $scope.send_form = function () {

            $scope.quick_order.product_id = $scope.global.quick_order.product_id;

            if ($scope.global.quick_order.product_id != 0) {
                reachYandexGoal('ONE_CLICK_BUY');
                reachGoogleGoal('Oneclick', 'Send', 'Buy Product In One Click');

            } else {
                reachYandexGoal('ORDER');
                reachGoogleGoal('Cart', 'Order', 'Make Order');
            }


            try {
                console.log(call_value)
                var callvalue = call_value;
            } catch (err) {
                var callvalue = '';
            }


            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/send_order',
                data: {
                    'order': $scope.quick_order,
                    'call_value': callvalue
                },
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {

                if (data.result != 'False') {
                    $scope.gracie = 1;
                    $scope.loader = 0;
                    $scope.global.cart.cart_sum = 0;
                    $scope.global.cart.cart_count = 0;
                    $('.cart_view').empty();
                }

                if (data.result == 'yandex') {
                    //$('#order_number').val(data.order_number);
                    //$('#payment_type').val(data.payment_type);
                    //$('#cps_phone').val($('#id_phone').val());
                    //$('#cps_email').val($('#id_email').val());
                    //$('#customer_number').val(data.customer_number);
                    //$('#quick_order_form').submit();
                }
            });
        };

        $scope.send_quick_order_form = function () {

            //yaCounter22100566.reachYandexGoal('ADD_TO_CARD');

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/send_quick_order_form',
                data: $scope.quick_order,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                if (data == 'True') {
                    $scope.gracie = 1;
                    $scope.loader = 0;
                }
            });
        }
    })
    .controller('creditCtr', function ($scope, Global, $http, $location) {
        $scope.global = Global;
        $scope.send_form = function () {
            $scope.gracie = 1;
            $scope.loader = 0;
        }
    })
    .controller('trideCtrl', function ($scope, Global, $http, $location, $dialog) {
        $scope.global = Global;


        $scope.$watch('global.tride_show', function (newValue, oldValue) {
            if (newValue == 1) {
                $('body').addClass('bottom_scroll');
            } else {
                $('body').removeClass('bottom_scroll');
            }
        });

        $scope.delete_tride = function (id) {

            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/delete_tride',
                data: {
                    "id": id
                },
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                if (data) {
                    jQuery(".tride_" + id).hide();
                }
            });

            var title = 'Удаление планировки';
            var msg = 'Вы действительно хотите удалить планировку?';
            var btns = [
                {result: 'no', label: 'Нет'},
                {result: 'yes', label: 'Да', cssClass: 'btn-primary'}
            ];

            $dialog.messageBox(title, msg, btns)
                .open()
                .then(function (result) {
                    if (result == 'yes') {
                        $scope.tride = {}
                        $scope.tride.id = id


                    }
                });
        }
    })
    .controller('mapCtr', function ($scope, Global, $http, $location) {
        $scope.global = Global;

        $scope.center = [38.53269, 67.752395];
        $scope.type = 'yandex#map';
        $scope.zoom = '10';

//    var map;
//    $scope.afterMapInit = function (map) {
//        map = map;
//    };
//    $scope.del = function () {
//        map.destroy();
//    };
//
//    $scope.changeCenter = function () {
//        $scope.center = [40.925358, 57.767265];
//    };
//
//    $scope.replace = function () {
////        $scope.type='yandex#publicMapHybrid';
//        map.panTo([34.461, 62.915], {
//            // Задержка перед началом перемещения.
//            delay: 1500
//        });
//    };


        var map;
        $scope.afterMapInit = function (map) {
            map = map;
        };

        $scope.mapGoal = function () {
            reachYandexGoal('MAP');
            reachGoogleGoal('Map', 'Open', 'Open Map With Shops');
        }

        $scope.init = function (map) {

            reachYandexGoal('MAP');
            reachGoogleGoal('Map', 'Open', 'Open Map With Shops');


            $.getScript('http://api-maps.yandex.ru/2.1/?coordorder=longlat&lang=ru_RU&onload=init_map');


        };

        $scope.load_saleplaces_menu = function (region_id) {

            $http.get('/saleplaces_menu/' + region_id).success(function (data) {
                $scope.global.saleplaces_menu = data;
                $scope.global.saleplaces_menu_show = 1;

                $('.scroll').mCustomScrollbar({
                    scrollButtons: {
                        enable: true
                    },
                    theme: "dark",
                    advanced: {
                        updateOnContentResize: true,
                        updateOnBrowserResize: true
                    }
                });
            });
        }

        $scope.$watch('global.map_show', function (newValue, oldValue) {
            if (newValue == 1) {
                $('body').addClass('bottom_scroll');
            } else {
                $('body').removeClass('bottom_scroll');
            }
        })

        $scope.showMap = function (address, coords, zoom) {

            if (zoom) {
                var zoom = zoom;
            } else {
                var zoom = 12;
            }


            if (coords) {
                var region = coords;
                var firstGeoObject = fromString(coords);
                window.myMap.panTo(firstGeoObject, {flying: true})
                    .then(function () {
                        window.myMap.setZoom(zoom, {duration: 500});
                    })
            } else {
                ymaps.geocode(address, {results: 1, searchCoordOrder: 'latlong'}).then(function (res) {
                    var firstGeoObject = res.geoObjects.get(0);
                    window.myMap.panTo(res.geoObjects.get(0).geometry.getCoordinates(), {flying: true})
                        .then(function () {
                            window.myMap.setZoom(zoom, {duration: 500});
                        })
                });
            }


            $scope.global.map_show = 1;

        }

        function fromString(val) {
            var parts = val.split(',');
            return [parseFloat(parts[1]), parseFloat(parts[0])];
        }
    })
    .controller('jsonCtrl', function ($scope, Global, $http, $location) {
        $scope.global = Global;

        $http.get('/media/docs/3tn.ru.json').success(function (data) {
            $scope.phones = data;
            $scope.test_json = $scope.phones[1];
        });
    })
    .controller('searchCtrl', function ($scope, Global, $http, $location) {
        $scope.global = Global;
    })
    .controller('priceCtrl', function ($scope, Global, $http, $location, cfg) {
        $scope.global = Global;

        //Set agree_make_review to global value
        //$scope.$watch('global.cart.agree_make_review', function (newValue, oldValue) {
        //    $scope.agree_make_review = $scope.global.cart.agree_make_review
        //});

        $scope.get_current_price = function (id, order_id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_current_price/' + id + '/' + order_id).success(function (data) {
                $scope.global.current_price = data;
                $scope.cur_price = data.cart_sum;
            });
        }
    })
    .controller('optionsCtrl', function ($scope, Global, $http, $location, cfg, $compile, $interpolate, $parse, $templateCache, $interval) {
        // Init Global
        $scope.global = Global;
        $scope.global.options = $scope;

        // Delay for update 3D
        $interval(function () {
            $scope.global.options.change3dOptions();
            if ($scope.global.current_product_menu == 'options') {
            }
        }, 1000);

        // SET THIS OPTION TO CURRENT
        $scope.setCurrent = function () {
            $scope.global.current_option = this;
        };

        // REMOVE VIDEO CACHE
        $scope.initVideojs = function () {
            $templateCache.removeAll();
        };

        // UPDATE 3D
        $scope.change3dOptions = function () {

            var options = [];

            //      COLLECT ACTIVE OPTIONS
            for (var group = $scope.$$childHead; group; group = group.$$nextSibling) {
                for (var option = group.$$childHead; option; option = option.$$nextSibling) {
                    if (option.active) {
                        options.push(option.slug);
                    }
                }
            }

            //      SEND OPTIONS TO 3D
            $('#ModelViewerInner').each(function () {
                //console.log('Here');
                if (this.updateOptions) {
                    //console.log('Here2');
                    this.updateOptions(options);
                }
                //console.log('Done');
                //this.showHideToolbar(true);
            });
        };

        $scope.checkDependent = function () {

            for (var group = $scope.$$childHead; group; group = group.$$nextSibling) {
                for (var option = group.$$childHead; option; option = option.$$nextSibling) {

                    var to_available = true;

                    if (option.dependent.length > 0) {
                        option.dependent.forEach(function (option_slug) {
                            var item = $scope.findBySlug(option_slug);
                            if (!item.active) {
                                to_available = false;
                            }
                        });
                    }

                    if (option.groupdependent.length > 0) {
                        option.groupdependent.forEach(function (group_slug) {
                            option.dependent.forEach(function (option_slug) {
                                if (option_slug == group_slug && $(".product_option." + option_slug).length == 0) {
                                    to_available = true;
                                }
                            });
                            var item = $scope.findGroupBySlug(group_slug);
                            if (item.active_option == "") {
                                to_available = false;
                            }
                        });
                    }

                    if (option.excludes.length > 0) {
                        option.excludes.forEach(function (option_slug) {
                            var item = option.findBySlug(option_slug);
                            if (item.active) {
                                to_available = false;
                            }
                        });
                    }

                    if (option.price == 0 && $scope.is_shop && option.none_price != 'True') {
                        to_available = false;
                    }

                    option.available = to_available;
                }
            }

        };

        $scope.findBySlug = function (slug) {

            for (var group = $scope.$$childHead; group; group = group.$$nextSibling) {
                for (var option = group.$$childHead; option; option = option.$$nextSibling) {
                    if (option.slug == slug) {
                        return option;
                    }
                }
            }
            return false;

        };

        $scope.findGroupBySlug = function (slug) {
            for (var group = $scope.$$childHead; group; group = group.$$nextSibling) {
                if (group.slug == slug) {
                    return group;
                }
            }
            return false;
        };


    })
    .controller('optionGroupCtrl', function ($scope, Global, $http, $location, cfg, $compile, $interpolate, $parse) {

        $scope.global = Global;

//        ALL CHILD OF OPTION GROUP
//    for (var cs = $scope.$$childHead; cs; cs = cs.$$nextSibling) {
//    }

    })
    .controller('optionCtrl', function ($scope, Global, $http, $location, cfg, $compile, $interpolate, $parse, $templateCache) {
        $scope.global = Global;
        $scope.dependent = [];
        $scope.groupdependent = [];
        $scope.excludes = [];
        $scope.groupexcludes = [];
        $scope.load = false;


        $scope.onOption = function () {
            $scope.$parent.active_option = $scope;
        };


        //    INIT OPTION
        $scope.initOption = function () {
            if ($scope.dependent.length > 0 || $scope.groupdependent.length > 0) {
                $scope.available = false;
            } else {
                $scope.available = true;
            }
        };

        //    WATCH AVAILEBLES
        $scope.$watch('available', function (newValue, oldValue) {
            if (newValue == false) {
                $scope.inactiveOption();
                $scope.global.options.checkDependent();
            }
        });

        //    ADD TO DEPENDENT
        $scope.addDependent = function (dependent_option_slug) {
            $scope.dependent.push(dependent_option_slug);
        };

        //    ADD TO EXCLUDE
        $scope.addExclude = function (exclude_option_slug) {
            $scope.excludes.push(exclude_option_slug);
        };

        //    ADD TO GROUP DEPENDENT
        $scope.addGroupDependent = function (dependent_group_slug) {
            $scope.groupdependent.push(dependent_group_slug);
        };

        //    ADD TO GROUP EXCLUDE
        $scope.addGroupExclude = function (exclude_group_slug) {
            $scope.groupexcludes.push(exclude_group_slug);
        };


        //    CLICK TO OPTION
        $scope.changeOption = function () {
            if ($scope.available) {

                if (!$scope.active) {
                    if ($scope.$parent.slug != 'none' && $scope.$parent.active_option && $scope.$parent.active_option != $scope) {
                        $scope.$parent.active_option.changeOption();
                    }

                    $scope.$parent.active_option = $scope;
                }

                if ($scope.active) {
                    $scope.$parent.active_option = false;
                }

                $scope.active = !$scope.active;

                $scope.sendToServer(true);
                //$scope.$parent.$parent.change3dOptions();
                $scope.global.options.change3dOptions();
                $scope.global.options.checkDependent();

            }
        };

        //    CLICK TO BASE COMPLECTATION
        $scope.changeBase = function () {
            $scope.active = !$scope.active;
        };

        //    SET OPTION TO ACTIVE
        $scope.activeOption = function () {

            $scope.active = true;
            $scope.$parent.active_option = $scope;

            $scope.sendToServer(false);

            //$scope.$parent.$parent.change3dOptions();
            $scope.global.options.change3dOptions();
        };

        //    SET OPTION TO INACTIVE
        $scope.inactiveOption = function () {

            $scope.active = false;
            $scope.$parent.active_option = false;

            $scope.sendToServer(false);

            //$scope.$parent.$parent.change3dOptions();
            $scope.global.options.change3dOptions();
        };


        //  SEND TO SERVER
        $scope.deleteFromOrder = function (option_id, order_id, product_id) {
            //$scope.load = true;

            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_option_to_order/' + order_id + '/' + option_id + '/' + product_id + '/false').success(function (result) {
                if (result) {
                    $scope.global.get_order(order_id);
                    $scope.load = false;
                }
            });
        };

        //  SEND TO SERVER
        $scope.sendToServer = function (update) {
            //$scope.load = true;

            console.log($scope.global.order_id);

            if (typeof $scope.global.order_id !== 'undefined') {

                $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_option_to_order/' + $scope.global.order_id + "/" + $scope.id + '/' + $scope.product_id + '/' + $scope.active).success(function (result) {
                    if (result && update) {
                    }

                    if (result) {
                        $scope.global.get_order($scope.global.order_id);
                        $scope.load = false;
                    }
                });

            } else {

                $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_option_to_session/' + $scope.id + '/' + $scope.product_id + '/' + $scope.active).success(function (result) {
                    if (result && update) {
                        $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                            $scope.global.cart = data;
                        });
                        $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_current_price/' + $scope.product_id).success(function (data) {
                            $scope.$parent.$parent.$parent.current_price = data;
                            $scope.global.current_price = data;
                        });
                    }

                    if (result) {
                        $scope.load = false;
                    }
                });
            }

        }
    })
    .controller('filterItemsCtrl', function ($scope, Global, $http, $location, cfg, $compile, $interpolate, $parse) {
        $scope.global = Global;
    })
    .controller('filterCtrl', function ($scope, Global, $http, $location, cfg, $compile, $interpolate, $parse) {
            $scope.global = Global;

            if (all_products == "") {
            }

            $scope.filterProducts = function () {
                if (!$scope.filter && $scope.cats > 0) {
                    $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + $location.path() + '?clear').success(function (data) {
                        all_products = $compile(data)($scope);
                        jQuery('#content').html(all_products);
                        jQuery('img.lazzy')
                            .visibility({
                                type: 'image',
                                transition: 'fade in',
                                duration: 1000
                            });
                    });
                }
                $scope.filter = true;
            }

        }
    ).controller('filterItemCtrl', function ($scope, Global, $http, $location, cfg, $compile, $interpolate, $parse) {
        $scope.global = Global;


    })
    .controller('productsCtrl', function ($scope, Global, $http, $location, cfg, $compile, $interpolate, $parse) {
        $scope.global = Global;

        $scope.$watch('global.cart_ids', function (newValue, oldValue) {
            try {
                $scope.global.cart_ids_arr = $scope.global.cart_ids.split(',');
            } catch (err) {
            }
        });

        $scope.$watch('global.comparsion_ids', function (newValue, oldValue) {
            try {
                $scope.global.comparsion_ids_arr = $scope.global.comparsion_ids.split(',');
            } catch (err) {
            }
        });

        $scope.$watch('global.favorite_ids', function (newValue, oldValue) {
            try {
                $scope.global.favorite_ids_arr = $scope.global.favorite_ids.split(',');
            } catch (err) {
            }
        });

        $scope.filterProducts = function (category) {
        }
    })
    .controller('productCtrl', function ($scope, Global, $location, $http, $compile, $element) {
        $scope.global = Global;

        $scope.$watch('id', function (newValue, oldValue) {
            try {
                if ($scope.global.cart_ids_arr.indexOf($scope.id) >= 0) {
                    $scope.in_cart = 1;
                }
                if ($scope.global.comparsion_ids_arr.indexOf($scope.id) >= 0) {
                    $scope.in_comparsion = 1;
                }
                if ($scope.global.favorite_ids_arr.indexOf($scope.id) >= 0) {
                    $scope.in_favorite = 1;
                }
            } catch (err) {
            }
        });


        // $scope.$watch('current_slide', function (newValue, oldValue) {
        //     var current_slide = $(".product_slide.index_"+newValue.replace("photo_", "")+" img");
        //     current_slide.css('left', '50%').css('position', 'absolute').css('margin-left', "-" + current_slide.width / 2 + "px");
        // });

        //Set agree_make_review to global value
        //$scope.$watch('global.cart.agree_make_review', function (newValue, oldValue) {
        //    if (newValue != oldValue) {
        //        console.log("PIPI")
        //        $scope.get_price();
        //
        //    }
        //});

        $scope.$watch('global.current_product_menu', function (newValue, oldValue) {
            try {
                $('.product_thumbs').attr('tab', newValue);
            } catch (e) {
            }
        });

        $scope.filter = function (group_name, filter_name) {
            return true;
        };

        $scope.menu_click = function (menu_name) {
            console.log("out from ", menu_name);
            $scope.global.current_product_menu = menu_name;
            product_video.pause();
        };

        $scope.bigger3D = function () {
            $('#ModelViewerInner').attr('width', '500px').attr('height', '450px');
        };

        $scope.smaller3D = function () {
            $('#ModelViewerInner').attr('width', '200px').attr('height', '150px');
        };

        // GET PRICE CURRENT PRODUCT
        $scope.get_price = function () {
            //$http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
            //    $scope.global.cart = data;
            //});
            //console.log("GET_PRICE", $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_current_price/' + $scope.id);
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_current_price/' + $scope.id).success(function (data) {
                //console.log(data);
                $scope.current_price = data;
                $scope.global.current_price = data;
            });
        };


        $scope.addCart = function () {
            reachYandexGoal('ADD_TO_CARD');
            reachGoogleGoal('Cart', 'AddTo', 'Add Product To Cart');

            //GAEvent('cart', 'to_cart', 'send', 'null');

            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_to_cart/' + $scope.id).success(function (result) {
                //console.log(result);
                if (result == 'True') {
                    $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                        $scope.global.cart = data;
                        $scope.cur_price = data;
                    });
                }
            });
        };

        $scope.deleteFromCart = function () {
            //console.log($scope.title);
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/delete_from_cart/' + $scope.id).success(function (result) {
                if (result == 'True') {
                    $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                        $scope.global.cart = data;
                        $($element).addClass('out_cart');
                    });
                }
            });
        };

        //        ADD PRODUCT TO ORDER
        $scope.addToOrder = function (order_id) {

            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_to_order/' + order_id + '/' + $scope.id).success(function (result) {
                if (result) {

                    $scope.global.get_order(order_id);
                    $scope.load = false;
                }
            });
        };

        //DELETE FROM ORDR
        $scope.deleteFromOrder = function (order_id) {
            //console.log($scope.title);
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/delete_from_order/' + order_id + '/' + $scope.id).success(function (result) {
                if (result == 'True') {
                    $($element).addClass('out_cart');
                    $scope.global.get_order(order_id);
                }
            });
        };

        //LOAD PRODUCT OPTIONS
        $scope.loadOptions = function (product_id) {
            $scope.global.complectation_loading = 1;
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/product_options/' + product_id).success(function (data) {
                var element = $compile(data)($scope);
                angular.element('.product_part-options').html(element);
                $scope.global.complectation_loading = 0;
                $scope.global.options.change3dOptions();
            });
        };

        //LOAD PRODUCT OPTIONS FOR ORDER
        $scope.loadOrderOptions = function (order_id, product_id) {
            $scope.global.complectation_loading = 1;
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/product_order_options/' + order_id + '/' + product_id).success(function (data) {
                var element = $compile(data)($scope);
                angular.element('.product_part-options').html(element);
                $scope.global.complectation_loading = 0;
                $scope.global.options.change3dOptions();
            });
        };

        $scope.clearAllOptions = function (product_id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/clear_all_option_from_session/' + product_id).success(function (result) {
                if (result == 'True') {

                    $scope.loadOptions(product_id);

                    $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                        $scope.global.cart = data;
                    });

                    $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_current_price/' + product_id).success(function (data) {
                        $scope.global.current_price = data;
                    });
                }
            });
        };

        $scope.loadOptionsComplectation = function (product_id, complectation_id) {
            $scope.global.complectation_loading = 1;

            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/load_options_complecation/' + product_id + '/' + complectation_id).success(function (result) {
                if (result == 'True') {

                    $scope.loadOptions(product_id);

                    $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_cart').success(function (data) {
                        $scope.global.cart = data;
                    });

                    $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/get_current_price/' + product_id).success(function (data) {
                        $scope.global.current_price = data;
                    });


                }
            });
        };


        //$scope.changePrice = function (price) {
        //    //$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        //    //var xsrf = $.param({price: "1000"});
        //    console.log(price);
        //    $http({
        //        method: 'POST',
        //        url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/change_price/' + $scope.id,
        //        data: {
        //            price: price
        //        },
        //        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        //    }).success(function (result) {
        //        if (result == 'True') {
        //        }
        //    });
        //}


        $scope.addFavorite = function () {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_to_favorite/' + $scope.id).success(function (result) {
                if (result == 'True') {
                }
            });
        };

        $scope.deleteFavorite = function () {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/delete_from_favorite/' + $scope.id).success(function (result) {
                if (result == 'True') {
                }
            });
        };

        $scope.addComparsion = function () {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/add_to_comparsion/' + $scope.id).success(function (result) {
                if (result == 'True') {
                }
            });
        };

        $scope.deleteComparsion = function () {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/delete_from_copmarsion/' + $scope.id).success(function (result) {
                if (result == 'True') {
                }
            });
        }
    })
    .controller('productinputCtrl', function ($scope, Global) {
        $scope.global = Global;
    })
    .controller('videoCtrl', function ($scope, Global) {
        $scope.global = Global;
    })
    .controller('adminCtrl', function ($scope, Global) {
        $scope.global = Global;
    })
    .controller('pageCtrl', function ($scope, Global) {
        $scope.global = Global;

        $scope.global.bathcolor = '#ffffff'

        $scope.$watch('global.bathcolor', function (newValue, oldValue) {
            console.log(newValue, oldValue);
            try {
                draw(newValue);
            } catch (err) {
            }
        });

    })
    .controller('seoCtrl', function ($scope, Global, $http, $location) {
        $scope.global = Global;

        $scope.send_form = function () {
            $http({
                method: 'POST',
                url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/change_meta',
                data: $scope.seo,
                headers: {'Content-Type': 'application/json'}
            }).success(function (data) {
                $scope.loader = 0;
                location.reload();
            });
        }
    })
    .controller('usersCtrl', function ($scope, Global, $element) {
        $scope.global = Global;
    })
    .controller('userCtrl', function ($scope, Global, $element, $http, $location) {
        $scope.global = Global;

        $scope.activate = function (id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/advuser/activate/' + $scope.type + '/' + $scope.id).success(function (data) {
                if (data == 'True') {
                    $scope.active = true;
                }
            });
        }

        $scope.activate_seller = function (id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/advuser/activate_seller/' + $scope.type + '/' + $scope.id + '/' + $scope.fee_value).success(function (data) {
                if (data == 'True') {
                    $scope.active = true;
                }
            });
        }

        $scope.deactivate = function (id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/advuser/deactivate/' + $scope.type + '/' + $scope.id).success(function (data) {
                if (data == 'True') {
                    $scope.active = false;
                }
            });
        }


        $scope.approve = function (project_id, user_id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/constructors/project/approve/' + project_id + '/' + user_id).success(function (data) {
                if (data == 'True') {
                    $scope.approve = true;
                }
            });
        }

        $scope.deapprove = function (project_id, user_id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/constructors/project/deapprove/' + project_id + '/' + user_id).success(function (data) {
                if (data == 'True') {
                    $scope.approve = false;
                }
            });
        }
        $scope.delete = function (id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/advuser/delete/' + $scope.type + '/' + $scope.id).success(function (data) {
                if (data == 'True') {
                    $($element).remove();
                }
            });
        }
    })
    .controller('itemCtrl', function ($scope, Global, $element, $http, $location, $templateCache) {
        $scope.global = Global;


        $scope.getPartial = function () {
            return $scope.global.blockdesc.slug;
        }

        $scope.approve_report = function (id) {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/advuser/approve_report/' + $scope.id).success(function (data) {
                if (data == 'True') {
                    $scope.approve = true;
                }
            });
        }
    })
    .controller('inlinesCtrl', function ($scope, Global, $element) {
        $scope.global = Global;
    })
    .controller('inlineCtrl', function ($scope, Global, $element, $compile) {
        $scope.global = Global;


        $scope.clone = function () {
            var new_inline = $($element).clone();
            new_inline.find('input').each(function () {
                $(this).val('');
                var old_id = $scope.global.total_forms - 1;
                var new_id = $(this).attr('id').replace(old_id, $scope.global.total_forms);
                var new_name = $(this).attr('name').replace(old_id, $scope.global.total_forms);
                $(this).attr('id', new_id);
                $(this).attr('name', new_name);
            });


            $scope.global.total_forms += 1;

            var element = $compile(new_inline)($scope);
            element.insertAfter($element)
        }

        $scope.remove = function () {
            $element.remove();
        }
    })
    .controller('projectsCtrl', function ($scope, Global, $element, $http, $location) {
        $scope.global = Global;
    })
    .controller('editorCtrl', function ($scope, Global, $element, $http, $location, $document) {
        $scope.global = Global;

    })
    .controller('editorButtonCtrl', function ($scope, Global, $element, $http, $location, $document) {
        $scope.global = Global;

        $scope.button_click = function () {
            if ($scope.command == 'image') {
                var img = '<div class="field_img" style="width: 1120px; height: 500px;"><img src="/media/cache/6c/15/6c151f2ce38e17bb771d66c09d4babe4.png" alt="" title=""></div>';
                document.execCommand("insertHTML", false, img);
            } else if ($scope.command == 'save') {
                $scope.article = {};
                $scope.article.title = angular.element('.article_details .field_title').html();
                $scope.article.teaser = angular.element('.article_details .field_teaser').html();
                $scope.article.body = angular.element('.article_details .field_body').html();

                $http({
                    method: 'POST',
                    url: $location.protocol() + '://' + $location.host() + ":" + $location.port() + '/magazine/save_article',
                    data: $scope.article,
                    headers: {'Content-Type': 'application/json'}
                }).success(function (data) {
                    if (data == 'True') {
                    }
                });
            } else {
                document.execCommand($scope.command, false, null);
            }
        }
    })
    .controller('projectCtrl', function ($scope, Global, $element, $compile, $http, $location) {
        $scope.global = Global;

        $scope.apply = function () {
            $http.get($location.protocol() + '://' + $location.host() + ":" + $location.port() + '/constructors/project/apply/' + $scope.id).success(function (data) {
                if (data == 'True') {
                    $scope.in_apply = true;
                }
            });
        }
    })
    .controller('showMessage', function ($scope, Global, $anchorScroll, $location, message) {
        $scope.global = Global;

        $location.hash('container');
        $anchorScroll();
        $scope.global.message = message;
        $('message').fadeIn(1000).delay(3000).fadeOut(1000);
    });


