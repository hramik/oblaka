# -*- coding: utf-8 -*-

import datetime, sys, os, re
from django.utils.encoding import force_unicode

from django.db import models
from django.forms import ModelForm
from django.db import models
from django import forms

from seo.models import *

'''
Форма комментария
'''


class MetaForm(ModelForm):
    path = forms.CharField(
        widget=forms.HiddenInput(
            attrs={
                'class': '',
                'ng-model': 'seo.path',
                'ng-initial': 'True',
                'ng-required': 'True',
            }
        ),
        label=u""
    )

    title = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'seo.title',
                'ng-initial': 'True',
                'ng-required': 'False',
            }
        ),
        label=u"Заголовок",
        required=False
    )

    second_title = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'seo.second_title',
                'ng-initial': 'True',
                'ng-required': 'False',
            }
        ),
        label=u"Подзаголовок",
        required=False
    )

    h1 = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'seo.h1',
                'ng-initial': 'True',
                'ng-required': 'False',
            }
        ),
        label=u"H1",
        required=False
    )

    h2 = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'seo.h2',
                'ng-initial': 'True',
                'ng-required': 'False',
            }
        ),
        label=u"H2",
        required=False
    )

    page_title = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'seo.page_title',
                'ng-initial': 'True',
                'ng-required': 'False',
            }
        ),
        label=u"Мета Заголовок",
        required=False
    )


    top_text = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': '',
                'ng-model': 'seo.top_text',
                'ng-required': 'False',
                'ng-initial': 'True',
                'msd-elastic': 'true',
                'rows': '3',
            }
        ),
        label=u"Невидимый код в верх body",
        required=False
    )

    meta_desc = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class': '',
                'ng-model': 'seo.meta_desc',
                'ng-required': 'False',
                'ng-initial': 'True',
                'msd-elastic': 'true',
                'rows': '3',
            }
        ),
        label=u"Мета дескриптион",
        required=False
    )

    meta_keywords = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'seo.meta_keywords',
                'ng-initial': 'True',
                'ng-required': 'False',
            }
        ),
        label=u"Мета кейворды",
        required=False
    )

    region_path = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class': '',
                'ng-model': 'seo.region_path',
                'ng-initial': 'True',
                'ng-required': 'False',
            }
        ),
        label=u"Region URL",
        required=False
    )


    noindex = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class': '',
                'ng-model': 'seo.noindex',
                'ng-initial': 'True',
            }
        ),
        required=False,
        label=u"Noindex"
    )


    class Meta:
        model = SEOFields
        fields = ('page_title', 'meta_desc', 'meta_keywords', 'h1', 'h2', 'title', 'second_title', 'region_path', 'noindex', 'top_text')


