# -*- coding: utf-8 -*-
__author__ = 'hramik'

from ipgeo.models import Range

import logging
from django.conf import settings

from node.models import Node
from products.models import Product
from questions.models import Question

from seo.models import *
from seo.forms import *
from node.models import SEONode
from tri.models import Page


def meta(request):
    # node =
    # print Node.on_site.get(dot_slug=request.path)
    try:
        # seo_meta = PathSEO.on_site.get(path=request.path)
        if request.path == '/':
            seo_meta = SEONode.on_site.filter(dot_slug=request.path)[:1].get()
        else:
            seo_meta = SEONode.on_site.filter(dot_slug=request.path[1:])[:1].get()
        page_title = seo_meta.page_title if seo_meta.page_title else seo_meta.title
        second_title = seo_meta.second_title

        print seo_meta.noindex

        meta_form = MetaForm(
            initial={
                'path': request.path[1:],
                'title': seo_meta.title,
                'second_title': seo_meta.second_title,
                'h1': seo_meta.h1,
                'h2': seo_meta.h2,
                'page_title': seo_meta.page_title,
                'meta_desc': seo_meta.meta_desc,
                'meta_keywords': seo_meta.meta_keywords,
                'region_path': seo_meta.region_dot_slug,
                'top_text': seo_meta.top_text,
                'noindex': seo_meta.noindex,
            }
        )
    except Exception, e:
        meta_form = MetaForm()
        seo_meta = "%s %s" % (Exception, e)
        second_title = False
        page_title = False
        noindex = False
        meta_form = MetaForm(
            initial={
                'path': request.path[1:],
            }
        )

    return {
        'page_title': page_title,
        'second_title': second_title,
        'path': request.path,
        'seo_meta': seo_meta,
        'meta_form': meta_form,
    }
