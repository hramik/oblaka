# -*- coding: utf-8 -*-

import os
import re

from django.db import models
from django.db.models import Q, F

from django.shortcuts import get_object_or_404

import json
import datetime
import random
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import get_current_site


from itertools import chain
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.template import RequestContext

from seo.models import *
from node.models import SEONode, NodeType
from custom_sites.models import SiteInfo

from seo.forms import *


def change_meta(request):
    result = False
    if request.method == 'POST':

        form = json.loads(request.body)
        meta_form = MetaForm(form)


        # TODO: edit seonode!
        if meta_form.is_valid():
            result = 'valid'
            # seo = PathSEO.on_site.get(path=form['path'])
            try:
                seo = SEONode.on_site.get(dot_slug=form['path'])
                seo.edit_user=request.user.username
                seo.save()
            except:
                # seo = SEONode(dot_slug=form['path'])
                # if not seo:
                # TODO: add site fields
                node_type = NodeType.objects.get(id=11)
                seo = SEONode(h1=form['h1'], h2=form['h2'], dot_slug=form['path'], region_dot_slug=form['region_path'], meta_desc=form['meta_desc'], top_text=form['top_text'], noindex=form['noindex'],
                              meta_keywords=form['meta_keywords'], node_type = node_type, user=request.user, edit_user=request.user.username)
                seo.save()
                # current_site = SiteInfo.on_site.get()
                current_site = SiteInfo.objects.select_related().get(site__domain=request.META['HTTP_HOST'])
                seo.sites.add(current_site.site)
                result = 'save'

            meta_form = MetaForm(form, instance=seo)
            m_form = meta_form.save()


            # result = True

    return HttpResponse(result)

