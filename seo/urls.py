# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse


urlpatterns = patterns('',
                       url(r'^change_meta$', 'seo.views.change_meta', name="change_meta"),
)
