# -*- coding: utf-8 -*-
__author__ = 'hramik'

from django.db import models
from django.utils.translation import gettext_lazy as _
from autoslug import AutoSlugField
from django.core.validators import RegexValidator
from django.contrib.auth.models import User

from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager



# from managers import *

class SEOFields(models.Model):
    '''
    SEO fields
    '''
    page_title = models.CharField(
        max_length=200,
        blank=True,
        null=True,
        help_text=_("for windows title"),
        default=''
    )

    meta_desc = models.TextField(
        blank=True,
        null=True,
        help_text=_("for meta description"),
        verbose_name=_("Meta Description"),
        default='')

    meta_keywords = models.CharField(
        max_length=1024,
        blank=True,
        null=True,
        default=''
    )


    class Meta():
        abstract = True



class PathSEO(SEOFields):
    '''
    SEO for path
    '''
    path = models.CharField(
        max_length=255,
    )

    slug = models.CharField(
        default='',
        blank=True,
        max_length=255,
    )

    sites = models.ForeignKey(Site)
    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return "%s %s" % (self.path, self.page_title)













