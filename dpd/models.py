# -*- coding: utf-8 -*-
__author__ = 'm13v246'

import logging
import sys
import suds
import time
import datetime

from suds.client import Client
from django.db import models
import re
from django.conf import settings
from datetime import *
import os

logging.disable(logging.DEBUG)
# logging.basicConfig(level=logging.ERROR)
# logging.getLogger('suds.client').setLevel(logging.ERROR)
# logging.getLogger('suds.xsd.schema').setLevel(logging.ERROR)
# logging.getLogger('suds.wsdl').setLevel(logging.ERROR)
# logging.getLogger('suds.transport').setLevel(logging.ERROR)


class DpdService(models.Model):

    messageArray = {}
    isActive = 1
    isTest = 0
    use_test_data = 0
    soapClient = ''
    myAccount = '1001034719'
    myKey = '2AEEDAC7E33EAE3C5420448D9A1D9A17C8088810'
    dpdHosts = {
        0: 'http://ws.dpd.ru/services/',  # рабочий хост
        1: 'http://wstest.dpd.ru/services/'   # тестовый хост
    }
    servicesArray = {   # сервисы: название => адрес
        'getCitiesCashPay': 'geography',    # География DPD (города доставки)
        'getTerminalsSelfDelivery': 'geography',    # список терминалов DPD
        'getServiceCost': 'calculator2',    # Расчёт стоимости
        'createOrder': 'order2',    # Создать заказ на доставку
        'createAddress': 'order2',  # Создать адрес с кодом
        'updateAddress': 'order2',  # Создать адрес с кодом
        'getOrderStatus': 'order2',     # Получить статус создания заказа
        'getNLAmount': 'nl',    # Отчет о предварительной стоимости перевозки за указный период.
        'getNLInvoice': 'nl',   # Отчет об окончательной стоимости перевозки за указный период
        'getStatesByClient': 'tracing',    # Получить все состояния посылок клиента, изменившиеся с момента последнего
                                            # вызова данного метода
        'getStatesByClientOrder': 'tracing',    # Получить историю состояний всех посылок заданного заказа. Заказ
                                                # идентифицируется по номеру заказа в информационной системе клиента
        'getStatesByDPDOrder': 'tracing',    # Получить историю состояний всех посылок заданного заказа. Заказ
                                                # идентифицируется по номеру заказа в информационной системе DPD
        'confirm': 'tracing',   # подтверждаем получение статусов

    }

    dpdServicesArray = {
        'BZP': 'DPD 18:00',
        'ECN': 'DPD ECONOMY',
        'TEN': 'DPD 10:00',
        'DPT': 'DPD 13:00',
        'CUR': 'DPD CLASSIC',
        'NDY': 'DPD EXPRESS',
        'CSM': 'DPD CONSUMER',
        'PCL': 'DPD CLASSIC Parcel',
        'DPI': 'DPD Classic international'
    }

    def get_service_cost(self, dataArray):
        """
        Определение стоимости доставки
        :param dataArray:   массив входных параметров
        :return:
        """
        obj = self._get_dpd_data('getServiceCost', dataArray, 1)
        return obj

    def get_cities_list(self):
        """
        Список городов доставки
        :return:
        """
        obj = self._get_dpd_data('getCitiesCashPay')
        return obj

    def _connect_to_dpd(self, method_name=''):
        """
        Коннект с соответствующим сервисом
        :param method_name: Запрашиваемый метод сервиса
        :return: bool
        """
        if not self.isActive:
            return False

        service = self.servicesArray[method_name]

        if not service:
            self.messageArray['str'] = 'В свойствах класса нет сервиса "%"' % method_name
            return False
        host = '%s%s?WSDL' % (self.dpdHosts[self.isTest], self.servicesArray[method_name])
        try:
            self.soapClient = Client(host)
            if not self.soapClient:
                raise Exception('Ошибка')
        except Exception:
            self.messageArray['str'] = 'Не удалось подключиться к сервисам DPD "%s"' % method_name
            raise
            return False
        return True

    def _get_dpd_data(self, method_name, data_array={}, header_array={}, is_request=False):
        """
        Запрос данных в методе сервиса
        :param method_name:                     Название метода Dpd-сервиса
        :param dataArray:                      Массив параметров, передаваемых в метод
        :param is_request:                  флаг упаковки запроса в поле 'request'
        :return: XZ_obj                    Объект, полученный от сервиса
        """

        request_data = {}

        if not self._connect_to_dpd(method_name):
            print method_name
            return False

        if is_request:
            request_data = self.soapClient.factory.create(is_request)

            request_data.auth.clientNumber = self.myAccount
            request_data.auth.clientKey = self.myKey
            request_data.header = header_array
            if is_request == 'orders':
                request_data.order = data_array
        else:
            if method_name == 'createAddress' or method_name == 'updateAddress':
                request_data['clientAddress'] = data_array
            if method_name == 'getOrderStatus':
                request_data['order'] = data_array
            if method_name == 'getNLAmount' or method_name == 'getNLInvoice' or method_name == 'getStatesByClientOrder' or method_name == 'getStatesByDPDOrder' or method_name == 'confirm':
                request_data = data_array
            request_data['auth'] = {
                'clientNumber': self.myAccount,
                'clientKey': self.myKey
            }

        # print request_data
        self._log_data(method_name, request_data)

        # return
        try:
            obj = self.soapClient.service[0][method_name](request_data)
            return obj
        # except suds.WebFault, err:
        #     print 'Suds error: ' + unicode(err)
        except:
            pass
        #     err = sys.exc_info()[1]
        #     print 'Other error: ' + str(err)
        #     pass
        return False

    def create_order(self, order_details={}, header_array={}):
        """
        :param order_details:
        :param header_array:
        :return:    orderNumberInternal: Номер заказа в информационной системе клиента
                    orderNum: Номер заказа DPD
                    status: Статус создания заказа. Возвращается в ответном сообщении
                        OK – заказ на доставку успешно создан с номером, указанным в поле orderNum.
                        OrderPending – заказ на доставку принят, но нуждается в ручной доработке сотрудником DPD,
                                (например, по причине того, что адрес доставки не распознан автоматически). Номер заказа
                                 будет присвоен ему, когда это доработка будет произведена.
                        OrderDuplicate – заказ на доставку не может быть принять по причине, указанной
                                    в поле errorMessage.
                        OrderError  – заказ на доставку не может быть создан по причине, указанной
                                    в поле errorMessage.
                    errorMessage: Текст ошибки
        """
        if self.isTest and self.use_test_data:
            order_details = self.order_details_example
            header_array = self.header_array_example
        obj = self._get_dpd_data('createOrder', order_details, header_array, 'orders')
        return obj

    def check_order_status(self, clientOrderNr, date_pickup=''):
        """
        :param clientOrderNr: номер заказа в системе
        :param date_pickup: дата отгрузки товара
        :return:
            OK – заказ на доставку успешно создан с номером, указанным в поле orderNum.
            OrderPending – заказ на доставку принят, но нуждается в ручной доработке сотрудником DPD, (например,
                  по причине того, что адрес доставки не распознан автоматически). Номер заказа будет присвоен ему,
                  когда это доработка будет произведена.
            OrderDuplicate – заказ на доставку не может быть принять по причине, указанной в поле errorMessage.
            OrderError  – заказ на доставку не может быть создан по причине, указанной в поле errorMessage.
        """
        data_array = {
            'orderNumberInternal': clientOrderNr,   # Номер заказа в системе
            # 'clientOrderNr': clientOrderNr,   # Номер заказа в системе
        }
        if date_pickup:
            data_array['datePickup'] = date_pickup
        # obj = self._get_dpd_data('getStatesByClientOrder', data_array)
        obj = self._get_dpd_data('getOrderStatus', data_array)
        return obj

    def check_order_status_by_dpd_id(self, dpd_id='', year=''):
        if not year:
            year = time.strftime('%Y')
        data_array = {
            'dpdOrderNr': dpd_id,   # Номер заказа в системе
            'pickupYear': year,   # Год совершения заказа
        }
        obj = self._get_dpd_data('getStatesByDPDOrder', data_array)
        return obj

    def check_order_status_by_local_id(self, local_order_id='', year=''):
        """
        Получить историю состояний всех посылок заданного заказа. Заказ идентифицируется по номеру заказа в
        информационной системе клиента
        :param clientOrderNr:   Номер заказа в информационной клиента
        :param date_pickup: Дата приёма груза (на случай, если номер заказа не уникален, и требуется уточнение по дате)

        :return:
        """

        # if not year:
        #     year = time.strftime('%Y')
        data_array = {
            'clientOrderNr': local_order_id,   # Номер заказа в системе
            # 'pickupYear': year,   # Год совершения заказа
        }
        obj = self._get_dpd_data('getStatesByClientOrder', data_array)
        return obj

    def confirm_order_statuses(self, doc_id=''):
        """
        Подтвердить получение состояний, переданных методом getStatesByClient
        :param doc_id:  Идентификатор документа, полученного в запросе getStatesByClient.
                        Все сообщения, переданные по данному документу, получат статус «Получение подтверждено».
        :return:
        """
        if doc_id:
            data_array = {
               'docId': doc_id   # Номер заказа в системе
            }
        else:
            return False
        obj = self._get_dpd_data('confirm', data_array)
        # obj = ''
        print 'Statuses for document %s confirmed' % doc_id
        return obj

    def get_states(self):
        data_array = {}
        obj = self._get_dpd_data('getStatesByClient', data_array)
        return

    def cancel_order(self, clientOrderNr):
        """
        Отмена заказа
        :param clientOrderNr:   номер заказа в система зазказчика
        :return:
        """
        data_array = {
            'orderNumberInternal': clientOrderNr,
        }
        obj = self._get_dpd_data('cancelOrder', data_array)
        return obj

    def get_orders_amount(self, date_from, date_to, final=False):
        """
        Отчет о предварительной/окончательной стоимости перевозки за указный период
            не разрешается вызывать метод, если еще не завершился предыдущий вызов;
            не разрешается вызывать метод менее чем через 30 минут после предыдущего вызова.
        :param date_from:       Начальная дата периода отчета 2014-05-21
        :param date_to:     Конечная дата периода отчета 2014-05-21
        :return:    amount: Стоимость перевозки,
                    ordernum: Номер заказа
        """

        data_array = {
            'dateFrom': date_from,
            'dateTo': date_to,
        }
        if final:
            method_name = 'getNLInvoice'
        else:
            method_name = 'getNLAmount'

        obj = self._get_dpd_data(method_name, data_array)
        return obj

    def _create_address_code(self, address={}):
        """
        :param order_details:
        :return:
        """
        ts = time.time()
        address['code'] = u'%s %s %s %s' % (re.sub('"', '', address['name']), address['city'], address['street'], address['house'])
        # address['code'] = u'%s %s %s' % (address['city'], address['street'], address['house'])
        # address['code'] = '%s' % (address['house'])
        print "Address: ", address
        obj = self._get_dpd_data('createAddress', address)
        print obj
        obj = self._get_dpd_data('updateAddress', address)
        print obj
        # print "create address", obj
        # if obj.status == 'code-already-exists':
        #     obj = self._get_dpd_data('updateAddress', address)
        #     print "update address", obj
        return obj


    def _parse_obj_to_array(self, obj, arr={}):
        """
        Парсер объекта в массив (рекурсия)
        :param obj:             Объект
        :param arr:         Внутренний cлужебный массив для обеспечения рекурсии
        :return: array
        """
        return

    def _get_delivery_price(self):
        pass

    def _log_data(self, method_name, data):
        now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        # file_name = location("/dpd_logs/%s" % now) settings.MEDIA_ROOT + '\\dpd_logs\\' + now + '-' + method_name
        file_name = os.path.join(settings.MEDIA_ROOT, 'dpd_logs',  '%s-%s' % (now,method_name))
        handle=open(file_name, 'w+')
        handle.write(str(data))
        handle.close()
        return


    header_array_example = {
        'datePickup': '2015-09-15',     # Дата приёма груза
        'senderAddress': {      # Адрес приёма груза
            'name': u'Иванов В.В,',  # Название отправителя/получателя. В случае, когда адрес приёма/доставки –
                                    # это магазин, филиал компании, дилерский центр и т.п., в эту строку пишется
                                    # его название. Если доставка осуществляется физическому лицу,
                                    # то пишется  Ф.И.О получателя.
            'countryName': u'Россия',    # Название страны
            'index': u'14100',        # Город
            'region': u'Московская',        # Город
            'city': u'Щелково',        # Город
            'street': u'Комарова',   # Улица (формат ФИАС)
            'streetAbbr': u'ул',       # Сокращения типа улицы (ул, пр-т, б-р и т.д.)
            'house': u'10',    # Дом, Нельзя передавать буквенные значения на английском языке.
                               #   В поле house нельзя передавать только буквенное значение, но можно передавать
                               #   букву после указания номера дома в таком варианте «1Б» или через «/» в таком
                               #   варианте «1/Б». Знаки препинания и другие знаки кроме «/» указывать нельзя
            # 'houseKorpus': u'10',    # Корпус, см. Дом
            # 'str': u'10',    # Строение, см. Дом
            # 'vlad': u'10',    # Владение, см. Дом
            # 'office': u'10',    # Офис, строка
            'flat': u'10',    # Квартира, строка
            'contactFio': u'Петров С.С.',       # Контактное лицо
            'contactPhone': '322-223',     # Контактный телефон
        },
        'pickupTimePeriod': '9-18',     # Интервал времени приёма груза. Доступные для выбора интервалы приёма
                    # 9-18 – в любое время с 09:00 до 18:00 (вариант по умолчанию);
                    # 9-13 – с 09:00 до 13:00;
                    # 13-18 – с 13:00 до 18:00.

    }
    order_details_example = {
        'orderNumberInternal': 107,     # Номер заказа в информационной системе клиента
        'serviceCode': 'ECN',   # Код услуги DPD (dpdServicesArray)
        'serviceVariant': u'ДД',     # ДТ – от двери отправителя до терминала DPD;
                                # ТД – от терминала DPD до двери получателя; ТТ – от терминала DPD до терминала DPD.
        'cargoNumPack': 1,  # Количество грузомест (посылок) в отправке
        'cargoWeight': 1,   # Вес отправки, кг
        'cargoValue': 3,        # Сумма объявленной ценности, руб.
        'cargoCategory': u'Ванна',        # Содержимое отправки
        'cargoRegistered': False,    # Ценный груз. Внутреннее вложение, включенное в перечень товаров, требующих
                    # дополнительных мер безопасности, снижающих риск его утери или повреждения при перевозке.
                    # Перечень товаров, относимых к категории «Ценный груз»:
                    # 1. Мобильные телефоны
                    # 2. Ноутбуки, планшеты
        'receiverAddress': {
            'name': u'Иванов В.В,',  # Название отправителя/получателя. В случае, когда адрес приёма/доставки –
                                    # это магазин, филиал компании, дилерский центр и т.п., в эту строку пишется
                                    # его название. Если доставка осуществляется физическому лицу,
                                    # то пишется  Ф.И.О получателя.
            'countryName': u'Россия',    # Название страны
            'index': u'14100',        # Город
            'region': u'Московская',        # Город
            'city': u'Щелково',        # Город
            'street': u'Иванова',   # Улица (формат ФИАС)
            'streetAbbr': u'ул',       # Сокращения типа улицы (ул, пр-т, б-р и т.д.)
            'house': u'10',    # Дом
            # 'houseKorpus': u'10',    # Корпус, см. Дом
            # 'str': u'10',    # Строение, см. Дом
            # 'vlad': u'10',    # Владение, см. Дом
            'office': u'10',    # Офис, строка
            # 'flat': u'10',    # Квартира, строка
            'contactFio': u'Петров С.С.',       # Контактное лицо
            'contactPhone': '322-223',     # Контактный телефон

        },
        'extraService': [
            {
                'esCode': u'НПП',  # сумма наложенного платежа
                'param': {
                    'name': 'sum_npp',
                    'value': '1'
                }
            },
            {
                'esCode': u'ПРД',   # нужны погрузочно-разгруз. работы
            }
        ]

        # 'extraService': {
        #     'esCode': u'НПП',  # сумма наложенного платежа
        #     'param': {
        #         'name': 'sum_npp',
        #         'value': '1'
        #     },
        #     # 'esCode': u'ПРД',   # нужны погрузочно-разгруз. работы
        # },
        # 'extraService': {
        #     'esCode': u'ПРД',   # нужны погрузочно-разгруз. работы
        # }
    }
