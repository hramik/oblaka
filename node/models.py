# -*- coding: utf-8 -*-

import datetime, sys, os, re
from django.utils.encoding import force_unicode

from django.db import models
from django.forms import ModelForm
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django import forms
from autoslug import AutoSlugField

from sorl.thumbnail import ImageField
from PIL import Image
from autoslug import AutoSlugField
from datetime import date, datetime
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.html import format_html

from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.template.defaultfilters import truncatewords, truncatechars


from django.core.urlresolvers import reverse

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from da_mailer.helper import *

from slugify import Slugify, UniqueSlugify, slugify, slugify_unicode
from slugify import slugify_url, slugify_filename
from slugify import slugify_ru

# from helper.models import *


class ShowedTagsManager(models.Manager):
    def get_query_set(self):
        return super(ShowedTagsManager, self).get_query_set().filter(show=True)


'''
Теги
'''


class Tag(models.Model):
    name = models.CharField(
        max_length=100,
        default=''
    )

    slug = models.SlugField(
        unique=True
    )

    desc = models.TextField(
        blank=True,
    )

    show = models.BooleanField(
        verbose_name="Show in list",
        default=False
    )

    objects = models.Manager()
    showed_objects = ShowedTagsManager()

    def __unicode__(self):
        return self.name


'''
Выдача нод с комментариями
'''


class WithCommentsManager(models.Manager):
    def get_query_set(self):
        node_ids = Comment.objects \
            .filter(publish=True) \
            .values_list("node__id", flat=True)

        return super(WithCommentsManager, self).get_query_set().filter(id__in=node_ids)


'''
Типы нод
'''


class NodeType(models.Model):
    name = models.CharField(
        max_length=100,
        default='',
        blank=True
    )

    desc = models.TextField(
        blank=True,
    )

    in_subscribe = models.BooleanField(
        default=False
    )

    slug = models.SlugField(
        unique=True
    )

    def __unicode__(self):
        return self.name



class InitialFields(models.Model):
    '''
    Start field: title, body, publich, etc
    '''

    title = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=u'Заголовок'
    )

    body = models.TextField(
        verbose_name=u"Содержимое",
        blank=True,
        null=True,
        default=''
    )

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True,
    )
    # pub_date.editable = True

    publish = models.BooleanField(
        verbose_name="Publish",
        default=True
    )

    class Meta():
        abstract = True

    def __unicode__(self):
        return self.title




class Node(models.Model):
    '''
    Нода — основополагающая сущность
    '''

    node_type = models.ForeignKey(
        NodeType,
        default=1,
        blank=True,
        verbose_name=u'Тип ноды'
    )

    title = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=u'Заголовок'
    )

    second_title = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name=u'Подзаголовок'
    )

    photo = models.ImageField(
        upload_to='mainphotos',
        blank=True
    )

    show_photo = models.BooleanField(
        default=True,
    )

    teaser = models.TextField(
        blank=True,
        null=True,
        default=''
    )

    body = models.TextField(
        verbose_name=_("Body"),
        blank=True,
        null=True,
        default=''
    )

    body_clear = models.BooleanField(
        default=False
    )

    tags = models.ManyToManyField(
        Tag,
        blank=True,
        verbose_name=u'Теги'
    )

    slug = models.SlugField(
        unique=False,
        blank=True,
        max_length=255,
        default=''
    )

    slug_with_dot = RegexValidator(r'^[0-9a-zA-Z_\.\-\/]*$',
                                   'Only only letters, numbers, underscores, hyphens or dot are allowed.')

    dot_slug = models.CharField(
        blank=True,
        max_length=255,
        validators=[slug_with_dot],
        default='',
    )

    region_dot_slug = models.CharField(
        blank=True,
        max_length=255,
        validators=[slug_with_dot],
        default='',
    )

    user = models.ForeignKey(
        User,
        blank=True,
        default=1,
        null=True
    )

    author = models.CharField(
        max_length=200,
        blank=True,
        default=""
    )

    email = models.EmailField(
        max_length=400,
        blank=True,
        default=""
    )

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True,
    )
    # pub_date.editable = True

    edit_date = models.DateTimeField(
        'date editing',
        auto_now=True,
    )
    # edit_date.editable = True

    edit_user = models.CharField(
        max_length=255,
        blank=True,
        default='admin',
        null=True,
    )

    add_date = models.DateTimeField(
        'add date',
        auto_now_add=True,
    )
    # add_date.editable = True

    last_comment_date = models.DateTimeField(
        'last comment date',
        auto_now_add=True,
    )
    # last_comment_date.editable = True

    import_id = models.IntegerField(
        default="0"
    )

    import_date = models.DateField(
        auto_now_add=True,
        null=True,
    )
    # import_date.editable = True

    publish = models.BooleanField(
        verbose_name="Publish",
        default=True
    )

    position = models.PositiveSmallIntegerField(
        default=10000
    )

    nid = models.CharField(
        max_length=100,
        blank=True,
        default=""
    )

    sites = models.ManyToManyField(
        Site,
        blank=True
    )

    related_nodes = models.ManyToManyField(
        'self',
        related_name='parent_node',
        blank=True
    )

    related_products = models.CharField(
        max_length=500,
        default="",
        blank=True
    )

    objects = models.Manager()
    objects_with_comments = WithCommentsManager()
    on_site = CurrentSiteManager()

    send_do_subscribe = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title \
            # + '/' + str(self.id) + '/' + str(self.nid)

    def tag_slugs(self):
        tags = []
        for tag in self.tags.all():
            tags.append(tag.slug)
        return tags

    # def __init__(self, *args, **kwargs):
    # print kwargs.pop('user', None)
    # super(Node, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.send_do_subscribe:
            try:
                node_subscribes = NodeSubscribe.objects.filter(node_type=self.node_type)
                for node_subscribe in node_subscribes:
                    DaMailer.send_subscribe_mail_node(name='subscribe_node', user=node_subscribe.user, data=self)
            except Exception, e:
                print 'save_node_error', Exception, e

        try:
            for site in self.sites.all():
                tag, create = Tag.objects.get_or_create(name=site.domain, slug=site.domain.replace('.', '-'))
                if not create:
                    self.tags.add(tag)
        except:
            pass

        if self.title == '':
            self.title = truncatewords(self.body, 10)

        if self.dot_slug == '':
            self.dot_slug = slugify_ru(self.title, to_lower=True)

        if self.slug == '':
            self.slug = self.dot_slug

        # if self.request:
        # print self.request.user

        super(Node, self).save(*args, **kwargs)

    def get_publish_comments(self):
        return self.comment_set.filter(publish=True)

    def get_publish_comments_part(self):
        return self.comment_set.filter(publish=True)[0:50]

    def get_all_comments(self):
        return self.comment_set.all()

    def get_first_tag(self):
        return self.tags

    def sites_names(self):
        return ', '.join([a.name for a in self.sites.all()])

    def first_site(self):
        try:
            site = self.sites.get(id=86)
        except:
            site = self.sites.all()[:1].get()
        return site

    class Meta:
        ordering = ['title']


'''
Нода с полями для СЕО
'''


class SEONode(Node):
    noindex = models.BooleanField(
        default=False,
    )

    h1 = models.CharField(
        max_length=255,
        blank=True,
        verbose_name=u'H1'
    )

    h2 = models.CharField(
        max_length=255,
        blank=True,
        default='',
        verbose_name=u'H2'
    )

    page_title = models.CharField(
        max_length=200,
        blank=True,
        null=True,
        help_text=_("for windows title"),
        default='',
        verbose_name=u'Meta Title'
    )

    meta_desc = models.TextField(
        blank=True,
        null=True,
        help_text=_("for meta description"),
        verbose_name=u"Meta Description",
        default='',
    )

    meta_keywords = models.CharField(
        max_length=1024,
        blank=True,
        null=True,
        default=''
    )

    PRIORITY_VALUES = (
        ('0.1', '0.1'),
        ('0.2', '0.2'),
        ('0.3', '0.3'),
        ('0.4', '0.4'),
        ('0.5', '0.5'),
        ('0.6', '0.6'),
        ('0.7', '0.7'),
        ('0.8', '0.8'),
        ('0.9', '0.9'),
        ('1.0', '1.0'),
    )

    CHANGEFREQ_VALUES = (
        ('always', 'always'),
        ('hourly', 'hourly'),
        ('daily', 'daily'),
        ('weekly', 'weekly'),
        ('monthly', 'monthly'),
        ('yearly', 'yearly'),
        ('never', 'never'),
    )

    priority = models.CharField(
        max_length=3,
        default='0.5',
        blank=True,
        choices=PRIORITY_VALUES,
    )

    changefreq = models.CharField(
        max_length=10,
        default='weekly',
        blank=True,
        choices=CHANGEFREQ_VALUES,
    )

    top_text = models.TextField(
        blank=True,
        null=True,
        verbose_name=u"Top text",
        default='',
    )

    # Sites
    # sites = models.ForeignKey(Site, default=1)
    #
    objects = models.Manager()
    on_site = CurrentSiteManager()

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse

        return reverse('nodes:node', args=[self.dot_slug])

    def editor(self):

        date_diff = (datetime.datetime.today() - self.edit_date).days

        user = User.objects.get(username=self.edit_user)
        user_profile = user.get_profile()
        result = format_html('<span style="background-color: {0}; color: white; padding: 2px 5px; '
                             'border-radius: '
                             '3px;">{1}</span>',
                             user_profile.color,
                             self.edit_user)
        return result


    editor.short_description = u'Крайний'
    editor.allow_tags = True

    def slug_with_url(self):
        try:
            url = "%s <br/> <a href='/%s' target='_blank'>на сайт</a>" % (str(self.dot_slug), str(self.dot_slug))
        except Exception, e:
            url = '%s %s' % (Exception, e)
        return url

    slug_with_url.short_description = u'Смотри на сайте'
    slug_with_url.allow_tags = True


    def is_h1(self):
        if self.h1:
            return True
        else:
            return False

    is_h1.boolean = True
    is_h1.short_description = u'H1'

    def is_h2(self):
        if self.h2:
            return True
        else:
            return False

    is_h2.boolean = True
    is_h2.short_description = u'H2'

    def is_teaser(self):
        if self.teaser:
            return True
        else:
            return False

    is_teaser.boolean = True
    is_teaser.short_description = u'Тизер'

    def is_body(self):
        if self.body:
            return True
        else:
            return False

    is_body.boolean = True
    is_body.short_description = u'Body'

    def is_page_title(self):
        if self.page_title:
            return True
        else:
            return False

    is_page_title.boolean = True
    is_page_title.short_description = u'Meta Title'

    def is_meta_keywords(self):
        if self.meta_keywords:
            return True
        else:
            return False

    is_meta_keywords.boolean = True
    is_meta_keywords.short_description = u'Meta Keywords'

    def is_meta_desc(self):
        if self.meta_desc:
            return True
        else:
            return False

    is_meta_desc.boolean = True
    is_meta_desc.short_description = u'Meta Description'

    def is_comments(self):
        if self.comment_set.all():
            return True
        else:
            return False

    is_comments.boolean = True
    is_comments.short_description = u'Комментарии'

    def is_images_alt(self):
        images = self.additionalphoto_set.all()
        result = True

        for image in images:
            if image.alt == '':
                result = False
                break

        return result

    is_images_alt.boolean = True
    is_images_alt.short_description = u'Альты'


    def is_images_title(self):
        images = self.additionalphoto_set.all()
        result = True

        for image in images:
            if image.title == '':
                result = False
                break

        return result

    is_images_title.boolean = True
    is_images_title.short_description = u'Титлы'



class Redirect(models.Model):

    slug_with_dot = RegexValidator(r'^[0-9a-zA-Z_\.\-\/]*$', 'Only only letters, numbers, underscores, hyphens or dot are allowed.')


    dot_slug = models.CharField(
        blank=True,
        max_length=255,
        validators=[slug_with_dot],
        default='',
    )

    sites = models.ManyToManyField(
        Site,
        blank=True
    )

    objects = models.Manager()
    objects_with_comments = WithCommentsManager()
    on_site = CurrentSiteManager()




'''
Фото/картинка
'''


class Photo(models.Model):
    name = models.CharField(
        max_length=200,
        blank=True,
        default=''
    )

    photo = models.ImageField(
        upload_to='photos/'
    )

    desc = models.TextField(
        blank=True,
        default=''
    )

    position = models.PositiveSmallIntegerField("Position")

    class Meta:
        ordering = ['position']


'''
Дополнительные файлы
'''


class AdditionalFile(models.Model):
    node = models.ForeignKey(Node)

    name = models.CharField(
        max_length=255,
        blank=True,
    )

    file = models.FileField(
        upload_to=_("additional_files"),
        blank=True,
    )

    link = models.CharField(
        max_length=1024,
        blank=True,
    )

    position = models.PositiveSmallIntegerField("Position")

    class Meta:
        ordering = ['position']


'''
Дополнительные фото
'''


class AdditionalPhoto(models.Model):
    node = models.ForeignKey(Node)

    photo = models.ImageField(
        upload_to='nodephotos',
        blank=True,
    )

    hover_photo = models.ImageField(
        upload_to='nodephotos',
        blank=True,
        null=True,
    )

    link = models.CharField(
        max_length=1024,
        blank=True,
    )

    alt = models.CharField(max_length=250, blank=True, default='')

    title = models.CharField(max_length=250, blank=True, default='')

    hover_title = models.CharField(max_length=250, blank=True, default='')

    position = models.PositiveSmallIntegerField("Position", default=0)

    # TODO: Delete this
    # def save(self, force_insert=False, force_update=False, using=None, size=(1000, 1000)):
    # if not self.id and not self.photo:
    # return
    #
    #     super(AdditionalPhoto, self).save()
    #
    #     image = Image.open(self.photo)
    #
    #     image.thumbnail(size, Image.ANTIALIAS)
    #     image.save(self.photo.path)

    class Meta:
        ordering = ['position']

    def __unicode__(self):
        return self.photo.url


'''
Дополнительное видео
'''


class AdditionalVideos(models.Model):
    VIDEO_TYPES = (
        ('youtube', 'youtube'),
        ('mp4', 'mp4'),
        ('webm', 'webm'),
    )
    node = models.ForeignKey(Node)
    video_url = models.CharField(max_length=500, verbose_name=_("Video URL"), default='')
    video_poster = models.ImageField(upload_to='nodevideo_posters', blank=True, default="images/misc/empty.png")
    video_type = models.CharField(max_length=20, default='youtube', choices=VIDEO_TYPES)
    position = models.PositiveSmallIntegerField("Position")

    class Meta:
        ordering = ['position']


RATE_CHOICES = (
    ('1', _('никуда не годится')),
    ('2', _('плохо')),
    ('3', _('удовлетворительно')),
    ('4', _('хорошо')),
    ('5', _('отлично')),
)

# TODO: удалить
# Bad class
class Comments(models.Model):
    node = models.ForeignKey(Node)
    title = models.CharField(max_length=200, blank=True, default="")
    body = models.TextField(verbose_name=_("Body"), blank=True, default="")
    author = models.CharField(max_length=200, blank=True, default='')
    email = models.EmailField(max_length=400, blank=True, default='')
    pub_date = models.DateTimeField('date published', auto_now_add=True)

    add_date = models.DateTimeField('date import', default=timezone.now)
    import_id = models.IntegerField(default="0")

    publish = models.BooleanField(verbose_name="Publish", default=False)

    def __unicode__(self):
        return self.body


'''
Комментарий
'''


class Comment(models.Model):
    node = models.ForeignKey(Node)
    title = models.CharField(max_length=200, blank=True, default="")
    body = models.TextField(verbose_name=_("Body"), blank=True, default="")
    user = models.ForeignKey(User, blank=True, default=0, null=True)
    author = models.CharField(max_length=200, blank=True, default="")
    email = models.EmailField(max_length=400, blank=True, default="")

    pub_date = models.DateTimeField(
        'date published',
        auto_now_add=True,
    )
    # pub_date.editable = True

    add_date = models.DateTimeField(
        'date import',
        auto_now_add=True,
    )
    # add_date.editable = True

    import_id = models.CharField(max_length=100, default="0")

    publish = models.BooleanField(verbose_name="Publish", default=True)

    cid = models.CharField(max_length=100, blank=True, default="")

    def __unicode__(self):
        return self.body

    class Meta:
        ordering = ['pub_date']


@receiver(post_save, sender=Comment)
def change_last_comment_date(instance, created, **kwargs):
    '''
    Update last comment date in node by сreate new comment
    '''
    if created:
        comment_node = SEONode.objects.get(id=instance.node.id)
        comment_node.last_comment_date = datetime.datetime.now()
        comment_node.publish = True
        if comment_node.node_type.slug == 'question' and comment_node.get_all_comments().count() == 1:
            comment_node.meta_desc = truncatechars(instance.body, 400)

        comment_node.save()


class CommentForm(ModelForm):
    '''
    Форма комментария
    '''
    node = forms.IntegerField(widget=forms.HiddenInput(attrs={'class': '', 'ng-model': 'comment.node'}), label="")
    body = forms.CharField(
        widget=forms.Textarea(attrs={'class': 'validate[required]', 'ng-model': 'comment.body', 'required': 'True', 'msd-elastic': 'true'}),
        label="Комментарий")

    class Meta:
        model = Comment
        fields = ('body',)


'''
Форма анонимного комментария
'''


class AnonimousCommentForm(ModelForm):
    node = forms.IntegerField(widget=forms.HiddenInput(attrs={'class': '', 'ng-model': 'comment.node'}), label="")
    author = forms.CharField(
        widget=forms.TextInput(attrs={'class': 'validate[required]', 'ng-model': 'comment.author', 'required': 'True'}),
        label='Имя')
    email = forms.EmailField(
        widget=forms.TextInput(attrs={'class': '', 'ng-model': 'comment.email', 'required': 'True', 'type': 'email'}))
    body = forms.CharField(widget=forms.Textarea(
        attrs={'class': 'validate[required]', 'ng-model': 'comment.body', 'required': 'True', 'msd-elastic': 'true'}),
                           label="Комментарий")

    class Meta:
        model = Comment
        fields = ('author', 'email', 'body')


'''
Лайк для комментария
'''


class CommentLike(models.Model):
    comment = models.ForeignKey(Comment)
    user = models.ForeignKey(User, default=0)
    like_date = models.DateTimeField('date published', auto_now_add=True)

    def __unicode__(self):
        return self.user.username + "/" + str(self.comment.id)

    class Meta:
        ordering = ['like_date']


'''
Дополнительные фото для комментария
'''


class CommentAdditionalPhoto(models.Model):
    comment = models.ForeignKey(Comment)
    photo = models.ImageField(upload_to='commentphotos')
    alt = models.CharField(max_length=250, blank=True, default='')
    title = models.CharField(max_length=250, blank=True, default='')
    position = models.PositiveSmallIntegerField("Position", default=0)

    def save(self, force_insert=False, force_update=False, using=None, size=(1000, 1000)):
        if not self.id and not self.photo:
            return

        super(CommentAdditionalPhoto, self).save()

        image = Image.open(self.photo)

        image.thumbnail(size, Image.ANTIALIAS)
        image.save(self.photo.path)

    class Meta:
        ordering = ['position']

    def __unicode__(self):
        return self.photo.url


class NodeSubscribe(models.Model):
    '''
    Subscribe on new nodes
    '''
    user = models.ForeignKey(User)
    node_type = models.ForeignKey(NodeType)

    def __unicode__(self):
        return "%s for %s" % (self.node_type.name, self.user.username)


class NodeSubscribeForm(forms.Form):
    '''
    Form for nodes subscribe
    '''
    try:
        SUBSCRIBE_NODE_TYPES_CHOICES = NodeType.objects.filter(in_subscribe=True).values_list('slug', 'name')

        # SUBSCRIBE_NODE_TYPES_CHOICES = [(0, _('все'))] + [(node_type.slug, node_type.name) for node_type in subscribe_node_types]

        node_types = forms.MultipleChoiceField(
            required=True,
            widget=forms.CheckboxSelectMultiple,
            choices=SUBSCRIBE_NODE_TYPES_CHOICES,
            label='Подписаться на обновления материалов'
        )
    except:
        pass

class SubscribeForm(forms.Form):
    '''
    Form for nodes subscribe
    '''
    SUBSCRIBE_NODE_TYPES_CHOICES = Subscription.objects.all().values_list('slug', 'name')

    # SUBSCRIBE_NODE_TYPES_CHOICES = [(0, _('все'))] + [(node_type.slug, node_type.name) for node_type in subscribe_node_types]

    try:
        subscribe = forms.MultipleChoiceField(
            required=True,
            widget=forms.CheckboxSelectMultiple,
            choices=SUBSCRIBE_NODE_TYPES_CHOICES,
            label='Подписаться на рассылки'
        )
    except:
        pass