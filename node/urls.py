# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse
# from oscar.app import application
# Uncomment the next two lines to enable the admin:

from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from tastypie.api import Api
from tri.api.resources import QuestionResource

from django.contrib import admin
from tri.views import questionViewSet, additionalPhotoViewSet

admin.autodiscover()

urlpatterns = patterns('',

                       # url(r'^product/(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.new_node', name='new_node'),

                       url(r'^(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.node', name="node"),  # products or static page
                       url(r'^(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.staticpage', name="staticpage"),
                       # url(r'^(?P<slug>[a-zA-Z0-9\-_\.\/]+)$', 'tri.views.redirect', name="redirect"), # products or static page
                       )
