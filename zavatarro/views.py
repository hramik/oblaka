# -*- coding: utf-8 -*-

import os
import re
import csv


from django.db import models
from django.db.models import Q, F

from django.shortcuts import get_object_or_404

import json
import datetime
import random
import logging

from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission, User

from datetime import timedelta

from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm, mm, inch, pica

from easy_pdf.views import PDFTemplateView
from easy_pdf.rendering import render_to_pdf_response

from django.core.urlresolvers import reverse

from itertools import chain
from django.shortcuts import render_to_response, redirect
from django.http import HttpResponse
from django.template import RequestContext

from seo.models import *
from node.models import SEONode, NodeType

from constructors.forms import *
from constructors.models import *
from node.models import *
from tri.models import UserProfile
from da_mailer.helper import *




# @login_required
def zavatarro(request):

    return render_to_response('zavatarro/zavatarro.html', {
    },
                              context_instance=RequestContext(request))
