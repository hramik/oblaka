__author__ = 'hramik'

from django import http
from django.conf import settings
from django.contrib.sites.models import Site
import re


class MultiSiteMiddleware(object):
    def process_request(self, request):
        try:
            host = request.get_host()
            site = Site.objects.get(domain=host)
            settings.SITE_ID = site.id
            Site.objects.clear_cache()
            return
        except Site.DoesNotExist:
            settings.SITE_ID = 1
            Site.objects.clear_cache()
            return



class NofollowLinkMiddleware(object):

    def process_response(self, request, response):

        NOFOLLOW_RE = re.compile('(href="http://(?![^\s]*3tn\.ru[^\s]*)[^\s]*")')
        try:
            if ("text" in response['Content-Type']):
                # response.content = re.sub(NOFOLLOW_RE, r'\1 rel="nofollow" ', response.content)
                print re.findall(NOFOLLOW_RE, response.content)
                return response
            else:
                return response
        except:
            return response