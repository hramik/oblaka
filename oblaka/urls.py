# -*- coding: utf-8 -*-


from django.views.generic import TemplateView
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.core.urlresolvers import reverse
# Uncomment the next two lines to enable the admin:


from django.contrib import admin

from oblaka import settings
from oblaka.sitemap import *
from django.contrib.sites.models import Site


# import xadmin
# xadmin.autodiscover()
#from xadmin.plugins import xversion
#xversion.register_models()

admin.autodiscover()

sitemaps = {
    'seonode': SEONodeSitemap,
}

urlpatterns = patterns('',


                       (r'', include('yandex_money_api.urls', namespace='yandex_money_api', app_name='yandex_money_api')),

                       (r'', include('tri.urls', namespace='tri', app_name='tri')),

                       (r'', include('triusers.urls', namespace='triusers', app_name='triusers')),

                       # (r'', include('constructors.urls', namespace='constructors', app_name='constructors')),

                       (r'zavatarro', include('zavatarro.urls', namespace='zavatarro', app_name='zavatarro')),

                       (r'', include('tride.urls', namespace='tride', app_name='tride')),

                       (r'', include('custom_admin.urls', namespace='custom_admin', app_name='custom_admin')),

                       (r'', include('da_mailer.urls', namespace='da_mailer', app_name='da_mailer')),

                       (r'', include('seo.urls', namespace='seo', app_name='seo')),
                       # (r'', include('dpd.urls', namespace='dpd', app_name='dpd')),
                       (r'', include('price_parsing.urls', namespace='price_parsing', app_name='price_parsing')),



                       # (r'^facebook/', include('django_facebook.urls')),
                       # (r'^accounts/', include('django_facebook.auth_urls')),

                       url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),

                       # url(r'', include('social_auth.urls')),

                       # Uncomment the admin/doc line below to enable admin documentation:
                       url(r'^admin_oblaka/doc/', include('django.contrib.admindocs.urls')),

                       # Uncomment the next line to enable the admin:
                       # (r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
                       #url(r'^xadmin/', include(xadmin.site.urls)),

                       url(r'^admin_oblaka/', include(admin.site.urls)),
                       #(r'^accounts/', include('registration.backends.simple.urls')),


                       url(r'^(favicon.ico)$', 'django.views.static.serve', {
                           'document_root': settings.STATIC_ROOT + '/images/',
                       }),


                       url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.STATIC_ROOT,
                       }),

                       # url(r'^media/3dpr/ModelViewer.swf$', 'django.views.static.serve', {
                       #     'document_root': settings.MEDIA_ROOT + 'media/3dpr/ModelViewer.swf',
                       # }),

                       url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT,
                       }),

                       url(r'^template/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.ANGULAR_TEMPLATE_ROOT,
                       }),

                       url(r'^images/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT + '/images/',
                       }),

                       url(r'^files/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT + '/files/',
                       }),

)

#urlpatterns += patterns('',
#    (r'', include(application.urls))
#)


if settings.DEBUG:
    urlpatterns += patterns('',
                            url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                                'document_root': settings.MEDIA_ROOT,
                            }),
    )

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns('',
                            url(r'^__debug__/', include(debug_toolbar.urls)),
    )

urlpatterns += patterns('',
                        (r'', include('node.urls', namespace='nodes', app_name='nodes')),

)






