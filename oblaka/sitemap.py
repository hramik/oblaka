# -*- coding: utf-8 -*-
__author__ = 'hramik'

from django.contrib.sitemaps import Sitemap
from tri.models import SEONode

class SEONodeSitemap(Sitemap):
    def items(self):
        seonodes =  SEONode.on_site.filter(dot_slug__gt = '')
        # print seonodes.count()
        return seonodes

    def lastmod(self, obj):
        return obj.edit_date

    def priority(self, obj):
        return obj.priority

    def changefreq(self, obj):
        return obj.changefreq