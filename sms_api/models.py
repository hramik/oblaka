# -*- coding: utf-8 -*-
__author__ = 'm13v246'

import json
import re
import requests
from django.db import models

class Sms_Api(models.Model):
    url = 'http://smsc.ru/sys/send.php'
    login = 'triton'
    password = 'qwe123'
    charset = 'utf-8'

    @classmethod
    def send_message(self, user='', message=''):
        # print "SMS", user, message
        try:
            phone = user.userprofile.phone
            phone = re.sub('[^\d]+', '', phone)
            # print 'phone'
            # print phone
            if not phone:
                return False
            if re.match('[\+\-()\d]{7,18}', phone) and message:
                query = "http://smsc.ru/sys/send.php?login=triton&psw=qwe123&phones=%s&charset=utf-8&mes=%s&fmt=3&all=0" % (phone, message)

                # print query
                # return True

                response = requests.get(query)
                content = json.loads(response.content)
                # content=''

                #return content
                # print content


                if 'error_code' in content:
                    return False
                else:
                    return True

                # Значение	Описание
                # 1	Ошибка в параметрах.
                # 2	Неверный логин или пароль.
                # 3	Сообщение не найдено. При множественном запросе для данной ошибки возвращается статус с кодом "-3".
                # 4	IP-адрес временно заблокирован.
                # 9	Попытка отправки более пяти запросов на получение статуса одного и того же сообщения в течение минуты.
            else:
                return False

        except Exception, e:
            print Exception, e
            pass

    @classmethod
    def send_message_to_phone_number(self, phone_list=None, message=''):
        for phone in phone_list:
            if not phone:
                continue
            phone = phone.replace(" ", "")
            if re.match('[\+\-()\d]{7,18}', phone) and message:
                query = "http://smsc.ru/sys/send.php?login=triton&psw=qwe123&phones=%s&charset=utf-8&mes=%s&fmt=3&all=0" % (phone, message)

                # print query
                # return True

                response = requests.get(query)
                content = json.loads(response.content)
                # content=''

                #return content
                # print content


                # if 'error_code' in content:
                #     return False
                # else:
                #     return True

                # Значение	Описание
                # 1	Ошибка в параметрах.
                # 2	Неверный логин или пароль.
                # 3	Сообщение не найдено. При множественном запросе для данной ошибки возвращается статус с кодом "-3".
                # 4	IP-адрес временно заблокирован.
                # 9	Попытка отправки более пяти запросов на получение статуса одного и того же сообщения в течение минуты.
            else:
                continue

